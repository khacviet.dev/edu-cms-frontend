import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import userApi from 'api/userApi';

export const getAvatar = createAsyncThunk('/avatar/getAvatar', async () => {
	try {
		const response = await userApi.getAvatar();
		return response.avt;
	} catch (error) {}
});

export const updateAvatar = createAsyncThunk(
	'/avatar/updateAvatar',
	async (params) => {
		try {
			const response = await userApi.changeAvatar(params);
			return response;
		} catch (error) {}
	}
);

const avatarSlice = createSlice({
	name: 'avatars',
	initialState: {
		avtLink: '',
		loading: false,
		error: '',
	},
	reducers: {},
	extraReducers: {
		[getAvatar.pending]: (state) => {
			state.loading = true;
		},
		[getAvatar.rejected]: (state, action) => {
			state.error = action.error;
		},
		[getAvatar.fulfilled]: (state, action) => {
			state.loading = false;
			state.avtLink = action.payload;
		},
		[updateAvatar.pending]: (state) => {
			state.loading = true;
		},
		[updateAvatar.rejected]: (state, action) => {
			state.error = action.error;
		},
		[updateAvatar.fulfilled]: (state, action) => {
			state.loading = false;
			state.avtLink = action.payload.avt;
		},
	},
});

const { reducer: avatarReducer } = avatarSlice;
export default avatarReducer;
