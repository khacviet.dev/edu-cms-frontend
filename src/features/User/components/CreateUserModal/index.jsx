import {
	Box,
	Button,
	Checkbox,
	Divider,
	Grid,
	IconButton,
	Input,
	ListItemText,
	makeStyles,
	MenuItem,
	Modal,
	Paper,
	Select,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import InputField from 'custom-fields/InputField';
import { FastField, Form, Formik } from 'formik';
import React, { useState } from 'react';
import * as Yup from 'yup';

const useStyles = makeStyles((theme) => ({
	modal: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%,-50%)',
		padding: theme.spacing(1.5),
		width: 750,
		outline: 0,
	},
	postPanel: {
		marginTop: '2rem',
		marginBottom: '0.5rem',
	},
	createPostTitle: {
		fontSize: '1.25rem',
		fontWeight: 'bold',
	},
	closeModal: {
		position: 'absolute',
		top: '50%',
		right: '0',
		transform: 'translate(0,-50%)',
	},
}));

function CreateUserModal({ role, roleName, handleCreateUser }) {
	const handleChange = (event) => {
		setSubjects(event.target.value);
	};

	const [subjects, setSubjects] = React.useState([
		{ name: 'Toán', check: false },
		{ name: 'Văn', check: false },
		{ name: 'Hóa', check: false },
	]);

	const [slot, setSlot] = React.useState([
		{ name: 'Sáng', check: false },
		{ name: 'Chiều', check: false },
	]);

	const classes = useStyles();

	const [open, setOpen] = useState(false);

	const handleOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const initialValues = {
		fullname: '',
		displayname: '',
		phone: '',
		email: '',
		address: '',
		note: '',
		role: role,
	};

	const validationShema = Yup.object().shape({
		fullname: Yup.string().required('Bạn cần nhập mục này'),
		displayname: Yup.string().required('Bạn cần nhập mục này'),
		phone: Yup.string()
			.required('Bạn cần nhập mục này')
			.matches(/^[0-9]+$/, 'Số điện thoại chỉ bao gồm chữ số')
			.min(10, 'Số điện thoại gồm ít nhất 10 số')
			.max(12, 'Số điện thoại gồm nhiều nhất 11 số'),
		email: Yup.string().email('Email không hợp lệ'),
		address: Yup.string(),
		note: '',
	});

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationShema}
			onSubmit={(values, { resetForm }) => {
				if (handleCreateUser) {
					handleCreateUser(values);

					handleClose();
				}
			}}
		>
			{(formikProps) => {
				return (
					<Box>
						<Button
							startIcon={<AddIcon />}
							color='primary'
							variant='contained'
							onClick={handleOpen}
							fullWidth
						>
							Thêm mới
						</Button>
						<Modal open={open} onClose={handleClose}>
							{
								<Paper className={classes.modal}>
									<Form>
										<Grid container alignItems='center' spacing={1}>
											<Grid
												item
												xs={12}
												md={12}
												lg={12}
												container
												justify='center'
												style={{ position: 'relative' }}
											>
												<Box className={classes.createPostTitle}>
													Thêm mới {roleName}
												</Box>
												<IconButton
													onClick={handleClose}
													className={classes.closeModal}
												>
													<CloseIcon />
												</IconButton>
											</Grid>

											<Grid item xs={12} md={12} lg={12}>
												<Divider />
											</Grid>
											<Grid
												container
												style={{ padding: '0 2rem 0 2rem' }}
												alignItems='center'
											>
												<Grid item md={2} lg={2}>
													Họ và tên:
												</Grid>
												<Grid item md={10} lg={10}>
													<FastField
														name='fullname'
														component={InputField}
														label=''
														fullWidth={true}
														size='small'
													/>
												</Grid>
												<Grid item md={2} lg={2}>
													Tên hiển thị:
												</Grid>
												<Grid item md={8} lg={8}>
													<FastField
														name='displayname'
														component={InputField}
														label={''}
														fullWidth={true}
														size='small'
													/>
												</Grid>
												<Grid item md={2} lg={2}></Grid>
												<Grid item md={2} lg={2}>
													Số điện thoại:
												</Grid>
												<Grid item md={7} lg={7}>
													<FastField
														name='phone'
														component={InputField}
														label={''}
														fullWidth={true}
														size='small'
													/>
												</Grid>
												<Grid item md={3} lg={3}></Grid>
												<Grid item md={2} lg={2}>
													Email:
												</Grid>
												<Grid item md={8} lg={8}>
													<FastField
														name='email'
														component={InputField}
														label={''}
														fullWidth={true}
														size='small'
													/>
												</Grid>
												<Grid item md={2} lg={2}></Grid>
												<Grid item md={2} lg={2}>
													Địa chỉ:
												</Grid>
												<Grid item md={10} lg={10}>
													<FastField
														name='address'
														component={InputField}
														label={''}
														fullWidth={true}
														size='small'
													/>
												</Grid>
												{role === '2' && (
													<Grid item md={12}>
														<Grid container alignItems='center'>
															<Grid item md={2} lg={2}>
																Ca làm
															</Grid>
															<Grid item md={3} lg={3}>
																<Select
																	multiple
																	value={subjects}
																	onChange={handleChange}
																	input={<Input />}
																	fullWidth
																>
																	{slot.map((s, index) => (
																		<MenuItem key={index} value={s.name}>
																			<Checkbox checked={s.check} />
																			<ListItemText primary={s.name} />
																		</MenuItem>
																	))}
																</Select>
															</Grid>
															<Grid item md={7} lg={7}></Grid>
														</Grid>
													</Grid>
												)}
												{(role === '3' || role == '4') && (
													<Grid item md={12}>
														<Grid container alignItems='center'>
															<Grid item md={2} lg={2}>
																Dạy môn:
															</Grid>
															<Grid item md={3} lg={3}>
																<Select
																	multiple
																	value={subjects}
																	onChange={handleChange}
																	input={<Input />}
																	fullWidth
																>
																	{subjects.map((subject, index) => (
																		<MenuItem key={index} value={subject.name}>
																			<Checkbox checked={subject.check} />
																			<ListItemText primary={subject.name} />
																		</MenuItem>
																	))}
																</Select>
															</Grid>
															<Grid item md={7} lg={7}></Grid>
														</Grid>
													</Grid>
												)}
												{role === '5' && (
													<Grid item md={12}>
														<Grid container alignItems='center'>
															<Grid item md={2} lg={2}>
																Lớp học
															</Grid>
															<Grid item md={3} lg={3}>
																<Select
																	multiple
																	value={subjects}
																	onChange={handleChange}
																	input={<Input />}
																	fullWidth
																>
																	{subjects.map((subject, index) => (
																		<MenuItem key={index} value={subject.name}>
																			<Checkbox checked={subject.check} />
																			<ListItemText primary={subject.name} />
																		</MenuItem>
																	))}
																</Select>
															</Grid>
															<Grid item md={7} lg={7}></Grid>
														</Grid>
													</Grid>
												)}

												<Grid item md={12} lg={12}>
													<FastField
														name='note'
														component={InputField}
														label={'Ghi chú'}
														multiline={true}
													/>
												</Grid>
												<Grid
													item
													xs={12}
													md={12}
													lg={12}
													className={classes.postPanel}
													container
													justify='center'
												>
													<Button
														style={{ padding: '0.5rem 1.5rem 0.5rem 1.5rem' }}
														type='submit'
														variant='contained'
														color='primary'
													>
														Tạo mới
													</Button>
												</Grid>
											</Grid>
										</Grid>
									</Form>
								</Paper>
							}
						</Modal>
					</Box>
				);
			}}
		</Formik>
	);
}

export default CreateUserModal;
