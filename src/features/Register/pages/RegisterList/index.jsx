import { Grid } from '@material-ui/core';
import classApi from 'api/classApi';
import excelApi from 'api/excelApi';
import permissionApi from 'api/permissionApi';
import registerApi from 'api/registerApi';
import SearchForm from 'common/components/SearchForm';
import MasterLayout from 'common/pages/MasterLayout';
import RegisterTable from 'features/Register/pages/RegisterTable';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { exportToCSV } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

const id = 'user_id';

const filterList1 = [
	{
		text: 'None',
		value: '',
	},
	{
		text: 'Chưa liên hệ',
		value: '0',
	},
	{
		text: 'Đã liên hệ',
		value: '1',
	},
	{
		text: 'Kiểm tra thanh toán',
		value: '2',
	},
	{
		text: 'Đã xóa',
		value: '3',
	},
];
const filterList2 = [
	{
		text: 'Tháng 1',
		value: '01',
	},
	{
		text: 'Tháng 2',
		value: '02',
	},
	{
		text: 'Tháng 3',
		value: '03',
	},
	{
		text: 'Tháng 4',
		value: '04',
	},
	{
		text: 'Tháng 5',
		value: '05',
	},
	{
		text: 'Tháng 6',
		value: '06',
	},
	{
		text: 'Tháng 7',
		value: '07',
	},
	{
		text: 'Tháng 8',
		value: '08',
	},
	{
		text: 'Tháng 9',
		value: '09',
	},
	{
		text: 'Tháng 10',
		value: '10',
	},
	{
		text: 'Tháng 11',
		value: '11',
	},
	{
		text: 'Tháng 12',
		value: '12',
	},
];
const filterList3 = [];
const listFilterLabel = ['Trạng thái', 'Tháng', 'Năm'];

const listFunction = {
	create: false,
	edit: true,
	delete: true,
	export: true,
	filter: 1,
};

const RenderTable = RegisterTable(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id,
	listFunction
);

export default function RegisterList() {
	let history = useHistory();
	const [loading, setLoading] = useState(false);
	const notify = useContext(ToastifyContext);
	const [posts, setPosts] = useState([]);
	const [totalRow, setTotalRow] = useState();
	const [listYear, setListYear] = useState([]);
	const [listRegister, setListRegister] = useState([]);
	const [listClasses, setListClasses] = useState([]);

	const [filters, setFilters] = useState({
		limit: 5,
		page: 1,
		q: '',
		filter1: '',
		filter2: ('0' + (new Date().getMonth() + 1)).slice(-2),
		filter3: new Date().getFullYear(),
	});

	const [functionsTable, setFunctionTable] = useState({
		view: false,
		edit: false,
		review: false,
		revert: false,
		created_student: false,
	});

	const handlePageChange = (newPage) => {
		setFilters({
			...filters,
			page: newPage,
		});
	};

	const handleSearchChange = (newSearch) => {
		setFilters({
			...filters,
			page: 1,
			q: newSearch.value,
		});
	};

	const handleFilterChange = (e, filter) => {
		setFilters({
			...filters,
			page: 1,
			[filter]: e.target.value,
		});
	};

	const resetFilter = () => {
		setFilters({
			...filters,
			filter1: '',
			filter2: '',
			filter3: '',
			page: 1,
		});
	};

	const [keys, setKeys] = useState([
		{
			header: 'Tên',
			align: 'left',
			field: 'full_name',
			display: true,
		},
		{
			header: 'Email',
			align: 'left',
			field: 'email',
			display: true,
		},
		{
			header: 'Số điện thoại',
			align: 'left',
			field: 'phone',
			display: true,
		},
		{
			header: 'Ngày đăng ký',
			align: 'left',
			field: 'created_at',
			display: true,
		},
		{
			header: 'Trạng Thái',
			align: 'left',
			field: 'register_status',
			display: true,
		},
	]);

	const [columnSelected, setColumnSelected] = React.useState([]);

	const changeDisplayColumn = (key) => {
		key.display = !key.display;
		setKeys(keys);
	};

	const handleColumnChange = (event) => {
		setColumnSelected(event.target.value);
	};

	const handleDeleteRegister = (regis_id) => {
		setLoading((loading) => !loading);
		if (regis_id) {
			const id = regis_id.split('-');
			const updateRegisStatus = async () => {
				try {
					const data = {
						regis_id: id,
						status: '3',
					};

					const response = await registerApi.updateRegisStatus(data);
					// setTotalRow(response.data[0].count);
					setLoading((loading) => !loading);
					notify('success', response.message);
					setFilters({
						...filters,
						delete: '',
					});
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			updateRegisStatus();
		} else {
			setLoading((loading) => !loading);
			notify('error', 'Something wrong');
		}
	};

	const handeleRevertRegister = (regis_id) => {
		setLoading((loading) => !loading);
		if (regis_id) {
			const id = regis_id.split('-');
			const updateRegisStatus = async () => {
				try {
					const data = {
						regis_id: id,
						status: '0',
					};

					const response = await registerApi.updateRegisStatus(data);
					// setTotalRow(response.data[0].count);
					setLoading((loading) => !loading);
					notify('success', response.message);
					setFilters({
						...filters,
						delete: '',
					});
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			updateRegisStatus();
		} else {
			setLoading((loading) => !loading);
			notify('error', 'Something wrong');
		}
	};

	const handeleStautsRegister = (regis_id, status) => {
		setLoading((loading) => !loading);
		if (regis_id) {
			const id = regis_id.split('-');
			const updateRegisStatus = async () => {
				try {
					const data = {
						regis_id: id,
						status: status,
					};

					const response = await registerApi.updateRegisStatus(data);
					// setTotalRow(response.data[0].count);
					setLoading((loading) => !loading);
					notify('success', response.message);
					setFilters({
						...filters,
						delete: '',
					});
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			updateRegisStatus();
		} else {
			setLoading((loading) => !loading);
			notify('error', 'Something wrong');
		}
	};

	// phân quyền
	const [functions, setFunctions] = useState([]);
	const [isPermiss, setIsPermiss] = useState(false);

	// check permission
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_list_register',
				};
				await permissionApi.checkPermission(params);
				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				if ((error.response.status = 403)) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			}
		};

		post();
	}, []);

	// get list function
	useEffect(() => {
		if (isPermiss) {
			const getFunctions = async () => {
				try {
					const respone = await permissionApi.getFunctions();
					const data = [];
					const a = {
						view: true,
						edit: false,
						delete: false,
					};
					for (let i = 0; i < respone.length; i++) {
						if (respone[i].function_id == 'function_edit_register') {
							a['edit'] = true;
						} else if (respone[i].function_id == 'function_delete_register') {
							a['delete'] = true;
						} else if (respone[i].function_id == 'function_create_student') {
							a['created_student'] = true;
						}
						data.push(respone[i].function_id);
					}
					setFunctionTable(a);
					setFunctions(data);
				} catch (error) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			getFunctions();
		}
	}, [isPermiss]);

	useEffect(() => {
		const value = [];
		for (let i = 0, l = keys.length; i < l; i += 1) {
			value.push(keys[i].header);
		}
		setColumnSelected(value);
	}, []);

	// get year
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const getRegisterYear = async () => {
				try {
					const response = await registerApi.getRegisterYear();
					setLoading((loading) => !loading);
					setListYear(response);
					// setTotalRow(response.data[0].count);
					// setIsLoading(loading => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getRegisterYear();
		}
	}, [isPermiss]);

	// get total record
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const params = {
				filters,
			};
			const getTotalRegister = async () => {
				try {
					const response = await registerApi.getTotalRegister(params);
					setTotalRow(response.data);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getTotalRegister();
		}
	}, [filters, isPermiss]);

	// get Classes Select
	useEffect(() => {
		if (isPermiss) {
			const getClassesSelect = async () => {
				try {
					const respone = await classApi.getClassesSelect();

					setListClasses(respone);

					// setLoading((loading) => !loading);
				} catch (error) {
					// setLoading((loading) => !loading);
				}
			};
			getClassesSelect();
		}
		// setLoading((loading) => !loading);
	}, [isPermiss]);

	const handle = () => {
		history.push({
			pathname: '/create/student',
			state: {
				data: {
					full_name: 'Mai',
					email: '',
					phone: '',
					class_id: [],
				},
			},
		});
	};

	// get register List
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const params = {
				filters,
			};
			const getRegisterList = async () => {
				try {
					const response = await registerApi.getRegisterList(params);
					setListRegister(response);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getRegisterList();
		}
	}, [filters, isPermiss]);

	const exportRegisterList = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getRegisterListExcel(filters);
				setLoading(false);
				const csvData = {
					data: response,
				};
				exportToCSV(csvData, 'Danh_sach_dang_ky');
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	return (
		<MasterLayout header='Đăng ký'>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Đăng ký | Xoài CMS</title>
			</Helmet>
			<Grid container>
				<Grid container style={{ marginBottom: '1rem' }}>
					<Grid item md={8}>
						<SearchForm
							onSubmit={handleSearchChange}
							label={'Tìm theo tên hoặc số điện thoại'}
						/>
					</Grid>
				</Grid>

				<RenderTable
					exportExcel={exportRegisterList}
					columnSelected={columnSelected}
					handleColumnChange={handleColumnChange}
					changeDisplayColumn={changeDisplayColumn}
					keys={keys}
					pagination={listRegister.pagination}
					data={listRegister.data}
					handleDelete={handleDeleteRegister}
					handleRevert={handeleRevertRegister}
					handleStatus={handeleStautsRegister}
					handlePageChange={handlePageChange}
					roleName={'bài viết'}
					filter1={filters.filter1}
					filter2={filters.filter2}
					filter3={filters.filter3}
					handleFilterChange={handleFilterChange}
					resetFilter={resetFilter}
					role={'2'}
					totalRow={totalRow}
					tableName={'registerList'}
					listFunctionTable={functionsTable}
					listYear={listYear}
					deleteName={`đăng ký`}
					listClasses={listClasses}
				/>
			</Grid>
		</MasterLayout>
	);
}
