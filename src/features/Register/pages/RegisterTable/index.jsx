import {
	Box,
	Button,
	Checkbox,
	FormControl,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography,
} from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import AddIcon from '@material-ui/icons/Add';
import ClassIcon from '@material-ui/icons/Class';
import FilterListIcon from '@material-ui/icons/FilterList';
import GetAppIcon from '@material-ui/icons/GetApp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import LaunchIcon from '@material-ui/icons/Launch';
import classApi from 'api/classApi';
import studentApi from 'api/studentApi';
import DeleteDialog from 'common/components/DeleteDialog';
import Pagination from 'common/components/Pagination';
import React, { useContext, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { default as Select1 } from 'react-select';
import { ToastifyContext } from 'utils/ToastifyConfig';

const useStyles = makeStyles({
	table: {
		minWidth: 650,
	},
	formControl: {
		minWidth: 120,
	},
	title: {
		color: 'blue',
	},
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
		},
	},
};

const LightTooltip = withStyles((theme) => ({
	tooltip: {
		backgroundColor: theme.palette.common.black,
		color: 'white',
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

const useRowStyles = makeStyles({
	root: {
		'& > *': {
			borderBottom: 'unset',
		},
	},
	input: {
		'&:invalid': {
			borderBottom: '2px solid red',
		},
	},
	none: {
		display: 'none',
	},
	title: {
		color: 'blue',
	},
});

function Row(props) {
	let history = useHistory();
	const {
		row,
		listFunction,
		handleDeleteRegis,
		handleRevertRegis,
		handleStatus,
		keys,
		listClasses,
	} = props;
	const [open, setOpen] = React.useState(false);
	const classes = useRowStyles();

	useEffect(() => {
		setOpen(false);
	}, [row]);

	const handleClass = (data, x) => {
		if (x == '1') {
			// const post = async () => {
			// 	try {
			// 		const response = await classApi.addStudentToClass(
			// 			'39',
			// 			'1'
			// 		);
			// 		notify('success', response.message);
			// 	} catch (error) {
			// 		notify('error', error.response.data.message);
			// 		if (error.response.status === 403) {
			// 			history.push('/');
			// 		}
			// 	}
			// };

			// post();
			alert('Chưa có màn thêm học sinh đã tồn tại vào 1 lớp nên tớ để ntn');
		} else {
			localStorage.setItem('registerInfo', JSON.stringify(data));
		}
	};

	return (
		<React.Fragment>
			<TableRow hover id={`row_1 ${row.id}`} className={classes.root}>
				<TableCell>
					<IconButton
						aria-label='expand row'
						size='small'
						onClick={() => setOpen(!open)}
					>
						{open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
					</IconButton>
				</TableCell>
				{keys.map((key, index) => (
					<TableCell
						align={key.align}
						style={{
							display: key.display ? '' : 'none',
							color:
								key.field == 'register_status' && row.register_status == '1'
									? 'green'
									: row.register_status == '3'
									? 'red'
									: '',
						}}
						key={`header${index}`}
					>
						{(key.field == 'register_status' &&
							((row.register_status == '0' && 'Chưa liên hệ') ||
								(row.register_status == '2' && 'Kiểm tra thanh toán') ||
								(row.register_status == '1' && 'Đã liên hệ') ||
								(row.register_status == '3' && 'Đã xóa'))) ||
							row[key.field]}
					</TableCell>
				))}

				{listFunction['edit'] &&
					(row['register_status'] == '0' || row['register_status'] == '2') && (
						<TableCell>
							<DeleteDialog
								title={`Đổi trạng thái`}
								description='Bạn có chắc chắn muốn đổi trạng thái sang đã liên lạc'
								doneText='Có'
								cancelText='Không'
								tooltip_text={`Chưa liên hệ`}
								icon='3'
								action={() => handleStatus(row.id, '1')}
							/>
						</TableCell>
					)}
				{listFunction['edit'] && row['register_status'] == '1' && (
					<TableCell>
						<DeleteDialog
							title={`Đổi trạng thái`}
							description='Bạn có chắc chắn muốn đổi trạng thái sang chưa liên lạc'
							doneText='Có'
							cancelText='Không'
							tooltip_text={`Đã liên hệ`}
							icon='4'
							action={() => handleStatus(row.id, '0')}
						/>
					</TableCell>
				)}

				{listFunction['delete'] && row['register_status'] == '3' && (
					<TableCell>
						<DeleteDialog
							title={`Khôi phục đăng ký`}
							description='Bạn có chắc chắn muốn khôi phục không'
							doneText='Có'
							cancelText='Không'
							tooltip_text={`Khôi phục đăng ký`}
							icon='2'
							action={() => handleRevertRegis(row.id)}
						/>
					</TableCell>
				)}

				{listFunction['delete'] && row['register_status'] != '3' && (
					<TableCell>
						<DeleteDialog
							icon='0'
							title={`Xóa đăng ký`}
							description='Bạn có chắc chắn muốn xóa không'
							doneText='Xóa'
							cancelText='Hủy'
							tooltip_text={`Xóa đăng ký`}
							action={() => handleDeleteRegis(row.id)}
						/>
					</TableCell>
				)}
			</TableRow>
			{/* Khong the  */}
			<TableRow id={row.id}>
				<TableCell colSpan={12}>
					{row.list_regis_class && row.list_regis_class.length > 0 && (
						<Collapse
							id={`col_1${row.id}`}
							in={open}
							timeout='auto'
							unmountOnExit
						>
							<Grid container md={12}>
								<Table>
									<TableHead>
										<TableRow>
											<TableCell
												className={classes.title}
												style={{ width: '20rem' }}
												align='center'
											>
												ID lớp học
											</TableCell>
											<TableCell
												className={classes.title}
												style={{ width: '20rem' }}
												align='left'
											>
												Tên lớp học
											</TableCell>
											<TableCell className={classes.title} align='left'>
												Ghi chú
											</TableCell>
											{listFunction.created_student && (
												<TableCell align='left'>
													{row.user_id == null && (
														<LightTooltip
															placement='top'
															title={'Tạo mới học sinh'}
														>
															<Link
																to={{ pathname: `/create/student` }}
																onClick={() => handleClass(row)}
																target='_blank'
															>
																<AddIcon></AddIcon>
															</Link>
														</LightTooltip>
													)}
												</TableCell>
											)}
										</TableRow>
									</TableHead>
									<TableBody>
										{row.list_regis_class.map((list_detail_Row, index) => (
											<TableRow key={`class ${index}`}>
												<TableCell align='center'>
													{list_detail_Row.class_id}
												</TableCell>
												<TableCell align='left'>
													{list_detail_Row.class_name}
												</TableCell>
												<TableCell align='left'>
													{list_detail_Row.note}
												</TableCell>
												<TableCell align='left'>
													{row.user_id != null && (
														<DialogAddStudent
															list_class={listClasses}
															class_name={`${list_detail_Row.class_name}(ID: ${list_detail_Row.class_id})`}
															student_id={row.user_id}
															class_id={list_detail_Row.class_id}
															full_name={row.full_name}
														></DialogAddStudent>
													)}
												</TableCell>
											</TableRow>
										))}
									</TableBody>
								</Table>
							</Grid>
						</Collapse>
					)}
					{row.list_regis_course && row.list_regis_course.length > 0 && (
						<Collapse
							id={`col_1${row.id}`}
							in={open}
							timeout='auto'
							unmountOnExit
						>
							<Grid container md={12}>
								<Table>
									<TableHead>
										<TableRow>
											<TableCell
												className={classes.title}
												style={{ width: '20rem' }}
												align='center'
											>
												ID khóa học
											</TableCell>
											<TableCell
												className={classes.title}
												style={{ width: '20rem' }}
												align='left'
											>
												Tiêu đề
											</TableCell>
											<TableCell className={classes.title}>Ghi chú</TableCell>
											<TableCell className={classes.title}></TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{row.list_regis_course.map((list_detail_Row, index) => (
											<TableRow key={`course${index}`}>
												<TableCell align='center'>
													{list_detail_Row.course_id}
												</TableCell>
												<TableCell align='left'>
													{list_detail_Row.title}
												</TableCell>
												<TableCell align='left'>
													{list_detail_Row.note}
												</TableCell>
												<TableCell align='left'>
													{list_detail_Row.status == '2' && (
														<LightTooltip
															placement='top'
															title={'Đến màn chi tiết học sinh'}
														>
															<Link
																to={{ pathname: `/student/${row.user_id}` }}
															>
																<LaunchIcon></LaunchIcon>
															</Link>
														</LightTooltip>
													)}
												</TableCell>
											</TableRow>
										))}
									</TableBody>
								</Table>
							</Grid>
						</Collapse>
					)}
				</TableCell>
			</TableRow>
		</React.Fragment>
	);
}

function DialogAddStudent(props) {
	const notify = useContext(ToastifyContext);
	const { class_name, class_id, student_id, full_name, list_class } = props;
	const [openAddStudent, setOpenAddStudent] = React.useState(false);
	const [errorAddStudent, setErrorAddStudent] = React.useState('');
	const [studentName, setStudentName] = React.useState('');
	const [studentID, setStudentId] = React.useState('');
	const [classes, setClasses] = React.useState('');

	useEffect(() => {
		setStudentName(full_name);
		setStudentId(student_id);
		for (let i = 0; i < list_class.length; i++) {
			if (class_id == list_class[i].value) {
				setClasses(list_class[i]);
			}
		}
	}, []);

	const handleStudentIdChange = (e) => {
		setStudentId(e.target.value);
	};

	const handleCloseAddStudent = () => {
		setOpenAddStudent(false);
	};

	const handleStudentIdBlur = (e) => {
		if (e.target.value) {
			const get = async () => {
				try {
					const response = await studentApi.getStudentById(e.target.value);
					if (response.count > 0) {
						setStudentName(response.data.full_name);
						setErrorAddStudent('');
					} else {
						setStudentName('');
						setErrorAddStudent('Không tìm thấy học sinh');
					}
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	};

	const handleAddStudentDone = () => {
		if (errorAddStudent) {
			return;
		}

		if (studentName === '') {
			setErrorAddStudent('Các trường không được để trống');
			return;
		}

		setErrorAddStudent('');

		const post = async () => {
			try {
				const response = await classApi.addStudentToClass(
					classes.value,
					studentID
				);
				notify('success', response.message);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		post();
		setOpenAddStudent(false);
	};

	const handleOpenAddStudent = () => {
		setOpenAddStudent(true);
	};

	const handleChangeClass = (e) => {
		setClasses(e);
	};

	return (
		<React.Fragment>
			<Box>
				<LightTooltip placement='top' title={'Thêm học sinh vào lớp học'}>
					<ClassIcon
						onClick={handleOpenAddStudent}
						style={{ cursor: 'pointer' }}
						variant='outlined'
						color='primary'
					></ClassIcon>
				</LightTooltip>

				<Dialog open={openAddStudent} onClose={handleCloseAddStudent}>
					<DialogTitle>{`Thêm học sinh ${class_name}`}</DialogTitle>
					<DialogContent>
						<Grid container>
							<Grid container>
								{errorAddStudent !== '' && (
									<Typography variant='body2' style={{ color: 'red' }}>
										{errorAddStudent}
									</Typography>
								)}
							</Grid>

							<Grid container alignItems='center'>
								<Grid item md={3}>
									<Typography>Lớp học </Typography>
								</Grid>
								<Grid item md={9}>
									<FormControl fullWidth>
										<Select1
											value={classes}
											onChange={(e) => handleChangeClass(e)}
											options={list_class}
										></Select1>
									</FormControl>
								</Grid>
							</Grid>

							<Grid container alignItems='center'>
								<Grid item md={3}>
									<Typography>Mã học sinh: </Typography>
								</Grid>
								<Grid item md={9}>
									<FormControl fullWidth>
										<TextField
											required
											name='studentId'
											value={studentID}
											variant='outlined'
											margin='dense'
											size='small'
											onChange={handleStudentIdChange}
											onBlur={handleStudentIdBlur}
										/>
									</FormControl>
								</Grid>
							</Grid>
							<Grid container alignItems='center'>
								<Grid item md={3}>
									<Typography>Họ và tên: </Typography>
								</Grid>
								<Grid item md={9}>
									<FormControl fullWidth>
										<TextField
											disabled
											value={studentName}
											variant='outlined'
											margin='dense'
											size='small'
										/>
									</FormControl>
								</Grid>
							</Grid>
						</Grid>
					</DialogContent>
					<DialogActions style={{ marginTop: '1rem' }}>
						<Button variant='contained' onClick={handleCloseAddStudent}>
							Hủy
						</Button>
						<Button
							style={{ marginRight: '1rem' }}
							variant='contained'
							color='primary'
							autoFocus
							onClick={handleAddStudentDone}
						>
							Thêm
						</Button>
					</DialogActions>
				</Dialog>
			</Box>
		</React.Fragment>
	);
}

export default function RegisterTable(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id,
	listFunction
) {
	return function (props) {
		const {
			handleCreate,
			handleDelete,
			handleColumnChange,
			changeDisplayColumn,
			handleFilterChange,
			columnSelected,
			handleUpdate,
			keys,
			pagination,
			data,
			handlePageChange,
			roleName,
			filter1,
			filter2,
			filter3,
			resetFilter,
			role,
			totalRow,
			tableName,
			listFunctionTable,
			handleRevert,
			handleStatus,
			listYear,
			listClasses,
			exportExcel,
		} = props;
		if (pagination != undefined) {
			pagination.totalRows = totalRow;
		}
		let history = useHistory();
		const [isLoading, setIsLoading] = React.useState(false);
		const classes = useStyles();
		if (listYear) {
			filterList3 = listYear;
		}
		return (
			<Grid container>
				<Grid container style={{ padding: '0.75rem' }} alignItems='center'>
					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						<FormControl className={classes.formControl}>
							<InputLabel>{listFilterLabel[0]}</InputLabel>
							<Select
								value={filter1}
								onChange={(e) => handleFilterChange(e, 'filter1')}
							>
								{filterList1.map((filter, index) => (
									<MenuItem key={`filter1${index}`} value={filter.value}>
										{filter.text}
									</MenuItem>
								))}
							</Select>
						</FormControl>
					</Grid>

					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						{filterList2.length > 0 && (
							<FormControl className={classes.formControl}>
								<InputLabel>{listFilterLabel[1]}</InputLabel>
								<Select
									value={filter2}
									onChange={(e) => handleFilterChange(e, 'filter2')}
								>
									{filterList2.map((filter, index) => (
										<MenuItem key={`filter2${index}`} value={filter.value}>
											{filter.text}
										</MenuItem>
									))}
								</Select>
							</FormControl>
						)}
					</Grid>

					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						{filterList3.length > 0 && (
							<FormControl className={classes.formControl}>
								<InputLabel>{listFilterLabel[2]}</InputLabel>
								<Select
									value={filter3}
									onChange={(e) => handleFilterChange(e, 'filter3')}
								>
									{filterList3.map((filter, index) => (
										<MenuItem key={`filter3${index}`} value={filter.value}>
											{filter.text}
										</MenuItem>
									))}
								</Select>
							</FormControl>
						)}
					</Grid>

					<Grid item md={2}>
						{(filter1 !== '' || filter2 !== '' || filter3 !== '') && (
							<Button
								onClick={resetFilter}
								startIcon={<FilterListIcon />}
								variant='contained'
								color='secondary'
							>
								Xóa lọc
							</Button>
						)}
					</Grid>
					<Grid item md={2} container justify='flex-end'>
						{listFunctionTable['view'] == true && (
							<Button
								startIcon={<GetAppIcon />}
								variant='text'
								onClick={exportExcel}
							>
								Xuất file
							</Button>
						)}
					</Grid>

					<Grid item md={2} container justify='flex-end'>
						<Select
							multiple
							value={columnSelected}
							onChange={handleColumnChange}
							input={<Input />}
							renderValue={() => 'Chọn cột hiển thị'}
							MenuProps={MenuProps}
						>
							{keys.map((key, index) => (
								<MenuItem
									onClick={() => changeDisplayColumn(key)}
									key={index}
									value={key.header}
								>
									<Checkbox checked={columnSelected.indexOf(key.header) > -1} />
									<ListItemText primary={key.header} />
								</MenuItem>
							))}
						</Select>
					</Grid>
				</Grid>

				<TableContainer component={Paper} style={{ marginTop: '1rem' }}>
					<Table className={classes.table} stickyHeader>
						<TableHead>
							<TableRow>
								<TableCell style={{ width: '5%' }}></TableCell>
								{keys.map((key, index) => (
									<TableCell
										align={key.align}
										style={{ display: key.display ? '' : 'none' }}
										key={`header${index}`}
									>
										{key.header}
									</TableCell>
								))}
								{listFunctionTable['delete'] && <TableCell></TableCell>}
								{listFunctionTable['edit'] && <TableCell></TableCell>}
							</TableRow>
						</TableHead>

						{data && data.length > 0 ? (
							<TableBody>
								{data.map((row, index) => (
									<Row
										listClasses={listClasses}
										key={`regis_1${index}`}
										row={row}
										listFunction={listFunctionTable}
										handleDeleteRegis={handleDelete}
										handleRevertRegis={handleRevert}
										handleStatus={handleStatus}
										keys={keys}
									/>
								))}
							</TableBody>
						) : (
							<TableBody>
								<TableRow>
									<TableCell align='center' colSpan={keys.length + 3}>
										Không tìm thấy dữ liệu
									</TableCell>
								</TableRow>
							</TableBody>
						)}
					</Table>
				</TableContainer>
				{data && (
					<Grid container alignItems='center' style={{ marginTop: '2rem' }}>
						<Grid item md={6}>
							<Typography style={{ fontWeight: 'bold' }}>
								Tổng số ({pagination.totalRows})
							</Typography>
						</Grid>
						<Grid item md={6}>
							<Pagination
								pagination={pagination}
								onPageChange={handlePageChange}
							/>
						</Grid>
					</Grid>
				)}
			</Grid>
		);
	};
}
