import { CKEditor } from '@ckeditor/ckeditor5-react';
import { TextField } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import CategoryIcon from '@material-ui/icons/Category';
import CopyrightIcon from '@material-ui/icons/Copyright';
import DehazeIcon from '@material-ui/icons/Dehaze';
import LinkIcon from '@material-ui/icons/Link';
import WebAssetIcon from '@material-ui/icons/WebAsset';
import lessonApi from 'api/lessonApi';
import permissionApi from 'api/permissionApi';
import Editor from 'ckeditor5-custom-build/build/ckeditor';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect } from 'react';
import { useHistory } from 'react-router';
import Select from 'react-select';
import { ToastifyContext } from 'utils/ToastifyConfig';

const useStyles = makeStyles((theme) => ({
	button_list_item: {
		padding: '0px !important',
	},
	input: {
		padding: '18.5px 14px',
	},
	videoLink: {
		padding: '18.5px 14px',
		'&:hover': {
			backgroundColor: '#00C642',
			opacity: '0.2',
		},
		cursor: 'pointer',
	},
	root: {
		flexGrow: 1,
	},
	img: {
		width: '100%',
	},
	black: {
		color: 'black',
	},
	green: {
		color: 'green',
	},
	red: {
		color: 'red',
	},
	bgGreen: {
		backgroundColor: '#ADFF2F',
	},
	bgGray: {
		backgroundColor: '#889FA5',
	},
	bgRed: {
		backgroundColor: '#DC143C',
	},
	width_10: {
		minWidth: '30px',
		width: '5px',
	},
	form: {
		width: '80%',
		[theme.breakpoints.down('xs')]: {
			width: '100%',
		},
		marginTop: theme.spacing(1),
	},
	paper: {
		padding: theme.spacing(1),
		textAlign: 'center',
		color: theme.palette.text.secondary,
	},
	richTextEditor: {
		'& .ck-editor__main > .ck-editor__editable': {
			maxHeight: `${window.innerHeight - 222}px`,
		},
	},

	textAreaMedium: {},
	medium: {
		minHeight: '100px',
	},
	fullWidth: {
		width: '100%',
	},
	minWidth: {
		'& .MuiSelect-select': {
			padding: '18.5px 14px',
			border: '1px solid rgba(0, 0, 0, 0.23);',
			borderRadius: '4px',
		},

		minWidth: '100%',
	},
	center: {
		margin: '0 auto',
	},
	displayNone: {
		display: 'none',
	},
	border: {
		borderTop: '1px groove #dedede',
		borderRight: '1px groove #dedede',
		borderLeft: '1px groove #dedede',
		borderRadius: '4px',
	},
	redBorder: {
		border: '1px groove red',
		borderRadius: '4px',
	},

	// new
	editOption: {
		borderColor: 'hsl(0, 0%, 80%)',
		borderRadius: '4px',
		borderStyle: 'solid',
		borderWidth: '1px',
		width: ' 90%',
		height: '100%',
		position: 'absolute',
		top: '0px',
		left: '0px',
		fontSize: '0.875rem',
		fontFamily: 'Roboto Helvetica Arial sans-serif',
		fontWeight: '400',
		lineHeight: '1.43',
		letterSpacing: ' 0.01071em',
		padding: '18px 14px',
	},
	listTopics: {
		position: 'relative',
	},
}));

const LightTooltip = withStyles((theme) => ({
	tooltip: {
		backgroundColor: theme.palette.common.black,
		color: 'white',
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

function getId(url) {
	const regex =
		/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;
	const match = url.match(regex);
	return match && match[5].length === 11 ? match[5] : null;
}

export default function LessonDetail(props) {
	const notify = useContext(ToastifyContext);
	let history = useHistory();

	const [isWrongCost, setIsWrongCost] = React.useState(false);

	const [thumbnail, setThumbnail] = React.useState([]);

	const [CKEditorDataDB, setCKEditorDataDB] = React.useState('');

	const [loading, setLoading] = React.useState(false);
	const [status, setStatus] = React.useState('');
	const [functions, setFunctions] = React.useState([]);
	const [cantEdit, setCantEdit] = React.useState(true);
	const [isReview, setIsReview] = React.useState(false);

	const state = props.location.state;

	const [isPermiss, setIsPermiss] = React.useState(false);
	// new
	const [topics, setTopics] = React.useState([]);
	const [selectedTopics, setSelectedTopics] = React.useState({});

	const [isOther, setIsOther] = React.useState(false);
	const [errorInput, setErrorInput] = React.useState({
		topicOrder: '',
		lessonOrder: '',
	});
	const [topicOrder, setTopicOrder] = React.useState('');
	const [lessonOrder, setLessonOrder] = React.useState('');
	const [dataForm, setDataForm] = React.useState({});
	// const { sourceId } = useParams();

	const [isDisableTopicOrder, setIsDisableTopicOrder] = React.useState(false);
	const [reloadTopic, setReloadTopic] = React.useState(false);
	const [errorLink, setErrorLink] = React.useState(true);
	const [embed, setEmbed] = React.useState('');
	let lessonId = '';
	// check permission
	useEffect(() => {
		setLoading((loading) => !loading);
		if (state.source_id == undefined) {
			const post = async () => {
				try {
					const params = {
						functionCode: 'function_create_lesson',
					};
					await permissionApi.checkPermission(params);

					setIsPermiss(true);
					setDataForm({
						topic: '',
						lesson_name: '',
						video_link: '',
						topicOrder: '',
						lessonOrder: '',
					});
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			post();
		} else {
			const post = async () => {
				try {
					const params = {
						functionCode: 'function_edit_lesson',
					};
					await permissionApi.checkPermission(params);
					setLoading((loading) => !loading);
					setIsPermiss(true);
				} catch (error) {
					if (
						error.response.status == 403 &&
						error.response.data.message == 'Not permission'
					) {
						try {
							const params1 = {
								functionCode: 'function_review_lesson',
							};
							await permissionApi.checkPermission(params1);
							setIsPermiss(true);
							setLoading((loading) => !loading);
						} catch (error) {
							setLoading((loading) => !loading);
							notify('error', error.response.data.message);
							localStorage.removeItem('menu');
							history.push('/');
						}
					} else {
						notify('error', error.response.data.message);
					}
				}
			};

			post();
		}
	}, []);

	// get list function
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const getFunctions = async () => {
				try {
					const respone = await permissionApi.getFunctions();

					const data = [];
					for (let i = 0; i < respone.length; i++) {
						data.push(respone[i].function_id);
					}
					if (
						data.includes('function_lesson_list') &&
						((state.source_id != undefined &&
							data.includes('function_edit_lesson')) ||
							(state.source_id == undefined &&
								data.includes('function_create_lesson')))
					) {
						setCantEdit(false);
					}
					setFunctions(data);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			getFunctions();
		}
	}, [isPermiss]);

	// get source by id
	useEffect(() => {
		if (isPermiss && state.source_id) {
			setLoading((loading) => !loading);

			const getSourceById = async () => {
				try {
					const response = await lessonApi.getSourceById(state.source_id);
					const data = response.data;
					if (data) {
						setSelectedTopics({
							value: data[0].lesson_id,
							label: data[0].label,
						});
						setIsOther(false);
						setTopicOrder(data[0].lesson_order.toString());
						const videoYoutube = getId(data[0].source_link);
						const normalLink = `https://www.youtube.com/watch?v=${videoYoutube}`;
						const object = {
							lesson_name: data[0].source_title,
							video_link: normalLink,
							lessonOrder: data[0].order.toString(),
						};
						setCKEditorDataDB(data[0].source_detail);
						setLessonOrder(data[0].order.toString());
						setDataForm(object);
						setIsDisableTopicOrder(true);
						if (document.getElementById('other') != null) {
							document.getElementById('other').value = '';
						}
					}
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getSourceById();
		}
	}, [isPermiss]);

	// check params
	// if(courseId==undefined){

	// }
	// check params

	if (state != undefined && state.course_id) {
		//get list topic ( chủ đề)

		useEffect(() => {
			if (isPermiss) {
				const params = state.course_id;

				setLoading((loading) => !loading);
				const getTopics = async () => {
					try {
						const response = await lessonApi.getTopics(params);
						const topic = response.data;
						topic.unshift({
							value: '0',
							label: 'Tạo chủ đề mới',
						});

						setTopics(topic);
						setLoading((loading) => !loading);
					} catch (error) {
						setLoading((loading) => !loading);
						notify('error', error.response.data.message);
					}
				};
				getTopics();
			}
		}, [isPermiss, reloadTopic]);
	}

	const handleChangeTopic = (event) => {
		setSelectedTopics(event);

		if (event.value == '0') {
			setIsOther(true);
			setIsDisableTopicOrder(false);
			setTopicOrder('');
			lessonId = '';
			setSelectedTopics({});
		} else {
			setIsOther(false);
			// setTopicOrder(event.target.value.toString());
			for (let i = 0; i < topics.length; i++) {
				if (topics[i].value == event.value) {
					setTopicOrder(topics[i].lessonorder);
					break;
				}
			}
			setIsDisableTopicOrder(true);

			if (document.getElementById('other') != null) {
				document.getElementById('other').value = '';
			}
		}
	};
	//change teacher
	const handleChangeTeacher = (event) => {
		setSelectedTeacher(event.target.value);
	};

	// save thumbnail picture
	const handleSave = (childData) => {
		setThumbnail(childData);
	};

	// set course status
	const handleReivew = (status) => {
		setStatus(status);
		setIsReview(true);
		if (status == '1') {
			document.getElementById('source_status').value = 'Đã duyệt';
		} else {
			document.getElementById('source_status').value = 'Từ chối';
		}
	};

	const classes = useStyles();

	// ckeditor config
	const editorConfiguration = {
		toolbar: {
			items: [
				'blockQuote',
				'bold',
				'imageTextAlternative',
				'link',
				'selectAll',
				'undo',
				'redo',
				'heading',
				'resizeImage:original',
				'resizeImage:25',
				'resizeImage:50',
				'resizeImage:75',
				'resizeImage',
				'imageResize',
				'imageStyle:full',
				'imageStyle:side',
				'imageUpload',
				'indent',
				'outdent',
				'italic',
				'numberedList',
				'bulletedList',
				'insertTable',
				'tableColumn',
				'tableRow',
				'mergeTableCells',
			],

			shouldNotGroupWhenFull: true,
		},

		image: {
			styles: ['alignLeft', 'alignCenter', 'alignRight'],
			toolbar: [
				'imageStyle:alignLeft',
				'imageStyle:alignCenter',
				'imageStyle:alignRight',
				'|',
				'|',
				'imageTextAlternative',
			],
		},

		ckfinder: {
			uploadUrl: `${process.env.REACT_APP_API_URL}/uploadImage`,
		},
	};

	// check topic order
	const handleBlurCheckNumber = (e) => {
		if (e.target.value.match(/^[0-9]*$/)) {
			setErrorInput({
				...errorInput,
				[e.target.name]: '',
			});
			if (e.target.name == 'topicOrder') {
				setTopicOrder(e.target.value);
			} else if (e.target.name == 'lessonOrder') {
				setLessonOrder(e.target.value);
			}
		} else {
			setErrorInput({
				...errorInput,
				[e.target.name]: 'Vui lòng nhập số',
			});
			if (e.target.name == 'topicOrder') {
				setTopicOrder('');
			} else if (e.target.name == 'lessonOrder') {
				setLessonOrder('');
			}
		}
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (
			selectedTopics.value == undefined &&
			(document.getElementById('other') == undefined ||
				document.getElementById('other').value == '')
		) {
			notify('error', 'Vui lòng nhập chọn chủ đề');
			return;
		}
		if (errorLink == false) {
			notify('error', 'Vui lòng nhập link youtube');
			return;
		}

		if (topicOrder.toString().length > 0 && lessonOrder.toString().length > 0) {
			if (isReview && state.source_id) {
				setLoading((loading) => !loading);
				const params = {
					source_id: state.source_id,
					status: status,
				};
				const updateLessonSourceStatus = async () => {
					try {
						const response = await lessonApi.updateLessonSourceStatus(params);
						setLoading((loading) => !loading);
						notify('success', response.message);
						setIsReview(false);
					} catch (error) {
						setIsReview(false);
						setLoading((loading) => !loading);
						notify('error', error.response.data.message);
						if (error.response.status == 403) {
							history.push('/');
						}
					}
				};
				updateLessonSourceStatus();
			} else if (!cantEdit && state.source_id != undefined) {
				if (CKEditorDataDB == '' && dataForm['video_link'] == '') {
					notify('error', 'Vui lòng nhập video hoặc nội dung bài học');
				} else {
					// setLoading(loading => !loading);
					let lesson_topic = '';
					if (isOther) {
						lesson_topic = document.getElementById('other').value;
					}

					const lesson = {
						course_id: state.course_id,
						lesson_topic: lesson_topic,
						topic_order: topicOrder,
						lesson_id: selectedTopics.value || '',
					};
					const videoId = getId(dataForm['video_link']);
					const embedVideo = `https://www.youtube.com/embed/${videoId}`;
					const source = {
						lesson_name: dataForm['lesson_name'],
						lesson_order: lessonOrder,
						video_link: embedVideo,
						source_detail: CKEditorDataDB,
						source_id: state.source_id,
					};

					const data = {
						lesson: lesson,
						source: source,
					};

					setLoading((loading) => !loading);
					const updateSource = async () => {
						try {
							const response = await lessonApi.updateSource(data);
							setLoading((loading) => !loading);
							notify('success', response.message);
						} catch (error) {
							setLoading((loading) => !loading);
							notify('error', error.response.data.message);
							if (error.response.status == 403) {
								history.push('/');
							}
						}
					};
					updateSource();
				}
			} else if (state.source_id == undefined) {
				if (CKEditorDataDB == '' && dataForm['video_link'] == '') {
					notify('error', 'Vui lòng nhập video hoặc nội dung bài học');
				} else {
					let lesson_topic = '';
					if (isOther) {
						lesson_topic = document.getElementById('other').value;
					}
					const lesson = {
						course_id: state.course_id,
						lesson_topic: lesson_topic,
						topic_order: topicOrder,
						lesson_id: selectedTopics.value || '',
					};
					const videoId = getId(dataForm['video_link']);
					const embedVideo = `https://www.youtube.com/embed/${videoId}`;
					const source = {
						lesson_name: dataForm['lesson_name'],
						lesson_order: lessonOrder,
						video_link: embedVideo,
						source_detail: CKEditorDataDB,
					};

					const data = {
						lesson: lesson,
						source: source,
					};
					if (isPermiss) {
						setLoading((loading) => !loading);
						const createLesson = async () => {
							try {
								const response = await lessonApi.createLesson(data);

								setLoading((loading) => !loading);
								setReloadTopic((load) => !load);
								setSelectedTopics({});
								setIsOther(false);
								setTopicOrder('');
								notify('success', response.message);
							} catch (error) {
								setLoading((loading) => !loading);
								notify('error', error.response.data.message);
							}
						};
						createLesson();
					}
				}
			}
		}
	};

	const handleBlurData = (e) => {
		setDataForm({
			...dataForm,
			[e.target.id]: e.target.value,
		});
	};

	const handleChangeTopicORder = (e) => {
		setTopicOrder(e.target.value);
	};

	const handleChangeFormDate = (e) => {
		setDataForm({
			...dataForm,
			[e.target.id]: e.target.value,
		});
	};

	const handleLink = (e) => {
		const link = e.target.value;
		const regex =
			/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;
		if (link.match(regex) || e.target.value.length == 0) {
			setErrorLink(true);
		} else {
			setErrorLink(false);
			notify('error', 'Vui lòng nhập link youtube');
		}
	};

	const handleCopy = (e) => {
		const copyText = document.getElementById('video_link');

		/* Select the text field */
		copyText.select();
		copyText.setSelectionRange(0, 99999); /* For mobile devices */

		/* Copy the text inside the text field */
		document.execCommand('copy');

		/* Alert the copied text */
		notify('success', 'Copied to clipboard');
	};
	return (
		<MasterLayout
			header={`${state.is_detail ? state.is_detail : 'Tạo bài học'}`}
			referer={[
				{
					value: `course/lessonlist`,
					display: `${state.title}`,
					state: `${state.title}`,
					course_id: `${state.course_id}`,
				},
			]}
		>
			{isPermiss && (
				<Grid container spacing={2}>
					{loading == true && (
						<div
							style={{
								position: 'fixed',
								top: '50%',
								left: '50%',
								width: '50px',
								transform: 'translate(-50%, -50%)',
								zIndex: '1000000000',
							}}
							className='cp-spinner cp-balls'
						></div>
					)}
					<form name='createLesson' onSubmit={(e) => handleSubmit(e)}>
						<Grid container spacing={2} className={classes.root}>
							<Grid item xs={12} sm={12}>
								<Paper className={classes.paper}>
									<Grid container spacing={2}>
										<Grid item md={8} sm={8}>
											<Grid item md={12}>
												<ListItem>
													<ListItemIcon className={classes.black}>
														<CategoryIcon />
													</ListItemIcon>
													<ListItemText
														className={classes.black}
														primary='Chủ đề'
													/>
												</ListItem>
											</Grid>
											<Grid item md={12} className={classes.listTopics}>
												<Select
													className={classes.minWidth}
													required={isOther ? false : true}
													disabled={cantEdit}
													value={selectedTopics}
													onChange={(e) => handleChangeTopic(e)}
													style={{
														padding: '100px',
													}}
													options={topics}
												/>
												{/* )
												} */}

												{isOther && (
													<input
														required
														placeholder='Nhập chủ đề'
														id='other'
														className={classes.editOption}
														style={{ display: isOther ? '' : 'none' }}
													></input>
												)}
											</Grid>
										</Grid>

										<Grid item md={4} sm={4}>
											<Grid item md={12}>
												<ListItem>
													<ListItemIcon className={classes.black}>
														<DehazeIcon />
													</ListItemIcon>
													<ListItemText
														className={classes.black}
														primary='Thứ tự Chủ đề'
													/>
												</ListItem>
											</Grid>

											<Grid item md={12}>
												<TextField
													required
													disabled={isDisableTopicOrder}
													className={classes.border}
													key='topic_order'
													type='number'
													name='topicOrder'
													value={topicOrder}
													onChange={handleChangeTopicORder}
													error={errorInput.topicOrder}
													inputProps={{
														className: classes.input,
														pattern: '^[0-9]*',
													}}
													onBlur={handleBlurCheckNumber}
													fullWidth
												></TextField>
												{errorInput.topicOrder && (
													<small style={{ color: 'red' }}>
														{errorInput.topicOrder}
													</small>
												)}
											</Grid>
										</Grid>

										<Grid item md={8} sm={8}>
											<Grid item md={12}>
												<ListItem>
													<ListItemIcon className={classes.black}>
														<WebAssetIcon />
													</ListItemIcon>
													<ListItemText
														className={classes.black}
														primary='Tên bài học'
													/>
												</ListItem>
											</Grid>
											<Grid item md={12} className={classes.listTopics}>
												<TextField
													key='lesson_name'
													id='lesson_name'
													required
													disabled={cantEdit}
													value={dataForm.lesson_name}
													onChange={handleChangeFormDate}
													className={classes.border}
													key='lesson_name'
													inputProps={{
														className: classes.input,
													}}
													fullWidth
												></TextField>
											</Grid>
										</Grid>

										<Grid item md={4} sm={4}>
											<Grid item md={12}>
												<ListItem>
													<ListItemIcon className={classes.black}>
														<DehazeIcon />
													</ListItemIcon>
													<ListItemText
														className={classes.black}
														primary='Thứ tự bài học'
													/>
												</ListItem>
											</Grid>

											<Grid item md={12}>
												<TextField
													required
													className={classes.border}
													key='lesson_order'
													type='number'
													name='lessonOrder'
													id='lessonOrder'
													disabled={cantEdit}
													value={dataForm.lessonOrder}
													error={errorInput.lessonOrder}
													onChange={handleChangeFormDate}
													inputProps={{
														className: classes.input,
														pattern: '^[0-9]*',
													}}
													onBlur={handleBlurCheckNumber}
													fullWidth
												></TextField>
												{errorInput.lessonOrder && (
													<small style={{ color: 'red' }}>
														{errorInput.lessonOrder}
													</small>
												)}
											</Grid>
										</Grid>

										<Grid item md={8} sm={8}>
											<Grid item md={12}>
												<ListItem>
													<ListItemIcon className={classes.black}>
														<LinkIcon />
													</ListItemIcon>
													<ListItemText
														className={classes.black}
														primary='Video Link (Youtube only)'
													/>
													<LightTooltip placement='top' title='Copy video link'>
														<CopyrightIcon onClick={handleCopy}></CopyrightIcon>
													</LightTooltip>
												</ListItem>
											</Grid>

											<Grid container item md={12}>
												<TextField
													id='video_link'
													value={dataForm.video_link}
													disabled={cantEdit}
													onChange={handleChangeFormDate}
													onBlur={handleLink}
													className={classes.border}
													key='redirct_link'
													inputProps={{
														className: classes.input,
													}}
													fullWidth
												></TextField>
											</Grid>
										</Grid>

										{state.source_status && (
											<Grid item md={4} sm={4}>
												<Grid item md={12}>
													<ListItem>
														<ListItemIcon className={classes.black}>
															<LinkIcon />
														</ListItemIcon>
														<ListItemText
															className={classes.black}
															primary='Trạng Thái'
														/>
													</ListItem>
												</Grid>

												<Grid container item md={12}>
													<TextField
														id='source_status'
														defaultValue={state.source_status}
														disabled={true}
														className={classes.border}
														inputProps={{
															className: classes.input,
														}}
														fullWidth
													></TextField>
												</Grid>
											</Grid>
										)}
									</Grid>
								</Paper>
							</Grid>

							<Grid item xs={12} sm={12}>
								<Paper className={`${classes.paper} ${classes.richTextEditor}`}>
									{functions.length > 0 && (
										<CKEditor
											editor={Editor}
											config={editorConfiguration}
											data={CKEditorDataDB}
											onReady={(editor) => {
												if (
													functions.includes('function_course_list') &&
													((state.source_id != undefined &&
														functions.includes('function_edit_course')) ||
														(state.source_id == undefined &&
															functions.includes('function_create_course')))
												) {
													editor.isReadOnly = false;
												} else {
													editor.isReadOnly = true;
												}
											}}
											onChange={(event, editor) => {
												const data = editor.getData();
												setCKEditorDataDB(data);
											}}
											onBlur={(event, editor) => {}}
											onFocus={(event, editor) => {}}
										/>
									)}
								</Paper>
							</Grid>
							<Grid container style={{ marginTop: '2rem' }} justify='center'>
								{cantEdit == false && (
									<Button
										// onClick
										style={{ marginRight: '1rem' }}
										type='submit'
										variant='contained'
										className={classes.bgGray}
									>
										<ListItem className={classes.button_list_item}>
											<ListItemIcon className={classes.width_10}>
												<svg
													xmlns='http://www.w3.org/2000/svg'
													class='icon icon-tabler icon-tabler-edit'
													width='44'
													height='44'
													viewBox='0 0 24 24'
													stroke-width='1.5'
													stroke='#2c3e50'
													fill='none'
													stroke-linecap='round'
													stroke-linejoin='round'
												>
													<path stroke='none' d='M0 0h24v24H0z' fill='none' />
													<path d='M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3' />
													<path d='M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3' />
													<line x1='16' y1='5' x2='19' y2='8' />
												</svg>
											</ListItemIcon>
											<ListItemText
												className={classes.black}
												primary={
													state.source_id != undefined ? 'Chỉnh sửa' : 'Tạo mới'
												}
											/>
										</ListItem>
									</Button>
								)}

								{state.source_id != undefined &&
									functions.includes('function_review_course') && (
										<div>
											<Button
												style={{ marginRight: '1rem' }}
												type='submit'
												onClick={() => handleReivew('1')}
												variant='contained'
												className={classes.bgGreen}
											>
												<ListItem className={classes.button_list_item}>
													<ListItemIcon className={classes.width_10}>
														<svg
															xmlns='http://www.w3.org/2000/svg'
															class='icon icon-tabler icon-tabler-circle-check'
															width='44'
															height='44'
															viewBox='0 0 24 24'
															stroke-width='1.5'
															stroke='#2c3e50'
															fill='none'
															stroke-linecap='round'
															stroke-linejoin='round'
														>
															<path
																stroke='none'
																d='M0 0h24v24H0z'
																fill='none'
															/>
															<circle cx='12' cy='12' r='9' />
															<path d='M9 12l2 2l4 -4' />
														</svg>
													</ListItemIcon>
													<ListItemText
														className={classes.black}
														primary={'Chấp nhận'}
													/>
												</ListItem>
											</Button>

											<Button
												type='submit'
												onClick={() => handleReivew('2')}
												variant='contained'
												className={classes.bgRed}
											>
												<ListItem className={classes.button_list_item}>
													<ListItemIcon className={classes.width_10}>
														<svg
															xmlns='http://www.w3.org/2000/svg'
															class='icon icon-tabler icon-tabler-ban'
															width='44'
															height='44'
															viewBox='0 0 24 24'
															stroke-width='1.5'
															stroke='#2c3e50'
															fill='none'
															stroke-linecap='round'
															stroke-linejoin='round'
														>
															<path
																stroke='none'
																d='M0 0h24v24H0z'
																fill='none'
															/>
															<circle cx='12' cy='12' r='9' />
															<line x1='5.7' y1='5.7' x2='18.3' y2='18.3' />
														</svg>
													</ListItemIcon>
													<ListItemText
														className={classes.black}
														primary={'Từ chối'}
													/>
												</ListItem>
											</Button>
										</div>
									)}
							</Grid>
						</Grid>
					</form>
				</Grid>
			)}
		</MasterLayout>
	);
}
