import { Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import excelApi from 'api/excelApi';
import lessonApi from 'api/lessonApi';
import permissionApi from 'api/permissionApi';
import CommonTableT from 'common/components/CommonTableT';
import SearchForm from 'common/components/SearchForm';
import MasterLayout from 'common/pages/MasterLayout';
import CreatePost from 'features/Post/components/CreatePost';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { exportToCSV } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

const id = 'user_id';

const filterList1 = [];
const listFilterLabel = ['Chủ đề', 'Trạng Thái'];

const filterList2 = [
	{
		text: 'None',
		value: '',
	},
	{
		text: 'Chưa duyệt',
		value: '0',
	},
	{
		text: 'Đã duyệt',
		value: '1',
	},
	{
		text: 'Từ chối',
		value: '2',
	},
	{
		text: 'Đã xóa',
		value: '3',
	},
];
const filterList3 = [];

const LightTooltip = withStyles((theme) => ({
	tooltip: {
		backgroundColor: theme.palette.common.black,
		color: 'white',
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

const RenderTable = CommonTableT(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id
);

export default function LessonList(props) {
	const notify = useContext(ToastifyContext);
	let history = useHistory();

	const state = props.location.state;
	const [title, setTitle] = useState('');
	if (state != undefined) {
		useEffect(() => {
			setTitle(state.title);
		}, []);
	} else {
		history.push('/');
		return true;
	}
	const [courses, setCourse] = useState([]);
	const [totalRow, setTotalRow] = useState();
	const [loading, setLoading] = useState(false);
	const [subjectList, setSubjectList] = useState([]);
	const [filters, setFilters] = useState({
		limit: 5,
		page: 1,
		q: '',
		filter1: '',
		filter2: '',
		filter3: '',
		course_id: state.course_id,
	});
	// phân quyền
	const [functions, setFunctions] = useState([]);
	const role = '1';
	const [isPermiss, setIsPermiss] = useState(false);
	const [functionsTable, setFunctionTable] = useState({
		view: false,
		edit: false,
		review: false,
		revert: false,
	});
	const [lessons, setLessons] = useState([]);
	// check permission
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_lesson_list',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', `Bạn không có quyền truy cập vào trang này`);
				localStorage.removeItem('menu');
				history.push('/');
			}
		};

		post();
	}, []);

	// get list function
	useEffect(() => {
		if (isPermiss) {
			const getFunctions = async () => {
				try {
					const respone = await permissionApi.getFunctions();
					const a = {
						view: true,
						edit: false,
						delete: false,
						review: false,
					};
					const data = [];
					for (let i = 0; i < respone.length; i++) {
						if (respone[i].function_id == 'function_edit_lesson') {
							a['edit'] = true;
						} else if (respone[i].function_id == 'function_delete_lesson') {
							a['delete'] = true;
						} else if (respone[i].function_id == 'function_review_lesson') {
							a['review'] = true;
						}
						data.push(respone[i].function_id);
					}
					setFunctions(data);
					setFunctionTable(a);
				} catch (error) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			getFunctions();
		}
	}, [isPermiss]);

	// set clounm display
	useEffect(() => {
		const value = [];
		for (let i = 0, l = keys.length; i < l; i += 1) {
			value.push(keys[i].header);
		}
		setColumnSelected(value);
	}, []);

	// get total record
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const params = {
				filters,
			};
			const getTotalLesson = async () => {
				try {
					const response = await lessonApi.getTotalLesson(params);
					setTotalRow(response.data[0].count);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};

			getTotalLesson();
		}
	}, [filters, isPermiss]);

	//get list filter 1
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const getTopics2 = async () => {
				try {
					const response = await lessonApi.getTopics2(state.course_id);
					const subjectFilter = response.data;
					subjectFilter.unshift({
						text: 'None',
						value: '',
					});
					setSubjectList(subjectFilter);

					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getTopics2();
		}
	}, [isPermiss]);

	//get list lessson
	useEffect(() => {
		const params = {
			filters,
		};
		if (isPermiss) {
			setLoading((loading) => !loading);
			const getListLesson = async () => {
				try {
					const response = await lessonApi.getListLesson(params);
					setLessons(response);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					// notify('error', error.response.data.message);
				}
			};
			getListLesson();
		}
	}, [filters, isPermiss]);

	const handlePageChange = (newPage) => {
		setFilters({
			...filters,
			page: newPage,
		});
	};

	const handleSearchChange = (newSearch) => {
		setFilters({
			...filters,
			page: 1,
			q: newSearch.value,
		});
	};

	const handleFilterChange = (e, filter) => {
		setFilters({
			...filters,
			page: 1,
			[filter]: e.target.value,
		});
	};

	const resetFilter = () => {
		setFilters({
			...filters,
			filter1: '',
			filter2: '',
			page: 1,
		});
	};

	const [keys, setKeys] = useState([
		{
			header: 'Thứ tự chủ đề',
			align: 'left',
			field: 'lesson_order',
			display: true,
		},
		{
			header: 'Chủ đề',
			align: 'left',
			field: 'topic',
			display: true,
		},
		{
			header: 'Bài học',
			align: 'left',
			field: 'source_title',
			display: true,
		},
		{
			header: 'Thứ tự bài học',
			align: 'left',
			field: 'order',
			display: true,
		},
		{
			header: 'Trạng thái',
			align: 'left',
			field: 'source_status',
			display: true,
		},
	]);

	const [columnSelected, setColumnSelected] = React.useState([]);

	const changeDisplayColumn = (key) => {
		key.display = !key.display;
		setKeys(keys);
	};

	const handleColumnChange = (event) => {
		setColumnSelected(event.target.value);
	};

	const handleDeleteLesson = (source_id) => {
		setLoading(true);
		const params = {
			source_id: source_id,
			status: '3',
		};
		const updateLessonSourceStatus = async () => {
			try {
				const response = await lessonApi.updateLessonSourceStatus(params);
				setLoading(false);
				notify('success', response.message);
				setFilters({
					...filters,
					delete: '',
				});
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateLessonSourceStatus();
	};

	const handeleRevertLesson = (source_id) => {
		setLoading((loading) => !loading);
		const params = {
			source_id: source_id,
			status: '0',
		};
		const updateLessonSourceStatus = async () => {
			try {
				const response = await lessonApi.updateLessonSourceStatus(params);
				setLoading((loading) => !loading);
				notify('success', response.message);
				setFilters({
					...filters,
					delete: '',
				});
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateLessonSourceStatus();
	};

	const exportLessonList = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getListLessonExcel(filters);
				setLoading(false);
				const csvData = {
					data: response,
				};
				exportToCSV(csvData, `Bai_Hoc_Cua_Khoa_Hoc${title}`);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	return (
		<MasterLayout
			header={title}
			referer={[
				{
					value: 'course',
					display: 'Danh sách khóa học',
				},
			]}
		>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Grid container>
				<Grid container style={{ marginBottom: '1rem' }}>
					<Grid item md={8}>
						<SearchForm
							onSubmit={handleSearchChange}
							label={'Tìm theo tác giả hoặc tiêu đề'}
						/>
					</Grid>
					<Grid item md={4} container justify='flex-end'>
						{functions.includes('function_create_lesson') && (
							<CreatePost
								path={`/createlesson`}
								title={title}
								course_id={state.course_id}
								text='Tạo bài học'
							></CreatePost>
						)}
					</Grid>
				</Grid>

				<RenderTable
					columnSelected={columnSelected}
					handleColumnChange={handleColumnChange}
					changeDisplayColumn={changeDisplayColumn}
					keys={keys}
					exportExcel={exportLessonList}
					pagination={lessons.pagination}
					data={lessons.data}
					handlePageChange={handlePageChange}
					handleDelete={handleDeleteLesson}
					handleRevert={handeleRevertLesson}
					roleName={'bài viết'}
					filter1={filters.filter1}
					filter2={filters.filter2}
					filter3={filters.filter3}
					subjectList={subjectList}
					handleFilterChange={handleFilterChange}
					resetFilter={resetFilter}
					role={'2'}
					totalRow={totalRow}
					tableName={'lessonList'}
					listFunctionTable={functionsTable}
					deleteName={'khóa học'}
					tooltip={'bài học'}
					course_id={state.course_id}
					lesson_title={title}
				/>
			</Grid>
		</MasterLayout>
	);
}
