import { Typography } from '@material-ui/core';
import userApi from 'api/userApi';
import React, { useEffect, useState } from 'react';
import { isEmpty } from 'utils/common';

function TutorDashboard(props) {
	const [user, setUser] = useState({});
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await userApi.getUserById();

				setLoading(false);
				setUser(response);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		get();
	}, []);
	return (
		<div>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			{!isEmpty(user) && (
				<Typography variant='h6'>
					Xin chào {user.full_name}, chúc bạn 1 ngày tốt lành
				</Typography>
			)}
		</div>
	);
}

export default TutorDashboard;
