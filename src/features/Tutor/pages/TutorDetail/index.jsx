import {
	Avatar,
	Box,
	Button,
	Checkbox,
	Chip,
	Divider,
	FormControl,
	Grid,
	IconButton,
	ListItemText,
	makeStyles,
	MenuItem,
	Select,
	TextField,
	Typography,
} from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CloseIcon from '@material-ui/icons/Close';
import classApi from 'api/classApi';
import permissionApi from 'api/permissionApi';
import subjectApi from 'api/subjectApi';
import tutorApi from 'api/tutorApi';
import MasterLayout from 'common/pages/MasterLayout';
import InputField from 'custom-fields/InputField';
import { Field, Form, Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useParams } from 'react-router';
import { useHistory } from 'react-router-dom';
import { checkShow } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as yup from 'yup';

const useStyles = makeStyles((theme) => ({
	postPanel: {
		marginTop: '5rem',
		marginBottom: '0.5rem',
	},
	createPostTitle: {
		fontSize: '1.25rem',
		fontWeight: 'bold',
	},
}));

const referer = [
	{
		value: 'tutor',
		display: 'Trợ giảng',
	},
];

export default function TutorDetail(props) {
	const notify = useContext(ToastifyContext);
	let history = useHistory();
	const classes = useStyles();

	const { tutorId } = useParams();

	const [loading, setLoading] = useState(false);
	const [profile, setProfile] = useState({});
	const [listClass, setListClass] = useState([]);
	const [subjects, setSubjects] = useState([]);
	const [tutorSubject, setTutorSubject] = useState([]);

	const [selectTutorSubject, setSelectTutorSubject] = useState([]);

	const [canEdit, setCanEdit] = useState(false);
	const [canDelete, setCanDelete] = useState(false);

	const [open, setOpen] = useState(false);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleSelectTutorSubjectChange = (e) => {
		const newList = e.target.value;
		setSelectTutorSubject(newList);
	};

	const handleProfileChange = (e) => {
		setProfile({
			...profile,
			[e.target.name]: e.target.value,
		});
	};

	const handleClassRouteChange = (classId) => {
		history.push(`/class/${classId}`);
	};

	const handleSalaryRouteChange = () => {
		history.push({
			pathname: '/salarydetail',
			state: {
				user_id: tutorId,
			},
		});
	};

	const handleDeleteTutor = () => {
		const post = async () => {
			try {
				setLoading(true);
				const response = await tutorApi.deleteTutor(tutorId);
				setLoading(false);
				notify('success', response.message);
				history.push('/tutor');
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	};

	const [isPermiss, setIsPermiss] = useState(false);
	const [functions, setFunctions] = useState([]);
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_list_tutor',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				if (error.response.status === 403) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const response = await permissionApi.getFunctions();
					const data = [];
					for (let i = 0; i < response.length; i++) {
						data.push(response[i].function_id);
					}

					if (data.includes('function_edit_tutor')) {
						setCanEdit(true);
					}

					if (data.includes('function_delete_tutor')) {
						setCanDelete(true);
					}

					setFunctions(data);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await tutorApi.getTutorById(tutorId);
					setLoading(false);
					setProfile(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 404) {
						history.push('/tutor');
					}
					if (error.response.status === 403) {
						localStorage.removeItem('menu');
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, tutorId]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await classApi.getClassByTeacherId(tutorId);
					setLoading(false);
					setListClass(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, tutorId]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await subjectApi.getSubjects();
					setLoading(false);
					setSubjects(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await subjectApi.getSubjectsByTeacherId(tutorId);
					setLoading(false);
					setTutorSubject(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, tutorId]);

	useEffect(() => {
		if (isPermiss) {
			let newList = [];
			for (let i = 0; i < tutorSubject.length; i++) {
				newList.push(tutorSubject[i].subject_id);
			}

			setSelectTutorSubject(newList);
		}
	}, [isPermiss, tutorSubject.length > 0]);

	const validationSchema = yup.object().shape({
		full_name: yup.string().required('Trường không được để trống'),
		display_name: yup.string().required('Trường không được để trống'),
		email: yup
			.string()
			.email('Email không hợp lệ')
			.required('Trường không được để trống'),
		phone: yup
			.string()
			.matches(/^[0-9]+$/, 'Số điện thoại chỉ bao gồm chữ số')
			.min(10, 'Số điện thoại gồm ít nhất 10 số')
			.max(12, 'Số điện thoại gồm nhiều nhất 11 số'),
		address: yup.string(),
		note: yup.string(),
	});

	return (
		<Formik
			initialValues={profile}
			enableReinitialize
			validationSchema={validationSchema}
			onSubmit={(values) => {
				const post = async () => {
					try {
						const data = {
							profile: values,
							tutorsubject: selectTutorSubject,
							defaultTutorSubject: tutorSubject,
						};
						setLoading(true);
						const response = await tutorApi.updateTutor(tutorId, data);
						setLoading(false);
						notify('success', response.message);
					} catch (error) {
						setLoading(false);
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/');
						}
					}
				};
				post();
			}}
		>
			{(formikProps) => {
				return (
					<MasterLayout referer={referer} header={profile.full_name}>
						{isPermiss && (
							<Box>
								{loading && (
									<div
										style={{
											position: 'fixed',
											top: '50%',
											left: '50%',
											width: '50px',
											transform: 'translate(-50%, -50%)',
											zIndex: '1000000000',
										}}
										className='cp-spinner cp-balls'
									></div>
								)}
								<Helmet>
									<title>{`${profile.full_name} | Trợ giảng | Xoài CMS`}</title>
								</Helmet>
								<Form>
									<Grid container spacing={2}>
										<Grid container spacing={2}>
											<Grid item md={3} style={{ marginTop: '1rem' }}>
												<Grid container justify='center'>
													<Avatar
														style={{
															width: '60%',
															height: 'auto',
															boxShadow:
																'0 16px 38px -12px rgb(0 0 0 / 16%), 0 4px 25px 0 rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%)',
														}}
														src={profile.avt_link}
													/>
												</Grid>
												<Grid
													container
													justify='center'
													style={{ marginTop: '0.75rem' }}
												>
													<Typography style={{ fontSize: '1rem' }}>
														{formikProps.values.full_name}
													</Typography>
												</Grid>
											</Grid>
											<Grid item md={9}>
												<Grid container>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Mã số:
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<TextField
																		value={profile.user_id}
																		variant='outlined'
																		size='small'
																		margin='normal'
																		disabled
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Email:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9}>
																<Field
																	name='email'
																	component={InputField}
																	fullWidth
																	size='small'
																	disabled={!canEdit}
																/>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Họ và tên:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<Field
																		name='full_name'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Số điện thoại:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9}>
																<FormControl fullWidth>
																	<Field
																		name='phone'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Tên hiển thị:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<Field
																		name='display_name'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Địa chỉ:
															</Grid>
															<Grid item md={9}>
																<FormControl fullWidth>
																	<Field
																		name='address'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Giới tính:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																{profile.gender && (
																	<FormControl
																		margin='normal'
																		style={{
																			minWidth: '120px',
																		}}
																		size='small'
																	>
																		<Select
																			disabled={!canEdit}
																			name='gender'
																			variant='outlined'
																			value={profile.gender}
																			onChange={handleProfileChange}
																		>
																			<MenuItem value={'male'}>Nam</MenuItem>
																			<MenuItem value={'female'}>Nữ</MenuItem>
																		</Select>
																	</FormControl>
																)}
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Ghi chú:
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<Field
																		name='note'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Ngày tạo:
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<TextField
																		value={new Date(
																			profile.created_at
																		).toLocaleString()}
																		variant='outlined'
																		size='small'
																		margin='normal'
																		disabled
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5} style={{ margin: '1rem 0' }}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={4} lg={4}>
																Các lớp đang dạy:
															</Grid>
															<Grid item md={8}>
																{listClass.length > 0
																	? listClass.map((cl, index) => (
																			<Chip
																				style={{
																					cursor: 'pointer',
																					marginRight: '0.25rem',
																				}}
																				onClick={() =>
																					handleClassRouteChange(cl.class_id)
																				}
																				key={index}
																				label={cl.class_name}
																			/>
																	  ))
																	: 'Không có'}
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Dạy môn:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9}>
																{subjects && (
																	<FormControl
																		style={{
																			minWidth: '120px',
																			maxWidth: '100%',
																		}}
																		margin='normal'
																		size='small'
																	>
																		<Select
																			disabled={!canEdit}
																			variant='outlined'
																			name='teachsubject'
																			id='teachsubject'
																			multiple
																			value={selectTutorSubject}
																			onChange={handleSelectTutorSubjectChange}
																			renderValue={() => {
																				let list = [];
																				for (
																					let i = 0;
																					i < selectTutorSubject.length;
																					i++
																				) {
																					const item = subjects.find(
																						(sj) =>
																							sj.subject_id ===
																							selectTutorSubject[i]
																					);
																					if (item !== undefined) {
																						list.push(item.subject_name);
																					}
																				}

																				return list.join(', ');
																			}}
																		>
																			{subjects.map((sj, index) => (
																				<MenuItem
																					key={index}
																					value={sj.subject_id}
																				>
																					<Checkbox
																						checked={
																							selectTutorSubject.indexOf(
																								sj.subject_id
																							) > -1
																						}
																					/>
																					<ListItemText
																						primary={sj.subject_name}
																					/>
																				</MenuItem>
																			))}
																		</Select>
																	</FormControl>
																)}
															</Grid>
														</Grid>
													</Grid>
												</Grid>
											</Grid>
										</Grid>

										<Grid
											container
											className={classes.postPanel}
											justify='flex-end'
										>
											<Grid item md={3}></Grid>
											<Grid item md={9}>
												<Grid container>
													<Grid item md={11}>
														<Grid container justify='flex-end'>
															{checkShow(functions, 'function_list_salary') && (
																<Button
																	onClick={handleSalaryRouteChange}
																	style={{
																		padding: '0.5rem 1.5rem 0.5rem 1.5rem',
																		marginRight: '1rem',
																	}}
																	variant='contained'
																>
																	Xem bảng lương
																</Button>
															)}
															{canEdit && (
																<Button
																	style={{
																		padding: '0.5rem 1.5rem 0.5rem 1.5rem',
																		marginRight: '1rem',
																	}}
																	type='submit'
																	variant='contained'
																	color='primary'
																>
																	Cập nhập
																</Button>
															)}
															{canDelete && (
																<Button
																	style={{
																		padding: '0.5rem 1.5rem 0.5rem 1.5rem',
																	}}
																	variant='contained'
																	color='secondary'
																	onClick={handleClickOpen}
																>
																	Xóa
																</Button>
															)}
															<Dialog open={open} onClose={handleClose}>
																<Grid container>
																	<Grid item xs={9} md={9} lg={9}>
																		<DialogTitle>Xóa</DialogTitle>
																	</Grid>
																	<Grid
																		item
																		container
																		justify='flex-end'
																		xs={3}
																		md={3}
																		lg={3}
																	>
																		<IconButton onClick={handleClose}>
																			<CloseIcon />
																		</IconButton>
																	</Grid>
																</Grid>
																<Divider />
																<DialogContent>
																	<DialogContentText>
																		Tất cả dữ liệu về trợ giảng này sẽ bị xóa ?
																	</DialogContentText>
																</DialogContent>
																<DialogActions>
																	<Button
																		variant='contained'
																		onClick={handleClose}
																	>
																		Hủy
																	</Button>
																	<Button
																		variant='contained'
																		color='secondary'
																		onClick={handleDeleteTutor}
																	>
																		Xóa
																	</Button>
																</DialogActions>
															</Dialog>
														</Grid>
													</Grid>
												</Grid>
											</Grid>
										</Grid>
									</Grid>
								</Form>
							</Box>
						)}
					</MasterLayout>
				);
			}}
		</Formik>
	);
}
