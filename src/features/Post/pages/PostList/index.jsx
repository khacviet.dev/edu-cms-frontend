import { Grid } from '@material-ui/core';
import excelApi from 'api/excelApi';
import permissionApi from 'api/permissionApi';
import postApi from 'api/postApi';
import CommonTableT from 'common/components/CommonTableT';
import SearchForm from 'common/components/SearchForm';
import MasterLayout from 'common/pages/MasterLayout';
import CreatePost from 'features/Post/components/CreatePost';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { exportToCSV } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

const id = 'user_id';

const filterList1 = [
	{
		text: 'None',
		value: '',
	},
	{
		text: 'Lưu nháp',
		value: '0',
	},
	{
		text: 'Đã duyệt',
		value: '1',
	},
	{
		text: 'Từ chối',
		value: '2',
	},
	{
		text: 'Đã xóa',
		value: '3',
	},
	{
		text: 'Đã hoàn thành',
		value: '4',
	},
];
const listFilterLabel = ['Trạng thái'];

const filterList2 = [];
const filterList3 = [];

const listFunction = {
	create: false,
	edit: true,
	delete: true,
	export: true,
	filter: 1,
};

const RenderTable = CommonTableT(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id,
	listFunction
);

export default function PostList() {
	const [loading, setLoading] = useState(false);
	const notify = useContext(ToastifyContext);
	const [posts, setPosts] = useState([]);
	const [totalRow, setTotalRow] = useState();

	const [filters, setFilters] = useState({
		limit: 5,
		page: 1,
		q: '',
		filter1: '',
		filter2: '',
		filter3: '',
	});

	const [functionsTable, setFunctionTable] = useState({
		view: false,
		edit: false,
		review: false,
		revert: false,
	});

	const handlePageChange = (newPage) => {
		setFilters({
			...filters,
			page: newPage,
		});
	};

	const handleSearchChange = (newSearch) => {
		setFilters({
			...filters,
			page: 1,
			q: newSearch.value,
		});
	};

	const handleFilterChange = (e, filter) => {
		setFilters({
			...filters,
			page: 1,
			[filter]: e.target.value,
		});
	};

	const resetFilter = () => {
		setFilters({
			...filters,
			filter1: '',
			page: 1,
		});
	};

	const [keys, setKeys] = useState([
		{
			header: 'Tiêu đề',
			align: 'left',
			field: 'title',
			display: true,
		},
		{
			header: 'Tác giả',
			align: 'left',
			field: 'full_name',
			display: true,
		},
		{
			header: 'Thể loại',
			align: 'left',
			field: 'category',
			display: true,
		},
		{
			header: 'Ngày tạo',
			align: 'left',
			field: 'date',
			display: true,
		},
		{
			header: 'Trạng Thái',
			align: 'left',
			field: 'post_status',
			display: true,
		},
	]);

	const [columnSelected, setColumnSelected] = React.useState([]);

	const changeDisplayColumn = (key) => {
		key.display = !key.display;
		setKeys(keys);
	};

	const handleColumnChange = (event) => {
		setColumnSelected(event.target.value);
	};

	const handleDeletePost = (post_id) => {
		setLoading(true);
		const deletePost = async () => {
			try {
				const data = {
					post_id: post_id,
					status: '3',
				};

				const response = await postApi.updatePostFlag(data);
				// setTotalRow(response.data[0].count);
				setLoading(false);
				notify('success', response.message);
				setFilters({
					...filters,
					delete: '',
				});
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};
		deletePost();
	};

	const handeleRevertPost = (post_id) => {
		setLoading(true);
		const deletePost = async () => {
			try {
				const data = {
					post_id: post_id,
					status: '0',
				};

				const response = await postApi.updatePostFlag(data);
				// setTotalRow(response.data[0].count);
				setLoading(false);
				notify('success', response.message);
				setFilters({
					...filters,
					delete: '',
				});
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};
		deletePost();
	};

	// phân quyền
	const [functions, setFunctions] = useState([]);
	const [isPermiss, setIsPermiss] = useState(false);
	// check permission
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_post_list',
				};
				await permissionApi.checkPermission(params);
				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				if ((error.response.status = 403)) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			}
		};

		post();
	}, []);

	// get list function
	useEffect(() => {
		if (isPermiss) {
			const getFunctions = async () => {
				try {
					const respone = await permissionApi.getFunctions();
					const data = [];
					const a = {
						view: true,
						edit: false,
						delete: false,
						review: false,
					};
					for (let i = 0; i < respone.length; i++) {
						if (respone[i].function_id == 'function_edit_post') {
							a['edit'] = true;
						} else if (respone[i].function_id == 'function_delete_post') {
							a['delete'] = true;
						} else if (respone[i].function_id == 'function_review_post') {
							a['review'] = true;
						}
						data.push(respone[i].function_id);
					}
					setFunctionTable(a);
					setFunctions(data);
				} catch (error) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			getFunctions();
		}
	}, [isPermiss]);

	useEffect(() => {
		const value = [];
		for (let i = 0, l = keys.length; i < l; i += 1) {
			value.push(keys[i].header);
		}
		setColumnSelected(value);
	}, []);

	//get post category
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);
			const getPostCategory = async () => {
				try {
					const response = await postApi.getPostCategory();

					setLoading(false);
					// setTotalRow(response.data[0].count);
					// setIsLoading(loading => !loading);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getPostCategory();
		}
	}, [isPermiss]);

	// get total record
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);
			const params = {
				role: '2',
				filters,
			};
			const getTotalPost = async () => {
				try {
					const response = await postApi.getTotalRecord(params);
					setTotalRow(response.data[0].count);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getTotalPost();
		}
	}, [filters, isPermiss]);
	// get post List
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);
			const params = {
				role: '2',
				filters,
			};
			const getListPost = async () => {
				try {
					const response = await postApi.getPostList(params);
					setPosts(response);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getListPost();
		}
	}, [filters, isPermiss]);

	const exportPostList = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getPostListExcel(filters);
				setLoading(false);
				const csvData = {
					data: response,
				};
				exportToCSV(csvData, 'Danh_Sach_Bai_viet');
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	return (
		<MasterLayout header='Danh sách bài viết'>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Bài viết | Xoài CMS</title>
			</Helmet>
			<Grid container>
				<Grid container style={{ marginBottom: '1rem' }}>
					<Grid item md={8}>
						<SearchForm
							onSubmit={handleSearchChange}
							label={'Tìm theo tác giả hoặc tiêu đề'}
						/>
					</Grid>

					<Grid item md={4} container justify='flex-end'>
						{functions.includes('function_create_post') && (
							<CreatePost path='postdetail' text='Tạo bài viết'></CreatePost>
						)}
					</Grid>
				</Grid>

				<RenderTable
					exportExcel={exportPostList}
					columnSelected={columnSelected}
					handleColumnChange={handleColumnChange}
					changeDisplayColumn={changeDisplayColumn}
					keys={keys}
					pagination={posts.pagination}
					data={posts.data}
					handleDelete={handleDeletePost}
					handleRevert={handeleRevertPost}
					handlePageChange={handlePageChange}
					roleName={'bài viết'}
					filter1={filters.filter1}
					filter2={filters.filter2}
					filter3={filters.filter3}
					handleFilterChange={handleFilterChange}
					resetFilter={resetFilter}
					role={'2'}
					totalRow={totalRow}
					tableName={'post'}
					listFunctionTable={functionsTable}
					deleteName={'bài viết'}
					tooltip={'bài viết'}
				/>
			</Grid>
		</MasterLayout>
	);
}
