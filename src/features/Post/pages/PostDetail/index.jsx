import { CKEditor } from '@ckeditor/ckeditor5-react';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import AssignmentIcon from '@material-ui/icons/Assignment';
import BookIcon from '@material-ui/icons/Book';
import CategoryIcon from '@material-ui/icons/Category';
import DescriptionIcon from '@material-ui/icons/Description';
import FaceIcon from '@material-ui/icons/Face';
import FlagIcon from '@material-ui/icons/Flag';
import ImageIcon from '@material-ui/icons/Image';
import permissionApi from 'api/permissionApi';
import postApi from 'api/postApi';
import Editor from 'ckeditor5-custom-build/build/ckeditor';
import MasterLayout from 'common/pages/MasterLayout';
import InputField from 'custom-fields/InputField';
import ImageUpload from 'features/Post/components/ImageUpload';
import { FastField, Field, Form, Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { getCookie } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as Yup from 'yup';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
			width: 250,
		},
	},
};

const LightTooltip = withStyles((theme) => ({
	tooltip: {
		backgroundColor: theme.palette.common.black,
		color: 'white',
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	button_list_item: {
		padding: '0px !important',
	},
	img: {
		width: '100%',
	},
	black: {
		color: 'black',
	},
	green: {
		color: 'green',
	},
	red: {
		color: 'red',
	},
	blue: {
		color: 'blue',
	},
	bgGreen: {
		backgroundColor: '#ADFF2F',
	},
	bgGray: {
		backgroundColor: '#889FA5',
	},
	bgRed: {
		backgroundColor: '#DC143C',
	},
	width_10: {
		minWidth: '30px',
		width: '5px',
	},
	form: {
		width: '80%',
		[theme.breakpoints.down('xs')]: {
			width: '100%',
		},
		marginTop: theme.spacing(1),
	},
	paper: {
		padding: theme.spacing(1),
		textAlign: 'center',
		color: theme.palette.text.secondary,
	},
	richTextEditor: {
		'& .ck-editor__main > .ck-editor__editable': {
			maxHeight: `${window.innerHeight - 222}px`,
		},
	},

	textAreaMedium: {},
	medium: {
		minHeight: '100px',
	},
	fullWidth: {
		width: '100%',
	},
	minWidth: {
		'& .MuiSelect-select': {
			padding: '18.5px 14px',
			border: '1px solid rgba(0, 0, 0, 0.23);',
			borderRadius: '4px',
		},

		minWidth: '100%',
	},
	center: {
		margin: '0 auto',
	},
	displayNone: {
		display: 'none',
	},
}));

function PostPage(props) {
	const role = getCookie('c_role');
	let history = useHistory();
	const notify = useContext(ToastifyContext);
	const [postCategory, setPostCategory] = useState([]);
	const [selectCategory, setSelectCategory] = useState([]);
	const [thumbnail, setThumbnail] = useState([]);
	const [initialValues, setInitialValues] = useState({
		title: '',
		brief_info: '',
		ebook_link: '',
		thumbnail: '',
	});
	const [CKEditorDataDB, setCKEditorDataDB] = useState('');
	const [imageUrl, setImageUrl] = useState('');
	const [loading, setLoading] = useState(false);
	const [cantEdit, setCantEdit] = useState(true);
	const [isDone, setIsDone] = useState(false);
	const state = props.location.state;

	const [isPermiss, setIsPermiss] = useState(false);
	const [functions, setFunctions] = useState([]);
	const [isReview, setIsReview] = useState(false);
	const [status, setStatus] = useState('');
	const [isFeatured, setIsFeatured] = React.useState(false);

	// check permission
	useEffect(() => {
		setLoading((loading) => !loading);
		if (state == undefined) {
			const post = async () => {
				try {
					const params = {
						functionCode: 'function_create_post',
					};
					await permissionApi.checkPermission(params);

					setIsPermiss(true);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			post();
		} else {
			const post = async () => {
				try {
					const params = {
						functionCode: 'function_edit_post',
					};
					await permissionApi.checkPermission(params);
					setLoading((loading) => !loading);
					setIsPermiss(true);
				} catch (error) {
					if (
						error.response.status == 403 &&
						error.response.data.message == 'Not permission'
					) {
						try {
							const params1 = {
								functionCode: 'function_review_post',
							};
							await permissionApi.checkPermission(params1);
							setIsPermiss(true);
							setLoading((loading) => !loading);
						} catch (error) {
							setLoading((loading) => !loading);
							notify('error', error.response.data.message);
							localStorage.removeItem('menu');
							history.push('/');
						}
					} else {
						setLoading((loading) => !loading);
						notify('error', error.response.data.message);
					}
				}
			};
			post();
		}
	}, []);

	// get list function
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const getFunctions = async () => {
				try {
					const respone = await permissionApi.getFunctions();
					const data = [];
					for (let i = 0; i < respone.length; i++) {
						data.push(respone[i].function_id);
					}
					if (
						data.includes('function_post_list') &&
						((state != undefined && data.includes('function_edit_post')) ||
							(state == undefined && data.includes('function_create_post')))
					) {
						setCantEdit(false);
					}
					setFunctions(data);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			getFunctions();
		}
	}, [isPermiss]);

	if (state != undefined) {
		useEffect(() => {
			if (isPermiss) {
				setLoading((loading) => !loading);
				const getPostById = async () => {
					try {
						const response = await postApi.getPostById(state.post_id);
						const data = response.data;
						if (data) {
							const initValue = {
								title: data[0].title,
								brief_info: data[0].brief_info,
								ebook_link: data[0].ebook_link,
								author: data[0].full_name,
							};
							setIsFeatured(data[0].is_featured);
							setStatus(data[0].status);
							setInitialValues(initValue);
							setImageUrl(data[0].thumbnail);
							setCKEditorDataDB(data[0].full_content);
							if (data[0].setting_id != null) {
								const initCategory = data[0].setting_id.split(',');
								setSelectCategory(initCategory);
							}
						}

						setLoading((loading) => !loading);
					} catch (error) {
						setLoading((loading) => !loading);
						notify('error', error.response.data.message);
					}
				};
				getPostById();
			}
		}, [isPermiss]);
	}
	// handleChangeCategory selected category
	const handleChangeCategory = (event) => {
		setSelectCategory(event.target.value);
	};

	const handleSave = (childData) => {
		setThumbnail(childData);
	};

	const handleReivew = (status) => {
		setStatus(status);
		setIsReview(true);
	};

	const handleFlag = () => {
		setIsFeatured((a) => !a);
	};

	const handleDone = () => {
		setStatus('4');
		setIsDone(true);
	};
	// get the loai
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const getAllPostCategory = async () => {
				try {
					const response = await postApi.getPostCategory();
					setPostCategory(response.data);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getAllPostCategory();
		}
	}, [isPermiss]);

	const classes = useStyles();

	const editorConfiguration = {
		toolbar: {
			items: [
				'blockQuote',
				'bold',
				'imageTextAlternative',
				'link',
				'selectAll',
				'undo',
				'redo',
				'heading',
				'resizeImage:original',
				'resizeImage:25',
				'resizeImage:50',
				'resizeImage:75',
				'resizeImage',
				'imageResize',
				'imageStyle:full',
				'imageStyle:side',
				'imageUpload',
				'indent',
				'outdent',
				'italic',
				'numberedList',
				'bulletedList',
				'insertTable',
				'tableColumn',
				'tableRow',
				'mergeTableCells',
			],

			shouldNotGroupWhenFull: true,
		},

		image: {
			styles: ['alignLeft', 'alignCenter', 'alignRight'],
			toolbar: [
				'imageStyle:alignLeft',
				'imageStyle:alignCenter',
				'imageStyle:alignRight',
				'|',
				'|',
				'imageTextAlternative',
			],
		},

		ckfinder: {
			uploadUrl: `${process.env.REACT_APP_API_URL}/uploadImage`,
		},
	};

	const validationSchema = Yup.object().shape({
		title: Yup.string().required('Bạn cần nhập mục này'),
	});

	return (
		<MasterLayout
			referer={[{ value: 'post', display: 'Danh sách bài viết' }]}
			header='Bài Viết'
		>
			{isPermiss && (
				<Grid container spacing={2}>
					{loading && (
						<div
							style={{
								position: 'fixed',
								top: '50%',
								left: '50%',
								width: '50px',
								transform: 'translate(-50%, -50%)',
								zIndex: '1000000000',
							}}
							className='cp-spinner cp-balls'
						></div>
					)}
					<Formik
						initialValues={initialValues}
						validationSchema={validationSchema}
						enableReinitialize
						onSubmit={(values) => {
							const formData = new FormData();
							const user_id = getCookie('c_user');
							formData.append('upload', thumbnail);
							formData.append('title', values.title);
							formData.append('brief_info', values.brief_info);
							formData.append('ebook_link', values.ebook_link);
							formData.append('category_list', selectCategory);
							formData.append('full_content', CKEditorDataDB);
							formData.append('user_id', user_id);
							formData.append('isFeatured', isFeatured);
							const createPost = async () => {
								setLoading((loading) => !loading);
								try {
									if (isDone == true) {
										setIsDone(false);
										const deletePost = async () => {
											try {
												const data = {
													post_id: state.post_id,
													status: '4',
												};

												const response = await postApi.updatePostFlag(data);
												// setTotalRow(response.data[0].count);
												setLoading((loading) => !loading);
												notify('success', response.message);
											} catch (error) {
												setLoading((loading) => !loading);
												notify('error', error.response.data.message);
											}
										};
										deletePost();
									}
									// chiỉnh sửa bài viết
									else if (
										isReview == true &&
										state != undefined &&
										functions.includes('function_review_post')
									) {
										const reivewPost = async () => {
											const data = {
												post_id: state.post_id,
												status: status,
												isFeatured: isFeatured,
											};

											const response = await postApi.reviewPost(data);
											setIsReview(false);
											setLoading((loading) => !loading);
											notify('success', response.message);
										};
										reivewPost();
									} else if (
										isReview == false &&
										state != undefined &&
										functions.includes('function_edit_post')
									) {
										setStatus('0');
										if (thumbnail.length == 0) {
											formData.append('thumbnailUrl', imageUrl);
										}
										formData.append('post_id', state.post_id);
										const response = await postApi.updatePostById(formData);
										setLoading((loading) => !loading);
										notify('success', response.message);
									} else if (functions.includes('function_create_post')) {
										const response = await postApi.createPost(formData);
										setLoading((loading) => !loading);
										notify('success', response.message);
									}

									// setPostCategory(response.data);
								} catch (error) {
									setLoading((loading) => !loading);
									notify('error', error.response.data.message);
								}
							};
							createPost();
						}}
					>
						{(formikProps) => {
							return (
								<Form>
									<Grid container spacing={2}>
										<Grid item xs={4} sm={4}>
											<Paper className={classes.paper}>
												<Grid container spacing={2}>
													<Grid item xs={12} sm={12}>
														<Grid container>
															{state != undefined && (
																<Grid item xs={12} sm={12}>
																	<ListItem>
																		<ListItemIcon className={classes.black}>
																			<AssignmentIcon />
																		</ListItemIcon>
																		<ListItemText
																			className={classes.black}
																			primary='Trạng Thái'
																		/>
																		<ListItemText
																			className={
																				status == '0'
																					? classes.black
																					: status == '1'
																					? classes.green
																					: status == '2' || status == '3'
																					? classes.red
																					: classes.blue
																			}
																			primary={
																				status == '0'
																					? 'Lưu nháp'
																					: status == '1'
																					? 'Đã duyệt'
																					: status == '2'
																					? 'Từ chối'
																					: status == '3'
																					? 'Đã xóa'
																					: 'Đã hoàn thành'
																			}
																		/>
																	</ListItem>
																</Grid>
															)}

															<Grid item xs={12} sm={12}>
																<ListItem>
																	<ListItemIcon className={classes.black}>
																		<BookIcon />
																	</ListItemIcon>
																	<ListItemText
																		className={classes.black}
																		primary='Tiêu đề'
																	/>
																	<LightTooltip
																		placement='left'
																		title='Nổi bật'
																	>
																		<FlagIcon
																			fontSize='large'
																			onClick={handleFlag}
																			style={{
																				color:
																					isFeatured == true
																						? '#0f0cf7'
																						: '#cccaca',
																				cursor: 'pointer',
																			}}
																		></FlagIcon>
																	</LightTooltip>
																</ListItem>
																<Field
																	name='title'
																	component={InputField}
																	autoFocus={false}
																	fullWidth={true}
																	required={true}
																	disabled={cantEdit}
																/>
															</Grid>

															<Grid item xs={12} sm={12}>
																<ListItem>
																	<ListItemIcon className={classes.black}>
																		<CategoryIcon />
																	</ListItemIcon>
																	<ListItemText
																		className={classes.black}
																		primary='Thể loại'
																	/>
																</ListItem>
																<Select
																	disabled={cantEdit}
																	className={classes.minWidth}
																	labelId='demo-mutiple-checkbox-label'
																	id='demo-mutiple-checkbox'
																	multiple
																	required
																	value={selectCategory}
																	onChange={handleChangeCategory}
																	input={<Input />}
																	renderValue={() => {
																		const text = postCategory
																			.filter((category) => {
																				return (
																					selectCategory.indexOf(category.id) >
																					-1
																				);
																			})
																			.map((category) => {
																				return category.text;
																			});
																		return text.join();
																	}}
																	MenuProps={MenuProps}
																>
																	{postCategory.map((category) => (
																		<MenuItem
																			key={category.id}
																			value={category.id}
																		>
																			<Checkbox
																				checked={
																					selectCategory.indexOf(category.id) >
																					-1
																				}
																			/>
																			<ListItemText primary={category.text} />
																		</MenuItem>
																	))}
																</Select>
															</Grid>

															{state != undefined && (
																<Grid item xs={12} sm={12}>
																	<ListItem>
																		<ListItemIcon className={classes.black}>
																			<FaceIcon />
																		</ListItemIcon>
																		<ListItemText
																			className={classes.black}
																			primary='Tác giả'
																		/>
																	</ListItem>
																	<FastField
																		name='author'
																		component={InputField}
																		autoFocus={false}
																		fullWidth={true}
																		required={true}
																		disabled={true}
																	/>
																</Grid>
															)}

															<Grid item xs={12} sm={12}>
																<ListItem>
																	<ListItemIcon className={classes.black}>
																		<DescriptionIcon />
																	</ListItemIcon>
																	<ListItemText
																		className={classes.black}
																		primary='Miêu tả ngắn'
																	/>
																</ListItem>

																<Field
																	multiline
																	rows='3'
																	disabled={cantEdit}
																	name='brief_info'
																	component={InputField}
																	autoFocus={false}
																	fullWidth={true}
																	required={true}
																/>
															</Grid>
															<Grid item xs={12} sm={12}>
																<ListItem>
																	<ListItemIcon className={classes.black}>
																		<ImageIcon />
																	</ListItemIcon>
																	<ListItemText
																		className={classes.black}
																		primary='Ảnh bài viết'
																	/>
																</ListItem>
																<FastField
																	disabled={cantEdit}
																	name='thumbnail'
																	component={ImageUpload}
																	autoFocus={false}
																	handleSave={handleSave}
																	imageUrl={imageUrl}
																/>
															</Grid>
														</Grid>
													</Grid>
												</Grid>
											</Paper>
										</Grid>
										<Grid item xs={8} sm={8}>
											<Paper
												className={`${classes.paper} ${classes.richTextEditor}`}
											>
												{functions.length > 0 && (
													<CKEditor
														editor={Editor}
														config={editorConfiguration}
														data={CKEditorDataDB}
														onReady={(editor) => {
															if (
																functions.includes('function_post_list') &&
																((state != undefined &&
																	functions.includes('function_edit_post')) ||
																	(state == undefined &&
																		functions.includes('function_create_post')))
															) {
																editor.isReadOnly = false;
															} else {
																editor.isReadOnly = true;
															}
														}}
														onChange={(event, editor) => {
															const data = editor.getData();
															setCKEditorDataDB(data);
														}}
														onBlur={(event, editor) => {}}
														onFocus={(event, editor) => {}}
													/>
												)}
											</Paper>
											<Grid
												container
												style={{ marginTop: '2rem' }}
												justify='center'
											>
												{cantEdit == false && (
													<Button
														style={{ marginRight: '1rem' }}
														type='submit'
														variant='contained'
														className={classes.bgGray}
													>
														<ListItem className={classes.button_list_item}>
															<ListItemIcon className={classes.width_10}>
																<svg
																	xmlns='http://www.w3.org/2000/svg'
																	class='icon icon-tabler icon-tabler-edit'
																	width='44'
																	height='44'
																	viewBox='0 0 24 24'
																	stroke-width='1.5'
																	stroke='#2c3e50'
																	fill='none'
																	stroke-linecap='round'
																	stroke-linejoin='round'
																>
																	<path
																		stroke='none'
																		d='M0 0h24v24H0z'
																		fill='none'
																	/>
																	<path d='M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3' />
																	<path d='M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3' />
																	<line x1='16' y1='5' x2='19' y2='8' />
																</svg>
															</ListItemIcon>
															<ListItemText
																className={classes.black}
																primary={
																	state != undefined ? 'Chỉnh sửa' : 'Tạo mới'
																}
															/>
														</ListItem>
													</Button>
												)}

												{cantEdit == false && state != undefined && (
													<Button
														onClick={handleDone}
														type='submit'
														variant='contained'
														style={{
															backgroundColor: '#6DB1BF',
															marginRight: '1rem',
														}}
													>
														<ListItem className={classes.button_list_item}>
															<ListItemIcon className={classes.width_10}>
																<svg
																	xmlns='http://www.w3.org/2000/svg'
																	class='icon icon-tabler icon-tabler-arrow-bar-up'
																	width='44'
																	height='44'
																	viewBox='0 0 24 24'
																	stroke-width='1.5'
																	stroke='#000000'
																	fill='none'
																	stroke-linecap='round'
																	stroke-linejoin='round'
																>
																	<path
																		stroke='none'
																		d='M0 0h24v24H0z'
																		fill='none'
																	/>
																	<line x1='12' y1='4' x2='12' y2='14' />
																	<line x1='12' y1='4' x2='16' y2='8' />
																	<line x1='12' y1='4' x2='8' y2='8' />
																	<line x1='4' y1='20' x2='20' y2='20' />
																</svg>
															</ListItemIcon>
															<ListItemText
																className={classes.black}
																primary={'Hoàn thành'}
															/>
														</ListItem>
													</Button>
												)}

												{state != undefined &&
													functions.includes('function_review_post') && (
														<div>
															<Button
																style={{ marginRight: '1rem' }}
																type='submit'
																onClick={() => handleReivew('1')}
																variant='contained'
																className={classes.bgGreen}
															>
																<ListItem className={classes.button_list_item}>
																	<ListItemIcon className={classes.width_10}>
																		<svg
																			xmlns='http://www.w3.org/2000/svg'
																			class='icon icon-tabler icon-tabler-circle-check'
																			width='44'
																			height='44'
																			viewBox='0 0 24 24'
																			stroke-width='1.5'
																			stroke='#2c3e50'
																			fill='none'
																			stroke-linecap='round'
																			stroke-linejoin='round'
																		>
																			<path
																				stroke='none'
																				d='M0 0h24v24H0z'
																				fill='none'
																			/>
																			<circle cx='12' cy='12' r='9' />
																			<path d='M9 12l2 2l4 -4' />
																		</svg>
																	</ListItemIcon>
																	<ListItemText
																		className={classes.black}
																		primary={'Chấp nhận'}
																	/>
																</ListItem>
															</Button>
															<Button
																type='submit'
																onClick={() => handleReivew('2')}
																variant='contained'
																className={classes.bgRed}
															>
																<ListItem className={classes.button_list_item}>
																	<ListItemIcon className={classes.width_10}>
																		<svg
																			xmlns='http://www.w3.org/2000/svg'
																			class='icon icon-tabler icon-tabler-ban'
																			width='44'
																			height='44'
																			viewBox='0 0 24 24'
																			stroke-width='1.5'
																			stroke='#2c3e50'
																			fill='none'
																			stroke-linecap='round'
																			stroke-linejoin='round'
																		>
																			<path
																				stroke='none'
																				d='M0 0h24v24H0z'
																				fill='none'
																			/>
																			<circle cx='12' cy='12' r='9' />
																			<line
																				x1='5.7'
																				y1='5.7'
																				x2='18.3'
																				y2='18.3'
																			/>
																		</svg>
																	</ListItemIcon>
																	<ListItemText
																		className={classes.black}
																		primary={'Từ chối'}
																	/>
																</ListItem>
															</Button>
														</div>
													)}
											</Grid>
										</Grid>
									</Grid>
								</Form>
							);
						}}
					</Formik>
				</Grid>
			)}
		</MasterLayout>
	);
}

export default PostPage;
