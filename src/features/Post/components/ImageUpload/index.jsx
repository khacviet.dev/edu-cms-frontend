import { FormControl, FormHelperText, TextField } from '@material-ui/core';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';
import { ErrorMessage } from 'formik';
import React, { useContext, useState } from 'react';
import { ToastifyContext } from 'utils/ToastifyConfig';

const useStyles = makeStyles((theme) => ({
	input: {
		opacity: 0,
		width: '1px',
	},
}));

function ImageUpload(props) {
	const {
		field,
		form,
		type,
		label,
		required,
		autoFocus,
		imageUrl,
		disabled,
		index,
	} = props;
	const [files, setFiles] = useState([]);
	const [image, setImage] = useState(false);
	const [isFirst, setIsFirst] = useState(false);
	const classes = useStyles();
	if (imageUrl.length != 0 && image == false && isFirst == false) {
		setImage(true);
		setIsFirst(true);
	}
	// imageUrl

	const getFileMetadata = (file) => {
		return {
			lastModified: file.lastModified,
			name: file.name,
			size: file.size,
			type: file.type,
			webkitRelativePath: file.webkitRelativePath,
		};
	};

	const handleSave = (i, file) => {
		if (i != undefined) {
			props.handleSave(i, file);
		} else {
			props.handleSave(file);
		}
	};
	const notify = useContext(ToastifyContext);
	const handleUploadClick = (event) => {
		let newstate = [];
		let file = event.target.files[0];
		if (
			file.type == 'image/jpeg' ||
			file.type == 'image/jpg' ||
			file.type == 'image/png'
		) {
			let metadata = getFileMetadata(file);
			let url = URL.createObjectURL(file);
			newstate = [...newstate, { url, metadata }];
			setFiles(newstate);
			if (index != undefined) {
				handleSave(index, file);
			} else {
				handleSave(file);
			}
		} else {
			setImage(false);
			setFiles([]);
			handleSave([]);
			event.target.value = '';
			notify('error', 'Vui lòng chọn file có định dạng jpg hoặc png hoặc jpeg');
		}
	};

	const imageResetHandler = (event) => {
		setImage(false);
		setFiles([]);
		handleSave([]);
	};

	const { name } = field;
	const { errors, touched } = form;
	const showError = errors[name] && touched[name];
	return files.length === 0 && image == false ? (
		<FormControl fullWidth error={showError}>
			<CardContent>
				<Grid container justify='center' alignItems='center'>
					<TextField
						id={name}
						{...field}
						label={label}
						type={type}
						variant='outlined'
						margin='normal'
						required={true}
						autoFocus={autoFocus}
						error={showError}
						accept='image/*'
						className={classes.input}
						multiple
						type='file'
						onChange={handleUploadClick}
					/>
					<ErrorMessage name={name} component={FormHelperText} />
					<label htmlFor={name}>
						<Fab component='span'>
							<AddPhotoAlternateIcon />
						</Fab>
					</label>
				</Grid>
			</CardContent>
		</FormControl>
	) : (
		<CardActionArea onClick={imageResetHandler} disabled={disabled}>
			<img
				width='100%'
				className={classes.media}
				src={image == false ? files[0].url : imageUrl}
			/>
		</CardActionArea>
	);
}

export default ImageUpload;
