import { Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import React from 'react';
import { useHistory } from 'react-router';

function CreatePost(props) {
	let history = useHistory();

	const handleButtonClick = () => {
		if (props.title) {
			history.push({
				pathname: props.path,
				state: {
					title: props.title,
					course_id: props.course_id,
				},
			});
		} else {
			history.push({
				pathname: props.path,
			});
		}
	};

	return (
		<Button
			startIcon={<AddIcon />}
			color='primary'
			variant='outlined'
			onClick={handleButtonClick}
		>
			{props.text}
		</Button>
	);
}

export default CreatePost;
