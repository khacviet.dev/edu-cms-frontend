import { Avatar, Divider, Grid, Paper, Typography } from '@material-ui/core';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import ClassIcon from '@material-ui/icons/Class';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import dashboardApi from 'api/dashboardApi';
import MyBarChart from 'common/components/MyBarChart';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';

function MarketerDashboard() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();

	const [loading, setLoading] = useState(false);
	const [postNotApprove, setPostNotApprove] = useState(-1);
	const [totalPost, setTotalPost] = useState(-1);
	const [totalCourse, setTotalCourse] = useState(-1);
	const [userNew, setUserNew] = useState(-1);
	const [postChart, setPostChart] = useState({});
	const handleRouterChange = (url) => {
		history.push(`/${url}`);
	};

	useEffect(() => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await dashboardApi.getMarketerDashboard();
				setLoading(false);

				setPostNotApprove(response.postNotApprove);
				setTotalPost(response.totalPost);
				setTotalCourse(response.totalCourse);
				setUserNew(response.userNew);
				setPostChart(response.postChart);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		get();
	}, []);

	return (
		<Grid container>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Grid container spacing={3}>
				{postNotApprove !== -1 && (
					<Grid
						item
						xs={12}
						md={3}
						lg={3}
						onClick={() => handleRouterChange('post')}
						style={{ cursor: 'pointer' }}
					>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Bài viết chờ duyệt
									</Typography>
									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{postNotApprove}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'rgb(229, 57, 53)',
											width: '56px',
											height: '56px',
										}}
									>
										<ClassIcon />
									</Avatar>
								</Grid>
							</Grid>
						</Paper>
					</Grid>
				)}
				{totalPost !== -1 && (
					<Grid item xs={12} md={3} lg={3}>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Tổng số bài viết
									</Typography>
									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{totalPost}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'rgb(67, 160, 71)',
											width: '56px',
											height: '56px',
										}}
									>
										<EmojiPeopleIcon />
									</Avatar>
								</Grid>
							</Grid>
						</Paper>
					</Grid>
				)}
				{totalCourse !== -1 && (
					<Grid item xs={12} md={3} lg={3}>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Tổng số khóa học
									</Typography>
									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{totalCourse}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'blue',
											width: '56px',
											height: '56px',
										}}
									>
										<AttachMoneyIcon />
									</Avatar>
								</Grid>
							</Grid>
						</Paper>
					</Grid>
				)}
				{userNew !== -1 && (
					<Grid item xs={12} md={3} lg={3}>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										User mới tháng này
									</Typography>
									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{userNew}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'orange',
											width: '56px',
											height: '56px',
										}}
									>
										<CreditCardIcon />
									</Avatar>
								</Grid>
							</Grid>
						</Paper>
					</Grid>
				)}
			</Grid>
			<Grid container spacing={3} style={{ marginTop: '1.5rem' }}>
				<Grid item xs={12} md={12} lg={12}>
					<Paper
						style={{
							height: '100%',
						}}
					>
						<Grid container>
							<Grid item xs={12} lg={12} md={12} style={{ padding: '1rem' }}>
								<Typography variant='body1' style={{ fontWeight: 'bold' }}>
									Số bài viết theo tháng
								</Typography>
							</Grid>
							<Grid item xs={12} lg={12} md={12}>
								<Divider />
							</Grid>
							<Grid item xs={12} lg={12} md={12} style={{ padding: '1rem' }}>
								{postChart && <MyBarChart data={postChart} />}
							</Grid>
						</Grid>
					</Paper>
				</Grid>
			</Grid>
		</Grid>
	);
}

export default MarketerDashboard;
