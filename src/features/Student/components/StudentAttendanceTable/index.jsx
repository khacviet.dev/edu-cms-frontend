import { Grid, Typography } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import attendanceApi from 'api/attendanceApi';
import React, { useContext, useEffect, useState } from 'react';
import { ToastifyContext } from 'utils/ToastifyConfig';

StudentAttendanceTable.propTypes = {};

const parseAttendanceStatus = (status, isActive) => {
	if (status === '0') {
		return <Typography variant='body2'>Chưa học</Typography>;
	}

	if (isActive === 'true' || isActive === '1') {
		return (
			<Typography variant='body2' style={{ color: 'green' }}>
				Có mặt
			</Typography>
		);
	}

	return (
		<Typography variant='body2' style={{ color: 'red' }}>
			Vắng mặt
		</Typography>
	);
};

function StudentAttendanceTable({ classId }) {
	const notify = useContext(ToastifyContext);
	const [attendances, setAttendances] = useState([]);
	const [absent, setAbsent] = useState({
		absentPercent: -1,
		totalAbsent: -1,
	});

	useEffect(() => {
		const get = async () => {
			try {
				const response = await attendanceApi.getStudentAttendanceByClassId(
					classId
				);
				setAttendances(response.attendances);
				setAbsent({
					absentPercent: response.absentPercent,
					totalAbsent: response.countAbsent,
				});
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		get();
	}, [classId]);

	return (
		<Grid container>
			<TableContainer>
				<Table>
					<TableHead>
						<TableRow>
							<TableCell align='left'>STT</TableCell>
							<TableCell align='left'>Ngày</TableCell>
							<TableCell align='left'>Slot</TableCell>
							<TableCell align='left'>Phòng</TableCell>
							<TableCell align='left'>Giáo viên</TableCell>
							<TableCell align='center'>Trạng thái điểm danh</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{attendances &&
							attendances.map((at, index) => (
								<TableRow key={index}>
									<TableCell component='th' scope='row'>
										{index + 1}
									</TableCell>
									<TableCell align='left'>
										{new Date(at.date).toLocaleDateString()}
									</TableCell>
									<TableCell align='left'>{at.slot_name}</TableCell>
									<TableCell align='left'>{at.room_name}</TableCell>
									<TableCell align='left'>{at.teacher_name}</TableCell>
									<TableCell align='center'>
										{parseAttendanceStatus(at.status, at.is_active)}
									</TableCell>
								</TableRow>
							))}
					</TableBody>
				</Table>
			</TableContainer>
			<Grid container style={{ marginTop: '2rem' }}>
				<Typography style={{ fontWeight: 'bold', fontSize: '1.1rem' }}>
					Vắng mặt: {absent.absentPercent}% vắng mặt cho đến nay (
					{absent.totalAbsent} trên tổng số {attendances.length})
				</Typography>
			</Grid>
		</Grid>
	);
}

export default StudentAttendanceTable;
