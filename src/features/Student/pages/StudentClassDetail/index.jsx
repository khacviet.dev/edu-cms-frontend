import {
	Avatar,
	Box,
	FormControl,
	Grid,
	makeStyles,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	TextField,
	Typography,
} from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import GroupIcon from '@material-ui/icons/Group';
import InfoIcon from '@material-ui/icons/Info';
import classApi from 'api/classApi';
import permissionApi from 'api/permissionApi';
import studentApi from 'api/studentApi';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';

const parseClassStatus = (status) => {
	switch (status) {
		case '0':
			return 'Chưa hoạt động';
		case '1':
			return 'Đang hoạt động';
		case '2':
			return 'Kết thúc';
		case '3':
			return 'Đã xóa';
	}
};

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
	},
	postPanel: {
		marginTop: '2rem',
		marginBottom: '0.5rem',
	},
	fab: {
		position: 'fixed',
		bottom: theme.spacing(8),
		right: theme.spacing(8),
	},
}));

function StudentClassDetail() {
	const notify = useContext(ToastifyContext);
	const { classId } = useParams();
	const classes = useStyles();
	let history = useHistory();

	const [loading, setLoading] = useState(false);

	const [numberStudent, setNumberStudent] = useState(0);
	const [classroom, setClassroom] = useState({});
	const [students, setStudents] = useState([]);
	const [teacher, setTeacher] = useState('');
	const [tutor, setTutor] = useState('');

	const [isPermiss, setIsPermiss] = useState(false);

	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_myclass',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await classApi.getClassDetailByStudentId(classId);

					setLoading(false);
					setClassroom(response.classInfo);
					setNumberStudent(response.numberStudent);
					setTeacher(response.teacher);
					setTutor(response.tutor);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await studentApi.getStudentsByClassId(classId);

					setLoading(false);
					setStudents(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	return (
		<MasterLayout header={classroom.class_name}>
			<Box>
				{loading && (
					<div
						style={{
							position: 'fixed',
							top: '50%',
							left: '50%',
							width: '50px',
							transform: 'translate(-50%, -50%)',
							zIndex: '1000000000',
						}}
						className='cp-spinner cp-balls'
					></div>
				)}

				<Grid container style={{ padding: '1rem 2rem 0.5rem 2rem' }}>
					<Accordion className={classes.root} defaultExpanded>
						<AccordionSummary expandIcon={<ExpandMoreIcon />}>
							<Grid container alignItems='center'>
								<InfoIcon />
								<Typography style={{ fontWeight: 'bold', fontSize: '1.25rem' }}>
									&nbsp;Thông tin lớp học
								</Typography>
							</Grid>
						</AccordionSummary>
						<AccordionDetails>
							{classroom && (
								<Grid container spacing={5}>
									<Grid item md={6}>
										<Grid container alignItems='center'>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Số học sinh:
												</Grid>
												<Grid item md={2} lg={2}>
													<FormControl fullWidth>
														<TextField
															disabled
															value={numberStudent.toString()}
															variant='outlined'
															margin='normal'
															size='small'
														/>
													</FormControl>
												</Grid>
												<Grid
													item
													md={1}
													lg={1}
													container
													justify='center'
													alignItems='center'
												>
													<Typography
														variant='body1'
														style={{ fontSize: '2rem', color: '#999' }}
													>
														/
													</Typography>
												</Grid>
												<Grid item md={2} lg={2}>
													<FormControl fullWidth>
														<TextField
															disabled
															value={classroom.max_student_in_class}
															variant='outlined'
															margin='normal'
															size='small'
															type='number'
														/>
													</FormControl>
												</Grid>
											</Grid>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Tên lớp:
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl fullWidth>
														<TextField
															disabled
															value={classroom.class_name}
															variant='outlined'
															margin='normal'
															size='small'
														/>
													</FormControl>
												</Grid>
											</Grid>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Môn học:
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl fullWidth>
														<TextField
															disabled
															value={classroom.subject_name}
															variant='outlined'
															margin='normal'
															size='small'
														/>
													</FormControl>
												</Grid>
											</Grid>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Trợ giảng:
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl fullWidth>
														<TextField
															disabled
															value={tutor === '' ? 'Không có' : tutor}
															variant='outlined'
															margin='normal'
															size='small'
														/>
													</FormControl>
												</Grid>
											</Grid>
										</Grid>
									</Grid>
									<Grid item md={6}>
										<Grid container>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Học phí:
												</Grid>
												<Grid item md={9} lg={9}>
													{classroom.price && (
														<FormControl fullWidth>
															<TextField
																disabled
																value={classroom.price.toLocaleString('vi-VN', {
																	style: 'currency',
																	currency: 'VND',
																})}
																variant='outlined'
																margin='normal'
																size='small'
															/>
														</FormControl>
													)}
												</Grid>
											</Grid>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Giáo viên:
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl fullWidth>
														<TextField
															disabled
															value={teacher}
															variant='outlined'
															margin='normal'
															size='small'
														/>
													</FormControl>
												</Grid>
											</Grid>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Khối:
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl fullWidth>
														<TextField
															disabled
															value={classroom.grade_level}
															variant='outlined'
															margin='normal'
															size='small'
														/>
													</FormControl>
												</Grid>
											</Grid>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Trạng thái:
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl fullWidth>
														<TextField
															disabled
															value={parseClassStatus(classroom.class_status)}
															variant='outlined'
															margin='normal'
															size='small'
														/>
													</FormControl>
												</Grid>
											</Grid>
										</Grid>
									</Grid>
								</Grid>
							)}
						</AccordionDetails>
					</Accordion>
					<Accordion className={classes.root} defaultExpanded>
						<AccordionSummary expandIcon={<ExpandMoreIcon />}>
							<Grid container alignItems='center'>
								<GroupIcon />
								<Typography style={{ fontWeight: 'bold', fontSize: '1.25rem' }}>
									&nbsp;Danh sách học sinh
								</Typography>
							</Grid>
						</AccordionSummary>
						<AccordionDetails>
							<Grid container>
								<Grid container>
									<TableContainer component={Paper}>
										<Table>
											<TableHead>
												<TableRow>
													<TableCell align='center'>STT</TableCell>
													<TableCell align='center'>Ảnh đại diện</TableCell>
													<TableCell align='left'>Mã học sinh</TableCell>
													<TableCell align='left'>Họ và tên</TableCell>
												</TableRow>
											</TableHead>
											<TableBody>
												{students.length > 0 ? (
													students.map((student, index) => (
														<TableRow>
															<TableCell align='center'>{index}</TableCell>
															<TableCell align='center'>
																<Grid container justify='center'>
																	<Avatar src={student.avt_link} alt='a' />
																</Grid>
															</TableCell>
															<TableCell align='left'>
																{student.user_id}
															</TableCell>
															<TableCell align='left'>
																{student.full_name}
															</TableCell>
														</TableRow>
													))
												) : (
													<TableRow>
														<TableCell align='center' colSpan={5}>
															Không tìm thấy dữ liệu
														</TableCell>
													</TableRow>
												)}
											</TableBody>
										</Table>
									</TableContainer>
								</Grid>
							</Grid>
						</AccordionDetails>
					</Accordion>
				</Grid>
			</Box>
		</MasterLayout>
	);
}

export default StudentClassDetail;
