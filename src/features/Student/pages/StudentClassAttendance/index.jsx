import { Grid, Paper } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import classApi from 'api/classApi';
import permissionApi from 'api/permissionApi';
import MasterLayout from 'common/pages/MasterLayout';
import StudentAttendanceTable from 'features/Student/components/StudentAttendanceTable';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';

StudentClassAttendance.propTypes = {};

function TabPanel(props) {
	const { children, value, index, ...other } = props;

	return (
		<div role='tabpanel' hidden={value !== index} {...other}>
			{value === index && (
				<Box p={3}>
					<Typography>{children}</Typography>
				</Box>
			)}
		</div>
	);
}

export default function StudentClassAttendance() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();
	const [value, setValue] = useState(0);

	const [classrooms, setClassrooms] = useState([]);
	const [loading, setLoading] = useState(false);
	const [isPermiss, setIsPermiss] = useState(false);

	useEffect(() => {
		const post = async () => {
			try {
				setLoading(true);
				const params = {
					functionCode: 'function_attendance_report',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
				setLoading(false);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await classApi.getClassByStudentId();
					setClassrooms(response);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	const handleChange = (event, newValue) => {
		setValue(newValue);
	};

	return (
		<MasterLayout header='Báo cáo điểm danh'>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Báo cáo điểm danh | Xoài CMS</title>
			</Helmet>
			{classrooms ? (
				<Grid container spacing={3}>
					<Grid item md={3}>
						<Paper>
							<Tabs
								orientation='vertical'
								variant='scrollable'
								value={value}
								onChange={handleChange}
							>
								{classrooms &&
									classrooms.map((cl, index) => (
										<Tab key={index} label={cl.class_name} />
									))}
							</Tabs>
						</Paper>
					</Grid>
					<Grid item md={9}>
						<Paper>
							{classrooms &&
								classrooms.map((cl, index) => (
									<TabPanel value={value} index={index}>
										<StudentAttendanceTable classId={cl.class_id} />
									</TabPanel>
								))}
						</Paper>
					</Grid>
				</Grid>
			) : (
				<Grid container justify='center'>
					<Typography>Chưa có dữ liệu điểm danh</Typography>
				</Grid>
			)}
		</MasterLayout>
	);
}
