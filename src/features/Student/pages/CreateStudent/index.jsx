import {
	Avatar,
	Box,
	Button,
	Checkbox,
	Fab,
	FormControl,
	Grid,
	ListItemText,
	makeStyles,
	MenuItem,
	Select,
	Tooltip,
	Typography,
} from '@material-ui/core';
import RestoreIcon from '@material-ui/icons/Restore';
import classApi from 'api/classApi';
import permissionApi from 'api/permissionApi';
import studentApi from 'api/studentApi';
import MasterLayout from 'common/pages/MasterLayout';
import InputField from 'custom-fields/InputField';
import { FastField, Form, Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as yup from 'yup';

const useStyles = makeStyles((theme) => ({
	postPanel: {
		marginTop: '5rem',
		marginBottom: '0.5rem',
	},
	createPostTitle: {
		fontSize: '1.25rem',
		fontWeight: 'bold',
	},
	fab: {
		position: 'absolute',
		bottom: theme.spacing(8),
		right: theme.spacing(8),
	},
}));

const referer = [
	{
		value: 'student',
		display: 'Học sinh',
	},
];

export default function CreateStudent(props) {
	const notify = useContext(ToastifyContext);
	const classes = useStyles();
	let history = useHistory();
	const [loading, setLoading] = useState(false);
	const [classroom, setClassroom] = useState([]);

	const [isPermiss, setIsPermiss] = useState(false);
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_create_student',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await classApi.getAllClass();
					setLoading(false);
					setClassroom(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};
			get();
		}
	}, [isPermiss]);

	const initialValues = {
		full_name:
			localStorage.getItem('registerInfo') !== null
				? JSON.parse(localStorage.getItem('registerInfo')).full_name
				: '',
		display_name: '',
		email:
			localStorage.getItem('registerInfo') !== null
				? JSON.parse(localStorage.getItem('registerInfo')).email
				: '',
		phone:
			localStorage.getItem('registerInfo') !== null
				? JSON.parse(localStorage.getItem('registerInfo')).phone
				: '',
		address: '',
		note: '',
		gender: '',
		classroom:
			localStorage.getItem('registerInfo') !== null
				? JSON.parse(localStorage.getItem('registerInfo')).list_regis_class.map(
						(x) => x.class_id
				  )
				: [],
		register:
			localStorage.getItem('registerInfo') !== null
				? JSON.parse(localStorage.getItem('registerInfo')).id
				: '',
	};

	const validationSchema = yup.object().shape({
		full_name: yup.string().required('Trường không được để trống'),
		display_name: yup.string().required('Trường không được để trống'),
		email: yup
			.string()
			.email('Email không hợp lệ')
			.required('Trường không được để trống'),
		phone: yup
			.string()
			.matches(/^[0-9]+$/, 'Số điện thoại chỉ bao gồm chữ số')
			.min(10, 'Số điện thoại gồm ít nhất 10 số')
			.max(11, 'Số điện thoại gồm nhiều nhất 11 số')
			.required('Trường không được để trống'),
		address: yup.string(),
		note: yup.string(),
	});

	return (
		<Formik
			initialValues={initialValues}
			enableReinitialize
			validationSchema={validationSchema}
			onSubmit={(values) => {
				const post = async () => {
					try {
						setLoading(true);
						const response = await studentApi.createStudent(values);
						setLoading(false);
						notify('info', response.message, false);
						localStorage.removeItem('registerInfo');
					} catch (error) {
						setLoading(false);
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/');
						}
					}
				};
				post();
			}}
		>
			{(formikProps) => {
				return (
					<MasterLayout referer={referer} header='Thêm mới'>
						{isPermiss && (
							<Box>
								<Tooltip title='Làm trống'>
									<Fab
										className={classes.fab}
										size='large'
										onClick={() => {
											formikProps.resetForm();
										}}
										color='secondary'
									>
										<RestoreIcon fontSize='large' />
									</Fab>
								</Tooltip>
								{loading && (
									<div
										style={{
											position: 'fixed',
											top: '50%',
											left: '50%',
											width: '50px',
											transform: 'translate(-50%, -50%)',
											zIndex: '1000000000',
										}}
										className='cp-spinner cp-balls'
									></div>
								)}
								<Helmet>
									<title>Thêm mới | Học sinh | Xoài CMS</title>
								</Helmet>
								<Form>
									<Grid container spacing={2}>
										<Grid container spacing={2}>
											<Grid item md={3} style={{ marginTop: '1rem' }}>
												<Grid container justify='center'>
													<Avatar
														style={{
															width: '60%',
															height: 'auto',
															boxShadow:
																'0 16px 38px -12px rgb(0 0 0 / 16%), 0 4px 25px 0 rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%)',
														}}
														src='https://i.pravatar.cc'
													/>
												</Grid>
												<Grid
													container
													justify='center'
													style={{ marginTop: '0.75rem' }}
												>
													<Typography style={{ fontSize: '1rem' }}>
														{formikProps.values.full_name}
													</Typography>
												</Grid>
											</Grid>
											<Grid item md={9}>
												<Grid container>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Họ và tên:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																<FastField
																	name='full_name'
																	component={InputField}
																	fullWidth
																	size='small'
																/>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>

													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Số điện thoại:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9}>
																<FastField
																	name='phone'
																	component={InputField}
																	fullWidth
																	size='small'
																/>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Tên hiển thị:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																<FastField
																	name='display_name'
																	component={InputField}
																	fullWidth
																	size='small'
																/>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Email:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9}>
																<FastField
																	name='email'
																	component={InputField}
																	fullWidth
																	size='small'
																/>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Giới tính:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl
																	style={{
																		minWidth: '120px',
																		maxWidth: '100%',
																	}}
																	margin='normal'
																	size='small'
																>
																	<Select
																		variant='outlined'
																		name='gender'
																		id='gender'
																		value={formikProps.values.gender}
																		onChange={formikProps.handleChange}
																	>
																		<MenuItem value={'male'}>Nam</MenuItem>
																		<MenuItem value={'female'}>Nữ</MenuItem>
																	</Select>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Địa chỉ:
															</Grid>
															<Grid item md={9}>
																<FastField
																	name='address'
																	component={InputField}
																	fullWidth
																	size='small'
																/>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Lớp học:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl
																	style={{
																		minWidth: '120px',
																		maxWidth: '100%',
																	}}
																	margin='normal'
																	size='small'
																>
																	<Select
																		required
																		variant='outlined'
																		name='classroom'
																		id='classroom'
																		multiple
																		value={formikProps.values.classroom}
																		onChange={formikProps.handleChange}
																		renderValue={() => {
																			if (classroom.length > 0) {
																				if (
																					formikProps.values.classroom.length <
																					1
																				) {
																					return 'Chọn lớp';
																				}
																				let list = [];
																				for (
																					let i = 0;
																					i <
																					formikProps.values.classroom.length;
																					i++
																				) {
																					const item = classroom.find(
																						(cl) =>
																							cl.class_id ===
																							formikProps.values.classroom[i]
																					);

																					list.push(item.class_name);
																				}

																				return list.join(',');
																			}
																		}}
																	>
																		{classroom.map((cl, index) => (
																			<MenuItem key={index} value={cl.class_id}>
																				<Checkbox
																					checked={
																						formikProps.values.classroom.indexOf(
																							cl.class_id
																						) > -1
																					}
																				/>
																				<ListItemText primary={cl.class_name} />
																			</MenuItem>
																		))}
																	</Select>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Ghi chú:
															</Grid>
															<Grid item md={9} lg={9}>
																<FastField
																	name='note'
																	component={InputField}
																	fullWidth
																	size='small'
																/>
															</Grid>
														</Grid>
													</Grid>
												</Grid>
											</Grid>
										</Grid>
										<Grid
											container
											className={classes.postPanel}
											justify='center'
										>
											<Button
												style={{
													padding: '0.5rem 3rem 0.5rem 3rem',
												}}
												type='submit'
												variant='contained'
												color='primary'
											>
												Tạo mới
											</Button>
										</Grid>
									</Grid>
								</Form>
							</Box>
						)}
					</MasterLayout>
				);
			}}
		</Formik>
	);
}
