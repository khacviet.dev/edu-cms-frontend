import {
	Button,
	Checkbox,
	Grid,
	Input,
	ListItemText,
	MenuItem,
	Select,
	Tooltip,
	Typography,
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import GetAppIcon from '@material-ui/icons/GetApp';
import LaunchIcon from '@material-ui/icons/Launch';
import classApi from 'api/classApi';
import excelApi from 'api/excelApi';
import permissionApi from 'api/permissionApi';
import ReportDialog from 'common/components/ReportDialog';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { exportToCSV } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

StudentClassList.propTypes = {};

const useStyles = makeStyles({
	formControl: {
		minWidth: 120,
	},
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
		},
	},
};

const parseClassStatus = (status) => {
	switch (status) {
		case '0':
			return 'Chưa hoạt động';
		case '1':
			return 'Đang hoạt động';
		case '2':
			return 'Kết thúc';
		case '3':
			return 'Đã xóa';
	}
};

function StudentClassList() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();
	const classes = useStyles();

	const [classroom, setClassroom] = useState([]);

	const [loading, setLoading] = useState(false);

	const [keys, setKeys] = useState([
		{
			header: 'Tên lớp',
			align: 'left',
			field: 'class_name',
			display: true,
		},
		{
			header: 'Môn học',
			align: 'left',
			field: 'subject_name',
			display: true,
		},
		{
			header: 'Giáo viên',
			align: 'left',
			field: 'teacher_name',
			display: true,
		},
		{
			header: 'Phòng học',
			align: 'left',
			field: 'room_name',
			display: true,
		},
		{
			header: 'Học phí',
			align: 'right',
			field: 'price',
			display: true,
		},
		{
			header: 'Trạng thái',
			align: 'left',
			field: 'class_status',
			display: true,
		},
	]);

	const exportClassesByStudent = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getClassesByStudentExcel();
				setLoading(false);
				const csvData = {
					data: response,
				};
				exportToCSV(csvData, 'Danh_sach_lop_hoc');
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	const [columnSelected, setColumnSelected] = React.useState([]);

	const changeDisplayColumn = (key) => {
		key.display = !key.display;
		setKeys(keys);
	};

	const handleChangeClassDetail = (classId) => {
		history.push({
			pathname: `/myclass/${classId}`,
		});
	};

	const handleColumnChange = (event) => {
		setColumnSelected(event.target.value);
	};

	const [isPermiss, setIsPermiss] = useState(false);
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_myclass',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				localStorage.removeItem('menu');
				history.push('/');
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await classApi.getClassByStudentId();

					setLoading(false);
					setClassroom(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const value = [];
			for (let i = 0, l = keys.length; i < l; i += 1) {
				value.push(keys[i].header);
			}
			setColumnSelected(value);
		}
	}, [isPermiss]);

	return (
		<div>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Grid container>
				<Grid container alignItems='center'>
					<Grid item md={8}>
						{classroom.length > 0 && (
							<Typography style={{ fontWeight: 'bold' }}>
								Tổng số lớp đang học ({classroom.length})
							</Typography>
						)}
					</Grid>
					<Grid item md={2} container justify='flex-end'>
						<Button
							startIcon={<GetAppIcon />}
							variant='text'
							onClick={exportClassesByStudent}
						>
							Xuất file
						</Button>
					</Grid>
					<Grid item md={2} container justify='flex-end'>
						<Select
							multiple
							value={columnSelected}
							onChange={handleColumnChange}
							input={<Input />}
							renderValue={() => 'Chọn cột hiển thị'}
							MenuProps={MenuProps}
						>
							{keys.map((key, index) => (
								<MenuItem
									onClick={() => changeDisplayColumn(key)}
									key={index}
									value={key.header}
								>
									<Checkbox checked={columnSelected.indexOf(key.header) > -1} />
									<ListItemText primary={key.header} />
								</MenuItem>
							))}
						</Select>
					</Grid>
				</Grid>

				<TableContainer component={Paper} style={{ marginTop: '1rem' }}>
					<Table className={classes.table} stickyHeader>
						<TableHead>
							<TableRow>
								<TableCell align='center' style={{ width: '5%' }}>
									STT
								</TableCell>
								{keys.map((key, index) => (
									<TableCell
										align={key.align}
										style={{ display: key.display ? '' : 'none' }}
										key={index}
									>
										{key.header}
									</TableCell>
								))}
								<TableCell align='center' style={{ width: '2%' }}></TableCell>
								<TableCell align='center' style={{ width: '2%' }}></TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{classroom ? (
								classroom.map((row, index) => (
									<TableRow hover key={index} align='center'>
										<TableCell align='center'>{index + 1}</TableCell>
										{keys.map((key, index) => (
											<TableCell
												style={{
													display: key.display ? '' : 'none',
												}}
												align={key.align}
												key={index}
											>
												{key.field === 'price' &&
													row['price'].toLocaleString('vi-VN', {
														style: 'currency',
														currency: 'VND',
													})}
												{key.field === 'class_status'
													? parseClassStatus(row[key.field])
													: key.field !== 'price'
													? row[key.field]
													: ''}
											</TableCell>
										))}
										<TableCell align='center'>
											{row['class_status'] !== '3' && (
												<Tooltip title='Chi tiết'>
													<LaunchIcon
														style={{ cursor: 'pointer' }}
														onClick={() =>
															handleChangeClassDetail(row['class_id'])
														}
													/>
												</Tooltip>
											)}
										</TableCell>
										<TableCell align='center'>
											{row['class_status'] !== '3' && (
												<ReportDialog
													classId={row['class_id']}
													className={row['class_name']}
												/>
											)}
										</TableCell>
									</TableRow>
								))
							) : (
								<TableRow>
									<TableCell align='center' colSpan={keys.length + 3}>
										Không tìm thấy dữ liệu
									</TableCell>
								</TableRow>
							)}
						</TableBody>
					</Table>
				</TableContainer>
			</Grid>
		</div>
	);
}

export default StudentClassList;
