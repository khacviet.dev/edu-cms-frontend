import {
	Avatar,
	Box,
	Button,
	Chip,
	Divider,
	FormControl,
	Grid,
	IconButton,
	makeStyles,
	MenuItem,
	Select,
	Tab,
	Tabs,
	TextField,
	Typography,
} from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CloseIcon from '@material-ui/icons/Close';
import classApi from 'api/classApi';
import courseApi from 'api/courseApi';
import permissionApi from 'api/permissionApi';
import studentApi from 'api/studentApi';
import MasterLayout from 'common/pages/MasterLayout';
import InputField from 'custom-fields/InputField';
import { Field, Form, Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useParams } from 'react-router';
import { Link, useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as yup from 'yup';

const useStyles = makeStyles((theme) => ({
	postPanel: {
		marginTop: '5rem',
		marginBottom: '0.5rem',
	},
	createPostTitle: {
		fontSize: '1.25rem',
		fontWeight: 'bold',
	},
}));

function TabPanel(props) {
	const { children, value, index, ...other } = props;

	return (
		<div role='tabpanel' hidden={value !== index} {...other}>
			{value === index && <Box p={1}>{children}</Box>}
		</div>
	);
}

const referer = [
	{
		value: 'student',
		display: 'Học sinh',
	},
];

export default function StudentDetail(props) {
	const notify = useContext(ToastifyContext);
	const classes = useStyles();
	let history = useHistory();

	const { studentId } = useParams();

	const [loading, setLoading] = useState(false);
	const [filters, setFilters] = useState({
		month: new Date().getMonth() + 1,
		year: new Date().getFullYear(),
	});
	const [value, setValue] = useState(0);
	const [profile, setProfile] = useState({});
	const [listCourse, setListCourse] = useState([]);
	const [listClass, setListClass] = useState([]);

	const [canEdit, setCanEdit] = useState(false);
	const [canDelete, setCanDelete] = useState(false);

	const [open, setOpen] = useState(false);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleFilterChange = (e) => {
		setFilters({
			...filters,
			[e.target.name]: e.target.value,
		});
	};

	const handleChange = (event, newValue) => {
		setValue(newValue);
	};

	const handleProfileChange = (e) => {
		setProfile({
			...profile,
			[e.target.name]: e.target.value,
		});
	};

	const handleListCourseChange = (row) => {
		row.join_date = row.join_date == null ? new Date() : null;
		const newList = [...listCourse];

		setListCourse(newList);
	};

	const handleListClassChange = (row) => {
		row.status = row.status == '1' ? '0' : '1';
		const newList = [...listClass];

		setListClass(newList);
	};

	const handleDeleteStudent = () => {
		const post = async () => {
			try {
				setLoading(true);
				const response = await studentApi.deleteStudent(studentId);
				setLoading(false);
				notify('success', response.message);
				history.push('/student');
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
				}
			}
		};

		post();
	};

	const [isPermiss, setIsPermiss] = useState(false);
	const [functions, setFunctions] = useState([]);
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_list_student',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const response = await permissionApi.getFunctions();
					const data = [];
					for (let i = 0; i < response.length; i++) {
						data.push(response[i].function_id);
					}

					if (data.includes('function_edit_student')) {
						setCanEdit(true);
					}

					if (data.includes('function_delete_student')) {
						setCanDelete(true);
					}

					setFunctions(data);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const getProfile = async () => {
				try {
					setLoading(true);
					const response = await studentApi.getStudentById(studentId);
					setLoading(false);
					setProfile(response.data);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 404) {
						history.push('/student');
					}
					if (error.response.status === 403) {
						localStorage.removeItem('menu');
						history.push('/');
					}
				}
			};

			getProfile();
		}
	}, [isPermiss, studentId]);

	useEffect(() => {
		if (isPermiss) {
			const getListClass = async () => {
				try {
					setLoading(true);
					const response = await classApi.getClassByStudentIdManagerScreen(
						studentId,
						filters
					);
					setLoading(false);
					setListClass(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			getListClass();
		}
	}, [isPermiss, studentId, filters]);

	useEffect(() => {
		if (isPermiss) {
			const getListCourse = async () => {
				try {
					setLoading(true);
					const response = await courseApi.getCourseByStudentId(studentId);
					setLoading(false);
					setListCourse(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			getListCourse();
		}
	}, [isPermiss, studentId]);

	const validationSchema = yup.object().shape({
		full_name: yup.string().required('Trường không được để trống'),
		display_name: yup.string().required('Trường không được để trống'),
		email: yup
			.string()
			.email('Email không hợp lệ')
			.required('Trường không được để trống'),
		phone: yup
			.string()
			.matches(/^[0-9]+$/, 'Số điện thoại chỉ bao gồm chữ số')
			.min(10, 'Số điện thoại gồm ít nhất 10 số')
			.max(12, 'Số điện thoại gồm nhiều nhất 11 số'),
		address: yup.string(),
		note: yup.string().nullable(),
	});

	return (
		<Formik
			initialValues={profile}
			enableReinitialize
			validationSchema={validationSchema}
			onSubmit={(values) => {
				const post = async () => {
					try {
						const data = {
							profile: values,
							listClass,
							listCourse,
						};
						setLoading(true);
						const response = await studentApi.updateStudent(studentId, data);
						setLoading(false);
						notify('success', response.message);
					} catch (error) {
						setLoading(false);
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/');
						}
					}
				};
				post();
			}}
		>
			{(formikProps) => {
				return (
					<MasterLayout referer={referer} header={profile.full_name}>
						{isPermiss && (
							<Box>
								{loading && (
									<div
										style={{
											position: 'fixed',
											top: '50%',
											left: '50%',
											width: '50px',
											transform: 'translate(-50%, -50%)',
											zIndex: '1000000000',
										}}
										className='cp-spinner cp-balls'
									></div>
								)}
								<Helmet>
									<title>{`${profile.full_name} | Học sinh | Xoài CMS`}</title>
								</Helmet>
								<Form>
									<Grid container spacing={2}>
										<Grid container spacing={2}>
											<Grid item md={3} style={{ marginTop: '1rem' }}>
												<Grid container justify='center'>
													<Avatar
														style={{
															width: '60%',
															height: 'auto',
															boxShadow:
																'0 16px 38px -12px rgb(0 0 0 / 16%), 0 4px 25px 0 rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%)',
														}}
														src={profile.avt_link}
													/>
												</Grid>
												<Grid
													container
													justify='center'
													style={{ marginTop: '0.75rem' }}
												>
													<Typography style={{ fontSize: '1rem' }}>
														{formikProps.values.full_name}
													</Typography>
												</Grid>
											</Grid>
											<Grid item md={9}>
												<Grid container>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Mã số:
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<TextField
																		value={profile.user_id}
																		variant='outlined'
																		size='small'
																		margin='normal'
																		disabled
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Email:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9}>
																<Field
																	name='email'
																	component={InputField}
																	fullWidth
																	size='small'
																	disabled={!canEdit}
																/>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Họ và tên:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<Field
																		name='full_name'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Số điện thoại:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9}>
																<FormControl fullWidth>
																	<Field
																		name='phone'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Tên hiển thị:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<Field
																		name='display_name'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Địa chỉ:
															</Grid>
															<Grid item md={9}>
																<FormControl fullWidth>
																	<Field
																		name='address'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Giới tính:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																{profile.gender !== undefined && (
																	<FormControl
																		margin='normal'
																		style={{
																			minWidth: '120px',
																		}}
																		size='small'
																	>
																		<Select
																			disabled={!canEdit}
																			name='gender'
																			variant='outlined'
																			value={profile.gender}
																			onChange={handleProfileChange}
																		>
																			<MenuItem value={'male'}>Nam</MenuItem>
																			<MenuItem value={'female'}>Nữ</MenuItem>
																		</Select>
																	</FormControl>
																)}
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Ghi chú:
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<Field
																		name='note'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Ngày tạo:
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<TextField
																		value={new Date(
																			profile.created_at
																		).toLocaleString()}
																		variant='outlined'
																		size='small'
																		margin='normal'
																		disabled
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
												</Grid>
											</Grid>
										</Grid>

										<Grid container style={{ margin: '1.5rem 0' }}>
											<Divider style={{ width: '100%' }} />
										</Grid>
										<Grid container>
											{value === 0 && (
												<Grid
													container
													alignItems='center'
													style={{ marginBottom: '1rem' }}
												>
													Tháng:
													<FormControl
														margin='normal'
														style={{
															minWidth: '120px',
															margin: '0 1rem',
														}}
														size='small'
													>
														<Select
															name='month'
															variant='outlined'
															value={filters.month}
															onChange={(e) => handleFilterChange(e)}
														>
															<MenuItem value={1}>1</MenuItem>
															<MenuItem value={2}>2</MenuItem>
															<MenuItem value={3}>3</MenuItem>
															<MenuItem value={4}>4</MenuItem>
															<MenuItem value={5}>5</MenuItem>
															<MenuItem value={6}>6</MenuItem>
															<MenuItem value={7}>7</MenuItem>
															<MenuItem value={8}>8</MenuItem>
															<MenuItem value={9}>9</MenuItem>
															<MenuItem value={10}>10</MenuItem>
															<MenuItem value={11}>11</MenuItem>
															<MenuItem value={12}>12</MenuItem>
														</Select>
													</FormControl>
													Năm:
													<FormControl
														margin='normal'
														style={{
															minWidth: '120px',
															margin: '0 1rem',
														}}
														size='small'
													>
														<Select
															name='year'
															variant='outlined'
															value={filters.year}
															onChange={(e) => handleFilterChange(e)}
														>
															<MenuItem value={new Date().getFullYear() - 2}>
																{new Date().getFullYear() - 2}
															</MenuItem>
															<MenuItem value={new Date().getFullYear() - 1}>
																{new Date().getFullYear() - 1}
															</MenuItem>
															<MenuItem value={new Date().getFullYear()}>
																{new Date().getFullYear()}
															</MenuItem>
														</Select>
													</FormControl>
												</Grid>
											)}
											<Grid container>
												<Tabs
													value={value}
													onChange={handleChange}
													indicatorColor='primary'
													textColor='primary'
													centered
												>
													<Tab label='Tại trung tâm' />
													<Tab label='Trực tuyến' />
												</Tabs>
											</Grid>
											<TabPanel
												value={value}
												index={0}
												style={{ width: '100%' }}
											>
												<TableContainer component={Paper}>
													<Table stickyHeader>
														<TableHead>
															<TableRow>
																<TableCell align='left'>STT</TableCell>
																<TableCell align='left'>Tên lớp</TableCell>
																<TableCell align='right'>
																	Tổng số buổi
																</TableCell>
																<TableCell align='right'>
																	Số tiền cần đóng
																</TableCell>
																<TableCell align='right'>{`Phí nợ (tháng ${
																	filters.month - 1
																})`}</TableCell>
																<TableCell align='right'>{`Phí thừa (tháng ${
																	filters.month - 1
																})`}</TableCell>
																<TableCell align='right'>Thực thu</TableCell>
																<TableCell align='center'>Trạng thái</TableCell>
															</TableRow>
														</TableHead>
														<TableBody>
															{listClass.length > 0 ? (
																listClass.map((row, index) => (
																	<TableRow hover key={index}>
																		<TableCell align='left'>
																			{index + 1}
																		</TableCell>
																		<TableCell align='left'>
																			<Link to={`/class/${row.class_id}`}>
																				{row.class_name}
																			</Link>
																		</TableCell>
																		<TableCell align='right'>
																			{row.total_lesson_in_month}
																		</TableCell>
																		<TableCell align='right'>
																			{row.total_money.toLocaleString('vi-VN', {
																				style: 'currency',
																				currency: 'VND',
																			})}
																		</TableCell>
																		<TableCell align='right'>
																			{row.additional_charges.toLocaleString(
																				'vi-VN',
																				{
																					style: 'currency',
																					currency: 'VND',
																				}
																			)}
																		</TableCell>
																		<TableCell align='right'>
																			{row.residual_fee.toLocaleString(
																				'vi-VN',
																				{
																					style: 'currency',
																					currency: 'VND',
																				}
																			)}
																		</TableCell>
																		<TableCell align='right'>
																			{row.amount.toLocaleString('vi-VN', {
																				style: 'currency',
																				currency: 'VND',
																			})}
																		</TableCell>
																		<TableCell align='center'>
																			{row.status == '1' ? (
																				<Chip
																					disabled={!canEdit}
																					onClick={() =>
																						handleListClassChange(row)
																					}
																					color='primary'
																					label='Đã đóng tiền'
																				/>
																			) : (
																				<Chip
																					disabled={!canEdit}
																					onClick={() =>
																						handleListClassChange(row)
																					}
																					label='Chưa đóng tiền'
																				/>
																			)}
																		</TableCell>
																	</TableRow>
																))
															) : (
																<TableRow>
																	<TableCell align='center' colSpan={8}>
																		Không tìm thấy dữ liệu
																	</TableCell>
																</TableRow>
															)}
														</TableBody>
													</Table>
												</TableContainer>
											</TabPanel>
											<TabPanel
												value={value}
												index={1}
												style={{ width: '100%' }}
											>
												<TableContainer component={Paper}>
													<Table stickyHeader>
														<TableHead>
															<TableRow>
																<TableCell align='left'>Tên khóa học</TableCell>
																<TableCell align='right'>Giá tiền</TableCell>
																<TableCell align='center'>Trạng thái</TableCell>
															</TableRow>
														</TableHead>
														<TableBody>
															{listCourse.length > 0 ? (
																listCourse.map((row, index) => (
																	<TableRow hover key={index}>
																		<TableCell align='left'>
																			<Link
																				to={{
																					pathname: '/coursedetail',
																					state: {
																						course_id: row.course_id,
																					},
																				}}
																			>
																				{row.title}
																			</Link>
																		</TableCell>
																		<TableCell align='right'>
																			{row.price.toLocaleString('vi-VN', {
																				style: 'currency',
																				currency: 'VND',
																			})}
																		</TableCell>
																		<TableCell align='center'>
																			{row.join_date !== null ? (
																				<Chip
																					disabled={!canEdit}
																					onClick={() =>
																						handleListCourseChange(row)
																					}
																					color='primary'
																					label='Đã đóng tiền'
																				/>
																			) : (
																				<Chip
																					disabled={!canEdit}
																					onClick={() =>
																						handleListCourseChange(row)
																					}
																					label='Chưa đóng tiền'
																				/>
																			)}
																		</TableCell>
																	</TableRow>
																))
															) : (
																<TableRow>
																	<TableCell align='center' colSpan={3}>
																		Không tìm thấy dữ liệu
																	</TableCell>
																</TableRow>
															)}
														</TableBody>
													</Table>
												</TableContainer>
											</TabPanel>
										</Grid>

										<Grid
											container
											className={classes.postPanel}
											justify='flex-end'
										>
											{canEdit && (
												<Button
													style={{
														padding: '0.5rem 1.5rem 0.5rem 1.5rem',
														marginRight: '1rem',
													}}
													type='submit'
													variant='contained'
													color='primary'
												>
													Cập nhập
												</Button>
											)}
											{canDelete && (
												<Button
													style={{
														padding: '0.5rem 1.5rem 0.5rem 1.5rem',
													}}
													variant='contained'
													color='secondary'
													onClick={handleClickOpen}
												>
													Xóa
												</Button>
											)}
											<Dialog open={open} onClose={handleClose}>
												<Grid container>
													<Grid item xs={9} md={9} lg={9}>
														<DialogTitle>Xóa</DialogTitle>
													</Grid>
													<Grid
														item
														container
														justify='flex-end'
														xs={3}
														md={3}
														lg={3}
													>
														<IconButton onClick={handleClose}>
															<CloseIcon />
														</IconButton>
													</Grid>
												</Grid>
												<Divider />
												<DialogContent>
													<DialogContentText>
														Tất cả dữ liệu về học sinh này sẽ bị xóa ?
													</DialogContentText>
												</DialogContent>
												<DialogActions>
													<Button variant='contained' onClick={handleClose}>
														Hủy
													</Button>
													<Button
														variant='contained'
														color='secondary'
														onClick={handleDeleteStudent}
													>
														Xóa
													</Button>
												</DialogActions>
											</Dialog>
										</Grid>
									</Grid>
								</Form>
							</Box>
						)}
					</MasterLayout>
				);
			}}
		</Formik>
	);
}
