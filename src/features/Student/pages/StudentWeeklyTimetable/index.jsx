import {
	Box,
	FormControl,
	Grid,
	MenuItem,
	Paper,
	Select,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	Typography,
} from '@material-ui/core';
import permissionApi from 'api/permissionApi';
import shiftApi from 'api/shiftApi';
import studentApi from 'api/studentApi';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { Link, useHistory } from 'react-router-dom';
import {
	checkDayBelongWeek,
	getDayInWeek,
	getDisplayThisWeek,
	getWeeks,
} from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

// thông tin của buổi học đó ở table
const showInfoScheduleDay = (schedules, day, slot) => {
	const schedule = schedules.find(
		(sl) => sl.day_of_week === day && sl.slot_id === slot
	);
	return schedule === undefined ? (
		'-'
	) : (
		<Typography variant='caption'>
			<Link to={`/myclass/${schedule.class_id}`}>{schedule.class_name}</Link>
			<br />
			{schedule.room_name}
			<br />
			{schedule.status !== '0' ? (
				schedule.is_active === '0' ? (
					<span style={{ color: 'red' }}>(Vắng mặt)</span>
				) : (
					<span style={{ color: 'green' }}>(Có mặt)</span>
				)
			) : (
				<span>(Chưa học)</span>
			)}
		</Typography>
	);
};

function StudentWeeklyTimetable() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();

	// select week
	const [week, setWeek] = useState(getDayInWeek());

	// hiện thị ngày tháng theo thứ
	const [displayThisWeek, setDisplayThisWeek] = useState(
		getDisplayThisWeek(getDayInWeek())
	);

	// tất cả slot
	const [defaultSlot, setDefaultSlot] = useState([]);

	// select year
	const [year, setYear] = useState(() => {
		return new Date().getFullYear();
	});

	const [loading, setLoading] = useState(false);

	// chi tiết lịch học theo từng ngày
	const [detailSchedule, setDetailSchedule] = useState([]);

	// show chi tiết lịch học theo tuần
	const [showDetailSchedule, setShowDetailSchedule] = useState([]);

	const [isPermiss, setIsPermiss] = useState(false);

	const handleYearChange = (e) => {
		setYear(e.target.value);
		setWeek(0);

		const showSchedule = detailSchedule.filter(
			(sl) =>
				new Date(sl.date).getFullYear() === e.target.value &&
				sl.week_of_year === 0
		);

		setShowDetailSchedule(() => {
			return showSchedule;
		});

		setDisplayThisWeek(() => {
			return getDisplayThisWeek(0);
		});
	};

	const handleWeekChange = (e) => {
		setWeek(e.target.value);
		setDisplayThisWeek(getDisplayThisWeek(e.target.value));

		const showSchedule = detailSchedule.filter(
			(sl) => sl.week_of_year === e.target.value
		);

		setShowDetailSchedule(() => {
			return showSchedule;
		});
	};

	useEffect(() => {
		const post = async () => {
			try {
				setLoading(true);
				const params = {
					functionCode: 'function_student_timetable',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
				setLoading(false);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await shiftApi.getSlots();
					setDefaultSlot(response);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await studentApi.getStudentWeeklyTimetable();

					setDetailSchedule(() => {
						return response;
					});

					if (response.length > 0) {
						const showSchedule = response.filter(
							(sl) => sl.week_of_year === checkDayBelongWeek()
						);

						setShowDetailSchedule(() => {
							return showSchedule;
						});
					}

					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	return (
		<MasterLayout header='Thời khóa biểu'>
			<Box>
				{loading && (
					<div
						style={{
							position: 'fixed',
							top: '50%',
							left: '50%',
							width: '50px',
							transform: 'translate(-50%, -50%)',
							zIndex: '1000000000',
						}}
						className='cp-spinner cp-balls'
					></div>
				)}
				<Helmet>
					<title>Thời khóa biểu | Xoài CMS</title>
				</Helmet>
				<TableContainer component={Paper}>
					<Table>
						<TableHead>
							<TableRow>
								<TableCell
									style={{
										padding: '0.25rem',
										width: '250px',
									}}
									rowSpan={2}
								>
									<Grid container>
										<Grid
											container
											alignItems='center'
											style={{ marginBottom: '0.25rem' }}
										>
											<Typography>Năm:&nbsp;&nbsp;</Typography>
											<FormControl size='small'>
												<Select
													disabled={detailSchedule ? false : true}
													variant='outlined'
													value={year}
													onChange={handleYearChange}
												>
													<MenuItem value={new Date().getFullYear() - 2}>
														{new Date().getFullYear() - 2}
													</MenuItem>
													<MenuItem value={new Date().getFullYear() - 1}>
														{new Date().getFullYear() - 1}
													</MenuItem>
													<MenuItem value={new Date().getFullYear()}>
														{new Date().getFullYear()}
													</MenuItem>
													<MenuItem value={new Date().getFullYear() + 1}>
														{new Date().getFullYear() + 1}
													</MenuItem>
												</Select>
											</FormControl>
										</Grid>
										<Grid container alignItems='center'>
											<Typography>Tuần:&nbsp;&nbsp;</Typography>
											<FormControl size='small'>
												<Select
													disabled={detailSchedule ? false : true}
													variant='outlined'
													value={week}
													onChange={handleWeekChange}
												>
													{getWeeks().map((week, index) => (
														<MenuItem key={index} value={week.value}>
															{week.display}
														</MenuItem>
													))}
												</Select>
											</FormControl>
										</Grid>
									</Grid>
								</TableCell>
								<TableCell style={{ padding: '0.25rem' }} align='center'>
									Thứ 2
								</TableCell>
								<TableCell style={{ padding: '0.25rem' }} align='center'>
									Thứ 3
								</TableCell>
								<TableCell style={{ padding: '0.25rem' }} align='center'>
									Thứ 4
								</TableCell>
								<TableCell style={{ padding: '0.25rem' }} align='center'>
									Thứ 5
								</TableCell>
								<TableCell style={{ padding: '0.25rem' }} align='center'>
									Thứ 6
								</TableCell>
								<TableCell style={{ padding: '0.25rem' }} align='center'>
									Thứ 7
								</TableCell>
								<TableCell style={{ padding: '0.25rem' }} align='center'>
									CN
								</TableCell>
							</TableRow>
							<TableRow>
								{displayThisWeek.map((w, index) => (
									<TableCell
										key={index}
										style={{ padding: '0.25rem' }}
										align='center'
									>
										{w}
									</TableCell>
								))}
							</TableRow>
						</TableHead>
						<TableBody>
							{detailSchedule ? (
								defaultSlot.map((sl, index) => (
									<TableRow key={index}>
										<TableCell component='th' scope='row'>
											{sl.start_time} - {sl.end_time}
										</TableCell>
										<TableCell align='center'>
											{showInfoScheduleDay(showDetailSchedule, 1, sl.slot_id)}
										</TableCell>
										<TableCell align='center'>
											<Typography variant='caption'>
												{showInfoScheduleDay(showDetailSchedule, 2, sl.slot_id)}
											</Typography>
										</TableCell>
										<TableCell align='center'>
											<Typography variant='caption'>
												{showInfoScheduleDay(showDetailSchedule, 3, sl.slot_id)}
											</Typography>
										</TableCell>
										<TableCell align='center'>
											<Typography variant='caption'>
												{showInfoScheduleDay(showDetailSchedule, 4, sl.slot_id)}
											</Typography>
										</TableCell>
										<TableCell align='center'>
											<Typography variant='caption'>
												{showInfoScheduleDay(showDetailSchedule, 5, sl.slot_id)}
											</Typography>
										</TableCell>
										<TableCell align='center'>
											<Typography variant='caption'>
												{showInfoScheduleDay(showDetailSchedule, 6, sl.slot_id)}
											</Typography>
										</TableCell>
										<TableCell align='center'>
											{showInfoScheduleDay(showDetailSchedule, 0, sl.slot_id)}
										</TableCell>
									</TableRow>
								))
							) : (
								<TableRow>
									<TableCell align='center' colSpan={8}>
										Chưa có lịch học
									</TableCell>
								</TableRow>
							)}
						</TableBody>
					</Table>
				</TableContainer>
			</Box>
		</MasterLayout>
	);
}

export default StudentWeeklyTimetable;
