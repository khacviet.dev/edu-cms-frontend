import { Avatar, Divider, Grid, Paper, Typography } from '@material-ui/core';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import ClassIcon from '@material-ui/icons/Class';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import dashboardApi from 'api/dashboardApi';
import MyBarChart from 'common/components/MyBarChart';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
TeacherDashboard.propTypes = {};

function TeacherDashboard(props) {
	const notify = useContext(ToastifyContext);
	let history = useHistory();

	const [loading, setLoading] = useState(false);
	const [student, setStudent] = useState(-1);
	const [salaryOn, setSalaryOn] = useState(-1);
	const [salaryOff, setSalaryOff] = useState(-1);
	const [classroom, setClassroom] = useState(-1);
	const [schedule, setSchedule] = useState({});

	useEffect(() => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await dashboardApi.getTeacherDashboard();
				setLoading(false);

				setStudent(response.student);
				setClassroom(response.class);
				setSalaryOn(response.salaryOn);
				setSalaryOff(response.salaryOff);
				setSchedule(response.schedule);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		get();
	}, []);

	const handleRouterChange = (url) => {
		history.push(`/${url}`);
	};

	return (
		<Grid container>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Grid container spacing={3}>
				{classroom !== -1 && (
					<Grid
						item
						xs={12}
						md={3}
						lg={3}
						onClick={() => handleRouterChange('class')}
						style={{ cursor: 'pointer' }}
					>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Lớp đang dạy
									</Typography>
									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{classroom}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'rgb(229, 57, 53)',
											width: '56px',
											height: '56px',
										}}
									>
										<ClassIcon />
									</Avatar>
								</Grid>
							</Grid>
						</Paper>
					</Grid>
				)}
				{student !== -1 && (
					<Grid
						item
						xs={12}
						md={3}
						lg={3}
						onClick={() => handleRouterChange('class')}
						style={{ cursor: 'pointer' }}
					>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Học sinh đang dạy
									</Typography>
									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{student}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'rgb(67, 160, 71)',
											width: '56px',
											height: '56px',
										}}
									>
										<EmojiPeopleIcon />
									</Avatar>
								</Grid>
							</Grid>
						</Paper>
					</Grid>
				)}
				{salaryOff !== -1 && (
					<Grid item xs={12} md={3} lg={3}>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Lương tại trung tâm
									</Typography>
									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{salaryOff.toLocaleString('vi-VN', {
											style: 'currency',
											currency: 'VND',
										})}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'blue',
											width: '56px',
											height: '56px',
										}}
									>
										<AttachMoneyIcon />
									</Avatar>
								</Grid>
							</Grid>
						</Paper>
					</Grid>
				)}
				{salaryOn !== -1 && (
					<Grid item xs={12} md={3} lg={3}>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Lương online
									</Typography>
									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{salaryOn.toLocaleString('vi-VN', {
											style: 'currency',
											currency: 'VND',
										})}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'orange',
											width: '56px',
											height: '56px',
										}}
									>
										<CreditCardIcon />
									</Avatar>
								</Grid>
							</Grid>
						</Paper>
					</Grid>
				)}
			</Grid>
			<Grid container spacing={3} style={{ marginTop: '1.5rem' }}>
				<Grid item xs={12} md={12} lg={12}>
					<Paper
						style={{
							height: '100%',
						}}
					>
						<Grid container>
							<Grid item xs={12} lg={12} md={12} style={{ padding: '1rem' }}>
								<Typography variant='body1' style={{ fontWeight: 'bold' }}>
									Số buổi dạy theo tháng
								</Typography>
							</Grid>
							<Grid item xs={12} lg={12} md={12}>
								<Divider />
							</Grid>
							<Grid item xs={12} lg={12} md={12} style={{ padding: '1rem' }}>
								{schedule && <MyBarChart data={schedule} />}
							</Grid>
						</Grid>
					</Paper>
				</Grid>
			</Grid>
		</Grid>
	);
}

export default TeacherDashboard;
