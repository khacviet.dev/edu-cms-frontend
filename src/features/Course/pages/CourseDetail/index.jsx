import { CKEditor } from '@ckeditor/ckeditor5-react';
import { TextField } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import AssignmentIcon from '@material-ui/icons/Assignment';
import BookIcon from '@material-ui/icons/Book';
import CategoryIcon from '@material-ui/icons/Category';
import DescriptionIcon from '@material-ui/icons/Description';
import FlagIcon from '@material-ui/icons/Flag';
import ImageIcon from '@material-ui/icons/Image';
import LinkIcon from '@material-ui/icons/Link';
import courseApi from 'api/courseApi';
import permissionApi from 'api/permissionApi';
import Editor from 'ckeditor5-custom-build/build/ckeditor';
import MasterLayout from 'common/pages/MasterLayout';
import InputField from 'custom-fields/InputField';
import ImageUpload from 'features/Post/components/ImageUpload';
import { FastField, Field, Form, Formik } from 'formik';
import React, { useContext, useEffect } from 'react';
import { useHistory } from 'react-router';
import { ToastifyContext } from 'utils/ToastifyConfig';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
			width: 250,
		},
	},
};

const useStyles = makeStyles((theme) => ({
	button_list_item: {
		padding: '0px !important',
	},
	input: {
		padding: '18.5px 14px',
	},
	root: {
		flexGrow: 1,
	},
	img: {
		width: '100%',
	},
	black: {
		color: 'black',
	},
	green: {
		color: 'green',
	},
	red: {
		color: 'red',
	},
	blue: {
		color: 'blue',
	},
	bgGreen: {
		backgroundColor: '#ADFF2F',
	},
	bgGray: {
		backgroundColor: '#889FA5',
	},
	bgRed: {
		backgroundColor: '#DC143C',
	},
	width_10: {
		minWidth: '30px',
		width: '5px',
	},
	form: {
		width: '80%',
		[theme.breakpoints.down('xs')]: {
			width: '100%',
		},
		marginTop: theme.spacing(1),
	},
	paper: {
		padding: theme.spacing(1),
		textAlign: 'center',
		color: theme.palette.text.secondary,
	},
	richTextEditor: {
		'& .ck-editor__main > .ck-editor__editable': {
			maxHeight: `${window.innerHeight - 222}px`,
		},
	},

	textAreaMedium: {},
	medium: {
		minHeight: '100px',
	},
	fullWidth: {
		width: '100%',
	},
	minWidth: {
		'& .MuiSelect-select': {
			padding: '18.5px 14px',
			border: '1px solid rgba(0, 0, 0, 0.23);',
			borderRadius: '4px',
		},

		minWidth: '100%',
	},
	center: {
		margin: '0 auto',
	},
	displayNone: {
		display: 'none',
	},
	border: {
		borderTop: '1px groove #dedede',
		borderRight: '1px groove #dedede',
		borderLeft: '1px groove #dedede',
		borderRadius: '4px',
	},
	redBorder: {
		border: '1px groove red',
		borderRadius: '4px',
	},
}));

const LightTooltip = withStyles((theme) => ({
	tooltip: {
		backgroundColor: theme.palette.common.black,
		color: 'white',
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

export default function CourseDetail(props) {
	const notify = useContext(ToastifyContext);
	let history = useHistory();

	const [postCategory, setPostCategory] = React.useState([]);
	const [isWrongCost, setIsWrongCost] = React.useState(false);
	const [selectedSubject, setSelectedSubject] = React.useState('');
	const [thumbnail, setThumbnail] = React.useState([]);
	const [initialValues, setInitialValues] = React.useState({
		title: '',
		teacher: '',
		price: '',
		discount: '',
		percent: '',
		thumbnail: '',
	});
	const [price, setPrice] = React.useState('');
	const [percent, setPercent] = React.useState('');
	const [discount, setDiscount] = React.useState('');

	const [subjectList, setSubjectList] = React.useState([]);
	const [CKEditorDataDB, setCKEditorDataDB] = React.useState('');
	const [imageUrl, setImageUrl] = React.useState('');
	const [loading, setLoading] = React.useState(false);
	const [listSubject, setListSubject] = React.useState([]);
	const [isFeatured, setIsFeatured] = React.useState(false);
	const [status, setStatus] = React.useState('');
	const [functions, setFunctions] = React.useState([]);
	const [cantEdit, setCantEdit] = React.useState(true);
	const [isReview, setIsReview] = React.useState(false);
	const [isDone, setIsDone] = React.useState(false);
	const state = props.location.state;

	const [isPermiss, setIsPermiss] = React.useState(false);

	// check permission
	useEffect(() => {
		setLoading((loading) => !loading);
		if (state == undefined) {
			const post = async () => {
				try {
					const params = {
						functionCode: 'function_create_course',
					};
					await permissionApi.checkPermission(params);

					setIsPermiss(true);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			post();
		} else {
			const post = async () => {
				try {
					const params = {
						functionCode: 'function_edit_course',
					};
					await permissionApi.checkPermission(params);
					setLoading((loading) => !loading);
					setIsPermiss(true);
				} catch (error) {
					if (
						error.response.status == 403 &&
						error.response.data.message == 'Not permission'
					) {
						try {
							const params1 = {
								functionCode: 'function_review_course',
							};
							await permissionApi.checkPermission(params1);
							setIsPermiss(true);
							setLoading((loading) => !loading);
						} catch (error) {
							setLoading((loading) => !loading);
							notify('error', error.response.data.message);
							localStorage.removeItem('menu');
							history.push('/');
						}
					} else {
						notify('error', error.response.data.message);
					}
				}
			};

			post();
		}
	}, []);

	// get list function
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const getFunctions = async () => {
				try {
					const respone = await permissionApi.getFunctions();
					const data = [];
					for (let i = 0; i < respone.length; i++) {
						data.push(respone[i].function_id);
					}
					if (
						data.includes('function_course_list') &&
						((state != undefined && data.includes('function_edit_course')) ||
							(state == undefined && data.includes('function_create_course')))
					) {
						setCantEdit(false);
					}
					setFunctions(data);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			getFunctions();
		}
	}, [isPermiss]);

	if (state != undefined) {
		// get course
		useEffect(() => {
			if (isPermiss) {
				setLoading((loading) => !loading);
				const getCourseById = async () => {
					try {
						const response = await courseApi.getCourseById(state.course_id);
						const data = response.data;
						if (data) {
							const initValue = {
								title: data[0].title,
								teacher: data[0].full_name,
							};
							setStatus(data[0].status);
							setIsFeatured(data[0].is_featured);
							if (data[0].subject_id != null) {
								setSelectedSubject(data[0].subject_id);
							}
							setInitialValues(initValue);
							setImageUrl(data[0].thumbnail);
							setCKEditorDataDB(data[0].description);
							setPrice(
								data[0].cost
									.toString()
									.replace(
										/(?:(^\d{1,3})(?=(?:\d{3})*$)|(\d{3}))(?!$)/gm,
										'$1$2.'
									)
							);
							setPercent(data[0].percent);
							setDiscount(
								data[0].discount
									.toString()
									.replace(
										/(?:(^\d{1,3})(?=(?:\d{3})*$)|(\d{3}))(?!$)/gm,
										'$1$2.'
									)
							);
						}

						setLoading((loading) => !loading);
					} catch (error) {
						setLoading((loading) => !loading);
						notify('error', error.response.data.message);
					}
				};
				getCourseById();
			}
		}, [isPermiss]);
	}

	//get list subject
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const getSubjectList = async () => {
				try {
					const response = await courseApi.getSubjectList();
					const subjectFilter = response.data;
					setSubjectList(subjectFilter);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getSubjectList();
		}
	}, [isPermiss]);

	// handleChangeSubject selected subject
	const handleChangeSubject = (event) => {
		setSelectedSubject(event.target.value);
	};
	//change teacher
	const handleChangeTeacher = (event) => {
		setSelectedTeacher(event.target.value);
	};

	// save thumbnail picture
	const handleSave = (childData) => {
		setThumbnail(childData);
	};

	// set course status
	const handleReivew = (status) => {
		setStatus(status);
		setIsReview(true);
	};

	const handleDone = () => {
		setStatus('4');
		setIsDone(true);
	};

	const classes = useStyles();

	// ckeditor config
	const editorConfiguration = {
		toolbar: {
			items: [
				'blockQuote',
				'bold',
				'imageTextAlternative',
				'link',
				'selectAll',
				'undo',
				'redo',
				'heading',
				'resizeImage:original',
				'resizeImage:25',
				'resizeImage:50',
				'resizeImage:75',
				'resizeImage',
				'imageResize',
				'imageStyle:full',
				'imageStyle:side',
				'imageUpload',
				'indent',
				'outdent',
				'italic',
				'numberedList',
				'bulletedList',
				'insertTable',
				'tableColumn',
				'tableRow',
				'mergeTableCells',
			],

			shouldNotGroupWhenFull: true,
		},

		image: {
			styles: ['alignLeft', 'alignCenter', 'alignRight'],
			toolbar: [
				'imageStyle:alignLeft',
				'imageStyle:alignCenter',
				'imageStyle:alignRight',
				'|',
				'|',
				'imageTextAlternative',
			],
		},
		wordcount: {
			showParagraphs: false,
			showWordCount: true,
			showCharCount: true,
			countSpacesAsChars: false,
			countHTML: false,
			maxWordCount: -1,
			maxCharCount: 4000,
		},

		ckfinder: {
			uploadUrl: `${process.env.REACT_APP_API_URL}/uploadImage`,
		},
	};

	// handle feartue flag
	const handleFlag = () => {
		setIsFeatured((a) => !a);
	};

	// hand price and discount and percent
	const handleBlur = (e, control) => {
		if (e.target.id != 'percent') {
			const value = e.target.value.replaceAll('.', '');
			if (value.match(/^[0-9]*$/)) {
				var result = value.replace(
					/(?:(^\d{1,3})(?=(?:\d{3})*$)|(\d{3}))(?!$)/gm,
					'$1$2.'
				);
				e.target.value = result;
				document.getElementById(control).innerHTML = '';
				document
					.getElementById(e.target.id)
					.classList.remove(classes.redBorder);
			} else {
				document.getElementById(control).innerHTML = 'Vui lòng nhập số';
				document.getElementById(e.target.id).classList.add(classes.redBorder);
			}
		} else {
			const value = e.target.value.replace(/\.+/g, '.');
			const check = value.replaceAll('.', '');
			if (check.match(/^[0-9]*$/) && check.length > 0) {
				e.target.value = value;
				document.getElementById(control).innerHTML = '';
				document
					.getElementById(e.target.id)
					.classList.remove(classes.redBorder);
			} else {
				document.getElementById(control).innerHTML = 'Vui lòng nhập số';
				document.getElementById(e.target.id).classList.add(classes.redBorder);
			}
		}
	};

	return (
		<MasterLayout
			header='Khóa học'
			referer={[
				{
					value: 'course',
					display: 'Danh sách khóa học',
				},
			]}
		>
			{isPermiss && (
				<Grid container spacing={2}>
					{loading == true && (
						<div
							style={{
								position: 'fixed',
								top: '50%',
								left: '50%',
								width: '50px',
								transform: 'translate(-50%, -50%)',
								zIndex: '1000000000',
							}}
							className='cp-spinner cp-balls'
						></div>
					)}
					<Formik
						initialValues={initialValues}
						enableReinitialize
						onSubmit={(values) => {
							const formData = new FormData();
							const title = values.title;
							const subject = selectedSubject;
							const price = document
								.getElementById('price')
								.value.replaceAll('.', '');

							formData.append('upload', thumbnail);
							formData.append('title', title);
							formData.append('price', price);
							formData.append('subject_id', subject);
							formData.append('full_content', CKEditorDataDB);
							formData.append('isFeatured', isFeatured);
							setLoading((loading) => !loading);
							const createCourse = async () => {
								try {
									// reivew course
									if (
										isReview == true &&
										state != undefined &&
										isDone == false &&
										functions.includes('function_review_course')
									) {
										const data = {
											price: document
												.getElementById('price')
												.value.replaceAll('.', ''),
											discount: document
												.getElementById('discount')
												.value.replaceAll('.', ''),
											percent: document.getElementById('percent').value,
											status: status,
											course_id: state.course_id,
											isFeatured: isFeatured,
										};

										const response = await courseApi.reviewCourse(data);
										setLoading((loading) => !loading);
										setIsReview(false);
										notify('success', response.message);

										//edit course
									} else if (
										isReview == false &&
										state != undefined &&
										isDone == false &&
										functions.includes('function_edit_course')
									) {
										setStatus('0');
										if (thumbnail.length == 0) {
											formData.append('thumbnailUrl', imageUrl);
										}
										formData.append('course_id', state.course_id);
										const response = await courseApi.updateCourse(formData);
										setLoading((loading) => !loading);
										notify('success', response.message);
										// create course
									} else if (
										functions.includes('function_create_course') &&
										isDone == false
									) {
										const response = await courseApi.createCourse(formData);
										setLoading((loading) => !loading);
										notify('success', response.message);
									} else if (isDone == true) {
										setIsDone(false);
										const params = {
											course_id: state.course_id,
											status: '4',
										};
										const updateCourseFlag = async () => {
											try {
												const response = await courseApi.updateCourseFlag(
													params
												);
												setLoading((loading) => !loading);
												notify('success', response.message);
											} catch (error) {
												setLoading((loading) => !loading);
												notify('error', error.response.data.message);
												if (error.response.status == 403) {
													history.push('/');
												}
											}
										};
										updateCourseFlag();
									}

									// setPostCategory(response.data);
								} catch (error) {
									setLoading((loading) => !loading);
									notify('error', error.response.data.message);
									if (error.response.status == 403) {
										history.push('/');
									}
								}
							};
							createCourse();
						}}
					>
						{(formikProps) => {
							return (
								<Form>
									<Grid container spacing={2} className={classes.root}>
										<Grid item xs={4} sm={4}>
											<Paper className={classes.paper}>
												<Grid container spacing={2}>
													<Grid item xs={12} sm={12}>
														<Grid container>
															{state != undefined && (
																<Grid item xs={12} sm={12}>
																	<ListItem>
																		<ListItemIcon className={classes.black}>
																			<AssignmentIcon />
																		</ListItemIcon>
																		<ListItemText
																			className={classes.black}
																			primary='Trạng Thái'
																		/>
																		<ListItemText
																			className={
																				status == '0'
																					? classes.black
																					: status == '1'
																					? classes.green
																					: status == '2'
																					? classes.red
																					: status == '3'
																					? classes.red
																					: classes.blue
																			}
																			primary={
																				status == '0'
																					? 'Lưu nháp'
																					: status == '1'
																					? 'Đã duyệt'
																					: status == '2'
																					? 'Từ chối'
																					: status == '3'
																					? 'Đã xóa'
																					: 'Đã hoàn thành'
																			}
																		/>
																	</ListItem>
																</Grid>
															)}

															<Grid item xs={12} sm={12}>
																<ListItem>
																	<ListItemIcon className={classes.black}>
																		<BookIcon />
																	</ListItemIcon>
																	<ListItemText
																		className={classes.black}
																		primary='Tiêu đề'
																	/>
																	<LightTooltip
																		placement='left'
																		title='Nổi bật'
																	>
																		<FlagIcon
																			fontSize='large'
																			onClick={handleFlag}
																			style={{
																				color:
																					isFeatured == true
																						? '#0f0cf7'
																						: '#cccaca',
																				cursor: 'pointer',
																			}}
																		></FlagIcon>
																	</LightTooltip>
																</ListItem>

																<Field
																	name='title'
																	component={InputField}
																	autoFocus={false}
																	fullWidth={true}
																	required={true}
																	disabled={cantEdit}
																/>
															</Grid>

															<Grid item xs={12} sm={12}>
																<ListItem>
																	<ListItemIcon className={classes.black}>
																		<CategoryIcon />
																	</ListItemIcon>
																	<ListItemText
																		className={classes.black}
																		primary='Môn học'
																	/>
																</ListItem>
																<Select
																	className={classes.minWidth}
																	required
																	disabled={cantEdit}
																	value={selectedSubject}
																	onChange={(e) => handleChangeSubject(e)}
																	input={<Input />}
																	renderValue={(e) => {
																		let text = '';
																		for (
																			let i = 0;
																			i < subjectList.length;
																			i++
																		) {
																			if (subjectList[i].value == e) {
																				text = subjectList[i].text;
																				break;
																			}
																		}
																		return text;
																	}}
																	MenuProps={MenuProps}
																>
																	{subjectList.map((subject) => (
																		<MenuItem
																			key={subject.value}
																			value={subject.value}
																		>
																			<ListItemText primary={subject.text} />
																		</MenuItem>
																	))}
																</Select>
															</Grid>

															{state != undefined && (
																<Grid item xs={12} sm={12}>
																	<ListItem>
																		<ListItemIcon className={classes.black}>
																			<DescriptionIcon />
																		</ListItemIcon>
																		<ListItemText
																			className={classes.black}
																			primary='Giáo viên'
																		/>
																	</ListItem>
																	<FastField
																		name='teacher'
																		component={InputField}
																		autoFocus={false}
																		fullWidth={true}
																		required={true}
																		disabled={true}
																	/>
																</Grid>
															)}

															<Grid item xs={12} sm={12}>
																<ListItem>
																	<ListItemIcon className={classes.black}>
																		<LinkIcon />
																	</ListItemIcon>
																	<ListItemText
																		className={classes.black}
																		primary='Giá'
																	/>
																</ListItem>
																<TextField
																	required
																	className={classes.border}
																	name='price'
																	key={price}
																	id={`price`}
																	defaultValue={price}
																	onBlur={(e, control) =>
																		handleBlur(e, 'controlPrice')
																	}
																	inputProps={{
																		className: classes.input,
																		pattern: '^[0-9.]*',
																	}}
																	fullWidth
																></TextField>
																<small
																	id={`controlPrice`}
																	style={{ display: 'inherit', color: 'red' }}
																></small>
															</Grid>

															{state != undefined &&
																functions.includes(
																	'function_review_course'
																) && (
																	<Grid item xs={12} sm={12}>
																		<ListItem>
																			<ListItemIcon className={classes.black}>
																				<LinkIcon />
																			</ListItemIcon>
																			<ListItemText
																				className={classes.black}
																				primary='Giảm giá'
																			/>
																		</ListItem>
																		<TextField
																			required
																			className={classes.border}
																			name='discount'
																			key={discount}
																			id={`discount`}
																			defaultValue={discount}
																			onBlur={(e, control) =>
																				handleBlur(e, 'controlDiscount')
																			}
																			inputProps={{
																				className: classes.input,
																				pattern: '^[0-9.]*',
																			}}
																			fullWidth
																		></TextField>
																		<small
																			id={`controlDiscount`}
																			style={{
																				display: 'inherit',
																				color: 'red',
																			}}
																		></small>
																	</Grid>
																)}
															{state != undefined &&
																functions.includes(
																	'function_review_course'
																) && (
																	<Grid item xs={12} sm={12}>
																		<ListItem>
																			<ListItemIcon className={classes.black}>
																				<LinkIcon />
																			</ListItemIcon>
																			<ListItemText
																				className={classes.black}
																				primary='% Hoa hồng'
																			/>
																		</ListItem>

																		<TextField
																			required
																			className={classes.border}
																			name='percent'
																			key={percent}
																			id={`percent`}
																			defaultValue={percent}
																			onBlur={(e, control) =>
																				handleBlur(e, 'controlpercent')
																			}
																			inputProps={{
																				className: classes.input,
																				pattern: '^[0-9.]*',
																			}}
																			fullWidth
																		></TextField>

																		<small
																			id={`controlpercent`}
																			style={{
																				display: 'inherit',
																				color: 'red',
																			}}
																		></small>
																	</Grid>
																)}

															<Grid item xs={12} sm={12}>
																<ListItem>
																	<ListItemIcon className={classes.black}>
																		<ImageIcon />
																	</ListItemIcon>
																	<ListItemText
																		className={classes.black}
																		primary='Ảnh khóa học'
																	/>
																</ListItem>
																<Field
																	name='thumbnail'
																	component={ImageUpload}
																	autoFocus={false}
																	handleSave={handleSave}
																	imageUrl={imageUrl}
																	disabled={cantEdit}
																/>
															</Grid>
														</Grid>
													</Grid>
												</Grid>
											</Paper>
										</Grid>

										<Grid item xs={8} sm={8}>
											<Paper
												className={`${classes.paper} ${classes.richTextEditor}`}
											>
												{functions.length > 0 && (
													<CKEditor
														editor={Editor}
														config={editorConfiguration}
														data={CKEditorDataDB}
														onReady={(editor) => {
															if (
																functions.includes('function_course_list') &&
																((state != undefined &&
																	functions.includes('function_edit_course')) ||
																	(state == undefined &&
																		functions.includes(
																			'function_create_course'
																		)))
															) {
																editor.isReadOnly = false;
															} else {
																editor.isReadOnly = true;
															}
														}}
														onChange={(event, editor) => {
															const data = editor.getData();
															setCKEditorDataDB(data);
														}}
														onBlur={(event, editor) => {}}
														onFocus={(event, editor) => {}}
													/>
												)}
											</Paper>
											<Grid
												container
												style={{ marginTop: '2rem' }}
												justify='center'
											>
												{cantEdit == false && (
													<Button
														style={{ marginRight: '1rem' }}
														type='submit'
														variant='contained'
														className={classes.bgGray}
													>
														<ListItem className={classes.button_list_item}>
															<ListItemIcon className={classes.width_10}>
																<svg
																	xmlns='http://www.w3.org/2000/svg'
																	class='icon icon-tabler icon-tabler-edit'
																	width='44'
																	height='44'
																	viewBox='0 0 24 24'
																	stroke-width='1.5'
																	stroke='#2c3e50'
																	fill='none'
																	stroke-linecap='round'
																	stroke-linejoin='round'
																>
																	<path
																		stroke='none'
																		d='M0 0h24v24H0z'
																		fill='none'
																	/>
																	<path d='M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3' />
																	<path d='M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3' />
																	<line x1='16' y1='5' x2='19' y2='8' />
																</svg>
															</ListItemIcon>
															<ListItemText
																className={classes.black}
																primary={
																	state != undefined ? 'Lưu nháp' : 'Tạo mới'
																}
															/>
														</ListItem>
													</Button>
												)}

												{cantEdit == false && state != undefined && (
													<Button
														onClick={handleDone}
														type='submit'
														variant='contained'
														style={{
															backgroundColor: '#6DB1BF',
															marginRight: '1rem',
														}}
													>
														<ListItem className={classes.button_list_item}>
															<ListItemIcon className={classes.width_10}>
																<svg
																	xmlns='http://www.w3.org/2000/svg'
																	class='icon icon-tabler icon-tabler-arrow-bar-up'
																	width='44'
																	height='44'
																	viewBox='0 0 24 24'
																	stroke-width='1.5'
																	stroke='#000000'
																	fill='none'
																	stroke-linecap='round'
																	stroke-linejoin='round'
																>
																	<path
																		stroke='none'
																		d='M0 0h24v24H0z'
																		fill='none'
																	/>
																	<line x1='12' y1='4' x2='12' y2='14' />
																	<line x1='12' y1='4' x2='16' y2='8' />
																	<line x1='12' y1='4' x2='8' y2='8' />
																	<line x1='4' y1='20' x2='20' y2='20' />
																</svg>
															</ListItemIcon>
															<ListItemText
																className={classes.black}
																primary={'Hoàn thành'}
															/>
														</ListItem>
													</Button>
												)}

												{state != undefined &&
													functions.includes('function_review_course') && (
														<div>
															<Button
																style={{ marginRight: '1rem' }}
																type='submit'
																onClick={() => handleReivew('1')}
																variant='contained'
																className={classes.bgGreen}
															>
																<ListItem className={classes.button_list_item}>
																	<ListItemIcon className={classes.width_10}>
																		<svg
																			xmlns='http://www.w3.org/2000/svg'
																			class='icon icon-tabler icon-tabler-circle-check'
																			width='44'
																			height='44'
																			viewBox='0 0 24 24'
																			stroke-width='1.5'
																			stroke='#2c3e50'
																			fill='none'
																			stroke-linecap='round'
																			stroke-linejoin='round'
																		>
																			<path
																				stroke='none'
																				d='M0 0h24v24H0z'
																				fill='none'
																			/>
																			<circle cx='12' cy='12' r='9' />
																			<path d='M9 12l2 2l4 -4' />
																		</svg>
																	</ListItemIcon>
																	<ListItemText
																		className={classes.black}
																		primary={'Chấp nhận'}
																	/>
																</ListItem>
															</Button>
															<Button
																type='submit'
																onClick={() => handleReivew('2')}
																variant='contained'
																className={classes.bgRed}
															>
																<ListItem className={classes.button_list_item}>
																	<ListItemIcon className={classes.width_10}>
																		<svg
																			xmlns='http://www.w3.org/2000/svg'
																			class='icon icon-tabler icon-tabler-ban'
																			width='44'
																			height='44'
																			viewBox='0 0 24 24'
																			stroke-width='1.5'
																			stroke='#2c3e50'
																			fill='none'
																			stroke-linecap='round'
																			stroke-linejoin='round'
																		>
																			<path
																				stroke='none'
																				d='M0 0h24v24H0z'
																				fill='none'
																			/>
																			<circle cx='12' cy='12' r='9' />
																			<line
																				x1='5.7'
																				y1='5.7'
																				x2='18.3'
																				y2='18.3'
																			/>
																		</svg>
																	</ListItemIcon>
																	<ListItemText
																		className={classes.black}
																		primary={'Từ chối'}
																	/>
																</ListItem>
															</Button>
														</div>
													)}
											</Grid>
										</Grid>
									</Grid>
								</Form>
							);
						}}
					</Formik>
				</Grid>
			)}
		</MasterLayout>
	);
}
