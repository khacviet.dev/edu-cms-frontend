import { Grid } from '@material-ui/core';
import courseApi from 'api/courseApi';
import excelApi from 'api/excelApi';
import permissionApi from 'api/permissionApi';
import CommonTableT from 'common/components/CommonTableT';
import SearchForm from 'common/components/SearchForm';
import MasterLayout from 'common/pages/MasterLayout';
import CreatePost from 'features/Post/components/CreatePost';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router';
import { exportToCSV } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

const id = 'user_id';

const filterList1 = [];
const listFilterLabel = ['Môn học', 'Trạng Thái'];

const filterList2 = [
	{
		text: 'None',
		value: '',
	},
	{
		text: 'Lưu nháp',
		value: '0',
	},
	{
		text: 'Đã duyệt',
		value: '1',
	},
	{
		text: 'Từ chối',
		value: '2',
	},
	{
		text: 'Đã xóa',
		value: '3',
	},
	{
		text: 'Đã hoàn thành',
		value: '4',
	},
	// {
	// 	text: 'Đã hoàn thành',
	// 	value: '4',
	// }
];
const filterList3 = [];

const RenderTable = CommonTableT(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id
);

export default function CourseList() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();

	const [courses, setCourse] = useState([]);
	const [totalRow, setTotalRow] = useState();
	const [loading, setLoading] = useState(false);
	const [subjectList, setSubjectList] = useState([]);
	const [filters, setFilters] = useState({
		limit: 5,
		page: 1,
		q: '',
		filter1: '',
		filter2: '',
		filter3: '',
	});
	// phân quyền
	const [functions, setFunctions] = useState([]);
	const role = '1';
	const [isPermiss, setIsPermiss] = useState(false);
	const [functionsTable, setFunctionTable] = useState({
		view: false,
		edit: false,
		review: false,
		revert: false,
	});
	// check permission
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_course_list',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', `Bạn không có quyền truy cập vào trang này`);
				localStorage.removeItem('menu');
				history.push('/');
			}
		};

		post();
	}, []);

	// get list function
	useEffect(() => {
		if (isPermiss) {
			const getFunctions = async () => {
				try {
					const respone = await permissionApi.getFunctions();
					const a = {
						view: true,
						edit: false,
						delete: false,
						review: false,
					};
					const data = [];
					for (let i = 0; i < respone.length; i++) {
						if (respone[i].function_id == 'function_edit_course') {
							a['edit'] = true;
						} else if (respone[i].function_id == 'function_delete_course') {
							a['delete'] = true;
						} else if (respone[i].function_id == 'function_review_course') {
							a['review'] = true;
						}
						data.push(respone[i].function_id);
					}
					setFunctions(data);
					setFunctionTable(a);
				} catch (error) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			getFunctions();
		}
	}, [isPermiss]);

	// set clounm display
	useEffect(() => {
		const value = [];
		for (let i = 0, l = keys.length; i < l; i += 1) {
			value.push(keys[i].header);
		}
		setColumnSelected(value);
	}, []);

	// get total record
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);
			const params = {
				role: '2',
				filters,
			};
			const getTotalCourseListByFilter = async () => {
				try {
					const response = await courseApi.getTotalCourseListByFilter(params);
					setTotalRow(response.data[0].count);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};

			getTotalCourseListByFilter();
		}
	}, [filters, isPermiss]);

	//get list filter 1
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);
			const getSubjectList = async () => {
				try {
					const response = await courseApi.getSubjectList();
					const subjectFilter = response.data;
					subjectFilter.unshift({
						text: 'None',
						value: '',
					});
					setSubjectList(subjectFilter);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getSubjectList();
		}
	}, [isPermiss]);

	//get list course
	useEffect(() => {
		if (isPermiss) {
			const params = {
				role: '2',
				filters,
			};
			setLoading(true);
			const getCourseList = async () => {
				try {
					const response = await courseApi.getCourseList(params);
					setCourse(response);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getCourseList();
		}
	}, [filters, isPermiss]);

	const handlePageChange = (newPage) => {
		setFilters({
			...filters,
			page: newPage,
		});
	};

	const handleSearchChange = (newSearch) => {
		setFilters({
			...filters,
			page: 1,
			q: newSearch.value,
		});
	};

	const handleFilterChange = (e, filter) => {
		setFilters({
			...filters,
			page: 1,
			[filter]: e.target.value,
		});
	};

	const resetFilter = () => {
		setFilters({
			...filters,
			filter1: '',
			filter2: '',
			page: 1,
		});
	};

	const [keys, setKeys] = useState([
		{
			header: 'Tiêu đề',
			align: 'left',
			field: 'title',
			display: true,
		},
		{
			header: 'Môn học',
			align: 'left',
			field: 'subject_name',
			display: true,
		},
		{
			header: 'Giá',
			align: 'left',
			field: 'course_cost',
			display: true,
		},
		{
			header: 'Giáo viên',
			align: 'left',
			field: 'teacher_name',
			display: true,
		},
		{
			header: 'Số lượng học sinh',
			align: 'right',
			field: 'number_student',
			display: true,
		},
		{
			header: 'Trạng Thái',
			align: 'center',
			field: 'course_status',
			display: true,
		},
		{
			header: 'Số lượng bài học',
			align: 'right',
			field: 'number_lesson',
			display: true,
		},
	]);

	const [columnSelected, setColumnSelected] = React.useState([]);

	const changeDisplayColumn = (key) => {
		key.display = !key.display;
		setKeys(keys);
	};

	const handleColumnChange = (event) => {
		setColumnSelected(event.target.value);
	};

	const handleDeleteCourse = (course_id) => {
		setLoading(true);
		const params = {
			course_id: course_id,
			status: '3',
		};
		const updateCourseFlag = async () => {
			try {
				const response = await courseApi.updateCourseFlag(params);
				setLoading(false);
				notify('success', response.message);
				setFilters({
					...filters,
					delete: '',
				});
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateCourseFlag();
	};

	const handeleRevertCourse = (course_id) => {
		setLoading(true);
		const params = {
			course_id: course_id,
			status: '0',
		};
		const updateCourseFlag = async () => {
			try {
				const response = await courseApi.updateCourseFlag(params);
				setLoading(false);
				notify('success', response.message);
				setFilters({
					...filters,
					delete: '',
				});
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateCourseFlag();
	};

	const exportCourseList = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getCourseListExcel(filters);
				setLoading(false);
				const csvData = {
					data: response,
				};
				exportToCSV(csvData, 'Khoa_Hoc');
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	return (
		<MasterLayout header='Danh sách khóa học'>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Khóa học | Xoài CMS</title>
			</Helmet>
			<Grid container>
				<Grid container style={{ marginBottom: '1rem' }}>
					<Grid item md={8}>
						<SearchForm
							onSubmit={handleSearchChange}
							label={'Tìm theo tác giả hoặc tiêu đề'}
						/>
					</Grid>
					<Grid item md={4} container justify='flex-end'>
						{functions.includes('function_create_course') && (
							<CreatePost path='/coursedetail' text='Tạo khóa học'></CreatePost>
						)}
					</Grid>
				</Grid>

				<RenderTable
					columnSelected={columnSelected}
					handleColumnChange={handleColumnChange}
					changeDisplayColumn={changeDisplayColumn}
					exportExcel={exportCourseList}
					keys={keys}
					pagination={courses.pagination}
					data={courses.data}
					handlePageChange={handlePageChange}
					handleDelete={handleDeleteCourse}
					handleRevert={handeleRevertCourse}
					roleName={'khóa học'}
					filter1={filters.filter1}
					filter2={filters.filter2}
					filter3={filters.filter3}
					subjectList={subjectList}
					handleFilterChange={handleFilterChange}
					resetFilter={resetFilter}
					role={'2'}
					totalRow={totalRow}
					tableName={'courseList'}
					listFunctionTable={functionsTable}
					deleteName={'khóa học'}
					tooltip={'khóa học'}
				/>
			</Grid>
		</MasterLayout>
	);
}
