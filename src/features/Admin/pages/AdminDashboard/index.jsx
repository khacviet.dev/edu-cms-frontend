import { Avatar, Divider, Grid, Paper, Typography } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import AssignmentIcon from '@material-ui/icons/Assignment';
import ClassIcon from '@material-ui/icons/Class';
import GroupIcon from '@material-ui/icons/Group';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import dashboardApi from 'api/dashboardApi';
import MyBarChart from 'common/components/MyBarChart';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';

function AdminDashboard() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();

	const [loading, setLoading] = useState(false);
	const [user, setUser] = useState({});
	const [course, setCourse] = useState({});
	const [classroom, setClassroom] = useState({});
	const [salary, setSalary] = useState({});
	const [revenue, setRevenue] = useState({});

	const handleRouterChange = (url) => {
		history.push(`/${url}`);
	};

	useEffect(() => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await dashboardApi.getAdminDashboard();
				setLoading(false);

				setUser(response.user);
				setCourse(response.course);
				setClassroom(response.class);
				setSalary(response.salaryTeacher);
				setRevenue(response.revenueChart);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		get();
	}, []);

	return (
		<Grid container>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Grid container spacing={3}>
				{user && (
					<Grid
						item
						xs={12}
						md={3}
						lg={3}
						onClick={() => handleRouterChange('student')}
						style={{ cursor: 'pointer' }}
					>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Tổng số người dùng
									</Typography>
									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{user.userThisMonth}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'rgb(229, 57, 53)',
											width: '56px',
											height: '56px',
										}}
									>
										<GroupIcon />
									</Avatar>
								</Grid>
								{user.userCompare &&
									(user.checkUserLastMonth !== '0' ? (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											{user.userCompare.negative ? (
												<ArrowDownwardIcon
													style={{ color: 'rgb(183, 28, 28)' }}
												/>
											) : (
												<ArrowUpwardIcon style={{ color: 'rgb(27, 94, 32)' }} />
											)}
											{user.userCompare.negative ? (
												<Typography style={{ color: 'rgb(183, 28, 28)' }}>
													{user.userCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											) : (
												<Typography style={{ color: 'rgb(27, 94, 32)' }}>
													{user.userCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											)}
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												So với tháng trước
											</Typography>
										</Grid>
									) : (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												Chưa có dữ liệu tháng trước
											</Typography>
										</Grid>
									))}
							</Grid>
						</Paper>
					</Grid>
				)}
				{course && (
					<Grid
						item
						xs={12}
						md={3}
						lg={3}
						onClick={() => handleRouterChange('course')}
						style={{ cursor: 'pointer' }}
					>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Tổng số khóa học
									</Typography>
									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{course.courseThisMonth}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'rgb(67, 160, 71)',
											width: '56px',
											height: '56px',
										}}
									>
										<AssignmentIcon />
									</Avatar>
								</Grid>
								{course.courseCompare &&
									(course.checkCourseLastMonth !== '0' ? (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											{course.courseCompare.negative ? (
												<ArrowDownwardIcon
													style={{ color: 'rgb(183, 28, 28)' }}
												/>
											) : (
												<ArrowUpwardIcon style={{ color: 'rgb(27, 94, 32)' }} />
											)}
											{course.courseCompare.negative ? (
												<Typography style={{ color: 'rgb(183, 28, 28)' }}>
													{course.courseCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											) : (
												<Typography style={{ color: 'rgb(27, 94, 32)' }}>
													{course.courseCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											)}
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												So với tháng trước
											</Typography>
										</Grid>
									) : (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												Chưa có dữ liệu tháng trước
											</Typography>
										</Grid>
									))}
							</Grid>
						</Paper>
					</Grid>
				)}
				{classroom && (
					<Grid
						item
						xs={12}
						md={3}
						lg={3}
						onClick={() => handleRouterChange('class')}
						style={{ cursor: 'pointer' }}
					>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Tổng số lớp học
									</Typography>
									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{classroom.classThisMonth}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'blue',
											width: '56px',
											height: '56px',
										}}
									>
										<ClassIcon />
									</Avatar>
								</Grid>
								{classroom.classCompare &&
									(classroom.checkClassLastMonth !== '0' ? (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											{classroom.classCompare.negative ? (
												<ArrowDownwardIcon
													style={{ color: 'rgb(183, 28, 28)' }}
												/>
											) : (
												<ArrowUpwardIcon style={{ color: 'rgb(27, 94, 32)' }} />
											)}
											{classroom.classCompare.negative ? (
												<Typography style={{ color: 'rgb(183, 28, 28)' }}>
													{classroom.classCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											) : (
												<Typography style={{ color: 'rgb(27, 94, 32)' }}>
													{classroom.classCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											)}
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												So với tháng trước
											</Typography>
										</Grid>
									) : (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												Chưa có dữ liệu tháng trước
											</Typography>
										</Grid>
									))}
							</Grid>
						</Paper>
					</Grid>
				)}
				{salary && (
					<Grid
						item
						xs={12}
						md={3}
						lg={3}
						onClick={() => handleRouterChange('salary')}
						style={{ cursor: 'pointer' }}
					>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Lương GV / trợ giảng
									</Typography>
									{salary.salaryTeacherThisMonth !== undefined && (
										<Typography
											variant='body2'
											style={{
												fontSize: '1.5rem',
												fontWeight: 'bold',
											}}
										>
											{salary.salaryTeacherThisMonth.toLocaleString('vi-VN', {
												style: 'currency',
												currency: 'VND',
											})}
										</Typography>
									)}
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'rgb(251, 140, 0)',
											width: '56px',
											height: '56px',
										}}
									>
										<MonetizationOnIcon />
									</Avatar>
								</Grid>
								{salary.salaryTeacherCompare &&
									(salary.checkSalaryTeacherLastMonth !== '0' ? (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											{salary.salaryTeacherCompare.negative ? (
												<ArrowDownwardIcon
													style={{ color: 'rgb(183, 28, 28)' }}
												/>
											) : (
												<ArrowUpwardIcon style={{ color: 'rgb(27, 94, 32)' }} />
											)}
											{salary.salaryTeacherCompare.negative ? (
												<Typography style={{ color: 'rgb(183, 28, 28)' }}>
													{salary.salaryTeacherCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											) : (
												<Typography style={{ color: 'rgb(27, 94, 32)' }}>
													{salary.salaryTeacherCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											)}
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												So với tháng trước
											</Typography>
										</Grid>
									) : (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												Chưa có dữ liệu tháng trước
											</Typography>
										</Grid>
									))}
							</Grid>
						</Paper>
					</Grid>
				)}
			</Grid>
			<Grid container spacing={3} style={{ marginTop: '1.5rem' }}>
				<Grid item xs={12} md={12} lg={12}>
					<Paper
						style={{
							height: '100%',
						}}
					>
						<Grid container>
							<Grid
								item
								xs={12}
								lg={12}
								md={12}
								style={{ padding: '1rem', cursor: 'pointer' }}
								onClick={() => handleRouterChange('revenue')}
							>
								<Typography variant='body1' style={{ fontWeight: 'bold' }}>
									Doanh thu theo tháng
								</Typography>
							</Grid>
							<Grid item xs={12} lg={12} md={12}>
								<Divider />
							</Grid>
							{revenue && (
								<Grid item xs={12} lg={12} md={12} style={{ padding: '1rem' }}>
									<MyBarChart data={revenue} />
								</Grid>
							)}
						</Grid>
					</Paper>
				</Grid>
			</Grid>
		</Grid>
	);
}

export default AdminDashboard;
