import {
	Box,
	Button,
	Chip,
	Grid,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	TextField,
	Typography,
} from '@material-ui/core';
import centerApi from 'api/centerApi';
import permissionApi from 'api/permissionApi';
import roomApi from 'api/roomApi';
import shiftApi from 'api/shiftApi';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { Link, useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';

const showSchedule = (schedules, slot, room) => {
	const schedule = schedules.find(
		(sl) => sl.room_id === room && sl.slot_id === slot
	);
	return schedule === undefined ? (
		'-'
	) : (
		<Link to={`/class/${schedule.class_id}`}>
			<Chip style={{ cursor: 'pointer' }} label={schedule.class_name}></Chip>
		</Link>
	);
};

function ScheduleCenter() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();

	const [date, setDate] = useState(() => {
		return new Date().toISOString().slice(0, 10);
	});

	const [slots, setSlots] = useState([]);
	const [rooms, setRooms] = useState([]);

	const [loading, setLoading] = useState(false);

	// chi tiết lịch học theo từng ngày
	const [schedules, setSchedules] = useState([]);

	const [isPermiss, setIsPermiss] = useState(false);

	const handleDateChange = (e) => {
		setDate(e.target.value);
	};

	const handleSettingRouterChange = () => {
		history.push('/setting');
	};

	useEffect(() => {
		const post = async () => {
			try {
				setLoading(true);
				const params = {
					functionCode: 'function_schedule',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
				setLoading(false);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await shiftApi.getSlots();
					setSlots(response);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await roomApi.getRooms();
					setRooms(response);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const params = {
						date,
					};
					const response = await centerApi.getScheduleOfCenter(params);

					setSchedules(response);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [date, isPermiss]);

	return (
		<MasterLayout header='Lịch giảng dạy'>
			<Box>
				{loading && (
					<div
						style={{
							position: 'fixed',
							top: '50%',
							left: '50%',
							width: '50px',
							transform: 'translate(-50%, -50%)',
							zIndex: '1000000000',
						}}
						className='cp-spinner cp-balls'
					></div>
				)}
				<Helmet>
					<title>Lịch giảng dạy | Xoài CMS</title>
				</Helmet>
				{slots.length > 0 && rooms.length > 0 ? (
					<Grid container>
						<Grid container style={{ marginBottom: '0.5rem' }}>
							<TextField
								value={date}
								onChange={handleDateChange}
								margin='normal'
								size='small'
								type='date'
								variant='outlined'
							/>
						</Grid>
						<Grid container>
							<TableContainer component={Paper}>
								<Table>
									<TableHead>
										<TableRow>
											<TableCell style={{ width: '10%', fontWeight: 'bold' }}>
												Slot&nbsp;&nbsp;/&nbsp;&nbsp;Phòng
											</TableCell>
											{rooms &&
												rooms.map((room, index) => (
													<TableCell
														key={index}
														style={{ padding: '0.25rem', fontWeight: 'bold' }}
														align='center'
													>
														{room.room_name}
													</TableCell>
												))}
										</TableRow>
									</TableHead>
									<TableBody>
										{slots &&
											slots.map((slot, index) => (
												<TableRow key={index}>
													<TableCell>{slot.slot_name}</TableCell>
													{rooms &&
														rooms.map((room, index1) => (
															<TableCell
																key={index1}
																style={{ padding: '0.25rem' }}
																align='center'
															>
																{showSchedule(
																	schedules,
																	slot.slot_id,
																	room.room_id
																)}
															</TableCell>
														))}
												</TableRow>
											))}
									</TableBody>
								</Table>
							</TableContainer>
						</Grid>
					</Grid>
				) : (
					<Grid container>
						<Grid item md={12} style={{ marginBottom: '1rem' }}>
							<Typography>Bạn chưa thiết lập phòng hoặc slot</Typography>
						</Grid>
						<Grid item md={12}>
							<Button
								variant='contained'
								color='primary'
								onClick={handleSettingRouterChange}
							>
								Thiết lập ngay
							</Button>
						</Grid>
					</Grid>
				)}
			</Box>
		</MasterLayout>
	);
}

export default ScheduleCenter;
