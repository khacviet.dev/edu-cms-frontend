import {
	Box,
	FormControl,
	Grid,
	MenuItem,
	Paper,
	Select,
	Typography,
} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import DragHandleIcon from '@material-ui/icons/DragHandle';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import attendanceApi from 'api/attendanceApi';
import permissionApi from 'api/permissionApi';
import ExportCSV from 'common/components/ExportCSV';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory, useParams } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';

const parseDate = (date) => {
	return (
		`0${new Date(date).getDate()}`.slice('-2') +
		'/' +
		`0${new Date(date).getMonth() + 1}`.slice('-2')
	);
};

const checkAttendance = (attendances, student, date, slot) => {
	const at = attendances.find(
		(attendance) =>
			new Date(attendance.date).getTime() === new Date(date).getTime() &&
			student.user_id === attendance.user_id &&
			attendance.slot_id === slot
	);

	if (at === undefined) {
		return (
			<Grid container justify='center'>
				<NotInterestedIcon color='disabled' />
			</Grid>
		);
	}

	if (at.status === '0') {
		return (
			<Grid container justify='center'>
				<DragHandleIcon />
			</Grid>
		);
	}

	if (at.is_active === '1') {
		return (
			<Grid container justify='center'>
				<CheckIcon style={{ color: 'green' }} />
			</Grid>
		);
	}

	return (
		<Grid container justify='center'>
			<CloseIcon color='error' />
		</Grid>
	);
};

function AttendanceHistory(props) {
	const notify = useContext(ToastifyContext);
	let history = useHistory();
	const { classname } = props.location.state;

	const { classId } = useParams();

	const referer = [
		{
			value: 'class',
			display: 'Lớp học',
		},
		{
			value: `class/${classId}`,
			display: classname,
		},
	];
	const [loading, setLoading] = useState(false);

	const [attendances, setAttendance] = useState([]);
	const [students, setStudents] = useState([]);
	const [scheduleDate, setScheduleDate] = useState([]);

	const [filters, setFilters] = useState({
		month: new Date().getMonth() + 1,
		year: new Date().getFullYear(),
	});

	const handleFilterChange = (e) => {
		setFilters({
			...filters,
			[e.target.name]: e.target.value,
		});
	};

	const [isPermiss, setIsPermiss] = useState(false);
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_attendance_history_all',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await attendanceApi.getDetailAttendanceHistory(
						classId,
						filters
					);
					setLoading(false);

					setAttendance(response.attendances);
					setStudents(response.students);
					setScheduleDate(response.scheduleDate);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, filters]);

	return (
		<MasterLayout referer={referer} header='Lịch sử điểm danh'>
			<Box>
				{loading && (
					<div
						style={{
							position: 'fixed',
							top: '50%',
							left: '50%',
							width: '50px',
							transform: 'translate(-50%, -50%)',
							zIndex: '1000000000',
						}}
						className='cp-spinner cp-balls'
					></div>
				)}
				<Helmet>
					<title>{`Lịch sử điểm danh | ${classname} | Xoài CMS`}</title>
				</Helmet>
				<Grid container spacing={2}>
					<Grid
						item
						xs={12}
						md={12}
						lg={12}
						container
						style={{ marginBottom: '1.5rem' }}
					>
						<Grid item md={6}>
							<Grid container alignItems='center'>
								<Typography>Tháng:</Typography>
								<FormControl
									style={{
										minWidth: '120px',
										marginRight: '2rem',
										marginLeft: '1rem',
									}}
									size='small'
								>
									<Select
										name='month'
										variant='outlined'
										value={filters.month}
										onChange={handleFilterChange}
									>
										<MenuItem value={1}>1</MenuItem>
										<MenuItem value={2}>2</MenuItem>
										<MenuItem value={3}>3</MenuItem>
										<MenuItem value={4}>4</MenuItem>
										<MenuItem value={5}>5</MenuItem>
										<MenuItem value={6}>6</MenuItem>
										<MenuItem value={7}>7</MenuItem>
										<MenuItem value={8}>8</MenuItem>
										<MenuItem value={9}>9</MenuItem>
										<MenuItem value={10}>10</MenuItem>
										<MenuItem value={11}>11</MenuItem>
										<MenuItem value={12}>12</MenuItem>
									</Select>
								</FormControl>
								<Typography>Năm:</Typography>
								<FormControl
									style={{
										minWidth: '120px',
										marginLeft: '1rem',
									}}
									size='small'
								>
									<Select
										name='year'
										variant='outlined'
										value={filters.year}
										onChange={handleFilterChange}
									>
										<MenuItem value={new Date().getFullYear() - 2}>
											{new Date().getFullYear() - 2}
										</MenuItem>
										<MenuItem value={new Date().getFullYear() - 1}>
											{new Date().getFullYear() - 1}
										</MenuItem>
										<MenuItem value={new Date().getFullYear()}>
											{new Date().getFullYear()}
										</MenuItem>
									</Select>
								</FormControl>
							</Grid>
						</Grid>
						<Grid item md={6} container justify='flex-end'>
							<ExportCSV csvData={attendances} fileName='data' />
						</Grid>
					</Grid>

					<Grid container>
						<TableContainer component={Paper}>
							{scheduleDate.length > 0 &&
							students.length > 0 &&
							attendances.length > 0 ? (
								<Table>
									<TableHead>
										<TableRow>
											<TableCell align='center' style={{ fontWeight: 'bold' }}>
												Học sinh&nbsp;&nbsp;/&nbsp;&nbsp;Ngày
											</TableCell>
											{scheduleDate.map((sd, index) => (
												<TableCell key={index} align='center'>
													<Typography
														variant='body2'
														style={{ fontWeight: 'bold' }}
													>
														{parseDate(sd.date)} <br /> ({sd.slot_name})
													</Typography>
												</TableCell>
											))}
										</TableRow>
									</TableHead>
									<TableBody>
										{students.map((st, index) => (
											<TableRow key={index}>
												<TableCell align='center'>{st.full_name}</TableCell>
												{scheduleDate.length > 0 &&
													scheduleDate.map((sd, index) => (
														<TableCell
															key={index}
															style={{ fontWeight: 'bold' }}
															align='center'
														>
															{checkAttendance(
																attendances,
																st,
																sd.date,
																sd.slot_id
															)}
														</TableCell>
													))}
											</TableRow>
										))}
									</TableBody>
								</Table>
							) : (
								<Table>
									<TableHead>
										<TableRow>
											<TableCell align='center'>
												<Typography variant='body1'>
													Chưa có dữ liệu điểm danh
												</Typography>
											</TableCell>
										</TableRow>
									</TableHead>
								</Table>
							)}
						</TableContainer>
					</Grid>
					<Grid container style={{ marginTop: '2rem' }}>
						<Grid
							container
							alignItems='center'
							style={{ marginBottom: '0.75rem' }}
						>
							<Typography style={{ fontWeight: 'bold', fontSize: '1.1rem' }}>
								Chú thích
							</Typography>
						</Grid>
						<Grid
							container
							alignItems='center'
							style={{ marginBottom: '0.5rem' }}
						>
							<NotInterestedIcon
								style={{ marginRight: '1rem' }}
								color='disabled'
							/>
							<Typography>Học sinh chưa tham gia lớp</Typography>
						</Grid>
						<Grid
							container
							alignItems='center'
							style={{ marginBottom: '0.5rem' }}
						>
							<CheckIcon
								style={{ marginRight: '1rem', color: 'green' }}
								color='disabled'
							/>
							<Typography>Có mặt</Typography>
						</Grid>
						<Grid
							container
							alignItems='center'
							style={{ marginBottom: '0.5rem' }}
						>
							<CloseIcon style={{ marginRight: '1rem', color: 'red' }} />
							<Typography>Vắng mặt</Typography>
						</Grid>
						<Grid container alignItems='center'>
							<DragHandleIcon style={{ marginRight: '1rem' }} />
							<Typography>Chưa học</Typography>
						</Grid>
					</Grid>
				</Grid>
			</Box>
		</MasterLayout>
	);
}

export default AttendanceHistory;
