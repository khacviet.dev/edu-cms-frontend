import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import attendanceApi from 'api/attendanceApi';
import excelApi from 'api/excelApi';
import permissionApi from 'api/permissionApi';
import SearchForm from 'common/components/SearchForm';
import MasterLayout from 'common/pages/MasterLayout';
import AttendanceListTable from 'features/Attendance/components/AttendanceListTable';
import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router';
import { exportToCSV } from 'utils/common';

AttendanceList.propTypes = {};

const useStyles = makeStyles({
	formControl: {
		minWidth: 120,
	},
});

const filterList1 = [
	{
		text: 'None',
		value: '',
	},
	{
		text: 'Chưa điểm danh',
		value: '0',
	},
	{
		text: 'Đã điểm danh',
		value: '1',
	},
];

function AttendanceList() {
	let history = useHistory();
	const [isPermiss, setIsPermiss] = React.useState(false);
	const [loading, setLoading] = useState(false);
	const [columnSelected, setColumnSelected] = React.useState([]);
	const [attendances, setAttendaces] = React.useState([]);
	const [functionsTable, setFunctionTable] = useState({
		view: false,
		edit: false,
		review: false,
		revert: false,
	});
	const [totalRow, setTotalRow] = useState();
	const handleColumnChange = (event) => {
		setColumnSelected(event.target.value);
	};
	const [keys, setKeys] = useState([
		{
			header: 'Tên lớp',
			align: 'left',
			field: 'class_name',
			display: true,
		},
		{
			header: 'Phòng',
			align: 'left',
			field: 'room_name',
			display: true,
		},
		{
			header: 'Môn học',
			align: 'left',
			field: 'subject_name',
			display: true,
		},
		{
			header: 'Giáo viên',
			align: 'left',
			field: 'teacher_name',
			display: true,
		},
		{
			header: 'Trợ giảng',
			align: 'left',
			field: 'tutor_name',
			display: true,
		},
		{
			header: 'Thời gian',
			align: 'left',
			field: 'time',
			display: true,
		},
		{
			header: 'Học sinh có mặt',
			align: 'center',
			field: 'total',
			display: true,
		},
		{
			header: 'Trạng thái',
			align: 'left',
			field: 'status',
			display: true,
		},
	]);

	const changeDisplayColumn = (key) => {
		key.display = !key.display;
		setKeys(keys);
	};

	const handlePageChange = (newPage) => {
		setFilters({
			...filters,
			page: newPage,
		});
	};

	const onHandleChangeDetail = (classId, scheduleId) => {
		history.push(`/attendance/${classId}/${scheduleId}`);
	};

	const [filters, setFilters] = useState({
		limit: 5,
		page: 1,
		q: '',
		filter1: '',
		date: new Date().toISOString().slice(0, 10),
	});

	const handleFilterChange = (e, filter) => {
		setFilters({
			...filters,
			page: 1,
			[filter]: e.target.value,
		});
	};

	// set clounm display
	useEffect(() => {
		const value = [];
		for (let i = 0, l = keys.length; i < l; i += 1) {
			value.push(keys[i].header);
		}
		setColumnSelected(value);
	}, []);
	// check quyen
	useEffect(() => {
		setLoading((loading) => !loading);
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_list_attendance',
				};
				await permissionApi.checkPermission(params);
				setLoading((loading) => !loading);
				setIsPermiss(true);
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', `Bạn không có quyền truy cập vào trang này`);
				localStorage.removeItem('menu');
				history.push('/');
			}
		};

		post();
	}, []);

	// get list function
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const getFunctions = async () => {
				try {
					const respone = await permissionApi.getFunctions();
					const a = {
						view: true,
						edit: false,
						delete: false,
						review: false,
					};
					const data = [];
					for (let i = 0; i < respone.length; i++) {
						if (respone[i].function_id == 'function_edit_attendance') {
							a['edit'] = true;
						}
						data.push(respone[i].function_id);
					}

					setFunctionTable(a);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			getFunctions();
		}
	}, [isPermiss]);

	// get list attendance
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const getAttendances = async () => {
				try {
					const respone = await attendanceApi.getAttendances(filters);
					setAttendaces(respone);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
				}
			};
			getAttendances();
		}
	}, [isPermiss, filters]);

	// // get total attendance count
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const getCountAttendance = async () => {
				try {
					const response = await attendanceApi.getCountAttendance(filters);
					setTotalRow(response.data[0].count);
					setLoading((loading) => !loading);
					// setAttendaces(respone);
				} catch (error) {
					setLoading((loading) => !loading);
				}
			};
			getCountAttendance();
		}
	}, [isPermiss, filters]);

	const handleSearchChange = (newSearch) => {
		setFilters({
			...filters,
			page: 1,
			q: newSearch.value,
		});
	};

	const exportAttendanceList = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getAttendanceDailyExcel(filters);
				setLoading(false);
				const csvData = {
					data: response,
				};
				exportToCSV(csvData, `Diem_Danh_Ngay_${filters.date}`);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		if (isPermiss) {
			get();
		}
	};

	return (
		<MasterLayout header='Điểm danh'>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Điểm danh | Xoài CMS</title>
			</Helmet>
			<Grid container>
				<Grid container style={{ marginBottom: '1rem' }}>
					<Grid item md={8}>
						<SearchForm
							onSubmit={handleSearchChange}
							label={'Tìm theo tên lớp và giáo viên'}
						/>
					</Grid>
				</Grid>

				<AttendanceListTable
					handleColumnChange={handleColumnChange}
					handlePageChange={handlePageChange}
					listFunctionTable={functionsTable}
					changeDisplayColumn={changeDisplayColumn}
					columnSelected={columnSelected}
					filterList1={filterList1}
					exportExcel={exportAttendanceList}
					keys={keys}
					data={attendances.data}
					pagination={attendances.pagination}
					totalRow={totalRow}
					tooltip={`điểm danh`}
					filter1={filters.filter1}
					handleFilterChange={handleFilterChange}
				></AttendanceListTable>
			</Grid>
		</MasterLayout>
	);
}

export default AttendanceList;
