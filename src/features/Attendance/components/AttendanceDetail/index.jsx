import {
	Box,
	Button,
	Divider,
	Grid,
	makeStyles,
	Tab,
	Tabs,
} from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Paper from '@material-ui/core/Paper';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import attendanceApi from 'api/attendanceApi';
import permissionApi from 'api/permissionApi';
import salaryApi from 'api/salaryApi';
import React, { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';

const useStyles = makeStyles((theme) => ({
	postPanel: {
		marginTop: '5rem',
		marginBottom: '0.5rem',
	},
	createPostTitle: {
		fontSize: '1.25rem',
		fontWeight: 'bold',
	},
	closeModal: {
		position: 'absolute',
		top: '50%',
		right: '0',
		transform: 'translate(0,-50%)',
	},
	table: {
		minWidth: 650,
	},
}));

function TabPanel(props) {
	const { children, value, index, ...other } = props;

	return (
		<div role='tabpanel' hidden={value !== index} {...other}>
			{value === index && <Box p={1}>{children}</Box>}
		</div>
	);
}

function AttendanceDetail(props) {
	const notify = useContext(ToastifyContext);
	const { classId, scheduleId } = useParams();
	const [value, setValue] = React.useState(0);
	const [students, setStudents] = useState([]);

	const [attendance, setAttendance] = useState({});
	const [isPermiss, setIsPermiss] = React.useState(false);
	const [loading, setLoading] = useState(false);
	const [isUpdate, setIsUpdate] = useState(false);
	let history = useHistory();

	const state = props.location.state;
	const handleChange = (event, newValue) => {
		setValue(newValue);
	};

	const classes = useStyles();
	if (state == undefined) {
	} else {
		// check quyen
		useEffect(() => {
			setLoading((loading) => !loading);
			const post = async () => {
				try {
					const params = {
						functionCode: 'function_list_attendance',
					};
					await permissionApi.checkPermission(params);
					setLoading((loading) => !loading);
					setIsPermiss(true);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', `Bạn không có quyền truy cập vào trang này`);
					localStorage.removeItem('menu');
					history.push('/');
				}
			};

			post();
		}, []);

		// get list attendance student
		useEffect(() => {
			const params = {
				scheduleId: state.scheduleId,
				classId: state.classId,
			};

			if (isPermiss) {
				setLoading((loading) => !loading);
				const getStudentsAttendanceDaily = async () => {
					try {
						const respone = await attendanceApi.getStudentsAttendanceDaily(
							params
						);
						setStudents(respone);
						const object = {};
						for (let i = 0; i < respone.length; i++) {
							object[[respone[i].attendance_id]] = respone[i].is_active;
						}
						setAttendance(object);
						setLoading((loading) => !loading);
					} catch (error) {
						setLoading((loading) => !loading);
					}
				};
				getStudentsAttendanceDaily();
			}
		}, [isPermiss]);

		// check xem la edit hay la diem dnah lan dau
		useEffect(() => {
			if (state.status == 1) {
				setIsUpdate(true);
			} else {
				setIsUpdate(false);
			}
		}, []);
	}

	const handleAttendance = (e, attendance_id) => {
		const present = e.target.value;
		setAttendance({
			...attendance,
			[attendance_id]: present,
		});
	};

	const handleSubmitAttendance = () => {
		const data = {
			attendance: attendance,
			schedule_id: students[0].schedule_id,
			class_id: students[0].class_id,
		};

		setLoading((loading) => !loading);
		const updateAttendanceDaily = async () => {
			try {
				const respone = await attendanceApi.updateAttendanceDaily(data);
				notify('success', respone.message);

				if (state == undefined) {
					return;
				} else {
					const data_1 = {
						schedule_id: students[0].schedule_id,
						class_id: students[0].class_id,
						teacher_id: state.teacher_id,
						tutor_id: state.tutor_id,
						is_update: isUpdate,
					};
					setLoading((loading) => !loading);
					const caculateScheduleSalary = async () => {
						try {
							const respone = await salaryApi.caculateScheduleSalary(data_1);
							// notify('success', respone.message);
							setLoading((loading) => !loading);
							setIsUpdate(true);
						} catch (error) {
							notify('error', error.response.data.message);
							setLoading((loading) => !loading);
						}
					};
					caculateScheduleSalary();
				}

				setLoading((loading) => !loading);
			} catch (error) {
				notify('error', error.response.data.message);
				setLoading((loading) => !loading);
			}
		};
		updateAttendanceDaily();
	};

	const handleBack = () => {
		history.push({
			pathname: `/attendance`,
		});
	};

	return (
		<Box style={{ padding: '1rem 2rem' }}>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Grid container spacing={2}>
				<Grid item xs={12} md={12} lg={12} container>
					<Tabs
						value={value}
						onChange={handleChange}
						indicatorColor='primary'
						textColor='primary'
						centered
					>
						<Tab label='Học sinh' />
					</Tabs>
				</Grid>
				<Grid item xs={12} md={12} lg={12}>
					<Divider />
				</Grid>
				<Grid container>
					<TabPanel value={value} index={0} style={{ width: '100%' }}>
						<Grid container>
							<Grid container>
								<TableContainer component={Paper}>
									<Table className={classes.table}>
										<TableHead>
											<TableRow>
												<TableCell align='center'>Ảnh</TableCell>
												<TableCell align='center'>Mã học sinh</TableCell>
												<TableCell align='center'>Họ và tên</TableCell>
												<TableCell align='center'>Số điện thoại</TableCell>
												<TableCell align='center'>Trạng thái</TableCell>
											</TableRow>
										</TableHead>
										<TableBody>
											{students.length > 0 &&
												students.map((student, index) => (
													<TableRow key={index}>
														<TableCell>
															<Grid container justify='center'>
																<img
																	loading='lazy'
																	style={{ width: '5.5rem', height: 'auto' }}
																	src={student.avt_link}
																	alt='avatar'
																/>
															</Grid>
														</TableCell>
														<TableCell align='center'>
															{student.user_id}
														</TableCell>
														<TableCell align='center'>
															{student.full_name}
														</TableCell>
														<TableCell align='center'>
															{student.phone}
														</TableCell>

														<TableCell align='center'>
															<Grid container justify='center'>
																<FormControl component='fieldset'>
																	<RadioGroup
																		row
																		name={student.user_id}
																		onChange={(e) =>
																			handleAttendance(e, student.attendance_id)
																		}
																		defaultValue={student.is_active}
																	>
																		<FormControlLabel
																			value='0'
																			control={<Radio color='secondary' />}
																			label='Vắng mặt'
																			labelPlacement='start'
																		/>
																		<FormControlLabel
																			value='1'
																			control={
																				<Radio style={{ color: 'green' }} />
																			}
																			label='Có mặt'
																			labelPlacement='start'
																		/>
																		{student.role_id == '4' && (
																			<FormControlLabel
																				value='2'
																				control={
																					<Radio style={{ color: 'blue' }} />
																				}
																				label='Đứng lớp'
																				labelPlacement='start'
																			/>
																		)}
																	</RadioGroup>
																</FormControl>
															</Grid>
														</TableCell>
													</TableRow>
												))}
										</TableBody>
									</Table>
								</TableContainer>
							</Grid>
							<Grid
								container
								justify='flex-end'
								style={{ margin: '4rem 0 1rem 0' }}
							>
								<Button
									onClick={handleBack}
									color='secondary'
									variant='contained'
									style={{ marginRight: '1rem' }}
								>
									Trở về màn điểm danh
								</Button>
								<Button
									variant='contained'
									onClick={handleSubmitAttendance}
									color='primary'
								>
									Hoàn thành
								</Button>
							</Grid>
						</Grid>
					</TabPanel>
				</Grid>
			</Grid>
		</Box>
	);
}

export default AttendanceDetail;
