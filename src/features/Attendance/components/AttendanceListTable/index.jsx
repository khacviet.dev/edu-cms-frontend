import {
	Button,
	Checkbox,
	FormControl,
	Grid,
	Input,
	InputLabel,
	ListItemText,
	MenuItem,
	Select,
	TextField,
	Typography,
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import EditIcon from '@material-ui/icons/Edit';
import GetAppIcon from '@material-ui/icons/GetApp';
import Pagination from 'common/components/Pagination';
import React, { useContext } from 'react';
import { useHistory } from 'react-router';
import { ToastifyContext } from 'utils/ToastifyConfig';
import './style.css';

const useStyles = makeStyles({
	table: {
		minWidth: 650,
	},
	formControl: {
		minWidth: 120,
	},
});

const LightTooltip = withStyles((theme) => ({
	tooltip: {
		backgroundColor: theme.palette.common.black,
		color: 'white',
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
		},
	},
};

export default function AttendanceListTable(props) {
	const {
		listFunctionTable,
		columnSelected,
		handleColumnChange,
		changeDisplayColumn,
		keys,
		data,
		totalRow,
		pagination,
		handlePageChange,
		tooltip,
		filterList1,
		filter1,
		handleFilterChange,
		exportExcel,
	} = props;

	if (pagination != undefined) {
		pagination.totalRows = totalRow;
	}
	const notify = useContext(ToastifyContext);

	let history = useHistory();

	const handleFilterChangeDate = (e, date) => {
		var d1 = new Date();
		var d2 = new Date(e.target.value);
		if (d1 < d2) {
			notify('error', 'Vui lòng chọn ngày hiện tại hoặc quá khứ');
		} else {
			handleFilterChange(e, date);
		}
	};

	const handleEdit = (
		scheduleId,
		classId,
		time,
		date,
		status,
		teacher_id,
		tutor_id
	) => {
		if (time) {
			const now = new Date().setHours(1, 0, 0, 0);
			const class_date = new Date(date).setHours(1, 0, 0, 0);

			if (now < class_date) {
				notify('error', 'Lớp học chưa đến thời gian bắt đầu');
			} else if (now > class_date) {
				history.push({
					pathname: `/attendancedetail`,
					state: {
						scheduleId: scheduleId,
						classId: classId,
						status: status,
						teacher_id: teacher_id,
						tutor_id: tutor_id == null ? '' : tutor_id,
					},
				});
			} else {
				const class_start_time = time.split('-');
				const current_time = new Date();
				const hour = current_time.getHours();
				const minus = current_time.getMinutes();
				const class_time = class_start_time[0].split(':');
				const class_hour = class_time[0];
				const class_minus = class_time[1];

				if (hour > class_hour || (hour == class_hour && minus > class_minus)) {
					history.push({
						pathname: `/attendancedetail`,
						state: {
							scheduleId: scheduleId,
							classId: classId,
							status: status,
							teacher_id: teacher_id,
							tutor_id: tutor_id == null ? '' : tutor_id,
						},
					});
				} else {
					notify('error', 'Lớp học chưa đến thời gian bắt đầu');
				}
			}
		} else {
			notify('error', 'Không lấy được thông tin lớp học');
		}
	};

	const classes = useStyles();
	return (
		<Grid container>
			<Grid container alignItems='center'>
				<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
					<form className={classes.container} noValidate>
						<TextField
							id='date'
							label='Date'
							type='date'
							inputProps={{
								max: new Date().toISOString().split('T')[0],
							}}
							onChange={(e) => handleFilterChangeDate(e, 'date')}
							defaultValue={new Date().toISOString().slice(0, 10)}
							className={classes.textField}
							InputLabelProps={{
								shrink: true,
							}}
						/>
					</form>
				</Grid>

				<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
					<FormControl className={classes.formControl}>
						<InputLabel>Trạng thái</InputLabel>
						<Select
							value={filter1}
							onChange={(e) => handleFilterChange(e, 'filter1')}
						>
							{filterList1.length > 0 &&
								filterList1.map((filter, index) => (
									<MenuItem key={index} value={filter.value}>
										{filter.text}
									</MenuItem>
								))}
						</Select>
					</FormControl>
				</Grid>

				<Grid item md={2} style={{ marginBottom: '0.75rem' }}></Grid>
				<Grid item md={2} style={{ marginBottom: '0.75rem' }}></Grid>
				<Grid item md={2} container justify='flex-end'>
					{/* {listFunctionTable['view'] == true && (
						<ExportCSV csvData={data} fileName='data' />
					)} */}
					{listFunctionTable['view'] == true && (
						<Button
							startIcon={<GetAppIcon />}
							variant='text'
							onClick={exportExcel}
						>
							Xuất file
						</Button>
					)}
				</Grid>

				<Grid item md={2} container justify='flex-end'>
					<Select
						multiple
						value={columnSelected}
						onChange={handleColumnChange}
						input={<Input />}
						renderValue={() => 'Chọn cột hiển thị'}
						MenuProps={MenuProps}
					>
						{keys.map((key, index) => (
							<MenuItem
								onClick={() => changeDisplayColumn(key)}
								key={index}
								value={key.header}
							>
								<Checkbox checked={columnSelected.indexOf(key.header) > -1} />
								<ListItemText primary={key.header} />
							</MenuItem>
						))}
					</Select>
				</Grid>
			</Grid>

			<TableContainer component={Paper} style={{ marginTop: '1rem' }}>
				<Table className={classes.table} stickyHeader>
					<TableHead>
						<TableRow>
							<TableCell style={{ width: '5%' }}>ID</TableCell>
							{keys.map((key, index) => (
								<TableCell
									style={{ display: key.display ? '' : 'none' }}
									key={index}
								>
									{key.header}
								</TableCell>
							))}
							{listFunctionTable['edit'] && (
								<TableCell style={{ width: '5%' }}></TableCell>
							)}
						</TableRow>
					</TableHead>
					<TableBody>
						{data && data.length > 0 ? (
							data.map((row, index) => (
								<TableRow hover key={index}>
									<TableCell>{row['schedule_id']}</TableCell>
									{keys.map((key, index) => (
										<TableCell
											style={{
												display: key.display ? '' : 'none',
												color:
													key.field == 'status' &&
													(row[key.field] == '0' || row[key.field] == '3')
														? 'red'
														: row[key.field] == '1'
														? 'green'
														: '',

												maxWidth: '200px',
												overflow: 'hidden',
												textOverflow: 'ellipsis',
											}}
											align={key.align}
											key={index}
										>
											{(key.field == 'status' &&
												(((row[key.field] == '0' || row[key.field] == '3') &&
													'Chưa điểm danh') ||
													(row[key.field] == '1' && 'Đã điểm danh'))) ||
												row[key.field]}
										</TableCell>
									))}

									{listFunctionTable['edit'] && (
										<TableCell>
											<LightTooltip
												placement='top'
												title={`Chỉnh sửa ${tooltip}`}
											>
												<EditIcon
													onClick={() =>
														handleEdit(
															row['schedule_id'],
															row['class_id'],
															row['time'],
															row['date'],
															row['status'],
															row['teacher_id'],
															row['tutor_id']
														)
													}
													color='action'
													style={{ cursor: 'pointer' }}
												/>
											</LightTooltip>
										</TableCell>
									)}
								</TableRow>
							))
						) : (
							<TableRow>
								<TableCell align='center' colSpan={keys.length + 3}>
									Không tìm thấy dữ liệu
								</TableCell>
							</TableRow>
						)}
					</TableBody>
				</Table>
			</TableContainer>

			{data && (
				<Grid container alignItems='center' style={{ marginTop: '2rem' }}>
					<Grid item md={6}>
						<Typography style={{ fontWeight: 'bold' }}>
							Tổng số ({pagination.totalRows})
						</Typography>
					</Grid>
					<Grid item md={6}>
						<Pagination
							pagination={pagination}
							onPageChange={handlePageChange}
						/>
					</Grid>
				</Grid>
			)}
		</Grid>
	);
}
