import { Box, Grid, Paper, Tab, Tabs } from '@material-ui/core';
import courseApi from 'api/courseApi';
import excelApi from 'api/excelApi';
import feedBackApi from 'api/feedbackApi';
import permissionApi from 'api/permissionApi';
import CommonTableT from 'common/components/CommonTableT';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { exportToCSV } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

feedBackList.propTypes = {};

const id = 'user_id';

const filterList1 = [];
const listFilterLabel = ['Lớp học', 'Giáo viên'];

const listFilterCourseLabel = ['Khóa học', 'Giáo viên'];

const filterList2 = [];

const filterList3 = [];

const listFunction = {
	create: false,
	edit: true,
	delete: true,
	export: true,
	filter: 1,
};

function TabPanel(props) {
	const { children, value, index, ...other } = props;

	return (
		<div role='tabpanel' hidden={value !== index} {...other}>
			{value === index && <Box p={1}>{children}</Box>}
		</div>
	);
}

const RenderTable = CommonTableT(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id,
	listFunction
);

const RenderTable1 = CommonTableT(
	filterList1,
	filterList2,
	filterList3,
	listFilterCourseLabel,
	id,
	listFunction
);

export default function feedBackList() {
	const [loading, setLoading] = useState(false);
	const notify = useContext(ToastifyContext);
	const [feedBacks, setFeedBacks] = useState([]);
	const [feedBacksCourse, setFeedBacksCourse] = useState([]);
	const [totalRow, setTotalRow] = useState();
	const [totalRowCourse, setTotalRowCourse] = useState();
	const [value, setValue] = useState(0);
	const [teacherList, setTeacherList] = useState([]);
	const [classList, setClassList] = useState([]);
	const [courseList, setCourseList] = useState([]);
	const [filters, setFilters] = useState({
		limit: 5,
		page: 1,
		q: '',
		filter1: '',
		filter2: '',
		filter3: '',
	});

	const [functionsTable, setFunctionTable] = useState({
		view: false,
		edit: false,
		review: false,
		revert: false,
	});

	const handleChange = (event, newValue) => {
		setFilters({
			...filters,
			filter1: '',
			filter2: '',
			page: 1,
		});
		setValue(newValue);
	};

	const handlePageChange = (newPage) => {
		setFilters({
			...filters,
			page: newPage,
		});
	};

	const handleSearchChange = (newSearch) => {
		setFilters({
			...filters,
			page: 1,
			q: newSearch.value,
		});
	};

	const handleFilterChange = (e, filter) => {
		setFilters({
			...filters,
			page: 1,
			[filter]: e.target.value,
		});
	};

	const resetFilter = () => {
		setFilters({
			...filters,
			filter1: '',
			filter2: '',
			page: 1,
		});
	};

	const [keys, setKeys] = useState([
		{
			header: 'Học sinh',
			align: 'left',
			field: 'full_name',
			display: true,
		},
		{
			header: 'Lớp',
			align: 'left',
			field: 'class_name',
			display: true,
		},
		{
			header: 'Giáo viên',
			align: 'left',
			field: 'teacher_name',
			display: true,
		},
		{
			header: 'Nội dung',
			align: 'left',
			field: 'feedback',
			display: true,
		},
		{
			header: 'Ngày tạo',
			align: 'left',
			field: 'date',
			display: true,
		},
		{
			header: 'Trạng thái',
			align: 'left',
			field: 'feedback_status',
			display: true,
		},
	]);

	const [columnSelected, setColumnSelected] = React.useState([]);

	const changeDisplayColumn = (key) => {
		key.display = !key.display;
		setKeys(keys);
	};

	const handleColumnChange = (event) => {
		setColumnSelected(event.target.value);
	};

	const handleChangeFeedBackStatus = (feedback_id, status) => {
		setLoading(true);
		const updateFeedBackStatus = async () => {
			try {
				const data = {
					feedback_id: feedback_id,
					status: status,
				};

				const response = await feedBackApi.updateFeedBackStatus(data);
				// setTotalRow(response.data[0].count);
				setLoading(false);
				notify('success', response.message);
				setFilters({
					...filters,
					delete: '',
				});
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};
		updateFeedBackStatus();
	};

	// phân quyền
	const [functions, setFunctions] = useState([]);
	const [isPermiss, setIsPermiss] = useState(false);
	// check permission
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_feedback_list',
				};
				await permissionApi.checkPermission(params);
				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				if ((error.response.status = 403)) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			}
		};

		post();
	}, []);

	// get list function
	useEffect(() => {
		if (isPermiss) {
			const getFunctions = async () => {
				try {
					const respone = await permissionApi.getFunctions();
					const data = [];
					const a = {
						view: true,
						edit: false,
						delete: false,
						review: false,
					};
					for (let i = 0; i < respone.length; i++) {
						if (respone[i].function_id == 'function_feedback_list') {
							a['edit'] = true;
						}
						data.push(respone[i].function_id);
					}
					setFunctionTable(a);
					setFunctions(data);
				} catch (error) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			getFunctions();
		}
	}, [isPermiss]);

	useEffect(() => {
		const value = [];
		for (let i = 0, l = keys.length; i < l; i += 1) {
			value.push(keys[i].header);
		}
		setColumnSelected(value);
	}, []);

	// get total record class
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);
			const params = {
				filters,
			};
			const getCountFeedBackClass = async () => {
				try {
					const response = await feedBackApi.getCountFeedBackClass(params);
					setTotalRow(response.data[0].count);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getCountFeedBackClass();
		}
	}, [filters, isPermiss]);

	// get feedback class offline
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);
			const params = {
				filters,
			};
			const getFeedBackClass = async () => {
				try {
					const response = await feedBackApi.getFeedBackClass(params);
					setFeedBacks(response);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getFeedBackClass();
		}
	}, [filters, isPermiss]);

	// get total record course
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);
			const params = {
				filters,
			};
			const getCountFeedBackCourse = async () => {
				try {
					const response = await feedBackApi.getCountFeedBackCourse(params);
					setTotalRowCourse(response.data[0].count);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getCountFeedBackCourse();
		}
	}, [filters, isPermiss]);
	// get feedback class online
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);
			const params = {
				filters,
			};
			const getFeedBackCourse = async () => {
				try {
					const response = await feedBackApi.getFeedBackCourse(params);
					setFeedBacksCourse(response);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getFeedBackCourse();
		}
	}, [filters, isPermiss]);

	//get filter teacher
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);

			const getTeacherList = async () => {
				try {
					const response = await courseApi.getTeacherList();
					setTeacherList(response.data);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getTeacherList();
		}
	}, [isPermiss]);

	// get filter class
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);

			const getClassesSelect = async () => {
				try {
					const response = await feedBackApi.getClassesSelect();
					// se(response.data);
					setClassList(response);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getClassesSelect();
		}
	}, [isPermiss]);

	// get filter course
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);

			const getCourseSelect = async () => {
				try {
					const response = await feedBackApi.getCourseSelect();
					// se(response.data);
					setCourseList(response);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getCourseSelect();
		}
	}, [isPermiss]);

	const exportFeedBackOffline = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getFeedBackClassExcel(filters);
				setLoading(false);
				const csvData = {
					data: response,
				};
				exportToCSV(csvData, 'Phan_Hoi_Lop_Offline');
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	const exportFeedBackOnline = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getFeedBackCourseExcel(filters);
				setLoading(false);
				const csvData = {
					data: response,
				};
				exportToCSV(csvData, 'Phan_Hoi_Lop_Offline');
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	return (
		<MasterLayout header='Phản hồi'>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Feedback | Xoài CMS</title>
			</Helmet>
			<Paper>
				<Grid
					item
					xs={12}
					md={12}
					lg={12}
					container
					style={{ position: 'relative' }}
				>
					<Tabs
						value={value}
						onChange={handleChange}
						indicatorColor='primary'
						textColor='primary'
						centered
					>
						<Tab label='Tại trung tâm' />
						<Tab label='Trực tuyến' />
					</Tabs>
				</Grid>

				<Grid container>
					<TabPanel value={value} index={0} style={{ width: '100%' }}>
						<RenderTable
							columnSelected={columnSelected}
							handleColumnChange={handleColumnChange}
							changeDisplayColumn={changeDisplayColumn}
							handleChangeFeedBackStatus={handleChangeFeedBackStatus}
							keys={keys}
							exportExcel={exportFeedBackOffline}
							pagination={feedBacks.pagination}
							data={feedBacks.data}
							handlePageChange={handlePageChange}
							roleName={'feedback'}
							filter1={filters.filter1}
							filter2={filters.filter2}
							listClass={classList}
							listTeacher={teacherList}
							handleFilterChange={handleFilterChange}
							resetFilter={resetFilter}
							role={'2'}
							totalRow={totalRow}
							tableName={'feedback_class'}
							listFunctionTable={functionsTable}
							deleteName={'feedback'}
							tooltip={'feedback'}
						/>
					</TabPanel>

					<TabPanel value={value} index={1} style={{ width: '100%' }}>
						<RenderTable1
							columnSelected={columnSelected}
							handleColumnChange={handleColumnChange}
							changeDisplayColumn={changeDisplayColumn}
							handleChangeFeedBackStatus={handleChangeFeedBackStatus}
							exportExcel={exportFeedBackOnline}
							keys={keys}
							pagination={feedBacksCourse.pagination}
							data={feedBacksCourse.data}
							handlePageChange={handlePageChange}
							roleName={'feedback'}
							filter1={filters.filter1}
							filter2={filters.filter2}
							listCourse={courseList}
							listTeacher={teacherList}
							handleFilterChange={handleFilterChange}
							resetFilter={resetFilter}
							role={'2'}
							totalRow={totalRowCourse}
							tableName={'feedback_course'}
							listFunctionTable={functionsTable}
							deleteName={'feedback'}
							tooltip={'feedback'}
						/>
					</TabPanel>
				</Grid>
			</Paper>
		</MasterLayout>
	);
}
