import {
	Box,
	Divider,
	Grid,
	makeStyles,
	Paper,
	Tab,
	Tabs,
} from '@material-ui/core';
import excelApi from 'api/excelApi';
import permissionApi from 'api/permissionApi';
import salaryApi from 'api/salaryApi';
import MasterLayout from 'common/pages/MasterLayout';
import CommonTableT2 from 'features/Salary/components/CommonTableT2';
import { Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { exportToCSV } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as Yup from 'yup';

const useStyles = makeStyles((theme) => ({
	modal: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%,-50%)',
		padding: theme.spacing(2),
		width: '100vw',
		height: '100vh',
		outline: 0,
		overflowY: 'auto',
	},
	postPanel: {
		marginTop: '5rem',
		marginBottom: '0.5rem',
	},
	createPostTitle: {
		fontSize: '1.25rem',
		fontWeight: 'bold',
	},
	closeModal: {
		position: 'absolute',
		top: '50%',
		right: '0',
		transform: 'translate(0,-50%)',
	},
}));

function TabPanel(props) {
	const { children, value, index, ...other } = props;

	return (
		<div role='tabpanel' hidden={value !== index} {...other}>
			{value === index && <Box p={1}>{children}</Box>}
		</div>
	);
}

SalaryDetail.propTypes = {};
const id = 'user_id';

const filterList1 = [
	{
		text: '01',
		value: '01',
	},
	{
		text: '02',
		value: '02',
	},
	{
		text: '03',
		value: '03',
	},
	{
		text: '04',
		value: '04',
	},
	{
		text: '05',
		value: '05',
	},
	{
		text: '06',
		value: '06',
	},
	{
		text: '07',
		value: '07',
	},
	{
		text: '08',
		value: '08',
	},
	{
		text: '09',
		value: '09',
	},
	{
		text: '10',
		value: '10',
	},
	{
		text: '11',
		value: '11',
	},
	{
		text: '12',
		value: '12',
	},
];

const filterList2 = [
	{
		text: '2021',
		value: '2021',
	},
	{
		text: '2022',
		value: '2022',
	},
];

const filterList3 = [
	{
		text: 'Chưa thanh toán',
		value: '0',
	},
	{
		text: 'Đã thanh toán',
		value: '1',
	},
];

const listFilterLabel = ['Tháng', 'Năm', 'Trạng thái'];

const listFunction = {
	create: false,
	edit: false,
	delete: false,
	export: true,
	filter: 0,
};

const RenderTable = CommonTableT2(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id,
	listFunction
);

const RenderTable2 = CommonTableT2(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id,
	listFunction
);

export default function SalaryDetail(props) {
	const notify = useContext(ToastifyContext);

	const [value, setValue] = useState(0);
	const [loading, setLoading] = useState(false);

	const [totalRow, setTotalRow] = useState();
	const [detailSalary, setDetailSalary] = useState();
	const [detailSalaryCourse, setDetailSalaryCourse] = useState();
	const [totalRowCourse, setTotalRowCourse] = useState();

	const [reloadClassSalary, setReloadClassSalary] = useState(false);

	const [reloadTable, setReloadTable] = useState(false);
	const history = useHistory();

	if (props.location.state == undefined) {
		history.push('/');
		notify('error', 'Bạn không có quyền truy cập màn này');
		return false;
	}

	const [filters, setFilters] = useState({
		limit: 5,
		page: 1,
		q: '',
		filter1: props.location.state.month,
		filter2: props.location.state.year,
		filter3: '',
		filter4: props.location.state.user_id,
	});

	const [functionsTable, setFunctionTable] = useState({
		view: false,
		edit: false,
		review: false,
		revert: false,
	});

	// Excel offline
	const exportSalaryListOff = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getDetailSalaryListExcel(filters);
				const csvData = {};
				const totalData = [];

				for (let i = 0; i < response.length; i++) {
					const { list_detail, ...newObj } = response[i];
					totalData.push(newObj);
					csvData[response[i]['Tên lớp']] = response[i].list_detail;
				}
				const finalCSV = { 'Tổng lương': totalData, ...csvData };
				setLoading(false);

				exportToCSV(
					finalCSV,
					`Bang_Luong_Offline_${props.location.state.user_name}`
				);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		get();
	};

	// Online excel getSalaryCourseExcel
	const exportSalaryListOn = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getSalaryCourseExcel(filters);
				const csvData = {
					['Lương online']: response,
				};

				setLoading(false);

				exportToCSV(
					csvData,
					`Bang_Luong_Online_${props.location.state.user_name}`
				);
			} catch (error) {
				setLoading(false);
				// notify('error', error.response.data.message);
				// if (error.response.status === 403) {
				// 	history.push('/');
				// }
			}
		};
		get();
	};

	const handlePageChange = (newPage) => {
		setFilters({
			...filters,
			page: newPage,
		});
	};

	const handleSearchChange = (newSearch) => {
		setFilters({
			...filters,
			page: 1,
			q: newSearch.value,
		});
	};

	const handleFilterChange = (e, filter) => {
		setFilters({
			...filters,
			page: 1,
			[filter]: e.target.value,
		});
	};

	const resetFilter = () => {
		setFilters({
			...filters,
			filter1: '',
			filter2: '',
			filter3: '',
		});
	};

	const [keys, setKeys] = useState([
		{
			header: 'Lớp đang dạy',
			align: 'left',
			field: 'class_name',
			display: true,
		},
		{
			header: 'Số tiền nhận được',
			align: 'left',
			field: 'pre_money',
			display: true,
		},
		{
			header: 'Tiền thưởng',
			align: 'left',
			field: 'bonus',
			display: true,
		},
		{
			header: 'Tổng nhận',
			align: 'left',
			field: '',
			display: true,
		},
		{
			header: 'Trạng Thái',
			align: 'left',
			field: 'status',
			display: true,
		},
	]);

	const [keysCourse, setKeysCourse] = useState([
		{
			header: 'Khoá học',
			align: 'left',
			field: 'course_name',
			display: true,
		},
		{
			header: 'Tổng số bán được',
			align: 'left',
			field: 'total_course_sold',
			display: true,
		},
		{
			header: 'Số tiền nhận được',
			align: 'left',
			field: 'total',
			display: true,
		},
		{
			header: 'Trạng Thái',
			align: 'left',
			field: 'status',
			display: true,
		},
	]);

	const [columnSelected, setColumnSelected] = React.useState([]);

	const changeDisplayColumn = (key) => {
		key.display = !key.display;
		setKeys(keys);
	};

	const handleColumnChange = (event) => {
		setColumnSelected(event.target.value);
	};

	const handleUpdate = (data) => {
		setLoading((loading) => !loading);
		setReloadTable((loading) => !loading);
		const updateSalary = async () => {
			try {
				const response = await salaryApi.updateSalary(data);
				setReloadClassSalary((loading) => !loading);
				setTimeout(() => {
					setLoading((loading) => !loading);
					setReloadTable((loading) => !loading);
					notify('success', response.message);
				}, 2000);
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', 'Something Wrong Please Try Again');
			}
		};
		updateSalary();
	};

	useEffect(() => {
		const value = [];
		for (let i = 0, l = keys.length; i < l; i += 1) {
			value.push(keys[i].header);
		}
		setColumnSelected(value);
	}, []);

	// get list function
	useEffect(() => {
		const getFunctions = async () => {
			try {
				const respone = await permissionApi.getFunctions();
				const a = {
					view: true,
					edit: false,
					delete: false,
					review: false,
				};
				const data = [];
				for (let i = 0; i < respone.length; i++) {
					if (respone[i].function_id == 'function_edit_salary') {
						a['edit'] = true;
					}
					data.push(respone[i].function_id);
				}

				setFunctionTable(a);
			} catch (error) {
				localStorage.removeItem('menu');
				history.push('/');
			}
		};
		getFunctions();
	}, []);

	// get total record offline by user
	useEffect(() => {
		const params = {
			role: '2',
			filters,
		};
		setLoading((loading) => !loading);
		const getTotalBillByUser = async () => {
			try {
				const response = await salaryApi.getTotalBillByUser(params);
				setTotalRow(response.data[0].count);
				setLoading((loading) => !loading);
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		getTotalBillByUser();
	}, [filters]);

	// get total record online by user
	useEffect(() => {
		const params = {
			role: '2',
			filters,
		};
		setLoading((loading) => !loading);
		const getTotalBillOnlineByUser = async () => {
			try {
				const response = await salaryApi.getTotalBillOnlineByUser(params);
				setTotalRowCourse(response.data[0].count);
				setLoading((loading) => !loading);
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		getTotalBillOnlineByUser();
	}, [filters]);

	/// get detail salary off test
	useEffect(() => {
		setLoading((loading) => !loading);
		const params = {
			role: '2',
			filters,
		};
		const getDetailSalaryList = async () => {
			try {
				const response = await salaryApi.getDetailSalaryList(params);

				setDetailSalary(response);
				setLoading((loading) => !loading);
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		getDetailSalaryList();
	}, [filters, reloadClassSalary]);

	// get detail salary on
	useEffect(() => {
		setLoading((loading) => !loading);
		const params = {
			role: '2',
			filters,
		};
		const getSalaryCourse = async () => {
			try {
				const response = await salaryApi.getSalaryCourse(params);
				setDetailSalaryCourse(response);
				setLoading((loading) => !loading);
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		getSalaryCourse();
	}, [filters, reloadClassSalary]);

	const handleChange = (event, newValue) => {
		setFilters({
			...filters,
			page: 1,
		});
		setValue(newValue);
	};
	const classes = useStyles();

	//update course salary
	const handleUpdateCourse = (data) => {
		setLoading((loading) => !loading);
		setReloadTable((loading) => !loading);
		const updateSalaryCourse = async () => {
			try {
				const response = await salaryApi.updateSalaryCourse(data);
				setReloadClassSalary((loading) => !loading);

				setTimeout(() => {
					setLoading((loading) => !loading);
					setReloadTable((loading) => !loading);
					notify('success', response.message);
				}, 2000);
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		updateSalaryCourse();
	};

	const initialValues = {};

	const validationShema = Yup.object().shape({});

	const referer = [
		{
			value: 'salary',
			display: 'Bảng lương',
		},
	];

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationShema}
			onSubmit={(values, { resetForm }) => {}}
		>
			{(formikProps) => {
				return (
					<MasterLayout
						header={props.location.state.user_name}
						referer={referer}
					>
						<Grid container spacing={2}>
							{loading && (
								<div
									style={{
										position: 'fixed',
										top: '50%',
										left: '50%',
										width: '50px',
										transform: 'translate(-50%, -50%)',
										zIndex: '1000000000',
									}}
									className='cp-spinner cp-balls'
								></div>
							)}
							<Helmet>
								<title>{`${props.location.state.user_name} | Bảng lương | Xoài CMS`}</title>
							</Helmet>
							<Grid container>
								{/* <Paper>
									<Grid container spacing={2}>
										<Grid
											item
											xs={12}
											md={12}
											lg={12}
											container
											style={{ position: 'relative' }}
										>
											<Grid container alignItems='center'>
												<InfoIcon></InfoIcon>
												<Typography
													style={{ fontWeight: 'bold', fontSize: '1.25rem' }}
												>
													&nbsp;Thông tin nhân viên
												</Typography>
											</Grid>

											<Grid container alignItems='center'>
												
												<Typography
												
												>
													&nbsp;Tên
												</Typography>
											</Grid>
										</Grid>
									</Grid>
								</Paper> */}

								<Paper>
									<Grid container spacing={2}>
										<Grid
											item
											xs={12}
											md={12}
											lg={12}
											container
											style={{ position: 'relative' }}
										>
											<Tabs
												value={value}
												onChange={handleChange}
												indicatorColor='primary'
												textColor='primary'
												centered
											>
												<Tab label='Tại trung tâm' />
												<Tab label='Trực tuyến' />
											</Tabs>
										</Grid>
										<Grid item xs={12} md={12} lg={12}>
											<Divider />
										</Grid>
										<Grid container style={{ padding: '1rem' }}>
											<TabPanel
												value={value}
												index={0}
												style={{ width: '100%' }}
											>
												<Grid container>
													<Grid container>
														{detailSalary && (
															<RenderTable
																columnSelected={columnSelected}
																handleColumnChange={handleColumnChange}
																changeDisplayColumn={changeDisplayColumn}
																keys={keys}
																pagination={detailSalary.pagination}
																data={detailSalary.data}
																handlePageChange={handlePageChange}
																// handleDelete={handleDeleteUser}
																handleUpdate={(data) => handleUpdate(data)}
																roleName={'bài viết'}
																exportExcel={exportSalaryListOff}
																filter1={filters.filter1}
																filter2={filters.filter2}
																filter3={filters.filter3}
																handleFilterChange={handleFilterChange}
																resetFilter={resetFilter}
																role={'2'}
																totalRow={totalRow}
																tableName={'salaryDetail'}
																reload={reloadTable}
																listFunctionTable={functionsTable}
															/>
														)}
													</Grid>
												</Grid>
											</TabPanel>

											<TabPanel
												value={value}
												index={1}
												style={{ width: '100%' }}
											>
												<Grid container>
													<Grid container>
														{detailSalaryCourse && (
															<RenderTable2
																columnSelected={columnSelected}
																keys={keysCourse}
																pagination={detailSalaryCourse.pagination}
																data={detailSalaryCourse.data}
																handlePageChange={handlePageChange}
																handleUpdate={(data) =>
																	handleUpdateCourse(data)
																}
																exportExcel={exportSalaryListOn}
																roleName={'bài viết'}
																filter1={filters.filter1}
																filter2={filters.filter2}
																filter3={filters.filter3}
																// exportExcel={exportSalaryList}
																handleFilterChange={handleFilterChange}
																resetFilter={resetFilter}
																role={'2'}
																totalRow={totalRowCourse}
																tableName={'salaryCourse'}
																reload={reloadTable}
																listFunctionTable={functionsTable}
															/>
														)}
													</Grid>
												</Grid>
											</TabPanel>
										</Grid>
									</Grid>
								</Paper>
							</Grid>
						</Grid>
					</MasterLayout>
				);
			}}
		</Formik>
	);
}
