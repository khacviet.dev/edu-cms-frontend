import { Avatar, Divider, Grid, Paper, Typography } from '@material-ui/core';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import excelApi from 'api/excelApi';
import permissionApi from 'api/permissionApi';
import salaryApi from 'api/salaryApi';
import CommonTableT from 'common/components/CommonTableT';
import SearchForm from 'common/components/SearchForm';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { exportToCSV } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

const id = 'user_id';

const filterList1 = [
	{
		text: '01',
		value: '01',
	},
	{
		text: '02',
		value: '02',
	},
	{
		text: '03',
		value: '03',
	},
	{
		text: '04',
		value: '04',
	},
	{
		text: '05',
		value: '05',
	},
	{
		text: '06',
		value: '06',
	},
	{
		text: '07',
		value: '07',
	},
	{
		text: '08',
		value: '08',
	},
	{
		text: '09',
		value: '09',
	},
	{
		text: '10',
		value: '10',
	},
	{
		text: '11',
		value: '11',
	},
	{
		text: '12',
		value: '12',
	},
];

const filterList2 = [
	// {
	// 	text: new Date().getFullYear() - 1,
	// 	value: new Date().getFullYear() - 1,
	// },
	// {
	// 	text: new Date().getFullYear(),
	// 	value: new Date().getFullYear(),
	// },
];

const filterList3 = [
	{
		text: 'None',
		value: '',
	},
	{
		text: 'Chưa thanh toán',
		value: '0',
	},
	{
		text: 'Đã thanh toán',
		value: '1',
	},
];

const listFilterLabel = ['Tháng', 'Năm', 'Trạng thái'];

const listFunction = {
	create: true,
	edit: true,
	delete: false,
	export: true,
	filter: 1,
};

const RenderTable = CommonTableT(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id,
	listFunction
);

export default function SalaryList() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();
	const [loading, setLoading] = useState(false);
	const [salary, setSalary] = useState([]);
	const [totalTeacherSalary, setTotalTeacherSalary] = useState();
	const [totalTutorSalary, setTotalTutorSalary] = useState();

	const [totalRow, setTotalRow] = useState();
	const [isPermiss, setIsPermiss] = useState(false);
	const [functions, setFunctions] = useState([]);
	const [functionsTable, setFunctionTable] = useState({
		view: false,
		edit: false,
		review: false,
		revert: false,
	});

	const [filters, setFilters] = useState({
		limit: 5,
		page: 1,
		q: '',
		filter1: ('0' + (new Date().getMonth() + 1)).slice(-2),
		filter2: new Date().getFullYear(),
		filter3: '',
	});

	const handlePageChange = (newPage) => {
		setFilters({
			...filters,
			page: newPage,
		});
	};

	const handleSearchChange = (newSearch) => {
		setFilters({
			...filters,
			page: 1,
			q: newSearch.value,
		});
	};

	const handleFilterChange = (e, filter) => {
		setFilters({
			...filters,
			page: 1,
			[filter]: e.target.value,
		});
	};

	const resetFilter = () => {
		setFilters({
			...filters,
			filter1: '',
			filter2: '',
			filter3: '',
			page: 1,
		});
	};

	const [keys, setKeys] = useState([
		{
			header: 'Tên',
			align: 'left',
			field: 'full_name',
			display: true,
		},
		{
			header: 'Tổng nhận',
			align: 'left',
			field: 'salary_total',
			display: true,
		},
		{
			header: 'Số điện thoại',
			align: 'left',
			field: 'phone',
			display: true,
		},
		{
			header: 'Trạng thái',
			align: 'left',
			field: 'salary_status',
			display: true,
		},
	]);

	const [columnSelected, setColumnSelected] = React.useState([]);

	const changeDisplayColumn = (key) => {
		key.display = !key.display;
		setKeys(keys);
	};

	const handleColumnChange = (event) => {
		setColumnSelected(event.target.value);
	};

	const handleCreateUser = (data) => {
		console.log(data);
	};

	const exportSalaryList = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getSalarysExcel(filters);
				setLoading(false);
				const csvData = {
					data: response,
				};
				exportToCSV(csvData, 'Bang_Luong');
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	useEffect(() => {
		const value = [];
		for (let i = 0, l = keys.length; i < l; i += 1) {
			value.push(keys[i].header);
		}
		setColumnSelected(value);
	}, []);

	// check permission
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_list_salary',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', `Bạn không có quyền truy cập vào trang này`);
				localStorage.removeItem('menu');
				history.push('/');
			}
		};

		post();
	}, []);

	// get list function
	useEffect(() => {
		if (isPermiss) {
			const getFunctions = async () => {
				try {
					const respone = await permissionApi.getFunctions();
					const a = {
						view: true,
						edit: false,
						delete: false,
						review: false,
					};
					const data = [];
					for (let i = 0; i < respone.length; i++) {
						if (respone[i].function_id == 'function_list_salary') {
							a['edit'] = true;
						}
						data.push(respone[i].function_id);
					}
					setFunctions(data);
					setFunctionTable(a);
				} catch (error) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			getFunctions();
		}
	}, [isPermiss]);

	// get total record
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);
			const params = {
				role: '2',
				filters,
			};
			const getTotalSalary = async () => {
				try {
					const response = await salaryApi.getTotalSalary(params);
					setTotalRow(response.data[0].count);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getTotalSalary();
		}
	}, [filters, isPermiss]);

	// get salary list
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);
			const params = {
				role: '2',
				filters,
			};
			const getSalaryList = async () => {
				try {
					const response = await salaryApi.getSalaryList(params);
					setLoading(false);
					setSalary(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getSalaryList();
		}
	}, [filters, isPermiss]);
	// get dashborad total salary
	useEffect(() => {
		if (isPermiss) {
			setLoading(true);
			const params = {
				filters,
			};
			const getTotalEmployeeSalary = async () => {
				try {
					const response = await salaryApi.getTotalEmployeeSalary(params);
					if (response.data.length > 0) {
						if (response.data[0]) {
							setTotalTeacherSalary(
								response.data[0].sum.toLocaleString('vi-VN', {
									style: 'currency',
									currency: 'VND',
								})
							);
						} else {
							setTotalTeacherSalary('0 đ');
						}

						if (response.data[1]) {
							setTotalTutorSalary(
								response.data[1].sum.toLocaleString('vi-VN', {
									style: 'currency',
									currency: 'VND',
								})
							);
						} else {
							setTotalTutorSalary('0 đ');
						}
					} else {
						setTotalTeacherSalary('0 đ');
						setTotalTutorSalary('0 đ');
					}

					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getTotalEmployeeSalary();
		}
	}, [filters, isPermiss]);

	return (
		<MasterLayout header='Bảng lương'>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Bảng lương | Xoài CMS</title>
			</Helmet>
			<Grid container style={{ marginBottom: '1rem' }}>
				<Grid item md={8}>
					<SearchForm
						onSubmit={handleSearchChange}
						label={'Tìm theo id hoặc tên'}
					/>
				</Grid>
			</Grid>

			<RenderTable
				columnSelected={columnSelected}
				handleColumnChange={handleColumnChange}
				changeDisplayColumn={changeDisplayColumn}
				keys={keys}
				pagination={salary.pagination}
				data={salary.data}
				exportExcel={exportSalaryList}
				handlePageChange={handlePageChange}
				roleName={'nhân viên cần trả lương'}
				handleCreate={handleCreateUser}
				filter1={filters.filter1}
				filter2={filters.filter2}
				filter3={filters.filter3}
				handleFilterChange={handleFilterChange}
				resetFilter={resetFilter}
				role={'2'}
				totalRow={totalRow}
				tableName={'salary'}
				listFunctionTable={functionsTable}
				tooltip={`bảng lương`}
			/>

			<Grid container style={{ margin: '2rem 0' }}>
				<Divider style={{ width: '100%' }} />
			</Grid>
			<Grid container spacing={3}>
				<Grid item xs={12} md={4} lg={4}>
					<Paper>
						<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
							<Grid item xs={8} md={8} lg={8}>
								<Typography
									variant='body1'
									style={{
										color: 'rgb(107, 119, 140)',
										margin: '0 0 0.35rem',
									}}
								>
									Số tiền phải trả cho giáo viên
								</Typography>
								<Typography
									variant='body1'
									style={{
										fontSize: '1.5rem',
										fontWeight: 'bold',
									}}
								>
									{totalTeacherSalary}
								</Typography>
							</Grid>
							<Grid
								item
								xs={4}
								md={4}
								lg={4}
								container
								justify='flex-end'
								alignItems='center'
							>
								<Avatar
									style={{
										backgroundColor: 'rgb(229, 57, 53)',
										width: '56px',
										height: '56px',
									}}
								>
									<MonetizationOnIcon />
								</Avatar>
							</Grid>
							{/* <Grid item md={12} container style={{ paddingTop: '0.75rem' }}>
								<ArrowUpwardIcon style={{ color: 'rgb(27, 94, 32)' }} />
								<Typography style={{ color: 'rgb(27, 94, 32)' }}>
									30%&nbsp;&nbsp;
								</Typography>
								<Typography
									style={{ color: 'rgb(107, 119, 140)', fontSize: '14px' }}
								>
									So với tháng trước
								</Typography>
							</Grid> */}
						</Grid>
					</Paper>
				</Grid>
				<Grid item xs={12} md={4} lg={4}>
					<Paper>
						<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
							<Grid item xs={8} md={8} lg={8}>
								<Typography
									variant='body1'
									style={{
										color: 'rgb(107, 119, 140)',
										margin: '0 0 0.35rem',
									}}
								>
									Số tiền phải trả cho trợ giảng
								</Typography>
								<Typography
									variant='body1'
									style={{
										fontSize: '1.5rem',
										fontWeight: 'bold',
									}}
								>
									{totalTutorSalary}
								</Typography>
							</Grid>
							<Grid
								item
								xs={4}
								md={4}
								lg={4}
								container
								justify='flex-end'
								alignItems='center'
							>
								<Avatar
									style={{
										backgroundColor: 'rgb(67, 160, 71)',
										width: '56px',
										height: '56px',
									}}
								>
									<MonetizationOnIcon />
								</Avatar>
							</Grid>
							{/* <Grid item md={12} container style={{ paddingTop: '0.75rem' }}>
								<ArrowDownwardIcon style={{ color: 'rgb(183, 28, 28)' }} />
								<Typography style={{ color: 'rgb(183, 28, 28)' }}>
									15%&nbsp;&nbsp;
								</Typography>
								<Typography
									style={{ color: 'rgb(107, 119, 140)', fontSize: '14px' }}
								>
									So với tháng trước
								</Typography>
							</Grid> */}
						</Grid>
					</Paper>
				</Grid>
				{/* <Grid item xs={12} md={4} lg={4}>
					<Paper>
						<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
							<Grid item xs={8} md={8} lg={8}>
								<Typography
									variant='body1'
									style={{
										color: 'rgb(107, 119, 140)',
										margin: '0 0 0.35rem',
									}}
								>
									Số tiền phải trả cho quản lý
								</Typography>
								<Typography
									variant='body1'
									style={{
										fontSize: '1.5rem',
										fontWeight: 'bold',
									}}
								>
									350.000.000 đ
								</Typography>
							</Grid>
							<Grid
								item
								xs={4}
								md={4}
								lg={4}
								container
								justify='flex-end'
								alignItems='center'
							>
								<Avatar
									style={{
										backgroundColor: 'rgb(57, 73, 171)',
										width: '56px',
										height: '56px',
									}}
								>
									<AttachMoneyIcon />
								</Avatar>
							</Grid>
							<Grid item md={12} container style={{ paddingTop: '0.75rem' }}>
								<ArrowUpwardIcon style={{ color: 'rgb(27, 94, 32)' }} />
								<Typography style={{ color: 'rgb(27, 94, 32)' }}>
									10%&nbsp;&nbsp;
								</Typography>
								<Typography
									style={{ color: 'rgb(107, 119, 140)', fontSize: '14px' }}
								>
									So với tháng trước
								</Typography>
							</Grid>
						</Grid>
					</Paper>
				</Grid> */}
			</Grid>
		</MasterLayout>
	);
}
