import {
	Button,
	FormControl,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography,
} from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FilterListIcon from '@material-ui/icons/FilterList';
import GetAppIcon from '@material-ui/icons/GetApp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import BootstrapSwitchButton from 'bootstrap-switch-button-react';
import Pagination from 'common/components/Pagination';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import './style.css';

const useStyles = makeStyles({
	table: {
		minWidth: 650,
	},
	formControl: {
		minWidth: 120,
	},
});

const useRowStyles = makeStyles({
	input: {
		'&:invalid': {
			borderBottom: '2px solid red',
		},
	},
	none: {
		display: 'none',
	},
});

function Row(props) {
	const { row, isEdit, month, year } = props;
	const [open, setOpen] = React.useState(false);
	const classes = useRowStyles();
	const [bonus, setBonus] = React.useState();
	useEffect(() => {
		const bon = document.getElementById(`bonus${row.class_id}${month}${year}`)
			? document.getElementById(`bonus${row.class_id}${month}${year}`).innerHTML
			: row.bonus
					.toString()
					.replace(/(?:(^\d{1,3})(?=(?:\d{3})*$)|(\d{3}))(?!$)/gm, '$1$2.');
		setBonus(bon);
	}, []);

	const handleBlur = (e) => {
		const value = e.target.value.replaceAll('.', '');
		if (value.match(/^[0-9]*$/)) {
			var result = value.replace(
				/(?:(^\d{1,3})(?=(?:\d{3})*$)|(\d{3}))(?!$)/gm,
				'$1$2.'
			);
			e.target.value = result;
			document.getElementById(`total${row.class_id}${month}${year}`).innerHTML =
				(BigInt(row.pre_money) + BigInt(value)).toLocaleString('vi-VN', {
					style: 'currency',
					currency: 'VND',
				});
			document.getElementById(
				`control${row.class_id}${month}${year}`
			).innerHTML = '';
		} else {
			document.getElementById(
				`control${row.class_id}${month}${year}`
			).innerHTML = 'Wrong Input';
		}
	};
	const [value, setValue] = React.useState(false);
	const change = (a) => {
		document.getElementById(`status${row.class_id}${month}${year}`).innerHTML =
			a;
	};

	return (
		<React.Fragment>
			<TableRow className={classes.root}>
				<div
					id={`status${row.class_id}${month}${year}`}
					className={classes.none}
				>
					{row.status == '0' ? 'false' : 'true'}
				</div>
				<TableCell>
					<IconButton
						aria-label='expand row'
						size='small'
						onClick={() => setOpen(!open)}
					>
						{open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
					</IconButton>
				</TableCell>
				<TableCell component='th' scope='row' align='left'>
					{row.class_name}
				</TableCell>
				<TableCell align='left'>
					{row.pre_money.toLocaleString('vi-VN', {
						style: 'currency',
						currency: 'VND',
					})}
				</TableCell>
				<TableCell align='left'>
					{bonus && (
						<TextField
							disabled={!isEdit}
							key={`bonus${row.class_id}${month}${year}`}
							id={`bonus${row.class_id}${month}${year}`}
							onBlur={(e) => handleBlur(e)}
							inputProps={{ className: classes.input, pattern: '^[0-9.]*' }}
							defaultValue={bonus}
						></TextField>
					)}

					<small
						id={`control${row.class_id}${month}${year}`}
						style={{ display: 'inherit', color: 'red' }}
					></small>
				</TableCell>

				<TableCell id={`total${row.class_id}${month}${year}`} align='left'>
					{(BigInt(row.pre_money) + BigInt(row.bonus)).toLocaleString('vi-VN', {
						style: 'currency',
						currency: 'VND',
					})}
				</TableCell>
				<TableCell align='left'>
					<BootstrapSwitchButton
						disabled={!isEdit}
						checked={row.status == '0' ? false : true}
						onlabel='Đã Thanh Toán'
						offlabel='Chưa Thanh Toán'
						onChange={(a) => change(a)}
					/>
				</TableCell>
			</TableRow>

			{row.list_detail && (
				<TableRow>
					<TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
						<Collapse in={open} timeout='auto' unmountOnExit>
							<Box margin={1}>
								<Table size='small'>
									<TableHead>
										<TableRow>
											<TableCell align='center'>Ngày</TableCell>

											<TableCell align='center'>Số học sinh đi học</TableCell>
											<TableCell align='center'>Tổng nhận ($)</TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{row.list_detail.map((list_detail_Row, index) => (
											<TableRow key={`${index}${list_detail_Row.date}`}>
												<TableCell align='center'>
													{list_detail_Row.date}
												</TableCell>
												<TableCell align='center'>
													{list_detail_Row.present_student}
												</TableCell>
												<TableCell align='center'>
													{list_detail_Row.amount.toLocaleString('vi-VN', {
														style: 'currency',
														currency: 'VND',
													})}
												</TableCell>
											</TableRow>
										))}
									</TableBody>
								</Table>
							</Box>
						</Collapse>
					</TableCell>
				</TableRow>
			)}
		</React.Fragment>
	);
}

function RowCourse(props) {
	const { row, isEdit, month, year } = props;
	const [open, setOpen] = React.useState(false);
	const classes = useRowStyles();
	const change = (a) => {
		document.getElementById(
			`status${row.salary_course_id}${month}${year}`
		).innerHTML = a;
	};

	return (
		<React.Fragment>
			<TableRow className={classes.root}>
				<TableCell>
					<div
						id={`status${row.salary_course_id}${month}${year}`}
						className={classes.none}
					>
						{row.status == '0' ? 'false' : 'true'}
					</div>
				</TableCell>
				<TableCell align='left'>{row.title}</TableCell>
				<TableCell align='left'>{row.total_course_sold}</TableCell>
				<TableCell align='left'>
					{row.total.toLocaleString('vi-VN', {
						style: 'currency',
						currency: 'VND',
					})}
				</TableCell>
				<TableCell align='left'>
					<BootstrapSwitchButton
						disabled={!isEdit}
						checked={row.status == '0' ? false : true}
						onlabel='Đã Thanh Toán'
						offlabel='Chưa Thanh Toán'
						onChange={(a) => change(a)}
					/>
				</TableCell>
			</TableRow>
		</React.Fragment>
	);
}

export default function CommonTableT2(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id,
	listFunction
) {
	return function (props) {
		const {
			handleCreate,
			handleDelete,
			handleColumnChange,
			changeDisplayColumn,
			handleFilterChange,
			columnSelected,
			handleUpdate,
			keys,
			pagination,
			data,
			handlePageChange,
			roleName,
			filter1,
			filter2,
			filter3,
			resetFilter,
			role,
			totalRow,
			tableName,
			reload,
			listFunctionTable,
			exportExcel,
		} = props;
		if (pagination != undefined) {
			pagination.totalRows = totalRow;
		}

		let history = useHistory();
		const onClickUpdate = (new_data) => {
			// document.getElementById('loading').style.display = '';

			if (tableName == 'salaryDetail') {
				const create_data = [];
				for (let i = 0; i < data.length; i++) {
					const control = document.getElementById(
						`control${data[i].class_id}${filter1}${filter2}`
					).innerHTML;
					if (control != '') {
						document
							.getElementById(`bonus${data[i].class_id}${filter1}${filter2}`)
							.focus();
						break;
					} else {
						let data_bonus = document.getElementById(
							`bonus${data[i].class_id}${filter1}${filter2}`
						).value;
						if (data_bonus == '') {
							data_bonus = '0';
						}
						const bonus = data_bonus.match(/\d/g).join('');
						const status = document.getElementById(
							`status${data[i].class_id}${filter1}${filter2}`
						).innerHTML;
						const element = {
							bonus: bonus,
							status: status == 'false' ? '0' : '1',
							class_id: data[i].class_id,
							list_salary_id: data[i].list_detail,
						};
						create_data.push(element);
					}
				}
				if (create_data.length == data.length) {
					handleUpdate(create_data);
				}
			} else if (tableName == 'salaryCourse') {
				const create_data = [];
				for (let i = 0; i < data.length; i++) {
					const status = document.getElementById(
						`status${data[i].salary_course_id}${filter1}${filter2}`
					).innerHTML;
					const element = {
						status: status == 'false' ? '0' : '1',
						salary_course_id: data[i].salary_course_id,
					};
					create_data.push(element);
				}

				handleUpdate(create_data);
			}
		};

		// const handleEdit = (data) => {
		// 	if (tableName == 'post') {
		// 		history.push({
		// 			pathname: '/postdetail',
		// 			state: {
		// 				post_id: data,
		// 			},
		// 		});
		// 	} else if (tableName == 'salary') {
		// 		history.push({
		// 			pathname: '/salarydetail',
		// 			state: {
		// 				user_id: data,
		// 			},
		// 		});
		// 	}
		// };

		const classes = useStyles();

		return (
			<Grid container>
				{/* {displayC==false && (
								<div
									style={{
										position: 'fixed',
										top: '50%',
										left: '50%',
										width: '50px',
										transform: 'translate(-50%, -50%)',
										zIndex: '1000000000',
									}}
									className='cp-spinner cp-balls'
								></div>
							)} */}
				<Grid container style={{ padding: '0.75rem' }} alignItems='center'>
					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						<FormControl className={classes.formControl}>
							<InputLabel>{listFilterLabel[0]}</InputLabel>
							<Select
								value={filter1}
								onChange={(e) => handleFilterChange(e, 'filter1')}
							>
								{filterList1.map((filter, index) => (
									<MenuItem key={index} value={filter.value}>
										{filter.text}
									</MenuItem>
								))}
							</Select>
						</FormControl>
					</Grid>

					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						{filterList2.length > 0 && (
							<FormControl className={classes.formControl}>
								<InputLabel>{listFilterLabel[1]}</InputLabel>
								<Select
									value={filter2}
									onChange={(e) => handleFilterChange(e, 'filter2')}
								>
									{filterList2.map((filter, index) => (
										<MenuItem key={index} value={filter.value}>
											{filter.text}
										</MenuItem>
									))}
								</Select>
							</FormControl>
						)}
					</Grid>

					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						{filterList3.length > 0 && (
							<FormControl className={classes.formControl}>
								<InputLabel>{listFilterLabel[2]}</InputLabel>
								<Select
									value={filter3}
									onChange={(e) => handleFilterChange(e, 'filter3')}
								>
									{filterList3.map((filter, index) => (
										<MenuItem key={index} value={filter.value}>
											{filter.text}
										</MenuItem>
									))}
								</Select>
							</FormControl>
						)}
					</Grid>

					<Grid item md={2}>
						{(filter1 !== '' || filter2 !== '' || filter3 !== '') && (
							<Button
								onClick={resetFilter}
								startIcon={<FilterListIcon />}
								variant='contained'
								color='secondary'
							>
								Xóa lọc
							</Button>
						)}
					</Grid>
					<Grid item md={2} container justify='flex-end'>
						{listFunctionTable['view'] == true && (
							<Button
								startIcon={<GetAppIcon />}
								variant='text'
								onClick={exportExcel}
							>
								Xuất file
							</Button>
						)}

						{/* {listFunction.export && (
							<ExportCSV csvData={data} fileName='data' />
						)} */}
					</Grid>
				</Grid>

				<TableContainer component={Paper} style={{ marginTop: '1rem' }}>
					<Table className={classes.table} stickyHeader>
						<TableHead>
							<TableRow>
								<TableCell style={{ width: '5%' }}></TableCell>
								{keys.map((key, index) => (
									<TableCell
										style={{ display: key.display ? '' : 'none' }}
										key={index}
									>
										{key.header}
									</TableCell>
								))}
							</TableRow>
						</TableHead>

						{data && data.length > 0 ? (
							<TableBody style={{ display: reload == false ? '' : 'none' }}>
								{tableName == 'salaryDetail' &&
									data.map((row, index) => (
										<Row
											key={`${index}${row.class_id}${filter1}${filter2}`}
											row={row}
											month={filter1}
											year={filter2}
											isEdit={listFunctionTable.edit}
										/>
									))}

								{tableName == 'salaryCourse' &&
									data.map((row, index) => (
										<RowCourse
											key={`course${index}${filter1}${filter2}`}
											row={row}
											month={filter1}
											year={filter2}
											isEdit={listFunctionTable.edit}
										/>
									))}
								{/* {tableName == 'salaryCourse' &&
									data.map((row,index) => (<RowCourse key={`course${index}`} row={row} />))} */}
							</TableBody>
						) : (
							<TableBody>
								<TableRow>
									<TableCell align='center' colSpan={keys.length + 3}>
										Không tìm thấy dữ liệu
									</TableCell>
								</TableRow>
							</TableBody>
						)}
					</Table>
				</TableContainer>

				{data && (
					<Grid container alignItems='center' style={{ marginTop: '2rem' }}>
						<Grid item md={6}>
							<Typography style={{ fontWeight: 'bold' }}>
								Tổng số ({pagination.totalRows})
							</Typography>
						</Grid>
						<Grid item md={6}>
							<Pagination
								pagination={pagination}
								onPageChange={handlePageChange}
							/>
						</Grid>
					</Grid>
				)}

				{listFunctionTable.edit == true && (
					<Grid container justify='center' style={{ margin: '4rem 0 0 0' }}>
						<Button
							onClick={(data) => onClickUpdate(data)}
							variant='contained'
							color='primary'
							style={{ padding: '0.5rem 3rem' }}
						>
							Cập nhập
						</Button>
					</Grid>
				)}
			</Grid>
		);
	};
}
