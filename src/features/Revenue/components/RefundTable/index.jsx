import {
	Button,
	FormControl,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography,
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FilterListIcon from '@material-ui/icons/FilterList';
import GetAppIcon from '@material-ui/icons/GetApp';
import BootstrapSwitchButton from 'bootstrap-switch-button-react';
import Pagination from 'common/components/Pagination';
import React from 'react';
import { useHistory } from 'react-router';
import './style.css';

const useStyles = makeStyles({
	table: {
		minWidth: 650,
	},
	formControl: {
		minWidth: 120,
	},
});

const useRowStyles = makeStyles({
	input: {
		'&:invalid': {
			borderBottom: '2px solid red',
		},
	},
	none: {
		display: 'none',
	},
});

function RowCourse(props) {
	const { row, isEdit, month, year } = props;
	const [open, setOpen] = React.useState(false);
	const classes = useRowStyles();
	const change = (a) => {
		document.getElementById(`refund${row.payment_id}`).innerHTML = a;
	};

	return (
		<React.Fragment>
			<div id={`refund${row.payment_id}`} className={classes.none}>
				{row.refund_status == '0' ? 'false' : 'true'}
			</div>
			<TableRow className={classes.root}>
				<TableCell align='left'>{row.full_name}</TableCell>
				<TableCell align='left'>{row.class_name}</TableCell>
				<TableCell align='center'>
					{row.residual_fee.toLocaleString('vi-VN', {
						style: 'currency',
						currency: 'VND',
					})}
				</TableCell>

				<TableCell align='center'>
					{row.additional_charges.toLocaleString('vi-VN', {
						style: 'currency',
						currency: 'VND',
					})}
				</TableCell>

				<TableCell align='center'>
					{row.refund.toLocaleString('vi-VN', {
						style: 'currency',
						currency: 'VND',
					})}
				</TableCell>

				<TableCell align='left'>
					<BootstrapSwitchButton
						disabled={!isEdit}
						checked={row.refund_status == '0' ? false : true}
						onlabel='Đã Trả'
						offlabel='Chưa Trả'
						onChange={(a) => change(a)}
					/>
				</TableCell>
			</TableRow>
		</React.Fragment>
	);
}

export default function RefundTable(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id,
	listFunction
) {
	return function (props) {
		const {
			handleCreate,
			handleDelete,
			handleColumnChange,
			changeDisplayColumn,
			handleFilterChange,
			columnSelected,
			handleUpdate,
			keys,
			pagination,
			data,
			handlePageChange,
			roleName,
			filter1,
			filter2,
			filter3,
			resetFilter,
			role,
			totalRow,
			tableName,
			reload,
			listFunctionTable,
			exportExcel,
			listClass,
		} = props;
		if (pagination != undefined) {
			pagination.totalRows = totalRow;
		}
		filterList1 = listClass;

		let history = useHistory();
		const onClickUpdate = (new_data) => {
			const create_data = [];
			for (let i = 0; i < data.length; i++) {
				const status = document.getElementById(
					`refund${data[i].payment_id}`
				).innerHTML;
				const element = {
					status: status == 'false' ? '0' : '1',
					payment_id: data[i].payment_id,
				};
				create_data.push(element);
			}

			handleUpdate(create_data);
		};

		const classes = useStyles();

		return (
			<Grid container>
				<Grid container style={{ padding: '0.75rem' }} alignItems='center'>
					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						<FormControl className={classes.formControl}>
							<InputLabel>{listFilterLabel[0]}</InputLabel>
							<Select
								value={filter1}
								onChange={(e) => handleFilterChange(e, 'filter1')}
							>
								{filterList1.map((filter, index) => (
									<MenuItem key={index} value={filter.value}>
										{filter.text}
									</MenuItem>
								))}
							</Select>
						</FormControl>
					</Grid>

					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						<TextField
							id='date'
							label='Tháng'
							type='month'
							inputProps={{
								pattern: '[0-9]{4}-[0-9]{2}',
							}}
							onChange={(e) => handleFilterChange(e, 'filter2')}
							defaultValue={new Date().toISOString().slice(0, 7)}
							className={classes.textField}
							InputLabelProps={{
								shrink: true,
							}}
						/>
					</Grid>

					<Grid item md={2} style={{ marginBottom: '0.75rem' }}></Grid>

					<Grid item md={2}>
						{filter1 !== '' && (
							<Button
								onClick={resetFilter}
								startIcon={<FilterListIcon />}
								variant='contained'
								color='secondary'
							>
								Xóa lọc
							</Button>
						)}
					</Grid>
					<Grid item md={2}></Grid>
					<Grid item md={2} container justify='flex-end'>
						{listFunctionTable['view'] == true && (
							<Button
								startIcon={<GetAppIcon />}
								variant='text'
								onClick={exportExcel}
							>
								Xuất file
							</Button>
						)}
					</Grid>
				</Grid>

				<TableContainer component={Paper} style={{ marginTop: '1rem' }}>
					<Table className={classes.table} stickyHeader>
						<TableHead>
							<TableRow>
								{keys.map((key, index) => (
									<TableCell
										style={{ display: key.display ? '' : 'none' }}
										key={index}
										align={key.align}
									>
										{key.header}
									</TableCell>
								))}
							</TableRow>
						</TableHead>

						{data && data.length > 0 ? (
							<TableBody style={{ display: reload == false ? '' : 'none' }}>
								{data.map((row, index) => (
									<RowCourse
										key={`refund${index}`}
										row={row}
										isEdit={listFunctionTable.edit}
									/>
								))}
							</TableBody>
						) : (
							<TableBody>
								<TableRow>
									<TableCell align='center' colSpan={keys.length + 3}>
										Không tìm thấy dữ liệu
									</TableCell>
								</TableRow>
							</TableBody>
						)}
					</Table>
				</TableContainer>

				{data && (
					<Grid container alignItems='center' style={{ marginTop: '2rem' }}>
						<Grid item md={6}>
							<Typography style={{ fontWeight: 'bold' }}>
								Tổng số ({pagination.totalRows})
							</Typography>
						</Grid>
						<Grid item md={6}>
							<Pagination
								pagination={pagination}
								onPageChange={handlePageChange}
							/>
						</Grid>
					</Grid>
				)}

				{data && data.length > 0 && listFunctionTable.edit == true && (
					<Grid container justify='center' style={{ margin: '4rem 0 0 0' }}>
						<Button
							onClick={(data) => onClickUpdate(data)}
							variant='contained'
							color='primary'
							style={{ padding: '0.5rem 3rem' }}
						>
							Cập nhập
						</Button>
					</Grid>
				)}
			</Grid>
		);
	};
}
