import {
	Avatar,
	Box,
	Divider,
	Grid,
	Paper,
	Tab,
	Tabs,
	Typography,
} from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import courseApi from 'api/courseApi';
import dashboardApi from 'api/dashboardApi';
import excelApi from 'api/excelApi';
import feedBackApi from 'api/feedbackApi';
import permissionApi from 'api/permissionApi';
import CommonTableT from 'common/components/CommonTableT';
import SearchForm from 'common/components/SearchForm';
import MasterLayout from 'common/pages/MasterLayout';
import RefundTable from 'features/Revenue/components/RefundTable';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router';
import { exportToCSV } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

RevenueList.propTypes = {};

const useStyles = makeStyles({
	table: {
		minWidth: 650,
	},
	formControl: {
		minWidth: 120,
	},
	td: {
		whiteSpace: 'nowrap',
	},
});

const id = 'user_id';

const filterList1 = [];
const listFilterLabel = ['Lớp', 'Tháng', 'Năm'];
const listFilterLabelOn = ['Khóa', 'Tháng', 'Năm'];

const filterList2 = [];
const filterList3 = [];

const LightTooltip = withStyles((theme) => ({
	tooltip: {
		backgroundColor: theme.palette.common.black,
		color: 'white',
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

const RenderTable = CommonTableT(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id
);

const RenderTable1 = CommonTableT(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabelOn,
	id
);

const RenderTable2 = RefundTable(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id
);

function TabPanel(props) {
	const { children, value, index, ...other } = props;

	return (
		<div role='tabpanel' hidden={value !== index} {...other}>
			{value === index && <Box p={1}>{children}</Box>}
		</div>
	);
}

export default function RevenueList() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();
	const classes = useStyles();
	const [startDate, setStartDate] = useState(new Date());
	const [value, setValue] = useState(0);
	const [payment, setPayment] = useState([]);
	const [totalRow, setTotalRow] = useState();
	const [loading, setLoading] = useState(false);
	const [classList, setClassList] = useState([]);
	const [subjectList, setSubjectList] = useState([]);
	const [proceeds, setProceeds] = useState('0');
	const [sale, setSale] = useState('0');
	const [refund, setRefund] = useState([]);
	const [proceedsOn, setProceedsOn] = useState('0');
	const [saleOn, setSaleOn] = useState('0');
	const [reloadTable, setReloadTable] = useState(false);
	const [listCourse, setListCourse] = useState([]);
	const [filters, setFilters] = useState({
		limit: 5,
		page: 1,
		q: '',
		filter1: '',
		filter2: new Date().toISOString().slice(0, 7),
		filter3: new Date().toISOString().slice(0, 7),
	});
	// phân quyền
	const [functions, setFunctions] = useState([]);
	const role = '1';
	const [isPermiss, setIsPermiss] = useState(false);
	const [functionsTable, setFunctionTable] = useState({
		view: false,
		edit: false,
		review: false,
		revert: false,
	});

	const [totalRowOn, setTotalRowOn] = useState('0');
	const [onlinePayment, setOnlinePayment] = useState('0');
	const [reloadRefund, setReloadRefund] = useState(false);
	const [feeInfo, setFeeInfo] = useState({});
	// check permission
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_list_revenue',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', `Bạn không có quyền truy cập vào trang này`);
				localStorage.removeItem('menu');
				history.push('/');
			}
		};

		post();
	}, []);

	// get list function
	useEffect(() => {
		if (isPermiss) {
			const getFunctions = async () => {
				try {
					const respone = await permissionApi.getFunctions();
					const a = {
						view: true,
						edit: true,
						delete: false,
						review: false,
					};
					const data = [];
					for (let i = 0; i < respone.length; i++) {
						data.push(respone[i].function_id);
					}
					setFunctions(data);
					setFunctionTable(a);
				} catch (error) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			getFunctions();
		}
	}, [isPermiss]);

	// set clounm display
	useEffect(() => {
		const value = [];
		for (let i = 0, l = keys.length; i < l; i += 1) {
			value.push(keys[i].header);
		}
		setColumnSelected(value);
	}, []);

	// get class filter
	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);

			const getClassesSelect = async () => {
				try {
					const response = await feedBackApi.getClassesSelect();
					// se(response.data);
					setClassList(response);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getClassesSelect();
		}
	}, [isPermiss]);

	// get revenue list
	useEffect(() => {
		if (isPermiss && value == 0) {
			const params = {
				role: '2',
				filters,
			};
			setLoading((loading) => !loading);
			const getRevenus = async () => {
				try {
					const response = await dashboardApi.getRevenus(params);
					setPayment(response);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
					if (error.response.status === 666) {
						setPayment([]);
					}
				}
			};
			getRevenus();
		}
	}, [filters, isPermiss, value]);
	// count
	useEffect(() => {
		if (isPermiss) {
			const params = {
				role: '2',
				filters,
			};
			setLoading((loading) => !loading);
			const getCountRevenus = async () => {
				try {
					const response = await dashboardApi.getCountRevenus(params);
					// setPayment(response);
					setTotalRow(response.count);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getCountRevenus();
		}
	}, [filters, isPermiss]);
	// get doanh thu doanh so
	useEffect(() => {
		if (isPermiss) {
			const params = {
				role: '2',
				filters,
			};
			setLoading((loading) => !loading);
			const getProceedsAndSale = async () => {
				try {
					const response = await dashboardApi.getProceedsAndSale(params);
					// setPayment(response);
					// setTotalRow(response.count)
					if (response.proceeds.proceeds) {
						setProceeds(response.proceeds.proceeds);
					} else {
						setProceeds('0');
					}
					if (response.sale.sale) {
						setSale(response.sale.sale);
					} else {
						setSale('0');
					}

					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getProceedsAndSale();
		}
	}, [filters, isPermiss]);

	useEffect(() => {
		if (isPermiss && value == 1) {
			const params = {
				role: '2',
				filters,
			};
			setLoading((loading) => !loading);
			const getRevenusOnline = async () => {
				try {
					const response = await dashboardApi.getRevenusOnline(params);
					// setPayment(response);
					// setTotalRow(response.count)
					setOnlinePayment(response);

					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
					if (error.response.status === 666) {
						setOnlinePayment([]);
					}
				}
			};
			getRevenusOnline();
		}
	}, [filters, isPermiss, value]);

	useEffect(() => {
		if (isPermiss) {
			const params = {
				role: '2',
				filters,
			};
			setLoading((loading) => !loading);
			const getCountRevenusOnline = async () => {
				try {
					const response = await dashboardApi.getCountRevenusOnline(params);
					// setPayment(response);
					// setTotalRow(response.count)
					setTotalRowOn(response.count);

					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getCountRevenusOnline();
		}
	}, [filters, isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const params = {
				role: '2',
				filters,
			};
			setLoading((loading) => !loading);
			const getProceedsAndSaleOn = async () => {
				try {
					const response = await dashboardApi.getProceedsAndSaleOn(params);
					// setPayment(response);
					// setTotalRow(response.count)
					// setTotalRowOn(response.count)
					if (response.proceeds.proceeds) {
						setProceedsOn(response.proceeds.proceeds);
					} else {
						setProceedsOn('0');
					}
					if (response.sale.sale) {
						setSaleOn(response.sale.sale);
					} else {
						setSaleOn('0');
					}
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getProceedsAndSaleOn();
		}
	}, [filters, isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			setLoading((loading) => !loading);
			const getCourseFilter = async () => {
				try {
					const response = await courseApi.getCourseFilter();
					setListCourse(response);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getCourseFilter();
		}
	}, [filters, isPermiss]);

	useEffect(() => {
		if (isPermiss && value == 2) {
			const params = {
				filters,
			};
			setLoading((loading) => !loading);
			const getRefundMoney = async () => {
				try {
					const response = await dashboardApi.getRefundMoney(params);
					setRefund(response);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
					if (error.response.status === 666) {
						setRefund([]);
					}
				}
			};
			getRefundMoney();
		}
	}, [filters, isPermiss, value, reloadRefund]);

	useEffect(() => {
		if (isPermiss && value == 2) {
			const params = {
				filters,
			};
			setLoading((loading) => !loading);
			const getCountRefundMoney = async () => {
				try {
					const response = await dashboardApi.getCountRefundMoney(params);
					setTotalRow(response.count);
					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
					if (error.response.status === 666) {
						setTotalRow('0');
					}
				}
			};
			getCountRefundMoney();
		}
	}, [filters, isPermiss, value, reloadRefund]);

	useEffect(() => {
		if (isPermiss && value == 2) {
			const params = {
				filters,
			};
			setLoading((loading) => !loading);
			const getTotalAdditionalChargesAndresidualFee = async () => {
				try {
					const response =
						await dashboardApi.getTotalAdditionalChargesAndresidualFee(params);

					setFeeInfo(response);

					setLoading((loading) => !loading);
				} catch (error) {
					setLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getTotalAdditionalChargesAndresidualFee();
		}
	}, [filters, isPermiss, value, reloadRefund]);

	const handlePageChange = (newPage) => {
		setFilters({
			...filters,
			page: newPage,
		});
	};

	const handleSearchChange = (newSearch) => {
		setFilters({
			...filters,
			page: 1,
			q: newSearch.value,
		});
	};

	const handleFilterChange = (e, filter) => {
		setFilters({
			...filters,
			page: 1,
			[filter]: e.target.value,
		});
	};

	const resetFilter = () => {
		setFilters({
			...filters,
			filter1: '',
			page: 1,
		});
	};

	const [keys, setKeys] = useState([
		{
			header: 'Họ và tên',
			align: 'left',
			field: 'full_name',
			display: true,
		},
		{
			header: 'Lớp học',
			align: 'left',
			field: 'class_name',
			display: true,
		},
		{
			header: 'Tháng',
			align: 'left',
			field: 'time',
			display: true,
		},
		{
			header: 'Thực thu',
			align: 'left',
			field: 'amount',
			display: true,
		},
		{
			header: 'Ngày thu',
			align: 'left',
			field: 'payment_date',
			display: true,
		},
	]);

	const [keysOn, setKeysOn] = useState([
		{
			header: 'Họ và tên',
			align: 'left',
			field: 'full_name',
			display: true,
		},
		{
			header: 'Khóa học',
			align: 'left',
			field: 'title',
			display: true,
		},
		{
			header: 'Ngày mua khóa học',
			align: 'left',
			field: 'buy_date',
			display: true,
		},
		{
			header: 'Thực cần thu',
			align: 'center',
			field: 'price',
			display: true,
		},
		{
			header: 'Ngày xác nhận',
			align: 'center',
			field: 'join_date',
			display: true,
		},
	]);

	const [keysRefund, setKeysRefund] = useState([
		{
			header: 'Họ và tên',
			align: 'left',
			field: 'full_name',
			display: true,
		},
		{
			header: 'Lớp học',
			align: 'left',
			field: 'class_name',
			display: true,
		},
		// {
		// 	header: 'Thực thu tháng trước',
		// 	align: 'left',
		// 	field: 'amount',
		// 	display: true,
		// },
		// {
		// 	header: 'Trạng thái',
		// 	align: 'left',
		// 	field: 'status',
		// 	display: true,
		// },
		{
			header: `Phí thừa tháng trước`,
			align: 'center',
			field: 'residual_fee',
			display: true,
		},
		{
			header: `Phí nợ tháng trước`,
			align: 'center',
			field: 'additional_charges',
			display: true,
		},
		{
			header: `Số tiền hoàn trả`,
			align: 'center',
			field: 'refund',
			display: true,
		},
		{
			header: `Trạng thái`,
			align: 'center',
			field: 'refund_status',
			display: true,
		},
	]);

	const [columnSelected, setColumnSelected] = React.useState([]);

	const changeDisplayColumn = (key) => {
		key.display = !key.display;
		setKeys(keys);
	};

	const handleColumnChange = (event) => {
		setColumnSelected(event.target.value);
	};

	const handleDeleteCourse = (course_id) => {
		setLoading((loading) => !loading);
		const params = {
			course_id: course_id,
			status: '3',
		};
		const updateCourseFlag = async () => {
			try {
				const response = await courseApi.updateCourseFlag(params);
				setLoading((loading) => !loading);
				notify('success', response.message);
				setFilters({
					...filters,
					delete: '',
				});
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateCourseFlag();
	};

	const handeleRevertCourse = (course_id) => {
		setLoading((loading) => !loading);
		const params = {
			course_id: course_id,
			status: '0',
		};
		const updateCourseFlag = async () => {
			try {
				const response = await courseApi.updateCourseFlag(params);
				setLoading((loading) => !loading);
				notify('success', response.message);
				setFilters({
					...filters,
					delete: '',
				});
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateCourseFlag();
	};

	const getRevenueExcelOff = () => {
		const get = async () => {
			try {
				setLoading((loading) => !loading);
				const response = await excelApi.getRevenueExcelOff(filters);
				setLoading((loading) => !loading);
				const csvData = {
					data: response,
				};
				exportToCSV(
					csvData,
					`Doanh_Thu_Offline_Lop_${filters.filter1}_Thang_${filters.filter2}_Nam_${filters.filter3}`
				);
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	const getRevenueExcelOn = () => {
		const get = async () => {
			try {
				setLoading((loading) => !loading);
				const response = await excelApi.getRevenueExcelOn(filters);
				setLoading((loading) => !loading);
				const csvData = {
					data: response,
				};
				exportToCSV(
					csvData,
					`Doanh_Thu_Online_Lop_${filters.filter1}_Thang_${filters.filter2}_Nam_${filters.filter3}`
				);
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	const getRefundMoneyExcel = () => {
		const get = async () => {
			try {
				setLoading((loading) => !loading);
				const response = await excelApi.getRefundMoneyExcel(filters);
				setLoading((loading) => !loading);
				const csvData = {
					data: response,
				};
				exportToCSV(
					csvData,
					`Tien_hoan_tra_${filters.filter1}_Thang_${filters.filter2}`
				);
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	const handleChange = (event, newValue) => {
		setFilters({
			...filters,
			filter1: '',
			filter2: new Date().toISOString().slice(0, 7),
			page: 1,
		});
		setValue(newValue);
	};

	const handleUpdateRefund = (data) => {
		setLoading((loading) => !loading);
		setReloadTable((loading) => !loading);

		const updateRefundMoney = async () => {
			try {
				const response = await dashboardApi.updateRefundMoney(data);
				setReloadRefund((loading) => !loading);
				// setLoading((loading) => !loading);
				// notify('success', response.message);
				setTimeout(() => {
					setLoading((loading) => !loading);
					setReloadTable((loading) => !loading);
					notify('success', response.message);
				}, 2000);
			} catch (error) {
				setLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		updateRefundMoney();
	};

	return (
		<MasterLayout header='Doanh số'>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Doanh số | Xoài CMS</title>
			</Helmet>
			<Grid container>
				<Grid container style={{ marginBottom: '1rem' }}>
					<Grid item md={8}>
						<SearchForm
							onSubmit={handleSearchChange}
							label={'Tìm theo tên hoặc mã học sinh'}
						/>
					</Grid>
				</Grid>

				<Paper style={{ width: '100%' }}>
					<Grid container spacing={2}>
						<Grid
							item
							xs={12}
							md={12}
							lg={12}
							container
							style={{ position: 'relative' }}
						>
							<Tabs
								value={value}
								onChange={handleChange}
								indicatorColor='primary'
								textColor='primary'
								centered
							>
								<Tab label='Tại trung tâm' />
								<Tab label='Trực tuyến' />
								<Tab label='Hoàn trả' />
							</Tabs>
						</Grid>
					</Grid>

					<TabPanel value={value} index={0} style={{ width: '100%' }}>
						<RenderTable
							columnSelected={columnSelected}
							handleColumnChange={handleColumnChange}
							changeDisplayColumn={changeDisplayColumn}
							exportExcel={getRevenueExcelOff}
							keys={keys}
							pagination={payment.pagination}
							data={payment.data}
							handlePageChange={handlePageChange}
							handleDelete={handleDeleteCourse}
							handleRevert={handeleRevertCourse}
							roleName={''}
							filter1={filters.filter1}
							filter2={filters.filter2}
							filter3={filters.filter3}
							subjectList={subjectList}
							handleFilterChange={handleFilterChange}
							resetFilter={resetFilter}
							role={'2'}
							totalRow={totalRow}
							tableName={'revenue'}
							listFunctionTable={functionsTable}
							deleteName={'khóa học'}
							tooltip={'khóa học'}
							listClass={classList}
						/>
						<Grid container style={{ margin: '2rem 0' }}>
							<Divider style={{ width: '100%' }} />
						</Grid>
						<Grid container spacing={3}>
							<Grid item xs={12} md={4} lg={4}>
								<Paper>
									<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
										<Grid item xs={8} md={8} lg={8}>
											<Typography
												variant='body1'
												style={{
													color: 'rgb(107, 119, 140)',
													margin: '0 0 0.35rem',
												}}
											>
												Tổng doanh thu
											</Typography>
											<Typography
												variant='body1'
												style={{
													fontSize: '1.5rem',
													fontWeight: 'bold',
												}}
											>
												{proceeds.toLocaleString('vi-VN', {
													style: 'currency',
													currency: 'VND',
												})}
											</Typography>
										</Grid>
										<Grid
											item
											xs={4}
											md={4}
											lg={4}
											container
											justify='flex-end'
											alignItems='center'
										>
											<Avatar
												style={{
													backgroundColor: 'rgb(229, 57, 53)',
													width: '56px',
													height: '56px',
												}}
											>
												<MonetizationOnIcon />
											</Avatar>
										</Grid>
									</Grid>
								</Paper>
							</Grid>
							<Grid item xs={12} md={4} lg={4}>
								<Paper>
									<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
										<Grid item xs={8} md={8} lg={8}>
											<Typography
												variant='body1'
												style={{
													color: 'rgb(107, 119, 140)',
													margin: '0 0 0.35rem',
												}}
											>
												Tổng doanh số
											</Typography>
											<Typography
												variant='body1'
												style={{
													fontSize: '1.5rem',
													fontWeight: 'bold',
												}}
											>
												{sale.toLocaleString('vi-VN', {
													style: 'currency',
													currency: 'VND',
												})}
											</Typography>
										</Grid>
										<Grid
											item
											xs={4}
											md={4}
											lg={4}
											container
											justify='flex-end'
											alignItems='center'
										>
											<Avatar
												style={{
													backgroundColor: 'rgb(67, 160, 71)',
													width: '56px',
													height: '56px',
												}}
											>
												<MonetizationOnIcon />
											</Avatar>
										</Grid>
									</Grid>
								</Paper>
							</Grid>
						</Grid>
					</TabPanel>

					<TabPanel value={value} index={1} style={{ width: '100%' }}>
						<RenderTable1
							columnSelected={columnSelected}
							handleColumnChange={handleColumnChange}
							changeDisplayColumn={changeDisplayColumn}
							exportExcel={getRevenueExcelOn}
							keys={keysOn}
							pagination={onlinePayment.pagination}
							data={onlinePayment.data}
							handlePageChange={handlePageChange}
							handleDelete={handleDeleteCourse}
							handleRevert={handeleRevertCourse}
							roleName={'doanh số online'}
							listCourse={listCourse}
							filter1={filters.filter1}
							filter2={filters.filter2}
							filter3={filters.filter3}
							subjectList={subjectList}
							handleFilterChange={handleFilterChange}
							resetFilter={resetFilter}
							role={'2'}
							totalRow={totalRowOn}
							tableName={'revenueOn'}
							listFunctionTable={functionsTable}
							deleteName={'khóa học'}
							tooltip={'khóa học'}
							listClass={classList}
						/>
						<Grid container style={{ margin: '2rem 0' }}>
							<Divider style={{ width: '100%' }} />
						</Grid>
						<Grid container spacing={3}>
							<Grid item xs={12} md={4} lg={4}>
								<Paper>
									<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
										<Grid item xs={8} md={8} lg={8}>
											<Typography
												variant='body1'
												style={{
													color: 'rgb(107, 119, 140)',
													margin: '0 0 0.35rem',
												}}
											>
												Tổng doanh thu
											</Typography>
											<Typography
												variant='body1'
												style={{
													fontSize: '1.5rem',
													fontWeight: 'bold',
												}}
											>
												{proceedsOn.toLocaleString('vi-VN', {
													style: 'currency',
													currency: 'VND',
												})}
											</Typography>
										</Grid>
										<Grid
											item
											xs={4}
											md={4}
											lg={4}
											container
											justify='flex-end'
											alignItems='center'
										>
											<Avatar
												style={{
													backgroundColor: 'rgb(229, 57, 53)',
													width: '56px',
													height: '56px',
												}}
											>
												<MonetizationOnIcon />
											</Avatar>
										</Grid>
									</Grid>
								</Paper>
							</Grid>
							<Grid item xs={12} md={4} lg={4}>
								<Paper>
									<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
										<Grid item xs={8} md={8} lg={8}>
											<Typography
												variant='body1'
												style={{
													color: 'rgb(107, 119, 140)',
													margin: '0 0 0.35rem',
												}}
											>
												Tổng doanh số
											</Typography>
											<Typography
												variant='body1'
												style={{
													fontSize: '1.5rem',
													fontWeight: 'bold',
												}}
											>
												{saleOn.toLocaleString('vi-VN', {
													style: 'currency',
													currency: 'VND',
												})}
											</Typography>
										</Grid>
										<Grid
											item
											xs={4}
											md={4}
											lg={4}
											container
											justify='flex-end'
											alignItems='center'
										>
											<Avatar
												style={{
													backgroundColor: 'rgb(67, 160, 71)',
													width: '56px',
													height: '56px',
												}}
											>
												<MonetizationOnIcon />
											</Avatar>
										</Grid>
									</Grid>
								</Paper>
							</Grid>
						</Grid>
					</TabPanel>

					<TabPanel value={value} index={2} style={{ width: '100%' }}>
						<RenderTable2
							columnSelected={columnSelected}
							keys={keysRefund}
							pagination={refund.pagination}
							data={refund.data}
							handlePageChange={handlePageChange}
							handleUpdate={(data) => handleUpdateRefund(data)}
							exportExcel={getRefundMoneyExcel}
							roleName={'bài viết'}
							filter1={filters.filter1}
							filter2={filters.filter2}
							filter3={filters.filter3}
							handleFilterChange={handleFilterChange}
							resetFilter={resetFilter}
							role={'2'}
							totalRow={totalRow}
							tableName={'refund'}
							reload={reloadTable}
							listFunctionTable={functionsTable}
							listClass={classList}
						/>
						<Grid container style={{ margin: '2rem 0' }}>
							<Divider style={{ width: '100%' }} />
						</Grid>
						<Grid container spacing={3}>
							<Grid item xs={12} md={4} lg={4}>
								<Paper>
									<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
										<Grid item xs={8} md={8} lg={8}>
											<Typography
												variant='body1'
												style={{
													color: 'rgb(107, 119, 140)',
													margin: '0 0 0.35rem',
												}}
											>
												Tổng phí nợ tháng trước
											</Typography>
											<Typography
												variant='body1'
												style={{
													fontSize: '1.5rem',
													fontWeight: 'bold',
												}}
											>
												{feeInfo.additional_charges &&
													feeInfo.additional_charges.toLocaleString('vi-VN', {
														style: 'currency',
														currency: 'VND',
													})}
											</Typography>
										</Grid>
										<Grid
											item
											xs={4}
											md={4}
											lg={4}
											container
											justify='flex-end'
											alignItems='center'
										>
											<Avatar
												style={{
													backgroundColor: 'rgb(229, 57, 53)',
													width: '56px',
													height: '56px',
												}}
											>
												<MonetizationOnIcon />
											</Avatar>
										</Grid>
									</Grid>
								</Paper>
							</Grid>
							<Grid item xs={12} md={4} lg={4}>
								<Paper>
									<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
										<Grid item xs={8} md={8} lg={8}>
											<Typography
												variant='body1'
												style={{
													color: 'rgb(107, 119, 140)',
													margin: '0 0 0.35rem',
												}}
											>
												Tổng phí thừa tháng trước
											</Typography>
											<Typography
												variant='body1'
												style={{
													fontSize: '1.5rem',
													fontWeight: 'bold',
												}}
											>
												{feeInfo.residual_fee &&
													feeInfo.residual_fee.toLocaleString('vi-VN', {
														style: 'currency',
														currency: 'VND',
													})}
											</Typography>
										</Grid>
										<Grid
											item
											xs={4}
											md={4}
											lg={4}
											container
											justify='flex-end'
											alignItems='center'
										>
											<Avatar
												style={{
													backgroundColor: 'rgb(67, 160, 71)',
													width: '56px',
													height: '56px',
												}}
											>
												<MonetizationOnIcon />
											</Avatar>
										</Grid>
									</Grid>
								</Paper>
							</Grid>
						</Grid>
					</TabPanel>
				</Paper>
			</Grid>
		</MasterLayout>
	);
}
