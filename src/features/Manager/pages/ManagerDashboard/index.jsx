import { Avatar, Divider, Grid, Paper, Typography } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ClassIcon from '@material-ui/icons/Class';
import GroupIcon from '@material-ui/icons/Group';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import dashboardApi from 'api/dashboardApi';
import MyBarChart from 'common/components/MyBarChart';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';

function ManagerDashboard(props) {
	const notify = useContext(ToastifyContext);
	let history = useHistory();

	const [loading, setLoading] = useState(false);
	const [feedback, setFeedback] = useState({});
	const [student, setStudent] = useState({});
	const [teacher, setTeacher] = useState({});
	const [classroom, setClassroom] = useState({});
	const [absent, setAbsent] = useState({});

	const handleRouterChange = (url) => {
		history.push(`/${url}`);
	};

	useEffect(() => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await dashboardApi.getManagerDashboard();
				setLoading(false);

				setFeedback(response.feedback);
				setStudent(response.student);
				setTeacher(response.teacher);
				setClassroom(response.class);
				setAbsent(response.absent);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		get();
	}, []);

	return (
		<Grid container>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Grid container spacing={3}>
				{feedback && (
					<Grid
						item
						xs={12}
						md={3}
						lg={3}
						onClick={() => handleRouterChange('feedBack')}
						style={{ cursor: 'pointer' }}
					>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Tổng số feedback
									</Typography>
									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{feedback.feedbackThisMonth}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'rgb(229, 57, 53)',
											width: '56px',
											height: '56px',
										}}
									>
										<GroupIcon />
									</Avatar>
								</Grid>
								{feedback.feedbackCompare &&
									(feedback.checkFeedbackLastMonth !== '0' ? (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											{feedback.feedbackCompare.negative ? (
												<ArrowDownwardIcon
													style={{ color: 'rgb(183, 28, 28)' }}
												/>
											) : (
												<ArrowUpwardIcon style={{ color: 'rgb(27, 94, 32)' }} />
											)}
											{feedback.feedbackCompare.negative ? (
												<Typography style={{ color: 'rgb(183, 28, 28)' }}>
													{feedback.feedbackCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											) : (
												<Typography style={{ color: 'rgb(27, 94, 32)' }}>
													{feedback.feedbackCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											)}
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												So với tháng trước
											</Typography>
										</Grid>
									) : (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												Chưa có dữ liệu tháng trước
											</Typography>
										</Grid>
									))}
							</Grid>
						</Paper>
					</Grid>
				)}
				{absent && (
					<Grid
						item
						xs={12}
						md={3}
						lg={3}
						onClick={() => handleRouterChange('attendance')}
						style={{ cursor: 'pointer' }}
					>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Tỷ lệ vắng mặt
									</Typography>
									{absent.absentThisMonth !== null ? (
										<Typography
											variant='body2'
											style={{
												fontSize: '1.5rem',
												fontWeight: 'bold',
											}}
										>
											{absent.absentThisMonth}
										</Typography>
									) : (
										<Typography
											variant='body1'
											style={{
												fontSize: '1.1rem',
												fontWeight: 'bold',
											}}
										>
											Chưa có dữ liệu
										</Typography>
									)}
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'rgb(67, 160, 71)',
											width: '56px',
											height: '56px',
										}}
									>
										<HourglassEmptyIcon />
									</Avatar>
								</Grid>
								{absent.absentCompare &&
									(absent.absentLastMonth !== null ? (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											{absent.absentCompare.negative ? (
												<ArrowDownwardIcon
													style={{ color: 'rgb(183, 28, 28)' }}
												/>
											) : (
												<ArrowUpwardIcon style={{ color: 'rgb(27, 94, 32)' }} />
											)}
											{absent.absentCompare.negative ? (
												<Typography style={{ color: 'rgb(183, 28, 28)' }}>
													{absent.absentCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											) : (
												<Typography style={{ color: 'rgb(27, 94, 32)' }}>
													{absent.absentCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											)}
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												So với tháng trước
											</Typography>
										</Grid>
									) : (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												Chưa có dữ liệu tháng trước
											</Typography>
										</Grid>
									))}
							</Grid>
						</Paper>
					</Grid>
				)}
				{teacher && (
					<Grid
						item
						xs={12}
						md={3}
						lg={3}
						onClick={() => handleRouterChange('teacher')}
						style={{ cursor: 'pointer' }}
					>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Tổng số giáo viên
									</Typography>

									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{teacher.teacherThisMonth}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'blue',
											width: '56px',
											height: '56px',
										}}
									>
										<PeopleAltIcon />
									</Avatar>
								</Grid>
								{teacher.teacherCompare &&
									(teacher.checkTeacherLastMonth !== '0' ? (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											{teacher.teacherCompare.negative ? (
												<ArrowDownwardIcon
													style={{ color: 'rgb(183, 28, 28)' }}
												/>
											) : (
												<ArrowUpwardIcon style={{ color: 'rgb(27, 94, 32)' }} />
											)}
											{teacher.teacherCompare.negative ? (
												<Typography style={{ color: 'rgb(183, 28, 28)' }}>
													{teacher.teacherCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											) : (
												<Typography style={{ color: 'rgb(27, 94, 32)' }}>
													{teacher.teacherCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											)}
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												So với tháng trước
											</Typography>
										</Grid>
									) : (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												Chưa có dữ liệu tháng trước
											</Typography>
										</Grid>
									))}
							</Grid>
						</Paper>
					</Grid>
				)}
				{classroom && (
					<Grid
						item
						xs={12}
						md={3}
						lg={3}
						onClick={() => handleRouterChange('class')}
						style={{ cursor: 'pointer' }}
					>
						<Paper>
							<Grid container style={{ padding: '1rem 1rem 1.5rem 1rem' }}>
								<Grid item xs={8} md={8} lg={8}>
									<Typography
										variant='body2'
										style={{
											color: 'rgb(107, 119, 140)',
											margin: '0 0 0.35rem',
											textTransform: 'uppercase',
										}}
									>
										Tổng số lớp học
									</Typography>

									<Typography
										variant='body2'
										style={{
											fontSize: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{classroom.classThisMonth}
									</Typography>
								</Grid>
								<Grid
									item
									xs={4}
									md={4}
									lg={4}
									container
									justify='flex-end'
									alignItems='center'
								>
									<Avatar
										style={{
											backgroundColor: 'rgb(251, 140, 0)',
											width: '56px',
											height: '56px',
										}}
									>
										<ClassIcon />
									</Avatar>
								</Grid>
								{classroom.classCompare &&
									(classroom.checkClassLastMonth !== '0' ? (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											{classroom.classCompare.negative ? (
												<ArrowDownwardIcon
													style={{ color: 'rgb(183, 28, 28)' }}
												/>
											) : (
												<ArrowUpwardIcon style={{ color: 'rgb(27, 94, 32)' }} />
											)}
											{classroom.classCompare.negative ? (
												<Typography style={{ color: 'rgb(183, 28, 28)' }}>
													{classroom.classCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											) : (
												<Typography style={{ color: 'rgb(27, 94, 32)' }}>
													{classroom.classCompare.percent}
													%&nbsp;&nbsp;
												</Typography>
											)}
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												So với tháng trước
											</Typography>
										</Grid>
									) : (
										<Grid
											item
											md={12}
											container
											style={{ paddingTop: '0.75rem' }}
										>
											<Typography
												style={{
													color: 'rgb(107, 119, 140)',
													fontSize: '14px',
												}}
											>
												Chưa có dữ liệu tháng trước
											</Typography>
										</Grid>
									))}
							</Grid>
						</Paper>
					</Grid>
				)}
			</Grid>
			<Grid container spacing={3} style={{ marginTop: '1.5rem' }}>
				<Grid item xs={12} md={12} lg={12}>
					<Paper
						style={{
							height: '100%',
						}}
					>
						<Grid container>
							<Grid item xs={12} lg={12} md={12} style={{ padding: '1rem' }}>
								<Typography variant='body1' style={{ fontWeight: 'bold' }}>
									Tổng số học sinh
								</Typography>
							</Grid>
							<Grid item xs={12} lg={12} md={12}>
								<Divider />
							</Grid>
							<Grid item xs={12} lg={12} md={12} style={{ padding: '1rem' }}>
								{student && <MyBarChart data={student} />}
							</Grid>
						</Grid>
					</Paper>
				</Grid>
			</Grid>
		</Grid>
	);
}

export default ManagerDashboard;
