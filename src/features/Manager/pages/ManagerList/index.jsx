import {
	Avatar,
	Button,
	Checkbox,
	FormControl,
	Grid,
	Input,
	InputLabel,
	ListItemText,
	makeStyles,
	MenuItem,
	Select,
	Tooltip,
	Typography,
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import FilterListIcon from '@material-ui/icons/FilterList';
import GetAppIcon from '@material-ui/icons/GetApp';
import excelApi from 'api/excelApi';
import managerApi from 'api/managerApi';
import permissionApi from 'api/permissionApi';
import shiftApi from 'api/shiftApi';
import MyDialog from 'common/components/MyDialog';
import Pagination from 'common/components/Pagination';
import SearchForm from 'common/components/SearchForm';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router';
import { checkShow, exportToCSV } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

const statusList = [
	{ value: 0, display: 'Dừng hoạt động' },
	{ value: 1, display: 'Đang hoạt động' },
];

const useStyles = makeStyles({
	formControl: {
		minWidth: 120,
	},
});

const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: 48 * 4.5 + 8,
		},
	},
};

export default function ManagerList() {
	const notify = useContext(ToastifyContext);
	const classes = useStyles();
	let history = useHistory();

	const [managers, setManagers] = useState([]);
	const [pagination, setPagination] = useState({});
	const [shift, setShift] = useState([]);
	const [loading, setLoading] = useState(false);
	const [filters, setFilters] = useState({
		page: 1,
		q: '',
		status: '1',
		shift: '',
	});

	const [isChange, setIsChange] = useState(false);

	const handleChangeManagerDetail = (id) => {
		history.push({
			pathname: `/manager/${id}`,
		});
	};

	const handleCreateChange = () => {
		history.push({
			pathname: '/create/manager',
		});
	};

	const handlePageChange = (newPage) => {
		setFilters({
			...filters,
			page: newPage,
		});
	};

	const handleSearchChange = (newSearch) => {
		setFilters({
			...filters,
			page: 1,
			q: newSearch.value,
		});
	};

	const handleFilterChange = (e, filter) => {
		setFilters({
			...filters,
			page: 1,
			[filter]: e.target.value,
		});
	};

	const resetFilter = () => {
		setFilters({
			...filters,
			status: '',
			shift: '',
			page: 1,
		});
	};

	const exportManagers = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getManagersExcel(filters);
				setLoading(false);
				const csvData = {
					data: response,
				};
				exportToCSV(csvData, 'Danh_sach_quan_ly');
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	const [keys, setKeys] = useState([
		{
			header: 'Ảnh đại diện',
			display: true,
			align: 'center',
		},
		{
			header: 'Họ và tên',
			display: true,
			align: 'left',
		},
		{
			header: 'Mã quản lý',
			display: true,
			align: 'left',
		},
		{
			header: 'Chức vụ',
			display: true,
			align: 'left',
		},
		{
			header: 'Giới tính',
			display: true,
			align: 'left',
		},
		{
			header: 'Điện thoại',
			display: true,
			align: 'right',
		},
		{
			header: 'Email',
			display: true,
			align: 'left',
		},
		{
			header: 'Địa chỉ',
			display: true,
			align: 'left',
		},
		{
			header: 'Ca làm',
			display: true,
			align: 'left',
		},
	]);

	const [columnSelected, setColumnSelected] = React.useState([]);

	const changeDisplayColumn = (key) => {
		key.display = !key.display;
		setKeys(keys);
	};

	const handleColumnChange = (event) => {
		setColumnSelected(event.target.value);
	};

	const handleDeleteManager = (userId) => {
		const post = async () => {
			try {
				setLoading(true);
				const response = await managerApi.deleteManager(userId);
				setLoading(false);
				setIsChange(!isChange);
				notify('success', response.message);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	};

	const handleRestoreManager = (userId) => {
		const post = async () => {
			try {
				setLoading(true);
				const response = await managerApi.restoreManager(userId);
				setLoading(false);
				setIsChange(!isChange);
				notify('success', response.message);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	};

	const [isPermiss, setIsPermiss] = useState(false);
	const [functions, setFunctions] = useState([]);
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_list_manager',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const response = await permissionApi.getFunctions();
					setFunctions(response);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const value = [];
			for (let i = 0, l = keys.length; i < l; i += 1) {
				value.push(keys[i].header);
			}
			setColumnSelected(value);
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await managerApi.getManagers(filters);
					setLoading(false);
					setManagers(response.data);
					setPagination(response.pagination);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						localStorage.removeItem('menu');
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, filters, isChange]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await shiftApi.getManagerShifts();
					setLoading(false);
					response.length > 0
						? response
						: response.unshift({ slot_id: '', slot_name: 'None' });
					setShift(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	return (
		<MasterLayout header='Quản lý'>
			{isPermiss && (
				<Grid container>
					{loading && (
						<div
							style={{
								position: 'fixed',
								top: '50%',
								left: '50%',
								width: '50px',
								transform: 'translate(-50%, -50%)',
								zIndex: '1000000000',
							}}
							className='cp-spinner cp-balls'
						></div>
					)}
					<Helmet>
						<title>Quản lý | Người dùng | Xoài CMS</title>
					</Helmet>
					<Grid container style={{ marginBottom: '1rem' }}>
						<Grid item md={8}>
							<SearchForm
								onSubmit={handleSearchChange}
								label={'Tìm theo tên và mã quản lý'}
							/>
						</Grid>
						<Grid item md={4} container justify='flex-end'>
							{checkShow(functions, 'function_create_manager') && (
								<Button
									startIcon={<AddIcon />}
									color='primary'
									variant='outlined'
									onClick={handleCreateChange}
								>
									Thêm quản lý
								</Button>
							)}
						</Grid>
					</Grid>
					<Grid container alignItems='center'>
						<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
							{shift.length > 0 && (
								<FormControl className={classes.formControl}>
									<InputLabel>Ca làm</InputLabel>
									<Select
										value={filters.shift}
										onChange={(e) => handleFilterChange(e, 'shift')}
									>
										{shift.map((s, index) => (
											<MenuItem key={index} value={s.slot_id}>
												{s.slot_name}
											</MenuItem>
										))}
									</Select>
								</FormControl>
							)}
						</Grid>

						<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
							<FormControl className={classes.formControl}>
								<InputLabel>Trạng thái</InputLabel>
								<Select
									value={filters.status}
									onChange={(e) => handleFilterChange(e, 'status')}
								>
									{statusList.map((status, index) => (
										<MenuItem key={index} value={status.value}>
											{status.display}
										</MenuItem>
									))}
								</Select>
							</FormControl>
						</Grid>
						<Grid item md={2}>
							{(filters.status !== '' || filters.shift !== '') && (
								<Button
									onClick={resetFilter}
									startIcon={<FilterListIcon />}
									variant='contained'
									color='secondary'
								>
									Xóa lọc
								</Button>
							)}
						</Grid>
						<Grid item md={2}></Grid>
						<Grid item md={2} container justify='flex-end'>
							<Button
								startIcon={<GetAppIcon />}
								variant='text'
								onClick={exportManagers}
							>
								Xuất file
							</Button>
						</Grid>
						<Grid item md={2} container justify='flex-end'>
							<Select
								multiple
								value={columnSelected}
								onChange={handleColumnChange}
								input={<Input />}
								renderValue={() => 'Chọn cột hiển thị'}
								MenuProps={MenuProps}
							>
								{keys.map((key, index) => (
									<MenuItem
										onClick={() => changeDisplayColumn(key)}
										key={index}
										value={key.header}
									>
										<Checkbox
											checked={columnSelected.indexOf(key.header) > -1}
										/>
										<ListItemText primary={key.header} />
									</MenuItem>
								))}
							</Select>
						</Grid>
					</Grid>

					<TableContainer component={Paper} style={{ marginTop: '1rem' }}>
						<Table stickyHeader>
							<TableHead>
								<TableRow>
									<TableCell align='center' style={{ width: '5%' }}>
										STT
									</TableCell>
									{keys.map((key, index) => (
										<TableCell
											style={{ display: key.display ? '' : 'none' }}
											key={index}
											align={key.align}
										>
											{key.header}
										</TableCell>
									))}
									<TableCell align='center' style={{ width: '5%' }}></TableCell>

									{checkShow(functions, 'function_delete_manager') && (
										<TableCell
											align='center'
											style={{ width: '5%' }}
										></TableCell>
									)}
								</TableRow>
							</TableHead>
							<TableBody>
								{managers.length > 0 ? (
									managers.map((row, index) => (
										<TableRow hover key={index}>
											<TableCell align='center'>
												{(pagination.page - 1) * 10 + index + 1}
											</TableCell>
											<TableCell
												style={{
													display: keys[0].display ? '' : 'none',
												}}
												align='center'
											>
												<Grid container justify='center'>
													<Avatar alt='No data' src={row['avt_link']} />
												</Grid>
											</TableCell>
											<TableCell
												style={{
													display: keys[0].display ? '' : 'none',
												}}
												align='left'
											>
												{row['full_name']}
											</TableCell>
											<TableCell
												style={{
													display: keys[3].display ? '' : 'none',
												}}
												align='left'
											>
												{row['user_id']}
											</TableCell>
											<TableCell
												style={{
													display: keys[3].display ? '' : 'none',
												}}
												align='left'
											>
												{row['role_id'] === '2'
													? 'Quản lý lớp học'
													: 'Quản lý bài viết'}
											</TableCell>
											<TableCell
												style={{
													display: keys[1].display ? '' : 'none',
												}}
												align='left'
											>
												{row['gender'] === 'male' ? 'Nam' : 'Nữ'}
											</TableCell>
											<TableCell
												style={{
													display: keys[2].display ? '' : 'none',
												}}
												align='right'
											>
												{row['phone']}
											</TableCell>

											<TableCell
												style={{
													display: keys[4].display ? '' : 'none',
												}}
												align='left'
											>
												{row['email']}
											</TableCell>
											<TableCell
												style={{
													display: keys[5].display ? '' : 'none',
												}}
												align='left'
											>
												{row['address']}
											</TableCell>
											<TableCell
												style={{
													display: keys[6].display ? '' : 'none',
												}}
												align='left'
											>
												{row['slot_info']}
											</TableCell>

											<TableCell align='center'>
												{row['status'] === '1' && (
													<Tooltip title='Chi tiết'>
														<EditIcon
															style={{ cursor: 'pointer' }}
															onClick={() =>
																handleChangeManagerDetail(row['user_id'])
															}
														/>
													</Tooltip>
												)}
											</TableCell>

											{checkShow(functions, 'function_delete_manager') && (
												<TableCell align='center'>
													{row['status'] === '1' ? (
														<MyDialog
															title={`Xóa`}
															description='Tất cả dữ liệu về quản lý này sẽ bị xóa ?'
															doneText='Xóa'
															cancelText='Hủy'
															colorDone='secondary'
															iconType={'0'}
															action={() => handleDeleteManager(row['user_id'])}
														/>
													) : (
														<MyDialog
															title={`Khôi phục`}
															description='Tất cả dữ liệu về quản lý này sẽ được khôi phục ?'
															doneText='Đồng ý'
															cancelText='Hủy'
															colorDone='secondary'
															iconType={'1'}
															action={() =>
																handleRestoreManager(row['user_id'])
															}
														/>
													)}
												</TableCell>
											)}
										</TableRow>
									))
								) : (
									<TableRow>
										<TableCell align='center' colSpan={keys.length + 3}>
											Không tìm thấy dữ liệu
										</TableCell>
									</TableRow>
								)}
							</TableBody>
						</Table>
					</TableContainer>
					{managers.length > 0 && (
						<Grid container alignItems='center' style={{ marginTop: '2rem' }}>
							<Grid item md={6}>
								<Typography style={{ fontWeight: 'bold' }}>
									Tổng số ({pagination.totalRows})
								</Typography>
							</Grid>
							<Grid item md={6}>
								<Pagination
									pagination={pagination}
									onPageChange={handlePageChange}
								/>
							</Grid>
						</Grid>
					)}
				</Grid>
			)}
		</MasterLayout>
	);
}
