import {
	Avatar,
	Box,
	Button,
	Checkbox,
	Divider,
	FormControl,
	Grid,
	IconButton,
	ListItemText,
	makeStyles,
	MenuItem,
	Select,
	TextField,
	Typography,
} from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CloseIcon from '@material-ui/icons/Close';
import classApi from 'api/classApi';
import managerApi from 'api/managerApi';
import permissionApi from 'api/permissionApi';
import shiftApi from 'api/shiftApi';
import MasterLayout from 'common/pages/MasterLayout';
import InputField from 'custom-fields/InputField';
import { Field, Form, Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useParams } from 'react-router';
import { Link, useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as yup from 'yup';

const useStyles = makeStyles((theme) => ({
	postPanel: {
		marginTop: '5rem',
		marginBottom: '0.5rem',
	},
	createPostTitle: {
		fontSize: '1.25rem',
		fontWeight: 'bold',
	},
}));

const referer = [
	{
		value: 'manager',
		display: 'Quản lý',
	},
];

export default function ManagerDetail(props) {
	const notify = useContext(ToastifyContext);
	let history = useHistory();
	const classes = useStyles();
	const { managerId } = useParams();

	const [loading, setLoading] = useState(false);
	const [profile, setProfile] = useState({});
	const [listClass, setListClass] = useState([]);
	const [shifts, setShifts] = useState([]);
	const [managerShift, setManagerShift] = useState([]);
	const [isChange, setIsChange] = useState(false);

	const [selectManagerShift, setSelectManagerShift] = useState([]);

	const [canEdit, setCanEdit] = useState(false);
	const [canDelete, setCanDelete] = useState(false);

	const [open, setOpen] = useState(false);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleSelectManagerShiftChange = (e) => {
		const newList = e.target.value;
		setSelectManagerShift(newList);
	};

	const handleProfileChange = (e) => {
		setProfile({
			...profile,
			[e.target.name]: e.target.value,
		});
	};

	const handleDeleteManager = () => {
		const post = async () => {
			try {
				setLoading(true);
				const response = await managerApi.deleteManager(managerId);
				setLoading(false);
				setOpen(false);
				notify('success', response.message);
				history.push('/manager');
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	};

	const [isPermiss, setIsPermiss] = useState(false);
	const [functions, setFunctions] = useState([]);
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_list_manager',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const response = await permissionApi.getFunctions();
					const data = [];
					for (let i = 0; i < response.length; i++) {
						data.push(response[i].function_id);
					}

					if (data.includes('function_edit_manager')) {
						setCanEdit(true);
					}

					if (data.includes('function_delete_manager')) {
						setCanDelete(true);
					}

					setFunctions(data);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await managerApi.getManagerById(managerId);
					setLoading(false);
					setProfile(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 404) {
						history.push('/manager');
					}
					if (error.response.status === 403) {
						localStorage.removeItem('menu');
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, managerId]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await classApi.getClassesByManagerShift(managerId);
					setLoading(false);
					setListClass(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, managerId, isChange]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await shiftApi.getManagerShifts();
					setLoading(false);
					setShifts(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, managerId]);

	useEffect(() => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await shiftApi.getShiftsByManagerId(managerId);
				setLoading(false);
				setManagerShift(response);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		get();
	}, [managerId]);

	useEffect(() => {
		let newList = [];
		for (let i = 0; i < managerShift.length; i++) {
			newList.push(managerShift[i].slot_id);
		}

		setSelectManagerShift(newList);
	}, [managerShift.length > 0]);

	const validationSchema = yup.object().shape({
		full_name: yup.string().required('Trường không được để trống'),
		display_name: yup.string().required('Trường không được để trống'),
		email: yup
			.string()
			.email('Email không hợp lệ')
			.required('Trường không được để trống'),
		phone: yup
			.string()
			.matches(/^[0-9]+$/, 'Số điện thoại chỉ bao gồm chữ số')
			.min(10, 'Số điện thoại gồm ít nhất 10 số')
			.max(12, 'Số điện thoại gồm nhiều nhất 11 số'),
		address: yup.string(),
		note: yup.string(),
	});

	return (
		<Formik
			initialValues={profile}
			enableReinitialize
			validationSchema={validationSchema}
			onSubmit={(values) => {
				const post = async () => {
					try {
						const data = {
							profile: values,
							managerShift: selectManagerShift,
							defaultManagerShift: managerShift,
						};
						setLoading(true);
						const response = await managerApi.updateManager(managerId, data);
						setLoading(false);
						setIsChange(!isChange);
						notify('success', response.message);
					} catch (error) {
						setLoading(false);
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/');
						}
					}
				};
				post();
			}}
		>
			{(formikProps) => {
				return (
					<MasterLayout referer={referer} header={profile.full_name}>
						{isPermiss && (
							<Box>
								{loading && (
									<div
										style={{
											position: 'fixed',
											top: '50%',
											left: '50%',
											width: '50px',
											transform: 'translate(-50%, -50%)',
											zIndex: '1000000000',
										}}
										className='cp-spinner cp-balls'
									></div>
								)}
								<Helmet>
									<title>{`${profile.full_name} | Quản lý | Xoài CMS`}</title>
								</Helmet>
								<Form>
									<Grid container spacing={2}>
										<Grid container spacing={2}>
											<Grid item md={3} style={{ marginTop: '1rem' }}>
												<Grid container justifyContent='center'>
													<Avatar
														style={{
															width: '60%',
															height: 'auto',
															boxShadow:
																'0 16px 38px -12px rgb(0 0 0 / 16%), 0 4px 25px 0 rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%)',
														}}
														src={profile.avt_link}
													/>
												</Grid>
												<Grid
													container
													justifyContent='center'
													style={{ marginTop: '0.75rem' }}
												>
													<Typography style={{ fontSize: '1rem' }}>
														{formikProps.values.full_name}
													</Typography>
												</Grid>
											</Grid>
											<Grid item md={9}>
												<Grid container>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Mã số:
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<TextField
																		value={profile.user_id}
																		variant='outlined'
																		size='small'
																		margin='normal'
																		disabled
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Email:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9}>
																<Field
																	name='email'
																	component={InputField}
																	fullWidth
																	size='small'
																	disabled={!canEdit}
																/>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Họ và tên:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<Field
																		name='full_name'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Số điện thoại:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9}>
																<FormControl fullWidth>
																	<Field
																		name='phone'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Tên hiển thị:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<Field
																		name='display_name'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Địa chỉ:
															</Grid>
															<Grid item md={9}>
																<FormControl fullWidth>
																	<Field
																		name='address'
																		component={InputField}
																		fullWidth
																		size='small'
																		disabled={!canEdit}
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Giới tính:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9} lg={9}>
																{profile.gender && (
																	<FormControl
																		margin='normal'
																		style={{
																			minWidth: '120px',
																		}}
																		size='small'
																	>
																		<Select
																			disabled={!canEdit}
																			name='gender'
																			variant='outlined'
																			value={profile.gender}
																			onChange={handleProfileChange}
																		>
																			<MenuItem value={'male'}>Nam</MenuItem>
																			<MenuItem value={'female'}>Nữ</MenuItem>
																		</Select>
																	</FormControl>
																)}
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Ghi chú:
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<Field
																		disabled={!canEdit}
																		name='note'
																		component={InputField}
																		fullWidth
																		size='small'
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={2}>
															<Grid item md={3} lg={3}>
																Ngày tạo:
															</Grid>
															<Grid item md={9} lg={9}>
																<FormControl fullWidth>
																	<TextField
																		value={new Date(
																			profile.created_at
																		).toLocaleString()}
																		variant='outlined'
																		size='small'
																		margin='normal'
																		disabled
																	/>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Ca làm:
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9}>
																{shifts && (
																	<FormControl
																		style={{
																			minWidth: '120px',
																			maxWidth: '100%',
																		}}
																		margin='normal'
																		size='small'
																	>
																		<Select
																			disabled={!canEdit}
																			variant='outlined'
																			name='teachsubject'
																			id='teachsubject'
																			multiple
																			value={selectManagerShift}
																			onChange={handleSelectManagerShiftChange}
																			renderValue={() => {
																				let list = [];
																				for (
																					let i = 0;
																					i < selectManagerShift.length;
																					i++
																				) {
																					const item = shifts.find(
																						(s) =>
																							s.slot_id ===
																							selectManagerShift[i]
																					);
																					if (item !== undefined) {
																						list.push(item.slot_name);
																					}
																				}

																				return list.join(', ');
																			}}
																		>
																			{shifts.map((s, index) => (
																				<MenuItem key={index} value={s.slot_id}>
																					<Checkbox
																						checked={
																							selectManagerShift.indexOf(
																								s.slot_id
																							) > -1
																						}
																					/>
																					<ListItemText primary={s.slot_name} />
																				</MenuItem>
																			))}
																		</Select>
																	</FormControl>
																)}
															</Grid>
														</Grid>
													</Grid>

													{profile && profile.role_id === '2' && (
														<Grid item md={5} style={{ margin: '1rem 0' }}>
															<Grid container spacing={1}>
																<Grid item md={3} lg={3}>
																	Các lớp trong ca:
																</Grid>
																<Grid item md={9}>
																	<Grid container>
																		{listClass.length > 0
																			? listClass.map((cl, index) => (
																					<Grid
																						key={index}
																						item
																						md={6}
																						style={{ marginBottom: '0.5rem' }}
																					>
																						<Link
																							to={`/class/${cl.class_id}`}
																							key={index}
																						>
																							{cl.class_name}
																						</Link>
																					</Grid>
																			  ))
																			: 'Không có'}
																	</Grid>
																</Grid>
															</Grid>
														</Grid>
													)}
													<Grid item md={1}></Grid>
													<Grid item md={5}>
														<Grid container alignItems='center' spacing={1}>
															<Grid item md={3} lg={3}>
																Chức vụ
																<span style={{ color: 'red' }}>&nbsp;*</span>
															</Grid>
															<Grid item md={9}>
																{profile.role_id && (
																	<FormControl
																		margin='normal'
																		style={{
																			minWidth: '120px',
																		}}
																		size='small'
																	>
																		<Select
																			disabled={!canEdit}
																			name='role_id'
																			variant='outlined'
																			value={profile.role_id}
																			onChange={handleProfileChange}
																		>
																			<MenuItem value={'2'}>
																				Quản lý lớp học
																			</MenuItem>
																			<MenuItem value={'7'}>
																				Quản lý bài viết
																			</MenuItem>
																		</Select>
																	</FormControl>
																)}
															</Grid>
														</Grid>
													</Grid>
												</Grid>
											</Grid>
										</Grid>
										<Grid
											container
											className={classes.postPanel}
											justifyContent='flex-end'
										>
											<Grid item md={3}></Grid>
											<Grid item md={9}>
												<Grid container>
													<Grid item md={11}>
														<Grid container justifyContent='flex-end'>
															{canEdit && (
																<Button
																	style={{
																		padding: '0.5rem 1.5rem 0.5rem 1.5rem',
																		marginRight: '1rem',
																	}}
																	type='submit'
																	variant='contained'
																	color='primary'
																>
																	Cập nhập
																</Button>
															)}
															{canDelete && (
																<Button
																	style={{
																		padding: '0.5rem 1.5rem 0.5rem 1.5rem',
																	}}
																	variant='contained'
																	color='secondary'
																	onClick={handleClickOpen}
																>
																	Xóa
																</Button>
															)}
															<Dialog open={open} onClose={handleClose}>
																<Grid container>
																	<Grid item xs={9} md={9} lg={9}>
																		<DialogTitle>Xóa</DialogTitle>
																	</Grid>
																	<Grid
																		item
																		container
																		justifyContent='flex-end'
																		xs={3}
																		md={3}
																		lg={3}
																	>
																		<IconButton onClick={handleClose}>
																			<CloseIcon />
																		</IconButton>
																	</Grid>
																</Grid>
																<Divider />
																<DialogContent>
																	<DialogContentText>
																		Tất cả dữ liệu về quản lý này sẽ bị xóa ?
																	</DialogContentText>
																</DialogContent>
																<DialogActions>
																	<Button
																		variant='contained'
																		onClick={handleClose}
																	>
																		Hủy
																	</Button>
																	<Button
																		variant='contained'
																		color='secondary'
																		onClick={handleDeleteManager}
																	>
																		Xóa
																	</Button>
																</DialogActions>
															</Dialog>
														</Grid>
													</Grid>
												</Grid>
											</Grid>
										</Grid>
									</Grid>
								</Form>
							</Box>
						)}
					</MasterLayout>
				);
			}}
		</Formik>
	);
}
