import { Box } from '@material-ui/core';
import permissionApi from 'api/permissionApi';
import MasterLayout from 'common/pages/MasterLayout';
import CategorySetting from 'features/Setting/components/CategorySetting';
import RoleSetting from 'features/Setting/components/RoleSetting';
import RoomSetting from 'features/Setting/components/RoomSetting';
import SlideSetting from 'features/Setting/components/SlideSetting';
import SlotSetting from 'features/Setting/components/SlotSetting';
import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router';

export default function AccountSettingPage() {
	let history = useHistory();
	const [isPermiss, setIsPermiss] = useState(false);
	const [functions, setFunctions] = useState([]);

	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_setting_list',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				localStorage.removeItem('menu');
				history.push('/');
			}
		};

		post();
	}, []);

	// get list function
	useEffect(() => {
		if (isPermiss) {
			const getFunctions = async () => {
				try {
					const respone = await permissionApi.getFunctions();
					const data = [];
					for (let i = 0; i < respone.length; i++) {
						data.push(respone[i].function_id);
					}
					setFunctions(data);
				} catch (error) {
					localStorage.removeItem('menu');
					history.push('/');
				}
			};
			getFunctions();
		}
	}, [isPermiss]);

	return (
		<MasterLayout header='Thiết lập'>
			{isPermiss && (
				<Box style={{ padding: '2rem 5rem 0 5rem' }}>
					<Helmet>
						<title>Tùy chỉnh | Xoài CMS</title>
					</Helmet>
					{functions.includes('function_setting_permission') && <RoleSetting />}
					{(functions.includes('function_setting_post_category') ||
						functions.includes('function_setting_subject_category')) && (
						<CategorySetting />
					)}
					{functions.includes('function_setting_slider') && <SlideSetting />}
					{functions.includes('function_setting_slot') && <SlotSetting />}
					{functions.includes('function_setting_room') && (
						<RoomSetting></RoomSetting>
					)}

					{/* <Accordion>
					<AccordionSummary expandIcon={<ExpandMoreIcon />}>
						<Grid container style={{ padding: '0.25rem 0' }}>
							<Grid item md={12} style={{ marginBottom: '0.5rem' }}>
								<Typography style={{ fontWeight: 'bold', fontSize: '1.25rem' }}>
									Thông tin liên lạc
								</Typography>
							</Grid>
							<Grid item md={12}>
								<Typography style={{ color: '#555', fontSize: '14px' }}>
									Thiết lập thông tin liên lạc ở website
								</Typography>
							</Grid>
						</Grid>
					</AccordionSummary>
					<AccordionDetails>
						<Grid container>
							<Grid container>
								<Grid container style={{ paddingLeft: '1rem' }}>
									<Grid item md={5}>
										<Grid container alignItems='center'>
											<Typography style={{ marginRight: '1rem' }}>
												Tư vấn viên:
											</Typography>
											<TextField
												style={{ width: '300px' }}
												size='small'
												variant='outlined'
											/>
										</Grid>
									</Grid>
									<Grid item md={5}>
										<Grid container alignItems='center'>
											<Typography style={{ marginRight: '1rem' }}>
												Số điện thoại:
											</Typography>
											<TextField
												style={{ width: '300px' }}
												size='small'
												variant='outlined'
											/>
										</Grid>
									</Grid>
									<Grid item md={2}></Grid>
								</Grid>
								<Grid
									container
									style={{ paddingLeft: '1rem', marginTop: '2rem' }}
								>
									<Grid item md={5}>
										<Grid container alignItems='center'>
											<Typography style={{ marginRight: '3rem' }}>
												Địa chỉ:
											</Typography>
											<TextField
												style={{ width: '300px' }}
												size='small'
												variant='outlined'
											/>
										</Grid>
									</Grid>
									<Grid item md={5}>
										<Grid container alignItems='center'>
											<Typography style={{ marginRight: '4.5rem' }}>
												Email:
											</Typography>
											<TextField
												style={{ width: '300px' }}
												size='small'
												variant='outlined'
											/>
										</Grid>
									</Grid>
									<Grid item md={2}></Grid>
								</Grid>
								<Grid
									container
									style={{ marginTop: '2rem', paddingLeft: '1rem' }}
								>
									<Button
										size='small'
										variant='contained'
										color='primary'
										style={{ padding: '0.25rem 2rem' }}
									>
										Lưu thay đổi
									</Button>
								</Grid>
							</Grid>
						</Grid>
					</AccordionDetails>
				</Accordion> */}
				</Box>
			)}
		</MasterLayout>
	);
}
