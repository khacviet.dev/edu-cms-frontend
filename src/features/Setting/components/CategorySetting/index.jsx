import { Button, Divider, Grid, TextField } from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import permissionApi from 'api/permissionApi';
import postApi from 'api/postApi';
import subjectApi from 'api/subjectApi';
import DeleteDialog from 'common/components/DeleteDialog';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { ToastifyContext } from 'utils/ToastifyConfig';

const useStyles = makeStyles((theme) => ({
	fullWidth: {
		width: '100%',
	},
}));

export default function CategorySetting() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();
	const [currentId, setCurrentId] = useState(0);
	const [listCurrent, setListCurrent] = useState([]);
	const [loading, setLoading] = useState(false);
	const [listCreate, setListCreate] = useState([]);
	const [reload, setReload] = useState(false);
	const [reloadSubject, setReloadSubject] = useState(false);
	const [listCurrentSubject, setListCurrentSubject] = useState([]);
	const [listCreateSubject, setListCreateSubject] = useState([]);
	const [currentSubjectId, setCurrentSubjectId] = useState(0);
	const classes = useStyles();
	const [functions, setFunctions] = useState([]);

	// get list function
	useEffect(() => {
		const getFunctions = async () => {
			try {
				const respone = await permissionApi.getFunctions();
				const data = [];
				for (let i = 0; i < respone.length; i++) {
					data.push(respone[i].function_id);
				}
				setFunctions(data);
			} catch (error) {
				localStorage.removeItem('menu');
				history.push('/');
			}
		};
		getFunctions();
	}, []);

	// get post category
	useEffect(() => {
		setLoading(true);
		const getPostCategory = async () => {
			try {
				const response = await postApi.getPostCategory();
				setListCurrent(response.data);
				setLoading(false);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getPostCategory();
	}, [reload]);

	const handleCreateCategory = () => {
		setCurrentId((a) => a + 1);
		const new_category = {
			id: `mine${currentId}`,
		};
		const new_list = [];
		for (let i = 0; i < listCreate.length; i++) {
			const a = { ...listCreate[i] };
			new_list.push(a);
		}
		new_list.push(new_category);
		setListCreate(new_list);
	};

	const handleUpdatePostCategory = (e) => {
		// insert list

		e.preventDefault();
		const insert_list = [];
		for (let i = 0; i < listCreate.length; i++) {
			const category = document.getElementById(
				`post_category${listCreate[i].id}`
			).value;
			const description = document.getElementById(
				`description${listCreate[i].id}`
			).value;
			if (category != '' && description != '') {
				insert_list.push({
					text: category,
					description: description,
				});
			}
		}

		// current list
		const current_list = [];
		for (let i = 0; i < listCurrent.length; i++) {
			const category = document.getElementById(
				`post_category${listCurrent[i].id}`
			).value;
			const description = document.getElementById(
				`description${listCurrent[i].id}`
			).value;
			if (category != '' && description != '') {
				current_list.push({
					id: listCurrent[i].id,
					text: category,
					description: description,
				});
			}
		}
		const data = {
			insert_list: insert_list,
			current_list: current_list,
		};

		setLoading(true);
		const updatePostCategory = async () => {
			try {
				const response = await postApi.updatePostCategory(data);
				setLoading(false);
				notify('success', response.message);
				setListCreate([]);
				setReload((load) => !load);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updatePostCategory();
	};

	const handleDelete = (setting_id) => {
		const data = {
			setting_id: setting_id,
		};
		setLoading(true);
		const updateCategoryFlagD = async () => {
			try {
				const response = await postApi.updateCategoryFlagD(data);
				setLoading(false);
				notify('success', response.message);
				setReload((load) => !load);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateCategoryFlagD();
	};

	const handleDeleteCreate = (setting_id) => {
		const new_list = [];
		for (let i = 0; i < listCreate.length; i++) {
			if (listCreate[i].id !== setting_id) {
				const new_object = { ...listCreate[i] };
				new_list.push(new_object);
			}
		}
		setListCreate(new_list);
	};

	//get subject category
	useEffect(() => {
		setLoading(true);

		const getSubjects = async () => {
			try {
				const response = await subjectApi.getSubjects();
				setListCurrentSubject(response);
				setLoading(false);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getSubjects();
	}, [reloadSubject]);

	const handleCreateSubject = () => {
		setCurrentSubjectId((a) => a + 1);
		const new_category = {
			subject_id: `mineSubject${currentSubjectId}`,
		};
		const new_list = [];
		for (let i = 0; i < listCreateSubject.length; i++) {
			const a = { ...listCreateSubject[i] };
			new_list.push(a);
		}
		new_list.push(new_category);

		setListCreateSubject(new_list);
	};

	const handleUpdateSubject = (e) => {
		// insert list
		e.preventDefault();
		const insert_list = [];
		for (let i = 0; i < listCreateSubject.length; i++) {
			const subject_name = document.getElementById(
				`subject_category${listCreateSubject[i].subject_id}`
			).value;
			const description = document.getElementById(
				`subject_description${listCreateSubject[i].subject_id}`
			).value;
			if (subject_name != '' && description != '') {
				insert_list.push({
					subject_name: subject_name,
					description: description,
				});
			}
		}

		// current list
		const current_list = [];
		for (let i = 0; i < listCurrentSubject.length; i++) {
			const subject_name = document.getElementById(
				`subject_category${listCurrentSubject[i].subject_id}`
			).value;
			const description = document.getElementById(
				`subject_description${listCurrentSubject[i].subject_id}`
			).value;
			if (subject_name != '' && description != '') {
				current_list.push({
					id: listCurrentSubject[i].subject_id,
					subject_name: subject_name,
					description: description,
				});
			}
		}
		const data = {
			insert_list: insert_list,
			current_list: current_list,
		};
		setLoading(true);
		const updateSubject = async () => {
			try {
				const response = await subjectApi.updateSubject(data);
				setLoading(false);
				notify('success', response.message);
				setListCreateSubject([]);
				setReloadSubject((load) => !load);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateSubject();
	};

	const handleDeleteSubject = (subject_id) => {
		const data = {
			subject_id: subject_id,
		};
		setLoading(true);
		const updateSubjectFlagD = async () => {
			try {
				const response = await subjectApi.updateSubjectFlagD(data);
				setLoading(false);
				notify('success', response.message);
				setReloadSubject((load) => !load);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateSubjectFlagD();
	};

	const handleDeleteCreateSubject = (subject_id) => {
		const new_list = [];
		for (let i = 0; i < listCreateSubject.length; i++) {
			if (listCreateSubject[i].subject_id !== subject_id) {
				const new_object = { ...listCreateSubject[i] };
				new_list.push(new_object);
			}
		}
		setListCreateSubject(new_list);
	};

	return (
		<Accordion>
			<AccordionSummary expandIcon={<ExpandMoreIcon />}>
				<Grid container style={{ padding: '0.25rem 0' }}>
					<Grid item md={12} style={{ marginBottom: '0.5rem' }}>
						<Typography style={{ fontWeight: 'bold', fontSize: '1.25rem' }}>
							Thể loại
						</Typography>
					</Grid>
					<Grid item md={12}>
						<Typography style={{ color: '#555', fontSize: '14px' }}>
							Thiết lập thể loại cho bài viết và môn học
						</Typography>
					</Grid>
				</Grid>
			</AccordionSummary>
			<AccordionDetails>
				{loading && (
					<div
						style={{
							position: 'fixed',
							top: '50%',
							left: '50%',
							width: '50px',
							transform: 'translate(-50%, -50%)',
							zIndex: '1000000000',
						}}
						className='cp-spinner cp-balls'
					></div>
				)}
				<Grid container>
					{functions.includes('function_setting_post_category') && (
						<Accordion className={classes.fullWidth}>
							<AccordionSummary expandIcon={<ExpandMoreIcon />}>
								<Grid container style={{ padding: '0.25rem 0' }}>
									<Grid item md={12} style={{ marginBottom: '0.5rem' }}>
										<Typography
											style={{ fontWeight: 'bold', fontSize: '1.25rem' }}
										>
											Bài viết
										</Typography>
									</Grid>
									<Grid item md={12}>
										<Typography style={{ color: '#555', fontSize: '14px' }}>
											Thiết lập thể loại cho bài viết
										</Typography>
									</Grid>
								</Grid>
							</AccordionSummary>
							<AccordionDetails>
								<Grid container>
									<form
										className={classes.fullWidth}
										onSubmit={(e) => handleUpdatePostCategory(e)}
									>
										{listCurrent.map((data) => (
											<Grid
												key={`category_current_${data.id}`}
												container
												style={{ paddingLeft: '1rem', marginTop: '1rem' }}
												spacing={1}
											>
												<Divider
													style={{ width: '100%', margin: '0.25rem 0 1rem 0' }}
												/>
												<Grid item md={1}>
													<Typography>Thể loại</Typography>
												</Grid>
												<Grid item md={3}>
													<TextField
														required
														id={`post_category${data.id}`}
														key={`post_category${data.id}`}
														defaultValue={data.text}
														rows={4}
														size='small'
														variant='outlined'
														fullWidth
													/>
												</Grid>
												<Grid item md={1}></Grid>
												<Grid item md={1}>
													<Typography>Nội dung: </Typography>
												</Grid>
												<Grid item md={5}>
													<TextField
														required
														id={`description${data.id}`}
														key={`description${data.id}`}
														defaultValue={data.description}
														multiline
														rows={3}
														size='small'
														variant='outlined'
														fullWidth
													/>
												</Grid>
												<Grid item md={1}>
													<DeleteDialog
														title={`Xóa thể loại bài viết`}
														description='Bạn có chắc chắn muốn xóa không?'
														doneText='Xóa'
														cancelText='Hủy'
														icon='1'
														action={() => handleDelete(data.id)}
													/>
												</Grid>
											</Grid>
										))}

										{listCreate.map((data) => (
											<Grid
												key={`category_create_${data.id}`}
												container
												style={{ paddingLeft: '1rem', marginTop: '1rem' }}
												spacing={1}
											>
												<Divider
													style={{ width: '100%', margin: '0.25rem 0 1rem 0' }}
												/>
												<Grid item md={1}>
													<Typography>Thể loại</Typography>
												</Grid>
												<Grid item md={3}>
													<TextField
														required
														id={`post_category${data.id}`}
														key={`post_category${data.id}`}
														defaultValue={data.text}
														rows={4}
														size='small'
														variant='outlined'
														fullWidth
													/>
												</Grid>
												<Grid item md={1}></Grid>
												<Grid item md={1}>
													<Typography>Nội dung: </Typography>
												</Grid>
												<Grid item md={5}>
													<TextField
														required
														id={`description${data.id}`}
														key={`description${data.id}`}
														defaultValue={data.description}
														multiline
														rows={3}
														size='small'
														variant='outlined'
														fullWidth
													/>
												</Grid>
												<Grid item md={1}>
													<DeleteDialog
														title={`Xóa thể loại bài viết`}
														description='Bạn có chắc chắn muốn xóa không?'
														doneText='Xóa'
														cancelText='Hủy'
														icon='1'
														action={() => handleDeleteCreate(data.id)}
													/>
												</Grid>
											</Grid>
										))}
										<Grid
											container
											style={{ marginTop: '2rem', paddingLeft: '1rem' }}
										>
											<Grid item md={9}>
												<Button
													type='submit'
													value='Submit'
													size='small'
													variant='contained'
													color='primary'
													style={{ padding: '0.25rem 2rem' }}
												>
													Lưu thay đổi
												</Button>
											</Grid>

											<Grid item md={3}>
												<Button
													onClick={handleCreateCategory}
													size='small'
													variant='outlined'
													color='primary'
													style={{ padding: '0.25rem 1rem' }}
													startIcon={<AddIcon />}
												>
													Thêm thể loại
												</Button>
											</Grid>
										</Grid>
									</form>
								</Grid>
							</AccordionDetails>
						</Accordion>
					)}
					{/* // Post */}

					{functions.includes('function_setting_subject_category') && (
						<Accordion className={classes.fullWidth}>
							<AccordionSummary expandIcon={<ExpandMoreIcon />}>
								<Grid container style={{ padding: '0.25rem 0' }}>
									<Grid item md={12} style={{ marginBottom: '0.5rem' }}>
										<Typography
											style={{ fontWeight: 'bold', fontSize: '1.25rem' }}
										>
											Môn học
										</Typography>
									</Grid>
									<Grid item md={12}>
										<Typography style={{ color: '#555', fontSize: '14px' }}>
											Thiết lập thêm, sửa, xóa các môn học trong trung tâm
										</Typography>
									</Grid>
								</Grid>
							</AccordionSummary>
							<AccordionDetails>
								<Grid container>
									<form
										className={classes.fullWidth}
										onSubmit={(e) => handleUpdateSubject(e)}
									>
										{listCurrentSubject.map((data) => (
											<Grid
												key={`subject_category_current_${data.subject_id}`}
												container
												style={{ paddingLeft: '1rem', marginTop: '1rem' }}
												spacing={1}
											>
												<Divider
													style={{ width: '100%', margin: '0.25rem 0 1rem 0' }}
												/>
												<Grid item md={1}>
													<Typography>Môn học</Typography>
												</Grid>
												<Grid item md={3}>
													<TextField
														required
														id={`subject_category${data.subject_id}`}
														key={`subject_category${data.subject_id}`}
														defaultValue={data.subject_name}
														rows={4}
														size='small'
														variant='outlined'
														fullWidth
													/>
												</Grid>
												<Grid item md={1}></Grid>
												<Grid item md={1}>
													<Typography>Nội dung: </Typography>
												</Grid>
												<Grid item md={5}>
													<TextField
														required
														id={`subject_description${data.subject_id}`}
														key={`subject_description${data.subject_id}`}
														defaultValue={data.description}
														multiline
														rows={3}
														size='small'
														variant='outlined'
														fullWidth
													/>
												</Grid>
												<Grid item md={1}>
													<DeleteDialog
														title={`Xóa thể loại bài viết`}
														description='Bạn có chắc chắn muốn xóa không?'
														doneText='Xóa'
														cancelText='Hủy'
														icon='1'
														action={() => handleDeleteSubject(data.subject_id)}
													/>
												</Grid>
											</Grid>
										))}

										{listCreateSubject.map((data) => (
											<Grid
												key={`subject_category_create_${data.subject_id}`}
												container
												style={{ paddingLeft: '1rem', marginTop: '1rem' }}
												spacing={1}
											>
												<Divider
													style={{ width: '100%', margin: '0.25rem 0 1rem 0' }}
												/>
												<Grid item md={1}>
													<Typography>Môn học</Typography>
												</Grid>
												<Grid item md={3}>
													<TextField
														required
														id={`subject_category${data.subject_id}`}
														key={`subject_category${data.subject_id}`}
														defaultValue={data.subject_name}
														rows={4}
														size='small'
														variant='outlined'
														fullWidth
													/>
												</Grid>
												<Grid item md={1}></Grid>
												<Grid item md={1}>
													<Typography>Nội dung: </Typography>
												</Grid>
												<Grid item md={5}>
													<TextField
														required
														id={`subject_description${data.subject_id}`}
														key={`subject_description${data.subject_id}`}
														defaultValue={data.description}
														multiline
														rows={3}
														size='small'
														variant='outlined'
														fullWidth
													/>
												</Grid>
												<Grid item md={1}>
													<DeleteDialog
														title={`Xóa môn học`}
														description='Bạn có chắc chắn muốn xóa không?'
														doneText='Xóa'
														cancelText='Hủy'
														icon='1'
														action={() =>
															handleDeleteCreateSubject(data.subject_id)
														}
													/>
												</Grid>
											</Grid>
										))}
										<Grid
											container
											style={{ marginTop: '2rem', paddingLeft: '1rem' }}
										>
											<Grid item md={9}>
												<Button
													type='submit'
													value='Submit'
													size='small'
													variant='contained'
													color='primary'
													style={{ padding: '0.25rem 2rem' }}
												>
													Lưu thay đổi
												</Button>
											</Grid>

											<Grid item md={3}>
												<Button
													onClick={handleCreateSubject}
													size='small'
													variant='outlined'
													color='primary'
													style={{ padding: '0.25rem 1rem' }}
													startIcon={<AddIcon />}
												>
													Thêm môn học
												</Button>
											</Grid>
										</Grid>
									</form>
								</Grid>
							</AccordionDetails>
						</Accordion>
					)}
					{/* // mon hoc */}
				</Grid>
			</AccordionDetails>
		</Accordion>
	);
}
