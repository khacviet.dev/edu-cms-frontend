import { FormControl, TextField } from '@material-ui/core';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';
import React, { useState } from 'react';

const useStyles = makeStyles((theme) => ({
	input: {
		opacity: 0,
		width: '1px',
	},
}));

function SlideImageUpload(props) {
	const { type, label, autoFocus, imageUrl, disabled, index, id } = props;
	const [files, setFiles] = useState([]);
	const [image, setImage] = useState(false);
	const [isFirst, setIsFirst] = useState(false);
	const classes = useStyles();
	if (imageUrl.length != 0 && image == false && isFirst == false) {
		setImage(true);
		setIsFirst(true);
	}
	// imageUrl

	const getFileMetadata = (file) => {
		return {
			lastModified: file.lastModified,
			name: file.name,
			size: file.size,
			type: file.type,
			webkitRelativePath: file.webkitRelativePath,
		};
	};

	const handleSave = (i, file) => {
		if (i != undefined) {
			props.handleSave(i, file);
		} else {
			props.handleSave(file);
		}
	};

	const handleUploadClick = (event) => {
		let newstate = [];
		let file = event.target.files[0];
		let metadata = getFileMetadata(file);
		let url = URL.createObjectURL(file);
		newstate = [...newstate, { url, metadata }];
		setFiles(newstate);
		if (index != undefined) {
			handleSave(index, file);
		} else {
			handleSave(file);
		}
	};

	const imageResetHandler = (event) => {
		setImage(false);
		setFiles([]);
	};

	return files.length === 0 && image == false ? (
		<FormControl fullWidth>
			<CardContent>
				<Grid container justify='center' alignItems='center'>
					<TextField
						id={id}
						label={label}
						type={type}
						variant='outlined'
						margin='normal'
						required={true}
						autoFocus={autoFocus}
						accept='image/*'
						className={classes.input}
						multiple
						type='file'
						onChange={handleUploadClick}
					/>
					<label htmlFor={id}>
						<Fab component='span'>
							<AddPhotoAlternateIcon />
						</Fab>
					</label>
				</Grid>
			</CardContent>
		</FormControl>
	) : (
		<CardActionArea
			id={`card_slide${id}`}
			onClick={imageResetHandler}
			disabled={disabled}
		>
			<img
				id={`image_slide${id}`}
				width='100%'
				alt='new'
				className={classes.media}
				src={image == false ? files[0].url : imageUrl}
			/>
		</CardActionArea>
	);
}

export default SlideImageUpload;
