import { Button, Divider, Grid, TextField } from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import permissionApi from 'api/permissionApi';
import settingApi from 'api/settingApi';
import DeleteDialog from 'common/components/DeleteDialog';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { ToastifyContext } from 'utils/ToastifyConfig';

const useStyles = makeStyles((theme) => ({
	fullWidth: {
		width: '100%',
	},
	textCenter: {
		textAlign: 'center',
	},
}));

export default function RoomSetting() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();
	const [currentId, setCurrentId] = useState(0);
	const [listCurrent, setListCurrent] = useState([]);
	const [loading, setLoading] = useState(false);
	const [listCreate, setListCreate] = useState([]);
	const [reload, setReload] = useState(false);
	const classes = useStyles();
	const [functions, setFunctions] = useState([]);
	const [error, setError] = useState([]);

	// get list function
	useEffect(() => {
		const getFunctions = async () => {
			try {
				const respone = await permissionApi.getFunctions();
				const data = [];
				for (let i = 0; i < respone.length; i++) {
					data.push(respone[i].function_id);
				}
				setFunctions(data);
			} catch (error) {
				localStorage.removeItem('menu');
				history.push('/');
			}
		};
		getFunctions();
	}, []);

	// get list room
	useEffect(() => {
		setLoading(true);
		const getListRooms = async () => {
			try {
				const response = await settingApi.getListRooms();
				setListCurrent(response.data);
				const list_error = [];
				// for(let i=0;i<respone.data.length;i++){
				// 	list_error.push('');
				// }
				setLoading(false);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getListRooms();
	}, [reload]);

	const handleCreateCategory = () => {
		setCurrentId((a) => a + 1);
		const new_category = {
			room_id: `room_create${currentId}`,
		};
		const new_list = [];
		for (let i = 0; i < listCreate.length; i++) {
			const a = { ...listCreate[i] };
			new_list.push(a);
		}
		new_list.push(new_category);
		setListCreate(new_list);
	};

	const handleUpdatePostCategory = (e) => {
		// insert list

		e.preventDefault();
		const insert_list = [];
		for (let i = 0; i < listCreate.length; i++) {
			const room_name = document.getElementById(
				`room${listCreate[i].room_id}`
			).value;

			const room_address = document.getElementById(
				`room_address${listCreate[i].room_id}`
			).value;
			const room_max = document.getElementById(
				`room_max${listCreate[i].room_id}`
			).value;
			if (room_name == '' || room_address == '' || room_max == '') {
				notify('error', 'Vui lòng điền hết các thông tin');
			} else {
				insert_list.push({
					room_name: room_name,
					room_address: room_address,
					room_max: room_max,
				});
			}
		}

		// current list
		const current_list = [];
		for (let i = 0; i < listCurrent.length; i++) {
			const room_name = document.getElementById(
				`room${listCurrent[i].room_id}`
			).value;

			const room_address = document.getElementById(
				`room_address${listCurrent[i].room_id}`
			).value;
			const room_max = document.getElementById(
				`room_max${listCurrent[i].room_id}`
			).value;

			if (room_name == '' || room_address == '' || room_max == '') {
				notify('error', 'Vui lòng điền hết các thông tin');
			} else {
				current_list.push({
					room_id: listCurrent[i].room_id,
					room_name: room_name,
					room_address: room_address,
					room_max: room_max,
				});
			}
		}
		const data = {
			insert_list: insert_list,
			update_list: current_list,
		};

		setLoading(true);
		const updateRooms = async () => {
			try {
				const response = await settingApi.updateRooms(data);
				setLoading(false);
				notify('success', response.message);
				setListCreate([]);
				setReload((load) => !load);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateRooms();
	};

	const handleDelete = (room_id) => {
		const data = {
			room_id: room_id,
			flag: true,
		};
		setLoading(true);
		const updateRoomFlag = async () => {
			try {
				const response = await settingApi.updateRoomFlag(data);
				setLoading(false);
				if (response.isError) {
					const class_list = response.data;
					let message =
						'Những lớp sau đang sử dụng slot này nên không thể xóa: \r\n';
					for (let i = 0; i < class_list.length; i++) {
						message += '- ' + class_list[i].class_name + '\r\n';
					}
					notify('info', message, false);
				} else {
					notify('success', response.message);
					setReload((load) => !load);
				}
				// notify('success', response.message);
				// setReload((load) => !load);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateRoomFlag();
	};

	const handleDeleteCreate = (setting_id) => {
		const new_list = [];
		for (let i = 0; i < listCreate.length; i++) {
			if (listCreate[i].room_id !== setting_id) {
				const new_object = { ...listCreate[i] };
				new_list.push(new_object);
			}
		}
		setListCreate(new_list);
	};

	const handleRoom = (e, room_id) => {
		const data = {
			room_address: e.target.value,
			room_id: room_id,
		};
		const checkExistRoom = async () => {
			try {
				const respone = await settingApi.checkExistRoom(data);
			} catch (error) {
				notify('error', error.response.data.message); // localStorage.removeItem('menu');
				// history.push('/');
			}
		};
		checkExistRoom();
	};

	return (
		<Grid container>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			{functions.includes('function_setting_room') && (
				<Accordion className={classes.fullWidth}>
					<AccordionSummary expandIcon={<ExpandMoreIcon />}>
						<Grid container style={{ padding: '0.25rem 0' }}>
							<Grid item md={12} style={{ marginBottom: '0.5rem' }}>
								<Typography style={{ fontWeight: 'bold', fontSize: '1.25rem' }}>
									Phòng học
								</Typography>
							</Grid>
							<Grid item md={12}>
								<Typography style={{ color: '#555', fontSize: '14px' }}>
									Thiết lập phòng học
								</Typography>
							</Grid>
						</Grid>
					</AccordionSummary>
					<AccordionDetails>
						<Grid container>
							<form
								className={classes.fullWidth}
								onSubmit={(e) => handleUpdatePostCategory(e)}
							>
								<Grid container alignItems='center' item md={12}>
									<Grid item md={4} className={classes.textCenter}>
										<Typography>Tên phòng</Typography>
									</Grid>

									<Grid item md={4} className={classes.textCenter}>
										<Typography> Địa chỉ</Typography>
									</Grid>

									<Grid item md={4} className={classes.textCenter}>
										<Typography>{`Sức chứa (người)`}</Typography>
									</Grid>
								</Grid>
								{listCurrent.map((data) => (
									<Grid
										key={`room_current_${data.room_id}`}
										container
										style={{ paddingLeft: '1rem', marginTop: '1rem' }}
										spacing={1}
									>
										<Divider
											style={{ width: '100%', margin: '0.25rem 0 1rem 0' }}
										/>

										<Grid item md={4}>
											<TextField
												required
												id={`room${data.room_id}`}
												key={`room${data.room_id}`}
												defaultValue={data.room_name}
												rows={4}
												size='small'
												variant='outlined'
												fullWidth
											/>
										</Grid>

										<Grid item md={4}>
											<TextField
												required
												onBlur={(e) => handleRoom(e, data.room_id)}
												id={`room_address${data.room_id}`}
												key={`room_address${data.room_id}`}
												defaultValue={data.room_address}
												size='small'
												variant='outlined'
												fullWidth
											/>
										</Grid>

										<Grid item md={3}>
											<TextField
												required
												id={`room_max${data.room_id}`}
												key={`room_max${data.room_id}`}
												type='number'
												defaultValue={data.max_student}
												inputProps={{
													className: classes.input,
													pattern: '^[0-9]*',
													min: 1,
												}}
												size='small'
												variant='outlined'
												fullWidth
											/>
										</Grid>
										<Grid item md={1}>
											<DeleteDialog
												title={`Xóa phòng học`}
												description='Bạn có chắc chắn muốn xóa không?'
												doneText='Xóa'
												tooltip_text='Xóa phòng học'
												cancelText='Hủy'
												icon='1'
												action={() => handleDelete(data.room_id)}
											/>
										</Grid>
									</Grid>
								))}

								{listCreate.map((data) => (
									<Grid
										key={`room_create_${data.room_id}`}
										container
										style={{ paddingLeft: '1rem', marginTop: '1rem' }}
										spacing={1}
									>
										<Divider
											style={{ width: '100%', margin: '0.25rem 0 1rem 0' }}
										/>

										<Grid item md={4}>
											<TextField
												required
												id={`room${data.room_id}`}
												key={`room${data.room_id}`}
												defaultValue={data.room_name}
												rows={4}
												size='small'
												variant='outlined'
												fullWidth
											/>
										</Grid>

										<Grid item md={4}>
											<TextField
												required
												onBlur={(e) => handleRoom(e, data.room_id)}
												id={`room_address${data.room_id}`}
												key={`room_address${data.room_id}`}
												defaultValue={data.room_address}
												size='small'
												variant='outlined'
												fullWidth
											/>
										</Grid>

										<Grid item md={3}>
											<TextField
												required
												id={`room_max${data.room_id}`}
												key={`room_max${data.room_id}`}
												type='number'
												defaultValue={data.max_student}
												inputProps={{
													className: classes.input,
													pattern: '^[0-9]*',
												}}
												size='small'
												variant='outlined'
												fullWidth
											/>
										</Grid>
										<Grid item md={1}>
											<DeleteDialog
												title={`Xóa phòng học`}
												description='Bạn có chắc chắn muốn xóa không?'
												doneText='Xóa'
												tooltip_text='Xóa phòng học'
												cancelText='Hủy'
												icon='1'
												action={() => handleDeleteCreate(data.room_id)}
											/>
										</Grid>
									</Grid>
								))}
								<Grid
									container
									style={{ marginTop: '2rem', paddingLeft: '1rem' }}
								>
									<Grid item md={9}>
										<Button
											type='submit'
											value='Submit'
											size='small'
											variant='contained'
											color='primary'
											style={{ padding: '0.25rem 2rem' }}
										>
											Lưu thay đổi
										</Button>
									</Grid>

									<Grid item md={3}>
										<Button
											onClick={handleCreateCategory}
											size='small'
											variant='outlined'
											color='primary'
											style={{ padding: '0.25rem 1rem' }}
											startIcon={<AddIcon />}
										>
											Thêm phòng học
										</Button>
									</Grid>
								</Grid>
							</form>
						</Grid>
					</AccordionDetails>
				</Accordion>
			)}
		</Grid>
	);
}
