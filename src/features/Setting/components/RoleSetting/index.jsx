import {
	Button,
	Checkbox,
	Divider,
	FormControl,
	FormControlLabel,
	Grid,
	MenuItem,
	Select,
} from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import permissionApi from 'api/permissionApi';
import settingApi from 'api/settingApi';
import React, { useContext, useEffect } from 'react';
import { ToastifyContext } from 'utils/ToastifyConfig';

function CustomerFormControlLabel(props) {
	const data = props.data;
	const [isChecked, setIsChecked] = React.useState(data.is_permiss);
	const handleChange = (event) => {
		setIsChecked(event.target.checked);
	};
	return (
		<Grid item md={4}>
			<FormControlLabel
				control={
					<Checkbox
						id={`role_setting${data.id}`}
						checked={isChecked}
						onChange={handleChange}
						name='checkedB'
						color='primary'
					/>
				}
				label={data.text}
			/>
		</Grid>
	);
}

export default function RoleSetting() {
	const notify = useContext(ToastifyContext);
	const [selectedRole, setSelectedRole] = React.useState({
		select: '2',
	});
	const [listFunction, setListFunction] = React.useState([]);
	const [loading, setLoading] = React.useState(false);
	const [listRole,setListRole]= React.useState([]);

	useEffect(() => {
		setLoading(true);
		const data = {
			role_id: selectedRole.select,
		};
		const getListFunctionByRole = async () => {
			try {
				const response = await settingApi.getListFunctionByRole(data);

				// const new_list = [];
				// const res_data=response.data;
				// for(let i=0;i<res_data.length;i++){
				// 	const new_object = {...res_data[i],};
				// 	new_list.push(new_object);
				// }
				setListFunction(response.data);
				setLoading(false);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getListFunctionByRole();
	}, [selectedRole]);

	useEffect(() => {
		setLoading(true);
		const getRolesSetting = async () => {
			try {
				const response = await settingApi.getRolesSetting();
				setListRole(response);
	
				setLoading(false);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getRolesSetting();
	}, []);

	




	const handleChange = (event) => {
		setState({ ...state, [event.target.name]: event.target.checked });
	};

	const handleUpdate = () => {
		const list_function_id = [];
		for (let i = 0; i < listFunction.length; i++) {
			const checked = document.getElementById(
				`role_setting${listFunction[i].id}`
			).checked;
			const data = {
				function_id: listFunction[i].id,
				checked: !checked,
			};
			list_function_id.push(data);
		}

		setLoading(true);
		const data = {
			list_function_id: list_function_id,
			role_id: selectedRole.select,
		};
		const updatePermission = async () => {
			try {
				const response = await permissionApi.updatePermission(data);

				// setListFunction(response.data);
				// const string_copy = selectedRole.slice();
				const new_select = {
					...selectedRole,
					message: '',
				};
				setSelectedRole(new_select);

				setLoading(false);
				notify('success', response.message);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updatePermission();
	};

	const handleSelectChange = (e) => {
		const select = {
			select: e.target.value,
		};
		setSelectedRole(select);
	};

	return (
		<Accordion>
			<AccordionSummary expandIcon={<ExpandMoreIcon />}>
				<Grid container style={{ padding: '0.25rem 0' }}>
					<Grid item md={12} style={{ marginBottom: '0.5rem' }}>
						<Typography style={{ fontWeight: 'bold', fontSize: '1.25rem' }}>
							Tài khoản
						</Typography>
					</Grid>
					<Grid item md={12}>
						<Typography style={{ color: '#555', fontSize: '14px' }}>
							Chỉ định các chức năng cho từng loại tài khoản
						</Typography>
					</Grid>
				</Grid>
			</AccordionSummary>

			<AccordionDetails>
				{loading && (
					<div
						style={{
							position: 'fixed',
							top: '50%',
							left: '50%',
							width: '50px',
							transform: 'translate(-50%, -50%)',
							zIndex: '1000000000',
						}}
						className='cp-spinner cp-balls'
					></div>
				)}
				<Grid container>
					<Grid container>
						<Grid container style={{ marginBottom: '1rem' }}>
							<Typography style={{ fontWeight: 'bold', fontSize: '1.1rem' }}>
								Phân quyền
							</Typography>
							<Divider style={{ width: '100%', marginTop: '0.25rem' }} />
						</Grid>
						<Grid container alignItems='center' style={{ paddingLeft: '1rem' }}>
							<Typography>Chọn loại:</Typography>
							<FormControl
								style={{
									minWidth: '200px',
									marginLeft: '5rem',
								}}
								size='small'
							>
								<Select
									onChange={(e) => handleSelectChange(e)}
									ariant='outlined'
									defaultValue={2}
								>
									{listRole &&
											listRole.map((data, index) => (
												<MenuItem key={`role${index}`} value={data.role_id}>{data.role_value}</MenuItem>
											))}
									{/* <MenuItem value={2}>Quản lý</MenuItem>
									<MenuItem value={3}>Giáo viên</MenuItem>
									<MenuItem value={4}>Trợ giảng</MenuItem>
									<MenuItem value={5}>Học sinh</MenuItem>
									<MenuItem value={6}>Marketing</MenuItem> */}
								</Select>
							</FormControl>
						</Grid>
						<Grid container style={{ marginTop: '2rem', paddingLeft: '1rem' }}>
							<Grid container>
								<Grid container style={{ marginBottom: '0.5rem' }}>
									<Grid item md={2}>
										<Typography>Chức năng:</Typography>
									</Grid>
									<Grid container item md={10}>
										{!loading &&
											listFunction.map((data, index) => (
												<CustomerFormControlLabel
													key={index}
													data={data}
												></CustomerFormControlLabel>
											))}
									</Grid>

									<Grid
										container
										style={{ marginTop: '2rem', paddingLeft: '1rem' }}
									>
										<Button
											size='small'
											variant='contained'
											color='primary'
											style={{ padding: '0.25rem 2rem' }}
											onClick={handleUpdate}
										>
											Lưu thay đổi
										</Button>
									</Grid>
								</Grid>
							</Grid>
						</Grid>
					</Grid>

					{/* <Grid container style={{ marginTop: '1rem' }}>
					<Grid container style={{ margin: '1rem 0rem' }}>
						<Typography
							style={{ fontWeight: 'bold', fontSize: '1.1rem' }}
						>
							Thiết lập tài khoản mới
						</Typography>
						<Divider style={{ width: '100%', marginTop: '0.25rem' }} />
					</Grid>
					<Grid container style={{ paddingLeft: '1rem' }}>
						<Grid md={1}>
							<Grid container justify='center'>
								<img
									style={{
										width: '100%',
										height: 'auto',
										marginBottom: '1rem',
									}}
									src='https://i.pravatar.cc'
									alt='no'
								/>
								<Button variant='outlined' size='small'>
									Thay ảnh
								</Button>
							</Grid>
						</Grid>
						<Grid item md={1}></Grid>
						<Grid md={9}>
							<Grid container alignItems='center'>
								<Typography style={{ marginRight: '1rem' }}>
									Mật khẩu mặc định:
								</Typography>
								<TextField size='small' variant='outlined' />
							</Grid>
						</Grid>
					</Grid>
				</Grid>
				<Grid
					container
					style={{ marginTop: '2rem', paddingLeft: '1rem' }}
				>
					<Button
						size='small'
						variant='contained'
						color='primary'
						style={{ padding: '0.25rem 2rem' }}
					>
						Lưu thay đổi
					</Button>
				</Grid> */}
				</Grid>
			</AccordionDetails>
		</Accordion>
	);
}
