import { Button, Grid, IconButton, TextField } from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import CopyrightIcon from '@material-ui/icons/Copyright';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ImageIcon from '@material-ui/icons/Image';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import settingApi from 'api/settingApi';
import DeleteDialog from 'common/components/DeleteDialog';
import SlideImageUpload from 'features/Setting/components/SlideImageUpload';
import React, { useContext, useEffect, useState } from 'react';
import { ToastifyContext } from 'utils/ToastifyConfig';

const useStyles = makeStyles((theme) => ({
	fullWidth: {
		width: '100%',
	},
	none: {
		display: 'none',
	},
}));

const LightTooltip = withStyles((theme) => ({
	tooltip: {
		backgroundColor: theme.palette.common.black,
		color: 'white',
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

export default function SlideSetting() {
	const classes = useStyles();
	const notify = useContext(ToastifyContext);
	const [loading, setLoading] = useState(false);
	const [imageUrl, setImageUrl] = useState('');
	const [listCurrent, setListCurrent] = useState([]);

	const [listCreated, setListCreated] = useState([]);
	const [isReload, setIsReload] = useState(false);

	const [currentId, setCurrentId] = useState(0);

	useEffect(() => {
		setLoading(true);
		const getListSlides = async () => {
			try {
				const response = await settingApi.getListSlides();
				setListCurrent(response.data);
				setLoading(false);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getListSlides();
	}, [isReload]);

	const handleSave = (index, childData) => {
		listCreated[index].image_link = childData;
		listCreated[index].isChangePic = true;
		setListCreated(listCreated);
	};

	const handleSaveCurrent = (index, childData) => {
		listCurrent[index].image_link = childData;
		listCurrent[index].isChangePic = true;
		setListCurrent(listCurrent);
	};

	const handleAddSlide = (index) => {
		setCurrentId((a) => a + 1);
		const new_slide = {
			slider_id: `new_slide${currentId}`,
			image_link: '',
			redirect_link: '',
			is_show: true,
			isChangePic: false,
		};
		const new_list = [];
		for (let i = 0; i < listCreated.length; i++) {
			const a = { ...listCreated[i] };
			new_list.push(a);
		}
		new_list.push(new_slide);
		setListCreated(new_list);
	};

	const handleUpdateSlide = (e) => {
		e.preventDefault();
		const formData = new FormData();
		const listInsert = [];
		for (let i = 0; i < listCreated.length; i++) {
			const image_link = listCreated[i].image_link;
			const redirect_link = document.getElementById(
				`slide_link_create${listCreated[i].slider_id}`
			).value;
			const is_show = document
				.getElementById(`EyeOff${listCreated[i].slider_id}`)
				.classList.contains(classes.none)
				? true
				: false;

			const data = {
				redirect_link: redirect_link,
				is_show: is_show,
				isChangePic: listCreated[i].isChangePic,
			};
			listInsert.push(data);
			formData.append('upload', image_link);
		}

		const listUpdate = [];
		for (let i = 0; i < listCurrent.length; i++) {
			const image_link = listCurrent[i].image_link;
			const redirect_link = document.getElementById(
				`slide_link${listCurrent[i].slider_id}`
			).value;
			const is_show = document
				.getElementById(`EyeOff${listCurrent[i].slider_id}`)
				.classList.contains(classes.none)
				? true
				: false;

			const data = {
				slider_id: listCurrent[i].slider_id,
				redirect_link: redirect_link,
				is_show: is_show,
				isChangePic: listCurrent[i].isChangePic,
			};
			listUpdate.push(data);
			if (
				listCurrent[i].isChangePic != undefined &&
				listCurrent[i].isChangePic == true
			) {
				formData.append('upload', image_link);
			}
		}
		formData.append('insert_list', JSON.stringify(listInsert));
		formData.append('update_list', JSON.stringify(listUpdate));

		setLoading(true);
		const updateSlides = async () => {
			try {
				const response = await settingApi.updateSlides(formData);
				// setListCurrentSubject(response);
				setListCreated([]);
				setIsReload((loading) => !loading);
				setLoading(false);
				notify('success', response.message);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateSlides();
	};

	const handdleVisibilityIcon = (type, slider_id) => {
		if (type == 0) {
			document
				.getElementById(`EyeOff${slider_id}`)
				.classList.remove(classes.none);
			document.getElementById(`EyeOn${slider_id}`).classList.add(classes.none);
		} else {
			document.getElementById(`EyeOff${slider_id}`).classList.add(classes.none);
			document
				.getElementById(`EyeOn${slider_id}`)
				.classList.remove(classes.none);
		}
	};

	const handleDeleteCurrent = (slider_id) => {
		setLoading(true);
		const data = {
			slider_id: slider_id,
		};
		const updateSlidesFlag = async () => {
			try {
				const response = await settingApi.updateSlidesFlag(data);
				setIsReload((loading) => !loading);
				setLoading(false);
				notify('success', response.message);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateSlidesFlag();
	};

	const handleDeleteCreate = (slider_id) => {
		setLoading(true);
		const new_list = [];
		for (let i = 0; i < listCreated.length; i++) {
			if (listCreated[i].slider_id !== slider_id) {
				const new_object = { ...listCreated[i] };
				new_list.push(new_object);
			}
		}
		setListCreated(new_list);
		setLoading(false);
	};

	const handleCopy = (id) => {
		const copyText = document.getElementById(id);

		/* Select the text field */
		copyText.select();
		copyText.setSelectionRange(0, 99999); /* For mobile devices */

		/* Copy the text inside the text field */
		document.execCommand('copy');

		/* Alert the copied text */
		notify('success', 'Copied to clipboard');
	};

	return (
		<Accordion>
			<AccordionSummary expandIcon={<ExpandMoreIcon />}>
				<Grid container style={{ padding: '0.25rem 0' }}>
					<Grid item md={12} style={{ marginBottom: '0.5rem' }}>
						<Typography style={{ fontWeight: 'bold', fontSize: '1.25rem' }}>
							Slide
						</Typography>
					</Grid>
					<Grid item md={12}>
						<Typography style={{ color: '#555', fontSize: '14px' }}>
							Thiết lập ảnh, số lượng slide ở website
						</Typography>
					</Grid>
				</Grid>
			</AccordionSummary>
			<AccordionDetails>
				<Grid container spacing={2}>
					{loading && (
						<div
							style={{
								position: 'fixed',
								top: '50%',
								left: '50%',
								width: '50px',
								transform: 'translate(-50%, -50%)',
								zIndex: '1000000000',
							}}
							className='cp-spinner cp-balls'
						></div>
					)}

					<form
						className={classes.fullWidth}
						onSubmit={(e) => handleUpdateSlide(e)}
					>
						<Grid container>
							{listCurrent.map((data, index) => (
								<Grid
									key={`slide_list_current${data.slider_id}`}
									container
									style={{ paddingLeft: '1rem' }}
								>
									<Grid item md={7}>
										<Grid item xs={11} sm={11}>
											<ListItem>
												<ListItemIcon className={classes.black}>
													<ImageIcon />
												</ListItemIcon>
												<ListItemText
													className={classes.black}
													primary='Thumbnail'
												/>
											</ListItem>
											<SlideImageUpload
												handleSave={(index, file) =>
													handleSaveCurrent(index, file)
												}
												imageUrl={data.image_link}
												id={`slide_image${data.slider_id}`}
												index={index}
											></SlideImageUpload>
										</Grid>
									</Grid>
									<Grid item md={3}>
										<ListItem>
											<ListItemIcon className={classes.black}>
												<ImageIcon />
											</ListItemIcon>
											<ListItemText
												className={classes.black}
												primary='Đường link'
											/>
											<ListItemIcon className={classes.black}>
												<LightTooltip placement='top' title='Copy video link'>
													<CopyrightIcon
														onClick={(e) =>
															handleCopy(`slide_link${data.slider_id}`)
														}
													></CopyrightIcon>
												</LightTooltip>
											</ListItemIcon>
										</ListItem>

										<TextField
											required
											id={`slide_link${data.slider_id}`}
											key={`slide_link${data.slider_id}`}
											defaultValue={data.redirect_link}
											rows={4}
											size='small'
											variant='outlined'
											fullWidth
										/>
									</Grid>

									<Grid item container md={1} alignItems='flex-start'>
										<DeleteDialog
											title={`Xóa slide`}
											description='Bạn có chắc chắn muốn xóa không?'
											doneText='Xóa'
											cancelText='Hủy'
											tooltip_text='Xóa slide'
											icon='1'
											action={() => handleDeleteCurrent(data.slider_id)}
										/>
									</Grid>

									<Grid
										id={`EyeOn${data.slider_id}`}
										item
										container
										md={1}
										alignItems='flex-start'
										className={data.is_show ? '' : classes.none}
									>
										<LightTooltip
											placement='top'
											title='Click để ẩn slide trên web'
										>
											<IconButton
												onClick={(type, id) =>
													handdleVisibilityIcon(0, `${data.slider_id}`)
												}
											>
												<VisibilityIcon />
											</IconButton>
										</LightTooltip>
									</Grid>

									<Grid
										id={`EyeOff${data.slider_id}`}
										item
										container
										md={1}
										alignItems='flex-start'
										className={data.is_show ? classes.none : ''}
									>
										<LightTooltip
											placement='top'
											title='Click để hiện slide trên web'
										>
											<IconButton
												onClick={(type, id) =>
													handdleVisibilityIcon(1, `${data.slider_id}`)
												}
											>
												<VisibilityOffIcon />
											</IconButton>
										</LightTooltip>
									</Grid>
								</Grid>
							))}

							{listCreated.map((data, index) => (
								<Grid
									key={`slide_list_create${data.slider_id}`}
									container
									style={{ paddingLeft: '1rem' }}
								>
									<Grid item md={7}>
										<Grid item xs={11} sm={11}>
											<ListItem>
												<ListItemIcon className={classes.black}>
													<ImageIcon />
												</ListItemIcon>
												<ListItemText
													className={classes.black}
													primary='Thumbnail'
												/>
											</ListItem>
											<SlideImageUpload
												handleSave={(index, file) => handleSave(index, file)}
												imageUrl={imageUrl}
												id={`slide_image_create${data.slider_id}`}
												index={index}
											></SlideImageUpload>
										</Grid>
									</Grid>
									<Grid item md={3}>
										<ListItem>
											<ListItemIcon className={classes.black}>
												<ImageIcon />
											</ListItemIcon>
											<ListItemText
												className={classes.black}
												primary='Đường link'
											/>
											<ListItemIcon className={classes.black}>
												<LightTooltip placement='top' title='Copy video link'>
													<CopyrightIcon
														onClick={(e) =>
															handleCopy(`slide_link_create${data.slider_id}`)
														}
													></CopyrightIcon>
												</LightTooltip>
											</ListItemIcon>
										</ListItem>
										<TextField
											required
											id={`slide_link_create${data.slider_id}`}
											key={`slide_link_create${data.slider_id}`}
											defaultValue={data.redirect_link}
											rows={4}
											size='small'
											variant='outlined'
											fullWidth
										/>
									</Grid>
									<Grid item container md={1} alignItems='flex-start'>
										<DeleteDialog
											title={`Xóa slide`}
											description='Bạn có chắc chắn muốn xóa không?'
											doneText='Xóa'
											cancelText='Hủy'
											tooltip_text='Xóa slide'
											icon='1'
											action={() => handleDeleteCreate(data.slider_id)}
										/>
									</Grid>

									<Grid
										id={`EyeOn${data.slider_id}`}
										item
										container
										md={1}
										alignItems='flex-start'
										className={data.is_show ? '' : classes.none}
									>
										<LightTooltip
											placement='top'
											title='Click để ẩn slide trên web'
										>
											<IconButton
												onClick={(type, id) =>
													handdleVisibilityIcon(0, `${data.slider_id}`)
												}
											>
												<VisibilityIcon />
											</IconButton>
										</LightTooltip>
									</Grid>

									<Grid
										id={`EyeOff${data.slider_id}`}
										item
										container
										md={1}
										alignItems='flex-start'
										className={data.is_show ? classes.none : ''}
									>
										<LightTooltip
											placement='top'
											title='Click để hiện slide trên web'
										>
											<IconButton
												onClick={(type, id) =>
													handdleVisibilityIcon(1, `${data.slider_id}`)
												}
											>
												<VisibilityOffIcon />
											</IconButton>
										</LightTooltip>
									</Grid>
								</Grid>
							))}

							<Grid
								container
								style={{ marginTop: '2rem', paddingLeft: '1rem' }}
							>
								<Grid item md={9}>
									<Button
										type='submit'
										value='Submit'
										size='small'
										variant='contained'
										color='primary'
										style={{ padding: '0.25rem 2rem' }}
									>
										Lưu thay đổi
									</Button>
								</Grid>

								<Grid item md={3}>
									<Button
										onClick={handleAddSlide}
										size='small'
										variant='outlined'
										color='primary'
										style={{ padding: '0.25rem 1rem' }}
										startIcon={<AddIcon />}
									>
										Thêm slide
									</Button>
								</Grid>
							</Grid>
						</Grid>
					</form>
				</Grid>
			</AccordionDetails>
		</Accordion>
	);
}
