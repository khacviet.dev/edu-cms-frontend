import { Button, Divider, Grid, TextField } from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import permissionApi from 'api/permissionApi';
import settingApi from 'api/settingApi';
import DeleteDialog from 'common/components/DeleteDialog';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { ToastifyContext } from 'utils/ToastifyConfig';

const useStyles = makeStyles((theme) => ({
	fullWidth: {
		width: '100%',
	},
}));

export default function SlotSetting() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();
	const [currentId, setCurrentId] = useState(0);
	const [listCurrent, setListCurrent] = useState([]);
	const [loading, setLoading] = useState(false);
	const [listCreate, setListCreate] = useState([]);
	const [reload, setReload] = useState(false);
	const [reloadSlotT1, setReloadSlotT1] = useState(false);
	const [listCurrentSlotT1, setListCurrentSlotT1] = useState([]);
	const [listCreateSlotT1, setListCreateSlotT1] = useState([]);
	const [currentSlotT1Id, setCurrentSlotT1Id] = useState(0);
	const classes = useStyles();
	const [functions, setFunctions] = useState([]);

	const [errorTime, setErrorTime] = useState([]);
	const [errorTimeCreate, setErrorTimeCreate] = useState([]);

	const [errorTimeT1, setErrorTimeT1] = useState([]);
	const [errorTimeT1Create, setErrorTimeT1Create] = useState([]);

	// get list function
	useEffect(() => {
		const getFunctions = async () => {
			try {
				const respone = await permissionApi.getFunctions();
				const data = [];
				for (let i = 0; i < respone.length; i++) {
					data.push(respone[i].function_id);
				}
				setFunctions(data);
			} catch (error) {
				localStorage.removeItem('menu');
				history.push('/');
			}
		};
		getFunctions();
	}, []);

	// get list slot
	useEffect(() => {
		setLoading(true);
		const getListSlot = async () => {
			try {
				const response = await settingApi.getListSlot('0');
				setListCurrent(response.data);
				const data = response.data;
				const listError = [];
				for (let i = 0; i < data.length; i++) {
					listError.push('');
				}
				setErrorTime(listError);
				setLoading(false);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getListSlot();
	}, [reload]);

	// them slot
	const handleCreateSlot = () => {
		setCurrentId((a) => a + 1);
		const new_slot = {
			slot_id: `slot_create${currentId}`,
			start_time: '01:00',
			end_time: '01:01',
		};
		const new_list = [];
		for (let i = 0; i < listCreate.length; i++) {
			const a = { ...listCreate[i] };
			new_list.push(a);
		}
		new_list.push(new_slot);

		setListCreate(new_list);

		const arrayCopy = [...errorTimeCreate];
		arrayCopy.push('');
		setErrorTimeCreate(arrayCopy);
	};

	const handleUpdateSlot = (e) => {
		// insert list

		e.preventDefault();
		for (let i = 0; i < errorTime.length; i++) {
			if (errorTime[i] == true) {
				notify('error', 'Vui lòng chọn lại thời gian');
				return;
			}
		}

		for (let i = 0; i < errorTimeCreate.length; i++) {
			if (errorTimeCreate[i] == true) {
				notify('error', 'Vui lòng chọn lại thời gian');
				return;
			}
		}

		const insert_list = [];
		for (let i = 0; i < listCreate.length; i++) {
			const slot_name = document.getElementById(
				`slot${listCreate[i].slot_id}`
			).value;
			const start_time = document.getElementById(
				`start_time${listCreate[i].slot_id}`
			).value;
			const end_time = document.getElementById(
				`end_time${listCreate[i].slot_id}`
			).value;
			insert_list.push({
				slot_id: listCreate[i].slot_id,
				slot_name: slot_name,
				start_time: start_time,
				end_time: end_time,
			});
		}

		// current list
		const current_list = [];
		for (let i = 0; i < listCurrent.length; i++) {
			const slot_name = document.getElementById(
				`slot${listCurrent[i].slot_id}`
			).value;
			const start_time = document.getElementById(
				`start_time${listCurrent[i].slot_id}`
			).value;
			const end_time = document.getElementById(
				`end_time${listCurrent[i].slot_id}`
			).value;
			current_list.push({
				slot_id: listCurrent[i].slot_id,
				slot_name: slot_name,
				start_time: start_time,
				end_time: end_time,
			});
		}
		const data = {
			insert_list: insert_list,
			update_list: current_list,
			type: '0',
		};
		setLoading(true);
		const updateSlot = async () => {
			try {
				const response = await settingApi.updateSlot(data);
				setLoading(false);
				notify('success', response.message);
				setListCreate([]);
				setReload((load) => !load);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateSlot();
	};

	// xoa 1 slot
	const handleDelete = (setting_id) => {
		const data = {
			slot_id: setting_id,
			flag: true,
		};
		setLoading(true);
		const updateSlotFlag = async () => {
			try {
				const response = await settingApi.updateSlotFlag(data);
				setLoading(false);
				if (response.isError) {
					const class_list = response.data;
					let message =
						'Những lớp sau đang sử dụng slot này nên không thể xóa: \r\n';
					for (let i = 0; i < class_list.length; i++) {
						message += '- ' + class_list[i].class_name + '\r\n';
					}
					notify('info', message, false);
				} else {
					notify('success', response.message);
					setReload((load) => !load);
				}
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateSlotFlag();
	};

	// xoa 1 slot vừa tạo xong
	const handleDeleteCreate = (setting_id) => {
		const new_list = [];
		for (let i = 0; i < listCreate.length; i++) {
			if (listCreate[i].slot_id !== setting_id) {
				const new_object = { ...listCreate[i] };
				new_list.push(new_object);
			}
		}
		setListCreate(new_list);
	};

	//get ca làm việc
	useEffect(() => {
		setLoading(true);
		const getListSlot = async () => {
			try {
				const response = await settingApi.getListSlot('1');
				setListCurrentSlotT1(response.data);
				const data = response.data;
				const listError = [];
				for (let i = 0; i < data.length; i++) {
					listError.push('');
				}
				setErrorTimeT1(listError);
				setLoading(false);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getListSlot();
	}, [reloadSlotT1]);

	const handleCreateSlotT1 = () => {
		setCurrentSlotT1Id((a) => a + 1);
		const new_category = {
			slot_id: `slotT1_create${currentSlotT1Id}`,
			start_time: '01:00',
			end_time: '01:01',
		};
		const new_list = [];
		for (let i = 0; i < listCreateSlotT1.length; i++) {
			const a = { ...listCreateSlotT1[i] };
			new_list.push(a);
		}
		new_list.push(new_category);

		const arrayCopy = [...errorTimeT1Create];

		arrayCopy.push('');
		setErrorTimeT1Create(arrayCopy);
		setListCreateSlotT1(new_list);
	};

	const handleUpdateSlotT1 = (e) => {
		e.preventDefault();
		for (let i = 0; i < errorTimeT1.length; i++) {
			if (errorTimeT1[i] == true) {
				notify('error', 'Vui lòng chọn lại thời gian');
				return;
			}
		}

		for (let i = 0; i < errorTimeT1Create.length; i++) {
			if (errorTimeT1Create[i] == true) {
				notify('error', 'Vui lòng chọn lại thời gian');
				return;
			}
		}

		const insert_list = [];
		for (let i = 0; i < listCreateSlotT1.length; i++) {
			const slot_name = document.getElementById(
				`slot_T1${listCreateSlotT1[i].slot_id}`
			).value;
			const start_time = document.getElementById(
				`start_time_T1${listCreateSlotT1[i].slot_id}`
			).value;
			const end_time = document.getElementById(
				`end_time_T1${listCreateSlotT1[i].slot_id}`
			).value;
			insert_list.push({
				slot_id: listCreateSlotT1[i].slot_id,
				slot_name: slot_name,
				start_time: start_time,
				end_time: end_time,
			});
		}

		// current list
		const current_list = [];
		for (let i = 0; i < listCurrentSlotT1.length; i++) {
			const slot_name = document.getElementById(
				`slot_T1${listCurrentSlotT1[i].slot_id}`
			).value;
			const start_time = document.getElementById(
				`start_time_T1${listCurrentSlotT1[i].slot_id}`
			).value;
			const end_time = document.getElementById(
				`end_time_T1${listCurrentSlotT1[i].slot_id}`
			).value;
			current_list.push({
				slot_id: listCurrentSlotT1[i].slot_id,
				slot_name: slot_name,
				start_time: start_time,
				end_time: end_time,
			});
		}

		const data = {
			insert_list: insert_list,
			update_list: current_list,
			type: '1',
		};

		setLoading(true);
		const updateSlot = async () => {
			try {
				const response = await settingApi.updateSlot(data);
				setLoading(false);
				notify('success', response.message);
				setListCreateSlotT1([]);
				setReloadSlotT1((load) => !load);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateSlot();
	};

	const handleDeleteSlotT1 = (setting_id) => {
		const data = {
			slot_id: setting_id,
			flag: true,
			type: 1,
		};
		setLoading(true);
		const updateSlotFlag = async () => {
			try {
				const response = await settingApi.updateSlotFlag(data);
				setLoading(false);

				if (response.isError) {
					const class_list = response.data;
					let message =
						'Những nhân viên sau đang làm việc ở slot này nên không thể xóa: \r\n';
					for (let i = 0; i < class_list.length; i++) {
						message += '- ' + class_list[i].full_name + '\r\n';
					}
					notify('info', message, false);
				} else {
					notify('success', response.message);
					setReloadSlotT1((load) => !load);
				}
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status == 403) {
					history.push('/');
				}
			}
		};
		updateSlotFlag();
	};

	const handleDeleteCreateSlotT1 = (slot_id) => {
		const new_list = [];
		for (let i = 0; i < listCreateSlotT1.length; i++) {
			if (listCreateSlotT1[i].slot_id !== slot_id) {
				const new_object = { ...listCreateSlotT1[i] };
				new_list.push(new_object);
			}
		}
		setListCreateSlotT1(new_list);
	};

	// typeList : 0 laf list có sẵn load từ Db . 1 là list mới tạo ra
	const handleSelectTime = (start_time, end_time, index, typeList, slot_id) => {
		const start_time_1 = document.getElementById(start_time).value;
		const end_time_1 = document.getElementById(end_time).value;
		if (start_time_1.length <= 0 || end_time_1.length <= 0) {
			notify('error', 'Vui lòng lựa chọn thời gian');

			//
			if (typeList == '0') {
				const arrayCopy = [...errorTime];
				arrayCopy[index] = true;
				setErrorTime(arrayCopy);
			} else {
				const arrayCopy = [...errorTimeCreate];
				arrayCopy[index] = true;
				setErrorTimeCreate(arrayCopy);
			}
		} else {
			const start = start_time_1.split(':');
			const end = end_time_1.split(':');
			const start_hour = start[0];
			const start_minus = start[1];
			const end_hour = end[0];
			const end_minus = end[1];
			if (
				start_hour < end_hour ||
				(start_hour == end_hour && start_minus < end_minus)
			) {
				//
				const data = {
					start_time: start_time_1,
					end_time: end_time_1,
					type: '0',
					slot_id: slot_id,
				};
				const checkExistSlot = async () => {
					try {
						const response = await settingApi.checkExistSlot(data);
						setLoading(false);

						if (response.result == false) {
						} else {
							if (typeList == '0') {
								const arrayCopy = [...errorTime];
								arrayCopy[index] = '';
								setErrorTime(arrayCopy);
							} else {
								const arrayCopy = [...errorTimeCreate];
								arrayCopy[index] = '';
								setErrorTimeCreate(arrayCopy);
							}
						}
					} catch (error) {
						setLoading(false);
						notify('error', error.response.data.message);
						if (error.response.status == 403) {
							history.push('/');
						} else if (error.response.status == 422) {
							if (typeList == '0') {
								const arrayCopy = [...errorTime];
								arrayCopy[index] = true;
								setErrorTime(arrayCopy);
							} else {
								const arrayCopy = [...errorTimeCreate];
								arrayCopy[index] = true;
								setErrorTimeCreate(arrayCopy);
							}
						}
					}
				};
				checkExistSlot();
			} else {
				//
				if (typeList == '0') {
					const arrayCopy = [...errorTime];
					arrayCopy[index] = true;
					setErrorTime(arrayCopy);
				} else {
					const arrayCopy = [...errorTimeCreate];
					arrayCopy[index] = true;
					setErrorTimeCreate(arrayCopy);
				}

				notify('error', 'Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc');
			}
		}
	};

	const handleSelectTimeSlotT1 = (
		start_time,
		end_time,
		index,
		typeList,
		slot_id
	) => {
		const start_time_1 = document.getElementById(start_time).value;
		const end_time_1 = document.getElementById(end_time).value;
		if (start_time_1.length <= 0 || end_time_1.length <= 0) {
			notify('error', 'Vui lòng lựa chọn thời gian');

			//
			if (typeList == '0') {
				const arrayCopy = [...errorTimeT1];
				arrayCopy[index] = true;
				setErrorTimeT1(arrayCopy);
			} else {
				const arrayCopy = [...errorTimeT1Create];
				arrayCopy[index] = true;
				setErrorTimeT1Create(arrayCopy);
			}
		} else {
			const start = start_time_1.split(':');
			const end = end_time_1.split(':');
			const start_hour = start[0];
			const start_minus = start[1];
			const end_hour = end[0];
			const end_minus = end[1];
			if (
				start_hour < end_hour ||
				(start_hour == end_hour && start_minus < end_minus)
			) {
				const data = {
					start_time: start_time_1,
					end_time: end_time_1,
					type: '1',
					slot_id: slot_id,
				};
				const checkExistSlot = async () => {
					try {
						const response = await settingApi.checkExistSlot(data);
						setLoading(false);

						if (typeList == '0') {
							const arrayCopy = [...errorTimeT1];
							arrayCopy[index] = '';
							setErrorTimeT1(arrayCopy);
						} else {
							const arrayCopy = [...errorTimeT1Create];
							arrayCopy[index] = '';
							setErrorTimeT1Create(arrayCopy);
						}
					} catch (error) {
						setLoading(false);
						notify('error', error.response.data.message);
						if (error.response.status == 403) {
							history.push('/');
						} else if (error.response.status == 422) {
							if (typeList == '0') {
								const arrayCopy = [...errorTimeT1];
								arrayCopy[index] = true;
								setErrorTimeT1(arrayCopy);
							} else {
								const arrayCopy = [...errorTimeT1Create];
								arrayCopy[index] = true;
								setErrorTimeT1Create(arrayCopy);
							}
						}
					}
				};
				checkExistSlot();

				//
			} else {
				if (typeList == '0') {
					const arrayCopy = [...errorTimeT1];
					arrayCopy[index] = true;
					setErrorTimeT1(arrayCopy);
				} else {
					const arrayCopy = [...errorTimeT1Create];
					arrayCopy[index] = true;
					setErrorTimeT1Create(arrayCopy);
				}
				notify('error', 'Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc');
			}
		}
	};

	return (
		<Accordion>
			<AccordionSummary expandIcon={<ExpandMoreIcon />}>
				<Grid container style={{ padding: '0.25rem 0' }}>
					<Grid item md={12} style={{ marginBottom: '0.5rem' }}>
						<Typography style={{ fontWeight: 'bold', fontSize: '1.25rem' }}>
							Ca học và Làm việc
						</Typography>
					</Grid>
					<Grid item md={12}>
						<Typography style={{ color: '#555', fontSize: '14px' }}>
							Thiết lập thời gian cho ca học và làm việc
						</Typography>
					</Grid>
				</Grid>
			</AccordionSummary>
			<AccordionDetails>
				{loading && (
					<div
						style={{
							position: 'fixed',
							top: '50%',
							left: '50%',
							width: '50px',
							transform: 'translate(-50%, -50%)',
							zIndex: '1000000000',
						}}
						className='cp-spinner cp-balls'
					></div>
				)}
				<Grid container>
					{functions.includes('function_setting_post_category') && (
						<Accordion className={classes.fullWidth}>
							<AccordionSummary expandIcon={<ExpandMoreIcon />}>
								<Grid container style={{ padding: '0.25rem 0' }}>
									<Grid item md={12} style={{ marginBottom: '0.5rem' }}>
										<Typography
											style={{ fontWeight: 'bold', fontSize: '1.25rem' }}
										>
											Ca học
										</Typography>
									</Grid>
									<Grid item md={12}>
										<Typography style={{ color: '#555', fontSize: '14px' }}>
											Thiết lập ca học
										</Typography>
									</Grid>
								</Grid>
							</AccordionSummary>
							<AccordionDetails>
								<Grid container>
									<form
										className={classes.fullWidth}
										onSubmit={(e) => handleUpdateSlot(e)}
									>
										{listCurrent.map((data, index) => (
											<Grid
												key={`slot_current_${data.slot_id}`}
												container
												style={{ paddingLeft: '1rem', marginTop: '1rem' }}
												spacing={3}
											>
												<Divider
													style={{ width: '100%', margin: '0.25rem 0 1rem 0' }}
												/>
												<Grid item md={1}>
													<Typography>Tên ca</Typography>
												</Grid>
												<Grid item md={3}>
													<TextField
														required
														id={`slot${data.slot_id}`}
														key={`slot_1${data.slot_id}`}
														defaultValue={data.slot_name}
														rows={4}
														size='small'
														variant='outlined'
														fullWidth
													/>
												</Grid>

												<Grid item md={3}>
													<TextField
														id={`start_time${data.slot_id}`}
														onBlur={() =>
															handleSelectTime(
																`start_time${data.slot_id}`,
																`end_time${data.slot_id}`,
																`${index}`,
																'0',
																data.slot_id
															)
														}
														label='Bắt đầu'
														type='time'
														error={`${errorTime[index]}`}
														defaultValue={data.start_time}
														className={classes.textField}
														InputLabelProps={{
															shrink: true,
														}}
														// inputProps={{
														// step: 300, // 5 min
														// }}
													/>
												</Grid>

												<Grid item md={3}>
													<TextField
														id={`end_time${data.slot_id}`}
														onBlur={() =>
															handleSelectTime(
																`start_time${data.slot_id}`,
																`end_time${data.slot_id}`,
																`${index}`,
																'0',
																data.slot_id
															)
														}
														error={`${errorTime[index]}`}
														label='Kết thúc'
														type='time'
														defaultValue={data.end_time}
														className={classes.textField}
														InputLabelProps={{
															shrink: true,
														}}
														// inputProps={{
														// step: 300, // 5 min
														// }}
													/>
												</Grid>

												<Grid item md={1}>
													<DeleteDialog
														title={`Xóa ca học`}
														description='Bạn có chắc chắn muốn xóa không?'
														doneText='Xóa'
														tooltip_text='Xóa ca học'
														cancelText='Hủy'
														icon='1'
														action={() => handleDelete(data.slot_id)}
													/>
												</Grid>
											</Grid>
										))}

										{listCreate.map((data, index) => (
											<Grid
												key={`slot_current_${data.slot_id}`}
												container
												style={{ paddingLeft: '1rem', marginTop: '1rem' }}
												spacing={3}
											>
												<Divider
													style={{ width: '100%', margin: '0.25rem 0 1rem 0' }}
												/>
												<Grid item md={1}>
													<Typography>Tên ca</Typography>
												</Grid>
												<Grid item md={3}>
													<TextField
														required
														id={`slot${data.slot_id}`}
														key={`slot_1${data.slot_id}`}
														defaultValue={data.slot_name}
														rows={4}
														size='small'
														variant='outlined'
														fullWidth
													/>
												</Grid>

												<Grid item md={3}>
													<TextField
														id={`start_time${data.slot_id}`}
														error={`${errorTimeCreate[index]}`}
														label='Bắt đầu'
														type='time'
														onBlur={() =>
															handleSelectTime(
																`start_time${data.slot_id}`,
																`end_time${data.slot_id}`,
																`${index}`,
																'1',
																data.slot_id
															)
														}
														defaultValue={data.start_time}
														className={classes.textField}
														InputLabelProps={{
															shrink: true,
														}}
														// inputProps={{
														// step: 300, // 5 min
														// }}
													/>
												</Grid>

												<Grid item md={3}>
													<TextField
														id={`end_time${data.slot_id}`}
														onBlur={() =>
															handleSelectTime(
																`start_time${data.slot_id}`,
																`end_time${data.slot_id}`,
																`${index}`,
																'1',
																data.slot_id
															)
														}
														error={`${errorTimeCreate[index]}`}
														label='Kết thúc'
														type='time'
														defaultValue={data.end_time}
														className={classes.textField}
														InputLabelProps={{
															shrink: true,
														}}
														// inputProps={{
														// step: 300, // 5 min
														// }}
													/>
												</Grid>

												<Grid item md={1}>
													<DeleteDialog
														title={`Xóa ca học`}
														description='Bạn có chắc chắn muốn xóa không?'
														doneText='Xóa'
														tooltip_text='Xóa ca học'
														cancelText='Hủy'
														icon='1'
														action={() => handleDeleteCreate(data.slot_id)}
													/>
												</Grid>
											</Grid>
										))}

										<Grid
											container
											style={{ marginTop: '2rem', paddingLeft: '1rem' }}
										>
											<Grid item md={9}>
												<Button
													type='submit'
													value='Submit'
													size='small'
													variant='contained'
													color='primary'
													style={{ padding: '0.25rem 2rem' }}
												>
													Lưu thay đổi
												</Button>
											</Grid>

											<Grid item md={3}>
												<Button
													onClick={handleCreateSlot}
													size='small'
													variant='outlined'
													color='primary'
													style={{ padding: '0.25rem 1rem' }}
													startIcon={<AddIcon />}
												>
													Thêm ca
												</Button>
											</Grid>
										</Grid>
									</form>
								</Grid>
							</AccordionDetails>
						</Accordion>
					)}

					{/* // Ca lam việc */}

					{functions.includes('function_setting_subject_category') && (
						<Accordion className={classes.fullWidth}>
							<AccordionSummary expandIcon={<ExpandMoreIcon />}>
								<Grid container style={{ padding: '0.25rem 0' }}>
									<Grid item md={12} style={{ marginBottom: '0.5rem' }}>
										<Typography
											style={{ fontWeight: 'bold', fontSize: '1.25rem' }}
										>
											Ca làm việc
										</Typography>
									</Grid>
									<Grid item md={12}>
										<Typography style={{ color: '#555', fontSize: '14px' }}>
											Thiết lập ca làm việc cho nhân viên
										</Typography>
									</Grid>
								</Grid>
							</AccordionSummary>
							<AccordionDetails>
								<Grid container>
									<form
										className={classes.fullWidth}
										onSubmit={(e) => handleUpdateSlotT1(e)}
									>
										{listCurrentSlotT1.map((data, index) => (
											<Grid
												key={`slot_T1_current_${data.slot_id}`}
												container
												style={{ paddingLeft: '1rem', marginTop: '1rem' }}
												spacing={3}
											>
												<Divider
													style={{ width: '100%', margin: '0.25rem 0 1rem 0' }}
												/>
												<Grid item md={1}>
													<Typography>Tên ca</Typography>
												</Grid>
												<Grid item md={3}>
													<TextField
														required
														id={`slot_T1${data.slot_id}`}
														key={`slot_1_T1${data.slot_id}`}
														defaultValue={data.slot_name}
														rows={4}
														size='small'
														variant='outlined'
														fullWidth
													/>
												</Grid>

												<Grid item md={3}>
													<TextField
														id={`start_time_T1${data.slot_id}`}
														onBlur={() =>
															handleSelectTimeSlotT1(
																`start_time_T1${data.slot_id}`,
																`end_time_T1${data.slot_id}`,
																`${index}`,
																'0',
																data.slot_id
															)
														}
														label='Bắt đầu'
														type='time'
														error={`${errorTimeT1[index]}`}
														defaultValue={data.start_time}
														className={classes.textField}
														InputLabelProps={{
															shrink: true,
														}}
														// inputProps={{
														// step: 300, // 5 min
														// }}
													/>
												</Grid>

												<Grid item md={3}>
													<TextField
														id={`end_time_T1${data.slot_id}`}
														onBlur={() =>
															handleSelectTimeSlotT1(
																`start_time_T1${data.slot_id}`,
																`end_time_T1${data.slot_id}`,
																`${index}`,
																'0',
																data.slot_id
															)
														}
														error={`${errorTimeT1[index]}`}
														label='Kết thúc'
														type='time'
														defaultValue={data.end_time}
														className={classes.textField}
														InputLabelProps={{
															shrink: true,
														}}
														// inputProps={{
														// step: 300, // 5 min
														// }}
													/>
												</Grid>

												<Grid item md={1}>
													<DeleteDialog
														title={`Xóa ca làm`}
														description='Bạn có chắc chắn muốn xóa không?'
														doneText='Xóa'
														tooltip_text='Xóa ca làm'
														cancelText='Hủy'
														icon='1'
														action={() => handleDeleteSlotT1(data.slot_id)}
													/>
												</Grid>
											</Grid>
										))}

										{listCreateSlotT1.map((data, index) => (
											<Grid
												key={`slot_current_${data.slot_id}`}
												container
												style={{ paddingLeft: '1rem', marginTop: '1rem' }}
												spacing={3}
											>
												<Divider
													style={{ width: '100%', margin: '0.25rem 0 1rem 0' }}
												/>
												<Grid item md={1}>
													<Typography>Tên ca</Typography>
												</Grid>
												<Grid item md={3}>
													<TextField
														required
														id={`slot_T1${data.slot_id}`}
														key={`slot_1${data.slot_id}`}
														defaultValue={data.slot_name}
														rows={4}
														size='small'
														variant='outlined'
														fullWidth
													/>
												</Grid>

												<Grid item md={3}>
													<TextField
														id={`start_time_T1${data.slot_id}`}
														error={`${errorTimeT1Create[index]}`}
														label='Bắt đầu'
														type='time'
														onBlur={() =>
															handleSelectTimeSlotT1(
																`start_time_T1${data.slot_id}`,
																`end_time_T1${data.slot_id}`,
																`${index}`,
																'1',
																data.slot_id
															)
														}
														defaultValue={data.start_time}
														className={classes.textField}
														InputLabelProps={{
															shrink: true,
														}}
														// inputProps={{
														// step: 300, // 5 min
														// }}
													/>
												</Grid>

												<Grid item md={3}>
													<TextField
														id={`end_time_T1${data.slot_id}`}
														onBlur={() =>
															handleSelectTimeSlotT1(
																`start_time_T1${data.slot_id}`,
																`end_time_T1${data.slot_id}`,
																`${index}`,
																'1',
																data.slot_id
															)
														}
														error={`${errorTimeT1Create[index]}`}
														label='Kết thúc'
														type='time'
														defaultValue={data.end_time}
														className={classes.textField}
														InputLabelProps={{
															shrink: true,
														}}
														// inputProps={{
														// step: 300, // 5 min
														// }}
													/>
												</Grid>

												<Grid item md={1}>
													<DeleteDialog
														title={`Xóa ca làm`}
														description='Bạn có chắc chắn muốn xóa không?'
														doneText='Xóa'
														tooltip_text='Xóa ca làm'
														cancelText='Hủy'
														icon='1'
														action={() =>
															handleDeleteCreateSlotT1(data.slot_id)
														}
													/>
												</Grid>
											</Grid>
										))}
										<Grid
											container
											style={{ marginTop: '2rem', paddingLeft: '1rem' }}
										>
											<Grid item md={9}>
												<Button
													type='submit'
													value='Submit'
													size='small'
													variant='contained'
													color='primary'
													style={{ padding: '0.25rem 2rem' }}
												>
													Lưu thay đổi
												</Button>
											</Grid>

											<Grid item md={3}>
												<Button
													onClick={handleCreateSlotT1}
													size='small'
													variant='outlined'
													color='primary'
													style={{ padding: '0.25rem 1rem' }}
													startIcon={<AddIcon />}
												>
													Thêm ca
												</Button>
											</Grid>
										</Grid>
									</form>
								</Grid>
							</AccordionDetails>
						</Accordion>
					)}
					{/* // mon hoc */}
				</Grid>
			</AccordionDetails>
		</Accordion>
	);
}
