import {
	Box,
	Button,
	Fab,
	FormControl,
	Grid,
	IconButton,
	makeStyles,
	MenuItem,
	Select,
	TextField,
	Tooltip,
	Typography,
} from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import RestoreIcon from '@material-ui/icons/Restore';
import classApi from 'api/classApi';
import permissionApi from 'api/permissionApi';
import selectApi from 'api/selectApi';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import NumberFormat from 'react-number-format';
import { Link, useHistory } from 'react-router-dom';
import {
	checkStartDateGreaterThanNow,
	compareStartDateWithEndDate,
	days,
	displayDayOfWeek,
	getDayByDate,
	grades,
} from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

const useStyles = makeStyles((theme) => ({
	postPanel: {
		marginTop: '2rem',
		marginBottom: '0.5rem',
	},
	fab: {
		position: 'fixed',
		bottom: theme.spacing(8),
		right: theme.spacing(8),
	},
}));

function NumberFormatCustom(props) {
	const { inputRef, onChange, ...other } = props;

	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={(values) => {
				onChange({
					target: {
						name: props.name,
						value: values.value,
					},
				});
			}}
			thousandSeparator
			isNumericString
		/>
	);
}

const referer = [
	{
		value: 'class',
		display: 'Lớp học',
	},
];

function CreateClass() {
	const notify = useContext(ToastifyContext);
	const classes = useStyles();
	let history = useHistory();

	const [loading, setLoading] = useState(false);
	const [open, setOpen] = useState({
		show: false,
		data: [],
	});
	const [subjects, setSubjects] = useState([]);
	const [teachers, setTeachers] = useState([]);
	const [tutors, setTutors] = useState([]);
	const [dataInput, setDataInput] = useState({
		startdate: '',
		enddate: '',
		classname: '',
		price: '',
		maxstudent: '',
		percent: '',
		subject: '',
		teacher: '',
		tutor: '',
		note: '',
		grade: '',
	});
	const [errorInput, setErrorInput] = useState({
		startdate: '',
		enddate: '',
		classname: '',
		price: '',
		maxstudent: '',
		percent: '',
		subject: '',
		teacher: '',
		tutor: '',
		note: '',
		schedule: '',
		grade: '',
	});
	// list slot của schedule tương ứng
	const [slots, setSlots] = useState({
		schedule0: [],
	});
	// list room của schedule tương ứng
	const [rooms, setRooms] = useState({
		schedule0: [],
	});
	// data của các schedule
	const [schedules, setSchedule] = useState([
		{
			day: -1,
			slot: '',
			room: '',
			type: '0',
		},
	]);

	const handleClose = () => {
		setOpen({
			...open,
			show: false,
			data: [],
		});
	};

	const handleDataInputChange = (e) => {
		setDataInput({
			...dataInput,
			[e.target.name]: e.target.value,
		});
	};

	const handleStartdateBlur = (e) => {
		if (checkStartDateGreaterThanNow(e.target.value)) {
			schedules[0].day = new Date(e.target.value).getDay();
			const scheduleArray = [...schedules];
			setSchedule(scheduleArray);
			setErrorInput({
				...errorInput,
				startdate: '',
			});

			if (dataInput.enddate !== '') {
				if (compareStartDateWithEndDate(e.target.value, dataInput.enddate)) {
					setErrorInput({
						...errorInput,
						startdate: '',
						enddate: '',
					});
				} else {
					setErrorInput({
						...errorInput,
						startdate: '',
						enddate: 'Ngày kết thúc phải lớn hơn ngày khai giảng',
					});
				}
			}
		} else {
			schedules[0].day = -1;
			const scheduleArray = [...schedules];
			setSchedule(scheduleArray);
			setErrorInput({
				...errorInput,
				startdate: 'Ngày khai giảng phải lớn hơn ngày hiện tại',
			});
		}
	};

	const handleEnddateBlur = (e) => {
		if (compareStartDateWithEndDate(dataInput.startdate, e.target.value)) {
			setErrorInput({
				...errorInput,
				enddate: '',
			});
			return;
		}

		setErrorInput({
			...errorInput,
			enddate: 'Ngày kết thúc phải lớn hơn ngày khai giảng',
		});
	};

	const handleMaxStudentBlur = (e) => {
		if (parseInt(e.target.value) > 0) {
			setErrorInput({
				...errorInput,
				maxstudent: '',
			});
			return;
		}

		setErrorInput({
			...errorInput,
			maxstudent: 'Số lượng học sinh phải lớn hơn 0',
		});
	};

	const handlePriceBlur = (e) => {
		const priceValue = e.target.value.toString().replaceAll(',', '');
		if (parseFloat(priceValue) > 0) {
			setErrorInput({
				...errorInput,
				price: '',
			});
			return;
		}

		setErrorInput({
			...errorInput,
			price: 'Giá tiền phải lớn hơn 0',
		});
	};

	const handlePercentBlur = (e) => {
		if (parseFloat(e.target.value) > 0) {
			setErrorInput({
				...errorInput,
				percent: '',
			});
			return;
		}

		setErrorInput({
			...errorInput,
			percent: 'Tỷ lệ hoa hồng phải lớn hơn 0',
		});
	};

	// Mỗi khi click button "Thêm Lịch Học", tạo ra 1 lịch học mới
	const handleAddMoreSchedule = () => {
		const newSchedule = {
			day: -1,
			slot: '',
			room: '',
			type: '1',
		};
		schedules.push(newSchedule);
		const scheduleArray = [...schedules];
		setSchedule(scheduleArray);
	};

	// xóa lịch học tương ứng
	const handleDeleteSchedule = (index) => {
		schedules.splice(index, 1);
		const scheduleArray = [...schedules];
		setSchedule(scheduleArray);

		delete rooms[[`schedule${index}`]];

		const newRoom = { ...rooms };
		setRooms(newRoom);

		delete slots[[`schedule${index}`]];

		const newSlot = { ...slots };
		setSlots(newSlot);
	};

	// mỗi khi onChange thứ, slot hoặc room chạy hàm này
	const handleChangeSchedule = (e, index) => {
		schedules[index][e.target.name] = e.target.value;
		// nếu đã chọn phòng học thì query tất cả các slot trống của phòng học đấy vào thứ tương ứng từ ngày khai giảng trở đi
		if (e.target.name === 'slot') {
			const getSelectRoom = async () => {
				try {
					const filter = {
						startdate: dataInput.startdate,
						slot: e.target.value,
						day: schedules[index].day,
					};
					const response = await selectApi.getRoomAddClassScreen(filter);

					setRooms({
						...rooms,
						[`schedule${index}`]: response,
					});
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			getSelectRoom();
			// nếu đã chọn slot thì hiện tất cả các phòng trống slot đó và thứ tương ứng từ ngày khai giảng trở đi
		} else if (e.target.name === 'room') {
			const getSelectSlot = async () => {
				try {
					const filter = {
						startdate: dataInput.startdate,
						room: e.target.value,
						day: schedules[index].day,
					};
					const response = await selectApi.getSlotAddClassScreen(filter);

					setSlots({
						...slots,
						[`schedule${index}`]: response,
					});
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			getSelectSlot();
		} else {
			// nếu chọn lại thứ thì reset slot, phòng tương ứng về trống
			schedules[index]['slot'] = '';
			schedules[index]['room'] = '';
			if (e.target.value !== -1) {
				const getSelectRoom = async () => {
					try {
						const filter = {
							startdate: dataInput.startdate,
							slot: '',
							day: '',
						};
						const response = await selectApi.getRoomAddClassScreen(filter);

						setRooms({
							...rooms,
							[`schedule${index}`]: response,
						});
					} catch (error) {
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/');
						}
					}
				};

				getSelectRoom();
				const getSelectSlot = async () => {
					try {
						const filter = {
							startdate: dataInput.startdate,
							room: '',
							day: '',
						};
						const response = await selectApi.getSlotAddClassScreen(filter);

						setSlots({
							...slots,
							[`schedule${index}`]: response,
						});
					} catch (error) {
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/');
						}
					}
				};

				getSelectSlot();
			}
		}

		const scheduleArray = [...schedules];
		setSchedule(scheduleArray);

		if (
			schedules[index].day !== -1 &&
			schedules[index].slot !== '' &&
			schedules[index].room !== ''
		) {
			for (let i = 0; i < schedules.length; i++) {
				if (i !== index) {
					if (
						schedules[index].day === schedules[i].day &&
						schedules[index].slot === schedules[i].slot &&
						schedules[index].room === schedules[i].room
					) {
						setErrorInput({
							...errorInput,
							schedule: `Lịch học ${i + 1} bị trùng với lịch học ${index + 1}`,
						});
						return;
					}
				}
			}

			setErrorInput({
				...errorInput,
				schedule: '',
			});
		}
	};

	// submit form
	const handleSubmitForm = (e) => {
		e.preventDefault();
		const post = async () => {
			setLoading(true);
			for (let property in errorInput) {
				if (errorInput[property] !== '') {
					setLoading(false);
					return;
				}
			}

			try {
				dataInput.price.toString().replaceAll(',', '');
				const data = {
					classinfo: dataInput,
					classtimetable: schedules,
				};

				const response = await classApi.createClass(data);
				setLoading(false);
				if (response.duplicate === true) {
					setOpen({
						...open,
						show: true,
						data: response.data,
					});
					return;
				}

				notify('success', response.message);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	};

	const handleResetForm = () => {
		setDataInput({
			...dataInput,
			startdate: '',
			enddate: '',
			classname: '',
			price: '',
			maxstudent: '',
			percent: '',
			subject: '',
			teacher: '',
			tutor: '',
			note: '',
		});

		setErrorInput({
			...errorInput,
			startdate: '',
			enddate: '',
			classname: '',
			price: '',
			maxstudent: '',
			percent: '',
			subject: '',
			teacher: '',
			tutor: '',
			note: '',
			schedule: '',
		});

		setSchedule([
			{
				day: -1,
				slot: '',
				room: '',
			},
		]);
	};

	const [isPermiss, setIsPermiss] = useState(false);
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_create_class',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	}, []);

	// nếu chưa chọn môn thì hiện tất cả các giáo viên của trung tâm
	// đã chọn môn học thì query theo những giáo viên dạy môn học đó
	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const filter = { subject: dataInput.subject };
					const response = await selectApi.getTeacherAddClassScreen(filter);
					setTeachers(response);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, dataInput.subject]);

	// nếu chưa chọn môn thì hiện tất cả các trợ giảng của trung tâm
	// đã chọn môn học thì query theo những trợ giảng dạy môn học đó
	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const filter = { subject: dataInput.subject };
					const response = await selectApi.getTutorAddClassScreen(filter);
					setTutors(response);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, dataInput.subject]);

	// nếu chưa chọn giáo viên thì hiện tất cả các môn học trung tâm dạy
	// đã chọn giáo viên thì query theo những môn học mà giáo viên đã chọn dạy
	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const filter = { subject: dataInput.teacher };
					const response = await selectApi.getSubjectAddClassScreen(filter);
					setSubjects(response);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, dataInput.teacher]);

	// lấy tất cả room của trung tâm cho lịch học đầu tiên
	useEffect(() => {
		if (isPermiss) {
			if (dataInput.startdate) {
				const get = async () => {
					try {
						const filter = { startdate: dataInput.startdate, slot: '' };
						const response = await selectApi.getRoomAddClassScreen(filter);
						setRooms({
							...rooms,
							schedule0: response,
						});
					} catch (error) {
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/');
						}
					}
				};

				get();
			}
		}
	}, [isPermiss, dataInput.startdate]);

	// lấy tất cả slot cho lịch học đầu tiên
	useEffect(() => {
		if (isPermiss) {
			if (dataInput.startdate) {
				const get = async () => {
					try {
						const filter = { startdate: dataInput.startdate, room: '' };
						const response = await selectApi.getSlotAddClassScreen(filter);
						setSlots({
							...slots,
							schedule0: response,
						});
					} catch (error) {
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/');
						}
					}
				};

				get();
			}
		}
	}, [isPermiss, dataInput.startdate]);

	return (
		<MasterLayout header='Thêm mới' referer={referer}>
			<Box>
				{loading && (
					<div
						style={{
							position: 'fixed',
							top: '50%',
							left: '50%',
							width: '50px',
							transform: 'translate(-50%, -50%)',
							zIndex: '1000000000',
						}}
						className='cp-spinner cp-balls'
					></div>
				)}
				<Helmet>
					<title>Thêm mới | Lớp học | Xoài CMS</title>
				</Helmet>
				<Tooltip title='Làm trống'>
					<Fab
						className={classes.fab}
						size='large'
						onClick={handleResetForm}
						color='secondary'
					>
						<RestoreIcon fontSize='large' />
					</Fab>
				</Tooltip>
				{open.data.length > 0 && (
					<Dialog open={open.show} onClose={handleClose}>
						<DialogTitle>Thông báo</DialogTitle>
						<DialogContent>
							<DialogContentText>
								{open.data.map((d, index) => (
									<div
										style={{ color: 'red', marginBottom: '0.75rem' }}
										key={index}
									>
										Vào {displayDayOfWeek(d.day_of_week)}, {d.slot_name}, phòng{' '}
										{d.room_name} đã có lớp{' '}
										<Link to={`/class/${d.class_id}`}>{d.class_name}</Link> của
										giáo viên {d.full_name}
									</div>
								))}
							</DialogContentText>
						</DialogContent>
						<DialogActions>
							<Button
								style={{ marginRight: '1rem' }}
								onClick={handleClose}
								color='primary'
								autoFocus
							>
								Đồng ý
							</Button>
						</DialogActions>
					</Dialog>
				)}
				<form onSubmit={handleSubmitForm}>
					<Grid container alignItems='center' spacing={1}>
						<Grid
							container
							style={{ padding: '1rem 2rem 0.5rem 2rem' }}
							spacing={5}
						>
							<Grid item md={6}>
								<Grid container alignItems='center'>
									<Grid container alignItems='center'>
										<Grid item md={3} lg={3}>
											Ngày khai giảng:
											<span style={{ color: 'red' }}>&nbsp;*</span>
										</Grid>
										<Grid item md={9} lg={9}>
											<FormControl fullWidth error={errorInput.startdate}>
												<TextField
													name='startdate'
													value={dataInput.startdate}
													onChange={handleDataInputChange}
													onBlur={handleStartdateBlur}
													type='date'
													variant='outlined'
													margin='normal'
													required
													error={errorInput.startdate}
													size='small'
												/>
												{errorInput.startdate !== '' && (
													<small style={{ color: 'red' }}>
														{errorInput.startdate}
													</small>
												)}
											</FormControl>
										</Grid>
									</Grid>
									<Grid container alignItems='center'>
										<Grid item md={3} lg={3}>
											Tên lớp:
											<span style={{ color: 'red' }}>&nbsp;*</span>
										</Grid>
										<Grid item md={9} lg={9}>
											<FormControl fullWidth error={errorInput.classname}>
												<TextField
													name='classname'
													value={dataInput.classname}
													onChange={handleDataInputChange}
													variant='outlined'
													margin='normal'
													required
													error={errorInput.classname}
													size='small'
												/>
												{errorInput.classname !== '' && (
													<small style={{ color: 'red' }}>
														{errorInput.classname}
													</small>
												)}
											</FormControl>
										</Grid>
									</Grid>
									<Grid container alignItems='center'>
										<Grid item md={3} lg={3}>
											Số lượng học sinh tối đa:
											<span style={{ color: 'red' }}>&nbsp;*</span>
										</Grid>
										<Grid item md={9} lg={9}>
											<FormControl fullWidth error={errorInput.maxstudent}>
												<TextField
													name='maxstudent'
													value={dataInput.maxstudent}
													onChange={handleDataInputChange}
													onBlur={handleMaxStudentBlur}
													variant='outlined'
													margin='normal'
													required
													error={errorInput.maxstudent}
													size='small'
													type='number'
												/>
												{errorInput.maxstudent !== '' && (
													<small style={{ color: 'red' }}>
														{errorInput.maxstudent}
													</small>
												)}
											</FormControl>
										</Grid>
									</Grid>
									<Grid container alignItems='center'>
										<Grid item md={3} lg={3}>
											Môn học:
											<span style={{ color: 'red' }}>&nbsp;*</span>
										</Grid>
										<Grid item md={9} lg={9}>
											<FormControl size='small' fullWidth margin='normal'>
												<Select
													name='subject'
													value={dataInput.subject}
													onChange={handleDataInputChange}
													variant='outlined'
													required
												>
													<MenuItem value=''>Bỏ chọn</MenuItem>
													{subjects.map((subject) => (
														<MenuItem value={subject.subject_id}>
															{subject.subject_name}
														</MenuItem>
													))}
												</Select>
											</FormControl>
										</Grid>
									</Grid>
									<Grid container alignItems='center'>
										<Grid item md={3} lg={3}>
											Trợ giảng:
										</Grid>
										<Grid item md={9} lg={9}>
											<FormControl size='small' fullWidth margin='normal'>
												<Select
													name='tutor'
													value={dataInput.tutor}
													onChange={handleDataInputChange}
													variant='outlined'
												>
													<MenuItem value=''>Bỏ chọn</MenuItem>
													{tutors.map((tutor) => (
														<MenuItem value={tutor.tutor_id}>
															{tutor.tutor_name}
														</MenuItem>
													))}
												</Select>
											</FormControl>
										</Grid>
									</Grid>
									<Grid container alignItems='center'>
										<Grid item md={3} lg={3}>
											Lớp:
											<span style={{ color: 'red' }}>&nbsp;*</span>
										</Grid>
										<Grid item md={9} lg={9}>
											<FormControl size='small' fullWidth margin='normal'>
												<Select
													required
													name='grade'
													value={dataInput.grade}
													onChange={handleDataInputChange}
													variant='outlined'
												>
													<MenuItem value=''>Chọn lớp</MenuItem>
													{grades.map((g) => (
														<MenuItem value={g.value}>{g.display}</MenuItem>
													))}
												</Select>
											</FormControl>
										</Grid>
									</Grid>
								</Grid>
							</Grid>
							<Grid item md={6}>
								<Grid container>
									<Grid container alignItems='center'>
										<Grid item md={3} lg={3}>
											Ngày kết thúc:
											<span style={{ color: 'red' }}>&nbsp;*</span>
										</Grid>
										<Grid item md={9} lg={9}>
											<FormControl fullWidth error={errorInput.enddate}>
												<TextField
													name='enddate'
													value={dataInput.enddate}
													onChange={handleDataInputChange}
													onBlur={handleEnddateBlur}
													type='date'
													variant='outlined'
													margin='normal'
													required
													error={errorInput.enddate}
													size='small'
												/>
												{errorInput.enddate && (
													<small style={{ color: 'red' }}>
														{errorInput.enddate}
													</small>
												)}
											</FormControl>
										</Grid>
									</Grid>
									<Grid container alignItems='center'>
										<Grid item md={3} lg={3}>
											Học phí/buổi (VNĐ):
											<span style={{ color: 'red' }}>&nbsp;*</span>
										</Grid>
										<Grid item md={9} lg={9}>
											<FormControl fullWidth error={errorInput.price}>
												<TextField
													name='price'
													value={dataInput.price}
													onChange={handleDataInputChange}
													onBlur={handlePriceBlur}
													variant='outlined'
													margin='normal'
													required
													error={errorInput.price}
													size='small'
													InputProps={{
														inputComponent: NumberFormatCustom,
													}}
												/>
												{errorInput.price && (
													<small style={{ color: 'red' }}>
														{errorInput.price}
													</small>
												)}
											</FormControl>
										</Grid>
									</Grid>
									<Grid container alignItems='center'>
										<Grid item md={3} lg={3}>
											Tỷ lệ hoa hồng (%):
											<span style={{ color: 'red' }}>&nbsp;*</span>
										</Grid>
										<Grid item md={9} lg={9}>
											<FormControl fullWidth error={errorInput.percent}>
												<TextField
													name='percent'
													value={dataInput.percent}
													onChange={handleDataInputChange}
													onBlur={handlePercentBlur}
													type='number'
													variant='outlined'
													margin='normal'
													required
													error={errorInput.percent}
													size='small'
												/>
												{errorInput.percent && (
													<small style={{ color: 'red' }}>
														{errorInput.percent}
													</small>
												)}
											</FormControl>
										</Grid>
									</Grid>
									<Grid container alignItems='center'>
										<Grid item md={3} lg={3}>
											Giáo viên:
											<span style={{ color: 'red' }}>&nbsp;*</span>
										</Grid>
										<Grid item md={9} lg={9}>
											<FormControl size='small' fullWidth margin='normal'>
												<Select
													required
													name='teacher'
													value={dataInput.teacher}
													onChange={handleDataInputChange}
													variant='outlined'
												>
													<MenuItem value=''>Bỏ chọn</MenuItem>
													{teachers.map((teacher) => (
														<MenuItem value={teacher.teacher_id}>
															{teacher.teacher_name}
														</MenuItem>
													))}
												</Select>
											</FormControl>
										</Grid>
									</Grid>
									<Grid container alignItems='center'>
										<Grid item md={3} lg={3}>
											Ghi chú:
										</Grid>
										<Grid item md={9} lg={9}>
											<FormControl fullWidth error={errorInput.note}>
												<TextField
													name='note'
													value={dataInput.note}
													onChange={handleDataInputChange}
													variant='outlined'
													margin='normal'
													error={errorInput.note}
													size='small'
												/>
												{errorInput.note && (
													<small style={{ color: 'red' }}>
														{errorInput.note}
													</small>
												)}
											</FormControl>
										</Grid>
									</Grid>
								</Grid>
							</Grid>
							<Grid container style={{ marginTop: '1.5rem' }}>
								<Grid container style={{ marginBottom: '1rem' }}>
									<Grid item md={2}></Grid>
									<Grid item md={10} container alignItems='center'>
										<Button
											variant='outlined'
											startIcon={<AddIcon />}
											onClick={handleAddMoreSchedule}
											style={{ marginRight: '2rem' }}
										>
											Thêm lịch học
										</Button>
										{errorInput.schedule && (
											<Typography variant='body2' style={{ color: 'red' }}>
												{errorInput.schedule}
											</Typography>
										)}
									</Grid>
								</Grid>
								<Grid container spacing={5}>
									<Grid
										item
										md={2}
										container
										alignItems='center'
										justify='center'
									></Grid>
									<Grid item md={2}>
										<Grid container alignItems='center'>
											<Grid item md={3}>
												Thứ:
											</Grid>
											<Grid item md={9}>
												<TextField
													disabled
													variant='outlined'
													size='small'
													margin='normal'
													value={getDayByDate(dataInput.startdate)}
												/>
											</Grid>
										</Grid>
									</Grid>
									<Grid item md={2}>
										<Grid container alignItems='center'>
											<Grid item md={3}>
												Slot:
											</Grid>
											<Grid item md={9}>
												<FormControl
													style={{
														padding: '1rem 0',
													}}
													size='small'
													fullWidth
												>
													<Select
														required
														name='slot'
														disabled={schedules[0].day === -1 ? true : false}
														value={schedules[0].slot}
														onChange={(e) => handleChangeSchedule(e, 0)}
														variant='outlined'
													>
														<MenuItem value=''>Bỏ chọn</MenuItem>
														{slots.schedule0.map((slot) => (
															<MenuItem value={slot.slot_id}>
																{slot.slot_name}
															</MenuItem>
														))}
													</Select>
												</FormControl>
											</Grid>
										</Grid>
									</Grid>
									<Grid item md={3}>
										<Grid container alignItems='center'>
											<Grid item md={5}>
												Phòng học:
											</Grid>
											<Grid item md={7}>
												<FormControl
													style={{
														padding: '1rem 0',
													}}
													size='small'
													fullWidth
												>
													<Select
														required
														name='room'
														disabled={schedules[0].day === -1 ? true : false}
														value={schedules[0].room}
														variant='outlined'
														onChange={(e) => handleChangeSchedule(e, 0)}
													>
														<MenuItem value=''>Bỏ chọn</MenuItem>
														{rooms.schedule0.map((room) => (
															<MenuItem value={room.room_id}>
																{room.room_name}
															</MenuItem>
														))}
													</Select>
												</FormControl>
											</Grid>
										</Grid>
									</Grid>
								</Grid>

								{schedules.length > 1 &&
									schedules.map(
										(schedule, index) =>
											index !== 0 && (
												<Grid container spacing={5}>
													<Grid
														item
														md={2}
														container
														alignItems='center'
														justify='center'
													></Grid>
													<Grid item md={2}>
														<Grid container alignItems='center'>
															<Grid item md={3}>
																Thứ:
															</Grid>
															<Grid item md={9}>
																<FormControl
																	style={{
																		padding: '1rem 0',
																	}}
																	size='small'
																	fullWidth
																>
																	<Select
																		required
																		name='day'
																		value={schedule.day}
																		variant='outlined'
																		onChange={(e) =>
																			handleChangeSchedule(e, index)
																		}
																	>
																		<MenuItem value={-1}>Bỏ chọn</MenuItem>
																		{days.map((day) => (
																			<MenuItem value={day.value}>
																				{day.display}
																			</MenuItem>
																		))}
																	</Select>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={2}>
														<Grid container alignItems='center'>
															<Grid item md={3}>
																Slot:
															</Grid>
															<Grid item md={9}>
																<FormControl
																	style={{
																		padding: '1rem 0',
																	}}
																	size='small'
																	fullWidth
																>
																	<Select
																		required
																		name='slot'
																		disabled={
																			schedule.day === -1 ? true : false
																		}
																		value={schedule.slot}
																		variant='outlined'
																		onChange={(e) =>
																			handleChangeSchedule(e, index)
																		}
																	>
																		<MenuItem value=''>Bỏ chọn</MenuItem>
																		{slots[`schedule${index}`] &&
																			slots[`schedule${index}`].map((slot) => (
																				<MenuItem value={slot.slot_id}>
																					{slot.slot_name}
																				</MenuItem>
																			))}
																	</Select>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={3}>
														<Grid container alignItems='center'>
															<Grid item md={5}>
																Phòng học:
															</Grid>
															<Grid item md={7}>
																<FormControl
																	style={{
																		padding: '1rem 0',
																	}}
																	size='small'
																	fullWidth
																>
																	<Select
																		required
																		name='room'
																		disabled={
																			schedule.day === -1 ? true : false
																		}
																		value={schedule.room}
																		variant='outlined'
																		onChange={(e) =>
																			handleChangeSchedule(e, index)
																		}
																	>
																		<MenuItem value=''>Bỏ chọn</MenuItem>
																		{rooms[`schedule${index}`] &&
																			rooms[`schedule${index}`].map((room) => (
																				<MenuItem value={room.room_id}>
																					{room.room_name}
																				</MenuItem>
																			))}
																	</Select>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
													<Grid item md={1} container alignItems='center'>
														<IconButton
															onClick={() => handleDeleteSchedule(index)}
														>
															<CloseIcon />
														</IconButton>
													</Grid>
												</Grid>
											)
									)}
							</Grid>
							<Grid
								item
								xs={12}
								md={12}
								lg={12}
								className={classes.postPanel}
								container
								justify='center'
							>
								<Button
									style={{ padding: '0.5rem 3rem' }}
									variant='contained'
									color='primary'
									type='submit'
								>
									Tạo mới
								</Button>
							</Grid>
						</Grid>
					</Grid>
				</form>
			</Box>
		</MasterLayout>
	);
}

export default CreateClass;
