import {
	Button,
	Checkbox,
	FormControl,
	Grid,
	Input,
	InputLabel,
	ListItemText,
	MenuItem,
	Select,
	Tooltip,
	Typography,
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import AddIcon from '@material-ui/icons/Add';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import EditIcon from '@material-ui/icons/Edit';
import FilterListIcon from '@material-ui/icons/FilterList';
import GetAppIcon from '@material-ui/icons/GetApp';
import classApi from 'api/classApi';
import excelApi from 'api/excelApi';
import permissionApi from 'api/permissionApi';
import roomApi from 'api/roomApi';
import subjectApi from 'api/subjectApi';
import MyDialog from 'common/components/MyDialog';
import Pagination from 'common/components/Pagination';
import SearchForm from 'common/components/SearchForm';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { checkShow, exportToCSV } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

ClassList.propTypes = {};

const useStyles = makeStyles({
	formControl: {
		minWidth: 120,
	},
});

const statusList = [
	{ value: '0', display: 'Chưa hoạt động' },
	{ value: '1', display: 'Đang hoạt động' },
	{ value: '2', display: 'Kết thúc' },
	{ value: '3', display: 'Đã xóa' },
];

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
		},
	},
};

const parseClassStatus = (status) => {
	switch (status) {
		case '0':
			return 'Chưa hoạt động';
		case '1':
			return 'Đang hoạt động';
		case '2':
			return 'Kết thúc';
		case '3':
			return 'Đã xóa';
	}
};

function ClassList() {
	const notify = useContext(ToastifyContext);
	let history = useHistory();
	const classes = useStyles();

	const [classroom, setClassroom] = useState([]);
	const [pagination, setPagination] = useState({});

	const [loading, setLoading] = useState(false);
	const [subjects, setSubjects] = useState([]);
	const [rooms, setRooms] = useState([]);

	const [filters, setFilters] = useState({
		limit: 10,
		page: 1,
		q: '',
		room: '',
		subject: '',
		status: '1',
	});

	const handlePageChange = (newPage) => {
		setFilters({
			...filters,
			page: newPage,
		});
	};

	const handleSearchChange = (newSearch) => {
		setFilters({
			...filters,
			page: 1,
			q: newSearch.value,
		});
	};

	const handleFilterChange = (e, filter) => {
		setFilters({
			...filters,
			page: 1,
			[filter]: e.target.value,
		});
	};

	const resetFilter = () => {
		setFilters({
			...filters,
			room: '',
			subject: '',
			page: 1,
			status: '',
		});
	};

	const exportClasses = () => {
		const get = async () => {
			try {
				setLoading(true);
				const response = await excelApi.getClassesExcel(filters);
				setLoading(false);
				const csvData = {
					data: response,
				};
				exportToCSV(csvData, 'Danh_sach_lop_hoc');
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};
		get();
	};

	const [keys, setKeys] = useState([
		{
			header: 'Tên lớp',
			align: 'left',
			field: 'class_name',
			display: true,
		},
		{
			header: 'Môn học',
			align: 'left',
			field: 'subject_name',
			display: true,
		},
		{
			header: 'Giáo viên',
			align: 'left',
			field: 'teacher_name',
			display: true,
		},
		{
			header: 'Học phí',
			align: 'right',
			field: 'price',
			display: true,
		},
		{
			header: 'Phòng',
			align: 'left',
			field: 'room_name',
			display: true,
		},
		{
			header: 'Trạng thái',
			align: 'left',
			field: 'class_status',
			display: true,
		},
		{
			header: 'Số học sinh',
			align: 'right',
			field: 'number_student',
			display: true,
		},
	]);

	const [columnSelected, setColumnSelected] = React.useState([]);

	const changeDisplayColumn = (key) => {
		key.display = !key.display;
		setKeys(keys);
	};

	const handleCreateChange = () => {
		history.push({
			pathname: '/create/class',
		});
	};

	const handleChangeClassDetail = (id) => {
		history.push({
			pathname: `/class/${id}`,
		});
	};

	const handleChangeAttendanceHistoryDetail = (classId, classname) => {
		history.push({
			pathname: `/class/${classId}/attendancehistory`,
			state: {
				classname,
			},
		});
	};

	const handleColumnChange = (event) => {
		setColumnSelected(event.target.value);
	};
	const [isChange, setIsChange] = useState(false);
	const handleDeleteClass = (classId) => {
		const post = async () => {
			try {
				setLoading(true);
				const response = await classApi.deleteClass(classId);
				setLoading(false);
				setIsChange(!isChange);
				notify('success', response.message);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	};

	const handleRestoreClass = (classId) => {
		const post = async () => {
			try {
				setLoading(true);
				const response = await classApi.restoreClass(classId);
				setLoading(false);
				setIsChange(!isChange);
				notify('success', response.message);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	};

	const [isPermiss, setIsPermiss] = useState(false);
	const [functions, setFunctions] = useState([]);
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_list_class',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				localStorage.removeItem('menu');
				history.push('/');
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const response = await permissionApi.getFunctions();
					setFunctions(response);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await classApi.getClasses(filters);

					setLoading(false);
					setClassroom(response.data);
					setPagination(response.pagination);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, filters, isChange]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await subjectApi.getSubjects();
					setLoading(false);
					setSubjects(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					setLoading(true);
					const response = await roomApi.getRooms();
					setLoading(false);
					setRooms(response);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const value = [];
			for (let i = 0, l = keys.length; i < l; i += 1) {
				value.push(keys[i].header);
			}
			setColumnSelected(value);
		}
	}, [isPermiss]);

	return (
		<MasterLayout header='Lớp học' handleSearchChange={handleSearchChange}>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Lớp học | Xoài CMS</title>
			</Helmet>
			<Grid container>
				<Grid container style={{ marginBottom: '1rem' }}>
					<Grid item md={8}>
						<SearchForm
							onSubmit={handleSearchChange}
							label={'Tìm theo tên lớp và giáo viên'}
						/>
					</Grid>
					<Grid item md={4} container justify='flex-end'>
						{checkShow(functions, 'function_create_class') && (
							<Button
								startIcon={<AddIcon />}
								color='primary'
								variant='outlined'
								onClick={handleCreateChange}
							>
								Thêm lớp học
							</Button>
						)}
					</Grid>
				</Grid>

				<Grid container alignItems='center'>
					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						{subjects && (
							<FormControl
								style={{
									minWidth: '120px',
									marginRight: '2rem',
								}}
								size='small'
							>
								<InputLabel>Môn học</InputLabel>
								<Select
									value={filters.subject}
									onChange={(e) => handleFilterChange(e, 'subject')}
								>
									{subjects.map((subject) => (
										<MenuItem value={subject.subject_id}>
											{subject.subject_name}
										</MenuItem>
									))}
								</Select>
							</FormControl>
						)}
					</Grid>
					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						{rooms && (
							<FormControl
								style={{
									minWidth: '120px',
									marginRight: '2rem',
								}}
								size='small'
							>
								<InputLabel>Phòng học</InputLabel>
								<Select
									value={filters.room}
									onChange={(e) => handleFilterChange(e, 'room')}
								>
									{rooms.map((room) => (
										<MenuItem value={room.room_id}>{room.room_name}</MenuItem>
									))}
								</Select>
							</FormControl>
						)}
					</Grid>
					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						<FormControl
							style={{
								minWidth: '120px',
								marginRight: '2rem',
							}}
						>
							<InputLabel>Trạng thái</InputLabel>
							<Select
								value={filters.status}
								onChange={(e) => handleFilterChange(e, 'status')}
							>
								{statusList.map((status, index) => (
									<MenuItem key={index} value={status.value}>
										{status.display}
									</MenuItem>
								))}
							</Select>
						</FormControl>
					</Grid>
					<Grid item md={2}>
						{(filters.room !== '' ||
							filters.subject !== '' ||
							filters.status !== '') && (
							<Button
								onClick={resetFilter}
								startIcon={<FilterListIcon />}
								variant='contained'
								color='secondary'
							>
								Xóa lọc
							</Button>
						)}
					</Grid>
					<Grid item md={2} container justify='flex-end'>
						<Button
							startIcon={<GetAppIcon />}
							variant='text'
							onClick={exportClasses}
						>
							Xuất file
						</Button>
					</Grid>
					<Grid item md={2} container justify='flex-end'>
						<Select
							multiple
							value={columnSelected}
							onChange={handleColumnChange}
							input={<Input />}
							renderValue={() => 'Chọn cột hiển thị'}
							MenuProps={MenuProps}
						>
							{keys.map((key, index) => (
								<MenuItem
									onClick={() => changeDisplayColumn(key)}
									key={index}
									value={key.header}
								>
									<Checkbox checked={columnSelected.indexOf(key.header) > -1} />
									<ListItemText primary={key.header} />
								</MenuItem>
							))}
						</Select>
					</Grid>
				</Grid>

				<TableContainer component={Paper} style={{ marginTop: '1rem' }}>
					<Table className={classes.table} stickyHeader>
						<TableHead>
							<TableRow>
								<TableCell align='center' style={{ width: '5%' }}>
									STT
								</TableCell>
								{keys.map((key, index) => (
									<TableCell
										align={key.align}
										style={{ display: key.display ? '' : 'none' }}
										key={index}
									>
										{key.header}
									</TableCell>
								))}
								<TableCell align='center' style={{ width: '2%' }}></TableCell>
								<TableCell align='center' style={{ width: '2%' }}></TableCell>
								{checkShow(functions, 'function_delete_class') && (
									<TableCell align='center' style={{ width: '2%' }}></TableCell>
								)}
							</TableRow>
						</TableHead>
						<TableBody>
							{classroom.length > 0 ? (
								classroom.map((row, index) => (
									<TableRow hover key={index} align='center'>
										<TableCell align='center'>
											{(pagination.page - 1) * pagination.limit + index + 1}
										</TableCell>
										{keys.map((key, index) => (
											<TableCell
												style={{
													display: key.display ? '' : 'none',
												}}
												align={key.align}
												key={index}
											>
												{key.field === 'price' &&
													row['price'].toLocaleString('vi-VN', {
														style: 'currency',
														currency: 'VND',
													})}
												{key.field === 'class_status'
													? parseClassStatus(row[key.field])
													: key.field !== 'price'
													? row[key.field]
													: ''}
											</TableCell>
										))}
										<TableCell align='center'>
											<Tooltip title='Lịch sử điểm danh'>
												<AssignmentTurnedInIcon
													onClick={() =>
														handleChangeAttendanceHistoryDetail(
															row['class_id'],
															row['class_name']
														)
													}
													color='action'
													style={{ cursor: 'pointer' }}
												/>
											</Tooltip>
										</TableCell>
										<TableCell align='center'>
											{row['class_status'] !== '3' && (
												<Tooltip title='Chi tiết'>
													<EditIcon
														style={{ cursor: 'pointer' }}
														onClick={() =>
															handleChangeClassDetail(row['class_id'])
														}
													/>
												</Tooltip>
											)}
										</TableCell>
										{checkShow(functions, 'function_delete_class') && (
											<TableCell align='center'>
												{row['class_status'] !== '3' ? (
													<MyDialog
														title={`Xóa`}
														description='Tất cả dữ liệu về lớp học này sẽ bị xóa ?'
														doneText='Xóa'
														cancelText='Hủy'
														colorDone='secondary'
														iconType={'0'}
														action={() => handleDeleteClass(row['class_id'])}
													/>
												) : (
													<MyDialog
														title={`Khôi phục`}
														description='Tất cả dữ liệu về lớp học này sẽ được khôi phục ?'
														doneText='Đồng ý'
														cancelText='Hủy'
														colorDone='secondary'
														iconType={'1'}
														action={() => handleRestoreClass(row['class_id'])}
													/>
												)}
											</TableCell>
										)}
									</TableRow>
								))
							) : (
								<TableRow>
									<TableCell align='center' colSpan={keys.length + 3}>
										Không tìm thấy dữ liệu
									</TableCell>
								</TableRow>
							)}
						</TableBody>
					</Table>
				</TableContainer>
				{classroom.length > 0 && (
					<Grid container alignItems='center' style={{ marginTop: '2rem' }}>
						<Grid item md={6}>
							<Typography style={{ fontWeight: 'bold' }}>
								Tổng số lớp học ({pagination.totalRows})
							</Typography>
						</Grid>
						<Grid item md={6}>
							<Pagination
								pagination={pagination}
								onPageChange={handlePageChange}
							/>
						</Grid>
					</Grid>
				)}
			</Grid>
		</MasterLayout>
	);
}

export default ClassList;
