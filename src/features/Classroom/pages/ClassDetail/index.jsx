import {
	Avatar,
	Box,
	Button,
	Divider,
	FormControl,
	Grid,
	IconButton,
	makeStyles,
	MenuItem,
	Paper,
	Select,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	TextField,
	Typography,
} from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AddIcon from '@material-ui/icons/Add';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import CloseIcon from '@material-ui/icons/Close';
import EventIcon from '@material-ui/icons/Event';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import GroupIcon from '@material-ui/icons/Group';
import InfoIcon from '@material-ui/icons/Info';
import classApi from 'api/classApi';
import permissionApi from 'api/permissionApi';
import selectApi from 'api/selectApi';
import shiftApi from 'api/shiftApi';
import studentApi from 'api/studentApi';
import MyDialog from 'common/components/MyDialog';
import MyDialog1 from 'common/components/MyDialog1';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import NumberFormat from 'react-number-format';
import { Link, useHistory, useParams } from 'react-router-dom';
import {
	checkDayBelongWeek,
	checkStartDateGreaterThanNow,
	compareStartDateWithEndDate,
	days,
	displayDayOfWeek,
	getDayByDate1,
	getDayInWeek,
	getDayInWeek1,
	getDays1,
	getDisplayThisWeek,
	getWeeks,
	grades,
} from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

const referer = [
	{
		value: 'class',
		display: 'Lớp học',
	},
];

function NumberFormatCustom(props) {
	const { inputRef, onChange, ...other } = props;

	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={(values) => {
				onChange({
					target: {
						name: props.name,
						value: values.value,
					},
				});
			}}
			thousandSeparator
			isNumericString
		/>
	);
}

// thông tin của buổi học đó ở table
const showInfoScheduleDay = (schedules, day, slot) => {
	const schedule = schedules.find(
		(sl) => sl.day_of_week === day && sl.slot_id === slot
	);
	return schedule === undefined ? (
		'-'
	) : (
		<Typography variant='caption'>
			{schedule.room_name}
			<br />
			{`(${schedule.teacher_name})`}
			<br />
			{schedule.status === '0' ? (
				<span style={{ color: 'red' }}>(Chưa học)</span>
			) : (
				<span style={{ color: 'green' }}>(Đã học)</span>
			)}
		</Typography>
	);
};

// format ngày về định dạng năm-tháng-ngày
const formatDate = (date) => {
	return new Date(date).toISOString().slice(0, 10);
};

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
	},
	postPanel: {
		marginTop: '2rem',
		marginBottom: '0.5rem',
	},
	fab: {
		position: 'fixed',
		bottom: theme.spacing(8),
		right: theme.spacing(8),
	},
}));

function ClassDetail() {
	const notify = useContext(ToastifyContext);
	const { classId } = useParams();
	const classes = useStyles();
	let history = useHistory();

	// select week
	const [week, setWeek] = useState(getDayInWeek());

	// hiện thị ngày tháng theo thứ
	const [displayThisWeek, setDisplayThisWeek] = useState(
		getDisplayThisWeek(getDayInWeek())
	);

	const [canEdit, setCanEdit] = useState(false);
	const [canDelete, setCanDelete] = useState(false);

	// select year
	const [year, setYear] = useState(() => {
		return new Date().getFullYear();
	});

	const [loading, setLoading] = useState(false);

	const [openDeleteClass, setOpenDeleteClass] = useState(false);

	const handleClickOpenDeleteClass = () => {
		setOpenDeleteClass(true);
	};

	const handleCloseDeleteClass = () => {
		setOpenDeleteClass(false);
	};

	const [open, setOpen] = useState({
		show: false,
		data: [],
	});
	const [subjects, setSubjects] = useState([]);
	const [teachers, setTeachers] = useState([]);
	const [tutors, setTutors] = useState([]);
	// thông tin chung của lớp học
	const [dataInput, setDataInput] = useState({
		startdate: '',
		enddate: '',
		classname: '',
		price: '',
		maxstudent: '',
		percent: '',
		subject: '',
		teacher: '',
		tutor: '',
		note: '',
		grade: '',
		status: '',
		numberstudent: '',
	});

	const [errorInput, setErrorInput] = useState({
		startdate: '',
		enddate: '',
		classname: '',
		price: '',
		maxstudent: '',
		percent: '',
		subject: '',
		teacher: '',
		tutor: '',
		note: '',
		schedule: '',
		grade: '',
		status: '',
	});
	// list slot của schedule tương ứng
	const [slots, setSlots] = useState({
		schedule0: [],
	});
	// list room của schedule tương ứng
	const [rooms, setRooms] = useState({
		schedule0: [],
	});
	// data của các schedule
	const [schedules, setSchedule] = useState([
		{
			day: -1,
			slot: '',
			room: '',
			type: '0',
		},
	]);

	// tất cả slot
	const [defaultSlot, setDefaultSlot] = useState([]);

	// chi tiết lịch học theo từng ngày
	const [detailSchedule, setDetailSchedule] = useState([]);

	// show chi tiết lịch học theo tuần
	const [showDetailSchedule, setShowDetailSchedule] = useState([]);

	const [students, setStudents] = useState([]);

	const [isPermiss, setIsPermiss] = useState(false);

	// load schedule xong thì mới render lịch học mặc định
	const [isScheduleLoad, setIsScheduleLoad] = useState(false);

	const [isStudentChange, setIsStudentChange] = useState(false);

	const [open1, setOpen1] = useState(false);

	const handleOpen1 = () => {
		setOpen1(true);
	};

	const handleClose1 = () => {
		setErrorAddSchedule({
			...errorAddSchedule,
			date: '',
			slot: '',
			room: '',
			teacher: '',
			tutor: '',
			total: '',
		});
		setOpen1(false);
	};

	const [addStudentData, setAddStudentData] = useState({
		studentId: '',
		studentName: '',
	});

	const handleStudentIdChange = (e) => {
		setAddStudentData({
			...addStudentData,
			studentId: e.target.value,
		});
	};

	const handleStudentIdBlur = (e) => {
		if (e.target.value) {
			const get = async () => {
				try {
					const response = await studentApi.getStudentById(e.target.value);
					if (response.count > 0) {
						setAddStudentData((prevState) => {
							return { ...prevState, studentName: response.data.full_name };
						});

						setErrorAddStudent('');

						return;
					}

					setAddStudentData((prevState) => {
						return { ...prevState, studentName: '' };
					});
					setErrorAddStudent('Không tìm thấy học sinh');
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	};

	const handleAddStudentDone = () => {
		if (errorAddStudent) {
			return;
		}

		if (addStudentData.studentId === '' || addStudentData.studentName === '') {
			setErrorAddStudent('Các trường không được để trống');
			return;
		}

		setErrorAddStudent('');

		const post = async () => {
			try {
				setLoading(true);
				const response = await classApi.addStudentToClass(
					classId,
					addStudentData.studentId
				);
				setIsStudentChange(!isStudentChange);
				setLoading(false);
				notify('success', response.message);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
		setAddStudentData({
			studentId: '',
			studentName: '',
		});
		setOpenAddStudent(false);
	};

	const handleDeleteStudentInClass = (studentId) => {
		const post = async () => {
			try {
				setLoading(true);
				const response = await classApi.deleteStudentInClass(
					classId,
					studentId
				);
				setLoading(false);
				setIsStudentChange(!isStudentChange);
				notify('success', response.message);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	};

	const [errorAddStudent, setErrorAddStudent] = useState('');

	const [openAddStudent, setOpenAddStudent] = useState(false);

	const handleOpenAddStudent = () => {
		setOpenAddStudent(true);
	};

	const handleCloseAddStudent = () => {
		setOpenAddStudent(false);
	};

	const [open2, setOpen2] = useState(false);

	const handleClose2 = () => {
		setErrorEditSchedule({
			...errorEditSchedule,
			oldDate: '',
			oldSlot: '',
			oldRoom: '',
			oldTeacher: '',
			oldTutor: '',
			newDate: '',
			newSlot: '',
			newRoom: '',
			newTeacher: '',
			newTutor: '',
			total: '',
			total1: '',
			total2: '',
		});
		setOpen2(false);
	};

	const handleYearChange = (e) => {
		setYear(e.target.value);
		setWeek(0);

		const showSchedule = detailSchedule.filter(
			(sl) =>
				new Date(sl.date).getFullYear() === e.target.value &&
				sl.week_of_year === 0
		);

		setShowDetailSchedule(() => {
			return showSchedule;
		});

		setDisplayThisWeek(() => {
			return getDisplayThisWeek(0);
		});
	};

	const handleWeekChange = (e) => {
		setWeek(e.target.value);
		setDisplayThisWeek(getDisplayThisWeek(e.target.value));

		const showSchedule = detailSchedule.filter(
			(sl) => sl.week_of_year === e.target.value
		);

		setShowDetailSchedule(() => {
			return showSchedule;
		});
	};

	const handleClose = () => {
		setOpen({
			...open,
			show: false,
			data: [],
		});
	};

	const handleDataInputChange = (e) => {
		setDataInput({
			...dataInput,
			[e.target.name]: e.target.value,
		});

		if (e.target.name === 'teacher' && e.target.value !== '') {
			const thisTeacher = teachers.find(
				(teacher) => teacher.teacher_id === e.target.value
			);

			for (let i = 0; i < detailSchedule.length; i++) {
				if (new Date(detailSchedule[i].date).getTime() > new Date().getTime()) {
					detailSchedule[i].teacher_id = thisTeacher.teacher_id;
					detailSchedule[i].teacher_name = thisTeacher.teacher_name;
				}
			}

			const newDetailSchedule = [...detailSchedule];
			setDetailSchedule(() => {
				return newDetailSchedule;
			});

			const showSchedule = newDetailSchedule.filter(
				(sl) =>
					sl.week_of_year === week && new Date(sl.date).getFullYear() === year
			);

			setShowDetailSchedule(() => {
				return showSchedule;
			});

			return;
		}

		if (e.target.name === 'tutor' && e.target.value !== '') {
			const thisTutor = tutors.find(
				(tutor) => tutor.tutor_id === e.target.value
			);

			for (let i = 0; i < detailSchedule.length; i++) {
				if (new Date(detailSchedule[i].date).getTime() > new Date().getTime()) {
					detailSchedule[i].tutor_id = thisTutor.tutor_id;
					detailSchedule[i].tutor_name = thisTutor.tutor_name;
				}
			}

			const newDetailSchedule = [...detailSchedule];
			setDetailSchedule(() => {
				return newDetailSchedule;
			});

			const showSchedule = newDetailSchedule.filter(
				(sl) =>
					sl.week_of_year === week && new Date(sl.date).getFullYear() === year
			);

			setShowDetailSchedule(() => {
				return showSchedule;
			});

			return;
		}
	};

	const handleStartdateBlur = (e) => {
		if (
			detailSchedule.find(
				(a) =>
					a.status === '1' &&
					new Date(a.date).getTime() < new Date(e.target.value).getTime()
			) !== undefined
		) {
			schedules[0].day = -1;
			const scheduleArray = [...schedules];
			setSchedule(scheduleArray);
			setErrorInput({
				...errorInput,
				startdate:
					'Không thể thay đổi ngày bắt đầu (có buổi đã học ở trước ngày bạn chọn)',
			});
			return;
		}

		setErrorInput({
			...errorInput,
			startdate: '',
		});

		if (checkStartDateGreaterThanNow(e.target.value)) {
			schedules[0].day = new Date(e.target.value).getDay();
			const scheduleArray = [...schedules];
			setSchedule(scheduleArray);
			setErrorInput({
				...errorInput,
				startdate: '',
			});

			if (dataInput.enddate !== '') {
				if (compareStartDateWithEndDate(e.target.value, dataInput.enddate)) {
					setErrorInput({
						...errorInput,
						startdate: '',
						enddate: '',
					});

					// code here
					const oldStartdate = detailSchedule[0].date;
					const oldSlot = detailSchedule[0].slot_id;
					const oldDayOfWeek = detailSchedule[0].day_of_week;
					const oldRoom = detailSchedule[0].room_id;

					// case startdate mới < startdate cũ
					if (
						new Date(oldStartdate).getTime() >
						new Date(e.target.value).getTime()
					) {
						// xóa lịch học mặc định theo startdate cũ
						for (let i = 0; i < detailSchedule.length; i++) {
							if (
								detailSchedule[i].slot_id === oldSlot &&
								detailSchedule[i].day_of_week === oldDayOfWeek &&
								detailSchedule[i].room_id === oldRoom &&
								detailSchedule[i].type !== '2'
							) {
								detailSchedule.splice(i, 1);
								i--;
							}
						}

						const defaultDays = getDays1(
							formatDate(e.target.value),
							new Date(e.target.value).getDay(),
							formatDate(dataInput.enddate)
						);

						const defaultRoom = rooms[Object.keys(rooms)[0]].find(
							(r) => r.room_id === schedules[0].room
						);

						const defaultTeacher = teachers.find(
							(teacher) => teacher.teacher_id === dataInput.teacher
						);
						let defaultTutor = undefined;
						if (dataInput.tutor !== '') {
							defaultTutor = tutors.find(
								(tutor) => tutor.tutor_id === dataInput.tutor
							);
						}

						const arrayValue = [];
						for (let j = 0; j < defaultDays.length; j++) {
							const newSchedule = {
								schedule_id: '-1',
								date: defaultDays[j],
								day_of_week: new Date(defaultDays[j]).getDay(),
								room_id: defaultRoom.room_id,
								room_name: defaultRoom.room_name,
								slot_id: schedules[0].slot,
								status: '0',
								type: '0',
								week_of_year: getDayInWeek1(new Date(defaultDays[j])),
								teacher_id: defaultTeacher.teacher_id,
								teacher_name: defaultTeacher.teacher_name,
								tutor_id:
									defaultTutor !== undefined ? defaultTutor.tutor_id : null,
								tutor_name:
									defaultTutor !== undefined ? defaultTutor.tutor_name : null,
							};

							arrayValue.push(newSchedule);
						}

						for (let i = 1; i < schedules.length; i++) {
							const days = getDays1(
								formatDate(e.target.value),
								schedules[i].day,
								formatDate(oldStartdate)
							);

							const thisRoom = rooms[Object.keys(rooms)[i]].find(
								(r) => r.room_id === schedules[i].room
							);

							const thisTeacher = teachers.find(
								(teacher) => teacher.teacher_id === dataInput.teacher
							);
							let thisTutor = undefined;
							if (dataInput.tutor !== '') {
								thisTutor = tutors.find(
									(tutor) => tutor.tutor_id === dataInput.tutor
								);
							}

							for (let j = 0; j < days.length; j++) {
								const newSchedule = {
									schedule_id: '-1',
									date: days[j],
									day_of_week: new Date(days[j]).getDay(),
									room_id: thisRoom.room_id,
									room_name: thisRoom.room_name,
									slot_id: schedules[i].slot,
									status: '0',
									type: '1',
									week_of_year: getDayInWeek1(new Date(days[j])),
									teacher_id: thisTeacher.teacher_id,
									teacher_name: thisTeacher.teacher_name,
									tutor_id: thisTutor !== undefined ? thisTutor.tutor_id : null,
									tutor_name:
										thisTutor !== undefined ? thisTutor.tutor_name : null,
								};

								detailSchedule.push(newSchedule);
							}
						}

						const newDetailSchedule = arrayValue.concat(detailSchedule);

						setDetailSchedule(() => {
							return newDetailSchedule;
						});

						const showSchedule = newDetailSchedule.filter(
							(sl) =>
								sl.week_of_year === week &&
								new Date(sl.date).getFullYear() === year
						);

						setShowDetailSchedule(() => {
							return showSchedule;
						});
					} else {
						// xóa lịch học mặc định theo startdate cũ
						for (let i = 0; i < detailSchedule.length; i++) {
							if (
								(detailSchedule[i].slot_id === oldSlot &&
									detailSchedule[i].day_of_week === oldDayOfWeek &&
									detailSchedule[i].room_id === oldRoom) ||
								new Date(detailSchedule[i].date).getTime() <=
									new Date(e.target.value).getTime()
							) {
								detailSchedule.splice(i, 1);
								i--;
							}
						}

						const defaultDays = getDays1(
							formatDate(e.target.value),
							new Date(e.target.value).getDay(),
							formatDate(dataInput.enddate)
						);

						const defaultRoom = rooms[Object.keys(rooms)[0]].find(
							(r) => r.room_id === schedules[0].room
						);

						const defaultTeacher = teachers.find(
							(teacher) => teacher.teacher_id === dataInput.teacher
						);
						let defaultTutor = undefined;
						if (dataInput.tutor !== '') {
							defaultTutor = tutors.find(
								(tutor) => tutor.tutor_id === dataInput.tutor
							);
						}

						const arrayValue = [];
						for (let j = 0; j < defaultDays.length; j++) {
							const newSchedule = {
								schedule_id: '-1',
								date: defaultDays[j],
								day_of_week: new Date(defaultDays[j]).getDay(),
								room_id: defaultRoom.room_id,
								room_name: defaultRoom.room_name,
								slot_id: schedules[0].slot,
								status: '0',
								type: '0',
								week_of_year: getDayInWeek1(new Date(defaultDays[j])),
								teacher_id: defaultTeacher.teacher_id,
								teacher_name: defaultTeacher.teacher_name,
								tutor_id:
									defaultTutor !== undefined ? defaultTutor.tutor_id : null,
								tutor_name:
									defaultTutor !== undefined ? defaultTutor.tutor_name : null,
							};

							arrayValue.push(newSchedule);
						}

						const newDetailSchedule = arrayValue.concat(detailSchedule);

						setDetailSchedule(() => {
							return newDetailSchedule;
						});

						const showSchedule = newDetailSchedule.filter(
							(sl) =>
								sl.week_of_year === week &&
								new Date(sl.date).getFullYear() === year
						);

						setShowDetailSchedule(() => {
							return showSchedule;
						});
					}
				} else {
					setErrorInput({
						...errorInput,
						startdate: '',
						enddate: 'Ngày kết thúc phải lớn hơn ngày khai giảng',
					});
				}
			}
		} else {
			schedules[0].day = -1;
			const scheduleArray = [...schedules];
			setSchedule(scheduleArray);
			setErrorInput({
				...errorInput,
				startdate: 'Ngày khai giảng phải lớn hơn ngày hiện tại',
			});
		}
	};

	const handleEnddateBlur = (e) => {
		if (
			detailSchedule.find(
				(a) =>
					a.status === '1' &&
					new Date(a.date).getTime() > new Date(e.target.value).getTime()
			) !== undefined
		) {
			setErrorInput({
				...errorInput,
				enddate:
					'Không thể thay đổi ngày kết thúc (có buổi đã học ở sau ngày bạn chọn)',
			});
			return;
		}

		if (compareStartDateWithEndDate(dataInput.startdate, e.target.value)) {
			setErrorInput({
				...errorInput,
				enddate: '',
			});

			// here

			const oldEnddate = detailSchedule.reduce(
				(value, scd) =>
					(value = value > new Date(scd.date) ? value : new Date(scd.date)),
				new Date(detailSchedule[0].date)
			);

			oldEnddate.setDate(oldEnddate.getDate() + 1);

			if (new Date(oldEnddate).getTime() < new Date(e.target.value).getTime()) {
				for (let i = 0; i < schedules.length; i++) {
					const days = getDays1(
						formatDate(oldEnddate),
						schedules[i].day,
						formatDate(e.target.value)
					);

					const thisRoom = rooms[Object.keys(rooms)[i]].find(
						(r) => r.room_id === schedules[i].room
					);

					const thisTeacher = teachers.find(
						(teacher) => teacher.teacher_id === dataInput.teacher
					);
					let thisTutor = undefined;
					if (dataInput.tutor !== '') {
						thisTutor = tutors.find(
							(tutor) => tutor.tutor_id === dataInput.tutor
						);
					}

					for (let j = 0; j < days.length; j++) {
						const newSchedule = {
							schedule_id: '-1',
							date: days[j],
							day_of_week: new Date(days[j]).getDay(),
							room_id: thisRoom.room_id,
							room_name: thisRoom.room_name,
							slot_id: schedules[i].slot,
							status: '0',
							type: schedules[i].type,
							week_of_year: getDayInWeek1(new Date(days[j])),
							teacher_id: thisTeacher.teacher_id,
							teacher_name: thisTeacher.teacher_name,
							tutor_id: thisTutor !== undefined ? thisTutor.tutor_id : null,
							tutor_name: thisTutor !== undefined ? thisTutor.tutor_name : null,
						};

						detailSchedule.push(newSchedule);
					}

					const newDetailSchedule = [...detailSchedule];
					setDetailSchedule(() => {
						return newDetailSchedule;
					});

					const showSchedule = newDetailSchedule.filter(
						(sl) =>
							sl.week_of_year === week &&
							new Date(sl.date).getFullYear() === year
					);

					setShowDetailSchedule(() => {
						return showSchedule;
					});
				}
			} else {
				for (let i = 0; i < detailSchedule.length; i++) {
					if (
						new Date(detailSchedule[i].date).getTime() >
							new Date(e.target.value).getTime() &&
						detailSchedule[i].type !== '2'
					) {
						detailSchedule.splice(i, 1);
						i--;
					}
				}

				const newDetailSchedule = [...detailSchedule];
				setDetailSchedule(() => {
					return newDetailSchedule;
				});

				const showSchedule = newDetailSchedule.filter(
					(sl) =>
						sl.week_of_year === week && new Date(sl.date).getFullYear() === year
				);

				setShowDetailSchedule(() => {
					return showSchedule;
				});
			}

			return;
		}

		setErrorInput({
			...errorInput,
			enddate: 'Ngày kết thúc phải lớn hơn ngày khai giảng',
		});
	};

	const handleMaxStudentBlur = (e) => {
		if (parseInt(e.target.value) > 0) {
			setErrorInput({
				...errorInput,
				maxstudent: '',
			});
			return;
		}

		setErrorInput({
			...errorInput,
			maxstudent: 'Số lượng học sinh phải lớn hơn 0',
		});
	};

	const handlePriceBlur = (e) => {
		const priceValue = e.target.value.toString().replaceAll(',', '');
		if (parseFloat(priceValue) > 0) {
			setErrorInput({
				...errorInput,
				price: '',
			});
			return;
		}

		setErrorInput({
			...errorInput,
			price: 'Giá tiền phải lớn hơn 0',
		});
	};

	const handlePercentBlur = (e) => {
		if (parseFloat(e.target.value) > 0) {
			setErrorInput({
				...errorInput,
				percent: '',
			});
			return;
		}

		setErrorInput({
			...errorInput,
			percent: 'Tỷ lệ hoa hồng phải lớn hơn 0',
		});
	};

	const [dataAddSchedule, setDataAddSchedule] = useState({
		date: '',
		slot: '',
		room: '',
		teacher: '',
		tutor: '',
	});

	const [errorAddSchedule, setErrorAddSchedule] = useState({
		date: '',
		slot: '',
		room: '',
		teacher: '',
		tutor: '',
		total: '',
	});

	const [slotAddSchedule, setSlotAddSchedule] = useState([]);
	const [roomAddSchedule, setRoomAddSchedule] = useState([]);

	const [dataEditSchedule, setDataEditSchedule] = useState({
		scheduleIndex: -1,
		oldDate: '',
		oldSlot: {
			value: '',
			display: '',
		},
		oldRoom: {
			value: '',
			display: '',
		},

		oldTeacher: '',
		oldTutor: '',
		newDate: '',
		newSlot: '',
		newRoom: '',
		newTeacher: '',
		newTutor: '',
	});

	const [errorEditSchedule, setErrorEditSchedule] = useState({
		oldDate: '',
		oldSlot: '',
		oldRoom: '',
		oldTeacher: '',
		oldTutor: '',
		newDate: '',
		newSlot: '',
		newRoom: '',
		newTeacher: '',
		newTutor: '',
		total: '',
		total1: '',
		total2: '',
	});

	const [slotEditSchedule, setSlotEditSchedule] = useState([]);
	const [roomEditSchedule, setRoomEditSchedule] = useState([]);

	const handleEditScheduleChange = (e) => {
		setDataEditSchedule((prevState) => {
			return {
				...prevState,
				[e.target.name]: e.target.value,
			};
		});
	};

	const handleAddScheduleDone = () => {
		if (
			dataAddSchedule.date !== '' &&
			dataAddSchedule.slot !== '' &&
			dataAddSchedule.room !== '' &&
			dataAddSchedule.teacher !== ''
		) {
			for (let i = 0; i < detailSchedule.length; i++) {
				if (
					new Date(dataAddSchedule.date).getTime() ===
						new Date(detailSchedule[i].date).getTime() &&
					dataAddSchedule.slot === detailSchedule[i].slot_id
				) {
					setErrorAddSchedule(() => {
						return {
							...errorAddSchedule,
							total: `Lịch học bị trùng`,
						};
					});
					return;
				}
			}

			setErrorAddSchedule({
				...errorAddSchedule,
				total: '',
			});

			// get room name
			const thisRoom = roomAddSchedule.find(
				(r) => r.room_id === dataAddSchedule.room
			);

			const thisTeacher = teachers.find(
				(teacher) => teacher.teacher_id === dataAddSchedule.teacher
			);
			const thisTutor = undefined;
			if (dataAddSchedule.tutor !== '') {
				thisTutor = tutors.find(
					(tutor) => tutor.tutor_id === dataAddSchedule.newTutor
				);
			}

			if (thisRoom !== undefined) {
				console.log(new Date(dataAddSchedule.date).getDay());
				const newSchedule = {
					schedule_id: '-1',
					date: new Date(dataAddSchedule.date),
					day_of_week: new Date(dataAddSchedule.date).getDay(),
					room_id: thisRoom.room_id,
					room_name: thisRoom.room_name,
					slot_id: dataAddSchedule.slot,
					type: '2',
					status: '0',
					week_of_year: getDayInWeek1(new Date(dataAddSchedule.date)),
					teacher_id: thisTeacher !== undefined ? thisTeacher.teacher_id : null,
					teacher_name:
						thisTeacher !== undefined ? thisTeacher.teacher_name : null,
					tutor_id: thisTutor !== undefined ? thisTutor.tutor_id : null,
					tutor_name: thisTutor !== undefined ? thisTutor.tutor_name : null,
				};

				detailSchedule.push(newSchedule);

				const newDetailSchedule = [...detailSchedule];
				setDetailSchedule(() => {
					return newDetailSchedule;
				});

				const showSchedule = newDetailSchedule.filter(
					(sl) =>
						sl.week_of_year === week && new Date(sl.date).getFullYear() === year
				);

				setShowDetailSchedule(() => {
					return showSchedule;
				});
			}
		}

		setOpen1(false);
		setErrorAddSchedule({
			...errorAddSchedule,
			date: '',
			slot: '',
			room: '',
			teacher: '',
			tutor: '',
			total: '',
		});
		setDataAddSchedule((prevState) => {
			return {
				...prevState,
				date: '',
				slot: '',
				room: '',
				teacher: '',
				tutor: '',
			};
		});
	};

	const checkCursor = (dayOfWeek, slot) => {
		const scheduleChange = showDetailSchedule.find(
			(sl) => sl.day_of_week === dayOfWeek && sl.slot_id === slot
		);

		if (
			scheduleChange === undefined ||
			scheduleChange.status === '1' ||
			!canEdit
		) {
			return false;
		}

		return true;
	};

	const handleOpenChangeScheduleDialog = (dayOfWeek, slot) => {
		if (open2) {
			return;
		}

		const scheduleChange = showDetailSchedule.find(
			(sl) => sl.day_of_week === dayOfWeek && sl.slot_id === slot
		);

		if (
			scheduleChange === undefined ||
			scheduleChange.status === '1' ||
			!canEdit
		) {
			return;
		}

		const thisSlot = slotEditSchedule.find((sl) => sl.slot_id === slot);

		const scheduleIndex = detailSchedule.findIndex(
			(ds) =>
				new Date(scheduleChange.date).getTime() ===
					new Date(ds.date).getTime() && scheduleChange.slot_id === ds.slot_id
		);

		const parseDate =
			new Date(scheduleChange.date).getFullYear() +
			'-' +
			('0' + (new Date(scheduleChange.date).getMonth() + 1)).slice(-2) +
			'-' +
			('0' + new Date(scheduleChange.date).getDate()).slice(-2);

		setDataEditSchedule((prevState) => {
			return {
				...prevState,
				scheduleIndex: scheduleIndex,
				oldDate: formatDate(parseDate),
				oldSlot: {
					value: thisSlot.slot_id,
					display: thisSlot.slot_name,
				},
				oldRoom: {
					value: scheduleChange.room_id,
					display: scheduleChange.room_name,
				},
				oldTeacher: scheduleChange.teacher_name,
				oldTutor:
					scheduleChange.tutor_name === null ? '' : scheduleChange.tutor_name,
			};
		});

		setOpen2(true);
	};

	const [openDelete, setOpenDelete] = useState(false);

	const handleOpenDelete = () => {
		setOpenDelete(true);
	};

	const handleCloseDelete = () => {
		setDataEditSchedule({
			...dataEditSchedule,
			scheduleIndex: -1,
			oldDate: '',
			oldSlot: {
				value: '',
				display: '',
			},
			oldRoom: {
				value: '',
				display: '',
			},
			oldTeacher: '',
			oldTutor: '',
			newDate: '',
			newSlot: '',
			newRoom: '',
			newTeacher: '',
			newTutor: '',
			total: '',
		});

		setErrorEditSchedule({
			...errorEditSchedule,
			oldDate: '',
			oldSlot: '',
			oldRoom: '',
			oldTeacher: '',
			oldTutor: '',
			newDate: '',
			newSlot: '',
			newRoom: '',
			newTeacher: '',
			newTutor: '',
			total: '',
			total1: '',
			total2: '',
		});

		setOpenDelete(false);
	};

	const handleDeleteDayScheduleDone = () => {
		detailSchedule.splice(dataEditSchedule.scheduleIndex, 1);

		const newDetailSchedule = [...detailSchedule];
		setDetailSchedule(() => {
			return newDetailSchedule;
		});

		const showSchedule = newDetailSchedule.filter(
			(sl) =>
				sl.week_of_year === week && new Date(sl.date).getFullYear() === year
		);

		setShowDetailSchedule(() => {
			return showSchedule;
		});

		setOpen2(false);

		setDataEditSchedule({
			...dataEditSchedule,
			scheduleIndex: -1,
			oldDate: '',
			oldSlot: {
				value: '',
				display: '',
			},
			oldRoom: {
				value: '',
				display: '',
			},
			oldTeacher: '',
			oldTutor: '',
			newDate: '',
			newSlot: '',
			newRoom: '',
			newTeacher: '',
			newTutor: '',
			total: '',
		});

		setErrorEditSchedule({
			...errorEditSchedule,
			oldDate: '',
			oldSlot: '',
			oldRoom: '',
			oldTeacher: '',
			oldTutor: '',
			newDate: '',
			newSlot: '',
			newRoom: '',
			newTeacher: '',
			newTutor: '',
			total: '',
			total1: '',
			total2: '',
		});
	};

	const handleEditScheduleDone = () => {
		if (
			!dataEditSchedule.newDate ||
			!dataEditSchedule.newSlot ||
			!dataEditSchedule.newRoom ||
			!dataEditSchedule.newTeacher
		) {
			setErrorEditSchedule((prevState) => {
				return {
					...prevState,
					total2: 'Các trường không được để trống',
				};
			});

			return;
		}

		if (
			new Date(dataEditSchedule.newDate).getTime() <
				new Date(dataInput.startdate).getTime() ||
			new Date(dataEditSchedule.newDate).getTime() >
				new Date(dataInput.enddate).getTime()
		) {
			setErrorEditSchedule((prevState) => {
				return {
					...prevState,
					total2:
						'Ngày không được nhỏ hơn ngày bắt đầu hoặc lớn hơn ngày kết thúc.',
				};
			});

			return;
		}

		setErrorEditSchedule((prevState) => {
			return {
				...prevState,
				total2: '',
			};
		});

		if (
			dataEditSchedule.newDate !== '' &&
			dataEditSchedule.newSlot !== '' &&
			dataEditSchedule.newRoom !== '' &&
			dataEditSchedule.newTeacher !== ''
		) {
			const oldDateParse = new Date(dataEditSchedule.oldDate);
			const newDateParse = new Date(dataEditSchedule.newDate);
			let isCheck = true;
			if (
				dataEditSchedule.oldSlot.value === dataEditSchedule.newSlot &&
				oldDateParse.getFullYear() === newDateParse.getFullYear() &&
				oldDateParse.getMonth() === newDateParse.getMonth() &&
				oldDateParse.getDate() === newDateParse.getDate()
			) {
				isCheck = false;
			}
			if (isCheck) {
				for (let i = 0; i < detailSchedule.length; i++) {
					let parseDate =
						new Date(detailSchedule[i].date).getFullYear() +
						'-' +
						('0' + (new Date(detailSchedule[i].date).getMonth() + 1)).slice(
							-2
						) +
						'-' +
						('0' + new Date(detailSchedule[i].date).getDate()).slice(-2);
					const selectDate = new Date(dataEditSchedule.newDate);
					const dataDate = new Date(parseDate);
					if (
						selectDate.getFullYear() === dataDate.getFullYear() &&
						selectDate.getMonth() === dataDate.getMonth() &&
						selectDate.getDate() === dataDate.getDate() &&
						dataEditSchedule.newSlot === detailSchedule[i].slot_id
					) {
						// fix
						setErrorEditSchedule((prevState) => {
							return {
								...prevState,
								total1: 'Lịch học bị trùng',
							};
						});

						return;
					}
				}
			}

			setErrorEditSchedule((prevState) => {
				return {
					...prevState,
					total1: '',
				};
			});

			const thisTeacher = teachers.find(
				(teacher) => teacher.teacher_id === dataEditSchedule.newTeacher
			);
			const thisTutor = undefined;
			if (dataEditSchedule.newTutor !== '') {
				thisTutor = tutors.find(
					(tutor) => tutor.tutor_id === dataEditSchedule.newTutor
				);
			}

			const thisRoom = roomEditSchedule.find(
				(r) => r.room_id === dataEditSchedule.newRoom
			);

			for (let i = 0; i < detailSchedule.length; i++) {
				let parseDate =
					new Date(detailSchedule[i].date).getFullYear() +
					'-' +
					('0' + (new Date(detailSchedule[i].date).getMonth() + 1)).slice(-2) +
					'-' +
					('0' + new Date(detailSchedule[i].date).getDate()).slice(-2);
				const selectDate = new Date(dataEditSchedule.oldDate);
				const dataDate = new Date(parseDate);

				if (
					selectDate.getFullYear() === dataDate.getFullYear() &&
					selectDate.getMonth() === dataDate.getMonth() &&
					selectDate.getDate() === dataDate.getDate() &&
					dataEditSchedule.oldSlot.value === detailSchedule[i].slot_id
				) {
					detailSchedule[i].date = new Date(dataEditSchedule.newDate);
					detailSchedule[i].slot_id = dataEditSchedule.newSlot;
					detailSchedule[i].room_id = thisRoom.room_id;
					detailSchedule[i].room_name = thisRoom.room_name;
					detailSchedule[i].day_of_week = new Date(
						dataEditSchedule.newDate
					).getDay();
					detailSchedule[i].week_of_year = getDayInWeek1(
						new Date(dataEditSchedule.newDate)
					);
					detailSchedule[i].status = '0';
					detailSchedule[i].type = '2';
					if (thisTeacher !== undefined) {
						detailSchedule[i].teacher_id = thisTeacher.teacher_id;
						detailSchedule[i].teacher_name = thisTeacher.teacher_name;
					}

					if (thisTutor !== undefined) {
						detailSchedule[i].tutor_id = thisTutor.tutor_id;
						detailSchedule[i].tutor_name = thisTutor.tutor_name;
					}

					const newDetailSchedule = [...detailSchedule];
					setDetailSchedule(() => {
						return newDetailSchedule;
					});

					const showSchedule = newDetailSchedule.filter(
						(sl) =>
							sl.week_of_year === week &&
							new Date(sl.date).getFullYear() === year
					);

					setShowDetailSchedule(() => {
						return showSchedule;
					});

					setDataEditSchedule({
						...dataEditSchedule,
						scheduleIndex: -1,
						oldDate: '',
						oldSlot: {
							value: '',
							display: '',
						},
						oldRoom: {
							value: '',
							display: '',
						},
						oldTeacher: '',
						oldTutor: '',
						newDate: '',
						newSlot: '',
						newRoom: '',
						newTeacher: '',
						newTutor: '',
						total: '',
					});

					setErrorEditSchedule({
						...errorEditSchedule,
						oldDate: '',
						oldSlot: '',
						oldRoom: '',
						oldTeacher: '',
						oldTutor: '',
						newDate: '',
						newSlot: '',
						newRoom: '',
						newTeacher: '',
						newTutor: '',
						total: '',
						total1: '',
						total2: '',
					});

					setOpen2(false);
					return;
				}
			}
		}
	};

	const handleDateAddScheduleChange = (e) => {
		setDataAddSchedule((prevState) => {
			return {
				...prevState,
				[e.target.name]: e.target.value,
			};
		});
	};

	const handleDataAddScheduleChange = (e, field) => {
		if (field === 'slot') {
			const getSelectRoom = async () => {
				try {
					const filter = {
						startdate: dataInput.startdate,
						slot: e.target.value,
						day: new Date(dataAddSchedule.date).getDay(),
					};
					const response = await selectApi.getRoomAddClassScreen(filter);

					setRoomAddSchedule(() => {
						return response;
					});
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				} finally {
				}
			};

			getSelectRoom();
		} else if (field === 'room') {
			const getSelectSlot = async () => {
				try {
					const filter = {
						startdate: dataInput.startdate,
						room: e.target.value,
						day: new Date(dataAddSchedule.date).getDay(),
					};
					const response = await selectApi.getSlotAddClassScreen(filter);

					setSlotAddSchedule(() => {
						return response;
					});
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			getSelectSlot();
		}

		setDataAddSchedule((prevState) => {
			return {
				...prevState,
				[field]: e.target.value,
			};
		});
	};

	const handleDateAddDialogBlur = (e) => {
		if (
			new Date(e.target.value).getTime() >=
				new Date(dataInput.startdate).getTime() &&
			new Date(e.target.value).getTime() <=
				new Date(dataInput.enddate).getTime()
		) {
			setErrorAddSchedule({
				...errorAddSchedule,
				date: '',
			});

			setDataAddSchedule(() => {
				return {
					...dataAddSchedule,
					slot: '',
					room: '',
				};
			});

			if (e.target.value !== '') {
				const getSelectRoom = async () => {
					try {
						const filter = {
							startdate: dataInput.startdate,
							slot: '',
							day: '',
						};
						const response = await selectApi.getRoomAddClassScreen(filter);

						setRoomAddSchedule(() => {
							return response;
						});
					} catch (error) {
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/');
						}
					}
				};

				getSelectRoom();
				const getSelectSlot = async () => {
					try {
						const filter = {
							startdate: dataInput.startdate,
							room: '',
							day: '',
						};
						const response = await selectApi.getSlotAddClassScreen(filter);

						setSlotAddSchedule(() => {
							return response;
						});
					} catch (error) {
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/');
						}
					}
				};

				getSelectSlot();
			}
			return;
		}

		setErrorAddSchedule(() => {
			return {
				...errorAddSchedule,
				date: 'Ngày phải lớn hơn hoặc bằng ngày khai giảng và nhỏ hơn hoặc bằng ngày kết thúc',
			};
		});
	};

	// Mỗi khi click button "Thêm Lịch Học", tạo ra 1 lịch học mới
	const handleAddMoreSchedule = () => {
		const newSchedule = {
			day: -1,
			slot: '',
			room: '',
			type: '1',
		};
		schedules.push(newSchedule);
		const scheduleArray = [...schedules];
		setSchedule(scheduleArray);
	};

	// xóa lịch học tương ứng
	const handleDeleteSchedule = (index) => {
		for (let i = 0; i < detailSchedule.length; i++) {
			if (
				schedules[index].day === detailSchedule[i].day_of_week &&
				schedules[index].slot === detailSchedule[i].slot_id
			) {
				if (detailSchedule[i].status === '0') {
					detailSchedule.splice(i, index);
					i--;
				} else {
					detailSchedule[i].type = '2';
				}
			}
		}

		const newDetailSchedule = [...detailSchedule];
		setDetailSchedule(() => {
			return newDetailSchedule;
		});

		const showSchedule = newDetailSchedule.filter(
			(sl) =>
				sl.week_of_year === week && new Date(sl.date).getFullYear() === year
		);

		setShowDetailSchedule(() => {
			return showSchedule;
		});

		let count = 0;
		for (let i = 0; i < newDetailSchedule.length; i++) {
			if (
				schedules[index].day === newDetailSchedule[i].day_of_week &&
				schedules[index].slot === newDetailSchedule[i].slot_id
			) {
				count++;
			}
		}
		if (count === 0) {
			setErrorInput({
				...errorInput,
				schedule: '',
			});
		}

		schedules.splice(index, 1);
		const scheduleArray = [...schedules];
		setSchedule(scheduleArray);

		delete rooms[[`schedule${index}`]];

		const newRoom = { ...rooms };
		setRooms(newRoom);

		delete slots[[`schedule${index}`]];

		const newSlot = { ...slots };
		setSlots(newSlot);
	};

	// thay đổi lịch học
	// mỗi khi onChange thứ, slot hoặc room chạy hàm này
	const handleChangeSchedule = (e, index) => {
		const oldDay = schedules[index].day;
		const oldSlot = schedules[index].slot;
		const oldRoom = schedules[index].room;
		let caseNew = 0;
		if (
			schedules[index].day === -1 ||
			schedules[index].slot === '' ||
			schedules[index].room === ''
		) {
			caseNew = 1;
		}
		schedules[index][e.target.name] = e.target.value;

		if (e.target.name === 'slot') {
			// nếu đã chọn phòng học thì query tất cả các slot trống của phòng học đấy vào thứ tương ứng từ ngày khai giảng trở đi
			const getSelectRoom = async () => {
				try {
					const filter = {
						startdate: dataInput.startdate,
						slot: '',
						day: schedules[index].day,
					};
					const response = await selectApi.getRoomAddClassScreen(filter);

					setRooms({
						...rooms,
						[`schedule${index}`]: response,
					});
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			getSelectRoom();
			// nếu đã chọn slot thì hiện tất cả các phòng trống slot đó và thứ tương ứng từ ngày khai giảng trở đi
		} else if (e.target.name === 'room') {
			const getSelectSlot = async () => {
				try {
					const filter = {
						startdate: dataInput.startdate,
						room: '',
						day: schedules[index].day,
					};
					const response = await selectApi.getSlotAddClassScreen(filter);

					setSlots({
						...slots,
						[`schedule${index}`]: response,
					});
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			getSelectSlot();
		} else {
			// nếu chọn lại thứ thì reset slot, phòng tương ứng về trống
			if (e.target.value !== -1) {
				const getSelectRoom = async () => {
					try {
						const filter = {
							startdate: dataInput.startdate,
							slot: '',
							day: '',
						};
						const response = await selectApi.getRoomAddClassScreen(filter);

						setRooms({
							...rooms,
							[`schedule${index}`]: response,
						});
					} catch (error) {
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/');
						}
					}
				};

				getSelectRoom();
				const getSelectSlot = async () => {
					try {
						const filter = {
							startdate: dataInput.startdate,
							room: '',
							day: '',
						};
						const response = await selectApi.getSlotAddClassScreen(filter);

						setSlots({
							...slots,
							[`schedule${index}`]: response,
						});
					} catch (error) {
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/');
						}
					}
				};

				getSelectSlot();
			}
		}

		const scheduleArray = [...schedules];
		setSchedule(() => {
			return scheduleArray;
		});

		if (
			schedules[index].day !== -1 &&
			schedules[index].slot !== '' &&
			schedules[index].room !== ''
		) {
			for (let i = 0; i < schedules.length; i++) {
				if (i !== index) {
					if (
						schedules[index].day === schedules[i].day &&
						schedules[index].slot === schedules[i].slot
					) {
						setErrorInput({
							...errorInput,
							schedule: `Lịch học ${i + 1} bị trùng với lịch học ${index + 1}`,
						});
						return;
					}
				}
			}
			for (let i = 0; i < detailSchedule.length; i++) {
				if (
					detailSchedule[i].day_of_week !== oldDay &&
					detailSchedule[i].slot_id !== oldSlot &&
					schedules[index].day === detailSchedule[i].day_of_week &&
					schedules[index].slot === detailSchedule[i].slot_id
				) {
					setErrorInput({
						...errorInput,
						schedule: `Lịch học bị trùng`,
					});
					return;
				}
				if (
					detailSchedule[i].type === '2' &&
					schedules[index].day === detailSchedule[i].day_of_week &&
					schedules[index].slot === detailSchedule[i].slot_id
				) {
					setErrorInput({
						...errorInput,
						schedule: `Lịch học bị trùng`,
					});
					return;
				}
			}

			setErrorInput({
				...errorInput,
				schedule: '',
			});

			if (caseNew === 1) {
				// get room name
				const thisRoom = rooms[Object.keys(rooms)[index]].find(
					(r) => r.room_id === schedules[index].room
				);

				const thisTeacher = teachers.find(
					(teacher) => teacher.teacher_id === dataInput.teacher
				);
				let thisTutor = undefined;
				if (dataInput.tutor !== '') {
					thisTutor = tutors.find(
						(tutor) => tutor.tutor_id === dataInput.tutor
					);
				}

				const days = getDays1(
					dataInput.startdate,
					schedules[index].day,
					dataInput.enddate
				);
				for (let i = 0; i < days.length; i++) {
					const newSchedule = {
						schedule_id: '-1',
						date: days[i],
						day_of_week: new Date(days[i]).getDay(),
						room_id: thisRoom.room_id,
						room_name: thisRoom.room_name,
						slot_id: schedules[index].slot,
						status: '0',
						week_of_year: getDayInWeek1(days[i]),
						teacher_id: dataInput.teacher,
						type: '1',
						teacher_name:
							thisTeacher !== undefined ? thisTeacher.teacher_name : null,
						tutor_id: thisTutor !== undefined ? thisTutor.tutor_id : null,
						tutor_name: thisTutor !== undefined ? thisTutor.tutor_name : null,
					};

					detailSchedule.push(newSchedule);
				}
			} else {
				// get room name
				const thisRoom = rooms[Object.keys(rooms)[index]].find(
					(r) => r.room_id === schedules[index].room
				);

				const thisTeacher = teachers.find(
					(teacher) => teacher.teacher_id === dataInput.teacher
				);
				let thisTutor = undefined;
				if (dataInput.tutor !== '') {
					thisTutor = tutors.find(
						(tutor) => tutor.tutor_id === dataInput.tutor
					);
				}

				for (let i = 0; i < detailSchedule.length; i++) {
					if (detailSchedule[i].status === '0') {
						if (
							detailSchedule[i].day_of_week === oldDay &&
							detailSchedule[i].slot_id === oldSlot &&
							detailSchedule[i].room_id === oldRoom
						) {
							detailSchedule.splice(i, 1);
							i--;
						}
					} else {
						detailSchedule[i].type = '2';
					}
				}

				const days = getDays1(
					dataInput.startdate,
					schedules[index].day,
					dataInput.enddate
				);
				for (let i = 0; i < days.length; i++) {
					const check = detailSchedule.findIndex(
						(ds) =>
							ds.status === '1' &&
							new Date(ds.date).getTime() === new Date(days[i].getTime())
					);
					if (
						new Date(days[i]).getTime() >= new Date().getTime() &&
						check === -1
					) {
						const newSchedule = {
							schedule_id: '-1',
							date: days[i],
							day_of_week: new Date(days[i]).getDay(),
							room_id: thisRoom.room_id,
							room_name: thisRoom.room_name,
							slot_id: schedules[index].slot,
							status: '0',
							week_of_year: getDayInWeek1(days[i]),
							teacher_id: dataInput.teacher,
							type: '1',
							teacher_name:
								thisTeacher !== undefined ? thisTeacher.teacher_name : null,
							tutor_id: thisTutor !== undefined ? thisTutor.tutor_id : null,
							tutor_name: thisTutor !== undefined ? thisTutor.tutor_name : null,
						};

						detailSchedule.push(newSchedule);
					}
				}
			}

			const newDetailSchedule = [...detailSchedule];
			setDetailSchedule(() => {
				return newDetailSchedule;
			});

			const showSchedule = newDetailSchedule.filter(
				(sl) =>
					sl.week_of_year === week && new Date(sl.date).getFullYear() === year
			);

			setShowDetailSchedule(() => {
				return showSchedule;
			});
		}
	};

	// submit form
	const handleSubmitForm = (e) => {
		e.preventDefault();
		const post = async () => {
			setLoading(true);
			for (let property in errorInput) {
				if (errorInput[property] !== '') {
					setLoading(false);
					return;
				}
			}

			for (let property in errorAddSchedule) {
				if (errorAddSchedule[property] !== '') {
					setLoading(false);
					return;
				}
			}

			for (let property in errorEditSchedule) {
				if (errorEditSchedule[property] !== '') {
					setLoading(false);
					return;
				}
			}

			try {
				dataInput.price.toString().replaceAll(',', '');
				const data = {
					classInfo: dataInput,
					detailSchedules: detailSchedule,
				};

				const response = await classApi.updateClass(data, classId);
				setLoading(false);
				if (response.duplicate === true) {
					setOpen({
						...open,
						show: true,
						data: response.data,
					});
					return;
				}

				notify('success', response.message);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	};

	const handleDeleteClass = () => {
		const post = async () => {
			try {
				setLoading(true);
				const response = await classApi.deleteClass(classId);
				setLoading(false);
				notify('success', response.message);
				setOpenDeleteClass(false);
				history.push('/class');
			} catch (error) {
				setLoading(false);
				setOpenDeleteClass(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	};

	const [functions, setFunctions] = useState([]);
	useEffect(() => {
		const post = async () => {
			try {
				const params = {
					functionCode: 'function_list_class',
				};
				await permissionApi.checkPermission(params);

				setIsPermiss(true);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
	}, []);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const response = await permissionApi.getFunctions();
					const data = [];
					for (let i = 0; i < response.length; i++) {
						data.push(response[i].function_id);
					}

					if (data.includes('function_edit_class')) {
						setCanEdit(true);
					}

					if (data.includes('function_delete_class')) {
						setCanDelete(true);
					}

					setFunctions(data);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const response = await shiftApi.getSlots();
					setDefaultSlot(response);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const response = await classApi.getClassByClassId(classId);
					const numberStudent = response.studentNumber;
					const classInfo = response.classinfo;
					const scheduleData = response.schedule;
					const detailSchedule = response.detailSchedule;

					setDetailSchedule(() => {
						return detailSchedule;
					});

					const showSchedule = response.detailSchedule.filter(
						(sl) => sl.week_of_year === checkDayBelongWeek()
					);

					setShowDetailSchedule(() => {
						return showSchedule;
					});

					setDataInput({
						...dataInput,
						maxstudent: classInfo.max_student_in_class,
						startdate: classInfo.start_day,
						enddate: classInfo.end_day,
						price: classInfo.price,
						classname: classInfo.class_name,
						percent: classInfo.percent,
						subject: classInfo.subject_id,
						teacher: classInfo.teacher_id,
						tutor: classInfo.tutor_id === null ? '' : classInfo.tutor_id,
						note: classInfo.note,
						grade: classInfo.grade_level,
						status: classInfo.class_status,
						numberstudent: numberStudent,
					});

					let count = 1;
					for (let i = 0; i < scheduleData.length; i++) {
						if (scheduleData[i].type === '0') {
							const filterSlot = {
								startdate: classInfo.start_day,
								room: scheduleData[i].room_id,
								day: scheduleData[i].day_of_week,
							};

							const responseSlot = await selectApi.getSlotAddClassScreen(
								filterSlot
							);
							setSlots((prevState) => {
								return {
									...prevState,
									schedule0: responseSlot,
								};
							});

							const filterRoom = {
								startdate: classInfo.start_day,
								slot: scheduleData[i].slot_id,
								day: scheduleData[i].day_of_week,
							};

							const responseRoom = await selectApi.getRoomAddClassScreen(
								filterRoom
							);
							setRooms((prevState) => {
								return {
									...prevState,
									schedule0: responseRoom,
								};
							});

							schedules[0].day = scheduleData[i].day_of_week;
							schedules[0].slot = scheduleData[i].slot_id;
							schedules[0].room = scheduleData[i].room_id;
						} else {
							const filterSlot = {
								startdate: classInfo.start_day,
								room: scheduleData[i].room_id,
								day: scheduleData[i].day_of_week,
							};

							const responseSlot = await selectApi.getSlotAddClassScreen(
								filterSlot
							);
							setSlots((prevState) => {
								return {
									...prevState,
									[`schedule${count}`]: responseSlot,
								};
							});

							const filterRoom = {
								startdate: classInfo.start_day,
								slot: scheduleData[i].slot_id,
								day: scheduleData[i].day_of_week,
							};

							const responseRoom = await selectApi.getRoomAddClassScreen(
								filterRoom
							);
							setRooms((prevState) => {
								return {
									...prevState,
									[`schedule${count}`]: responseRoom,
								};
							});

							const newSchedule = {
								day: scheduleData[i].day_of_week,
								slot: scheduleData[i].slot_id,
								room: scheduleData[i].room_id,
								type: '1',
							};

							schedules.push(newSchedule);
							count++;
						}
					}

					const scheduleArray = [...schedules];
					setSchedule(scheduleArray);

					setIsScheduleLoad(!isScheduleLoad);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 402) {
						history.push('/class');
					}
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss]);

	// nếu chưa chọn môn thì hiện tất cả các giáo viên của trung tâm
	// đã chọn môn học thì query theo những giáo viên dạy môn học đó
	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const filter = { subject: dataInput.subject };
					const response = await selectApi.getTeacherAddClassScreen(filter);
					setTeachers(response);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, dataInput.subject]);

	// nếu chưa chọn môn thì hiện tất cả các trợ giảng của trung tâm
	// đã chọn môn học thì query theo những trợ giảng dạy môn học đó
	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const filter = { subject: dataInput.subject };
					const response = await selectApi.getTutorAddClassScreen(filter);
					setTutors(response);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, dataInput.subject]);

	// nếu chưa chọn giáo viên thì hiện tất cả các môn học trung tâm dạy
	// đã chọn giáo viên thì query theo những môn học mà giáo viên đã chọn dạy
	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const filter = { teacher: dataInput.teacher };
					const response = await selectApi.getSubjectAddClassScreen(filter);
					setSubjects(response);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, dataInput.teacher]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const filterRoom = {
						startdate: dataInput.startdate,
						slot: '',
						day: '',
					};

					const response = await selectApi.getRoomAddClassScreen(filterRoom);
					setRoomEditSchedule(() => {
						return response;
					});
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, dataInput.startdate]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const filterSlot = {
						startdate: dataInput.startdate,
						room: '',
						day: '',
					};

					const response = await selectApi.getSlotAddClassScreen(filterSlot);
					setSlotEditSchedule(() => {
						return response;
					});
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, dataInput.startdate]);

	useEffect(() => {
		if (isPermiss) {
			const get = async () => {
				try {
					const response = await studentApi.getStudentsByClassId(classId);
					setStudents(response);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};

			get();
		}
	}, [isPermiss, isStudentChange]);

	return (
		<MasterLayout referer={referer} header={dataInput.classname}>
			<Box>
				{loading && (
					<div
						style={{
							position: 'fixed',
							top: '50%',
							left: '50%',
							width: '50px',
							transform: 'translate(-50%, -50%)',
							zIndex: '1000000000',
						}}
						className='cp-spinner cp-balls'
					></div>
				)}
				<Helmet>
					<title>{`${dataInput.classname} | Lớp học | Xoài CMS`}</title>
				</Helmet>
				{open.data.length > 0 && (
					<Dialog open={open.show} onClose={handleClose}>
						<DialogTitle>Thông báo</DialogTitle>
						<DialogContent>
							<DialogContentText>
								{open.data.map((d, index) => (
									<div
										style={{ color: 'red', marginBottom: '0.75rem' }}
										key={index}
									>
										Vào {displayDayOfWeek(d.day_of_week)}, {d.slot_name}, phòng{' '}
										{d.room_name} đã có lớp{' '}
										<Link to={`/class/${d.class_id}`}>{d.class_name}</Link> của
										giáo viên {d.full_name}
									</div>
								))}
							</DialogContentText>
						</DialogContent>
						<DialogActions>
							<Button
								style={{ marginRight: '1rem' }}
								onClick={handleClose}
								color='primary'
								autoFocus
							>
								Đồng ý
							</Button>
						</DialogActions>
					</Dialog>
				)}
				<form onSubmit={handleSubmitForm}>
					<Grid container style={{ padding: '1rem 2rem 0.5rem 2rem' }}>
						<Accordion className={classes.root} defaultExpanded>
							<AccordionSummary expandIcon={<ExpandMoreIcon />}>
								<Grid container alignItems='center'>
									<InfoIcon />
									<Typography
										style={{ fontWeight: 'bold', fontSize: '1.25rem' }}
									>
										&nbsp;Thông tin lớp học
									</Typography>
								</Grid>
							</AccordionSummary>
							<AccordionDetails>
								<Grid container spacing={5}>
									<Grid item md={6}>
										<Grid container alignItems='center'>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Số học sinh:
													<span style={{ color: 'red' }}>&nbsp;*</span>
												</Grid>
												<Grid item md={2} lg={2}>
													<FormControl fullWidth>
														<TextField
															disabled
															value={dataInput.numberstudent}
															variant='outlined'
															margin='normal'
															size='small'
														/>
													</FormControl>
												</Grid>
												<Grid
													item
													md={1}
													lg={1}
													container
													justify='center'
													alignItems='center'
												>
													<Typography
														variant='body1'
														style={{ fontSize: '2rem', color: '#999' }}
													>
														/
													</Typography>
												</Grid>
												<Grid item md={2} lg={2}>
													<FormControl fullWidth error={errorInput.maxstudent}>
														<TextField
															disabled={dataInput.status === '2' || !canEdit}
															name='maxstudent'
															value={dataInput.maxstudent}
															onChange={handleDataInputChange}
															onBlur={handleMaxStudentBlur}
															variant='outlined'
															margin='normal'
															required
															error={errorInput.maxstudent}
															size='small'
															type='number'
														/>
													</FormControl>
												</Grid>
												{errorInput.maxstudent !== '' && (
													<small style={{ color: 'red' }}>
														{errorInput.maxstudent}
													</small>
												)}
											</Grid>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Tên lớp:
													<span style={{ color: 'red' }}>&nbsp;*</span>
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl fullWidth error={errorInput.classname}>
														<TextField
															disabled={dataInput.status === '2' || !canEdit}
															name='classname'
															value={dataInput.classname}
															onChange={handleDataInputChange}
															variant='outlined'
															margin='normal'
															required
															error={errorInput.classname}
															size='small'
														/>
														{errorInput.classname !== '' && (
															<small style={{ color: 'red' }}>
																{errorInput.classname}
															</small>
														)}
													</FormControl>
												</Grid>
											</Grid>

											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Môn học:
													<span style={{ color: 'red' }}>&nbsp;*</span>
												</Grid>
												<Grid item md={9} lg={9}>
													{subjects && (
														<FormControl size='small' fullWidth margin='normal'>
															<Select
																disabled={
																	dataInput.status === '1' ||
																	dataInput.status === '2' ||
																	dataInput.status === '3' ||
																	!canEdit
																}
																name='subject'
																value={dataInput.subject}
																onChange={handleDataInputChange}
																variant='outlined'
																required
															>
																<MenuItem value=''>Bỏ chọn</MenuItem>
																{subjects.map((subject, index) => (
																	<MenuItem
																		key={index}
																		value={subject.subject_id}
																	>
																		{subject.subject_name}
																	</MenuItem>
																))}
															</Select>
														</FormControl>
													)}
												</Grid>
											</Grid>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Trợ giảng:
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl size='small' fullWidth margin='normal'>
														<Select
															disabled={dataInput.status === '2' || !canEdit}
															name='tutor'
															value={dataInput.tutor}
															onChange={handleDataInputChange}
															variant='outlined'
														>
															<MenuItem value=''>Chọn trợ giảng</MenuItem>
															{tutors.map((tutor, index) => (
																<MenuItem key={index} value={tutor.tutor_id}>
																	{tutor.tutor_name}
																</MenuItem>
															))}
														</Select>
													</FormControl>
												</Grid>
											</Grid>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Lớp:
													<span style={{ color: 'red' }}>&nbsp;*</span>
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl size='small' fullWidth margin='normal'>
														<Select
															disabled={dataInput.status === '2' || !canEdit}
															required
															name='grade'
															value={dataInput.grade}
															onChange={handleDataInputChange}
															variant='outlined'
														>
															<MenuItem value=''>Chọn lớp</MenuItem>
															{grades.map((g, index) => (
																<MenuItem key={index} value={g.value}>
																	{g.display}
																</MenuItem>
															))}
														</Select>
													</FormControl>
												</Grid>
											</Grid>
										</Grid>
									</Grid>
									<Grid item md={6}>
										<Grid container>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Học phí/buổi (VNĐ):
													<span style={{ color: 'red' }}>&nbsp;*</span>
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl fullWidth error={errorInput.price}>
														<TextField
															disabled={dataInput.status === '2' || !canEdit}
															name='price'
															value={dataInput.price}
															onChange={handleDataInputChange}
															onBlur={handlePriceBlur}
															variant='outlined'
															margin='normal'
															required
															error={errorInput.price}
															size='small'
															InputProps={{
																inputComponent: NumberFormatCustom,
															}}
														/>
														{errorInput.price && (
															<small style={{ color: 'red' }}>
																{errorInput.price}
															</small>
														)}
													</FormControl>
												</Grid>
											</Grid>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Tỷ lệ hoa hồng (%):
													<span style={{ color: 'red' }}>&nbsp;*</span>
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl fullWidth error={errorInput.percent}>
														<TextField
															disabled={dataInput.status === '2' || !canEdit}
															name='percent'
															value={dataInput.percent}
															onChange={handleDataInputChange}
															onBlur={handlePercentBlur}
															type='number'
															variant='outlined'
															margin='normal'
															required
															error={errorInput.percent}
															size='small'
														/>
														{errorInput.percent && (
															<small style={{ color: 'red' }}>
																{errorInput.percent}
															</small>
														)}
													</FormControl>
												</Grid>
											</Grid>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Giáo viên:
													<span style={{ color: 'red' }}>&nbsp;*</span>
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl size='small' fullWidth margin='normal'>
														<Select
															disabled={dataInput.status === '2' || !canEdit}
															required
															name='teacher'
															value={dataInput.teacher}
															onChange={handleDataInputChange}
															variant='outlined'
														>
															<MenuItem value=''>Bỏ chọn</MenuItem>
															{teachers.map((teacher, index) => (
																<MenuItem
																	key={index}
																	value={teacher.teacher_id}
																>
																	{teacher.teacher_name}
																</MenuItem>
															))}
														</Select>
													</FormControl>
												</Grid>
											</Grid>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Ghi chú:
												</Grid>
												<Grid item md={9} lg={9}>
													<FormControl fullWidth error={errorInput.note}>
														<TextField
															disabled={dataInput.status === '2' || !canEdit}
															name='note'
															value={dataInput.note}
															onChange={handleDataInputChange}
															variant='outlined'
															margin='normal'
															error={errorInput.note}
															size='small'
														/>
														{errorInput.note && (
															<small style={{ color: 'red' }}>
																{errorInput.note}
															</small>
														)}
													</FormControl>
												</Grid>
											</Grid>
										</Grid>
									</Grid>
								</Grid>
							</AccordionDetails>
						</Accordion>
						<Accordion className={classes.root} defaultExpanded>
							<AccordionSummary expandIcon={<ExpandMoreIcon />}>
								<Grid container alignItems='center'>
									<EventIcon />
									<Typography
										style={{ fontWeight: 'bold', fontSize: '1.25rem' }}
									>
										&nbsp;Lịch học
									</Typography>
								</Grid>
							</AccordionSummary>
							<AccordionDetails>
								<Grid container>
									<Grid container spacing={5}>
										<Grid item md={6}>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Ngày khai giảng:
													<span style={{ color: 'red' }}>&nbsp;*</span>
												</Grid>
												<Grid item md={9} lg={9}>
													{dataInput.startdate && (
														<FormControl fullWidth error={errorInput.startdate}>
															<TextField
																disabled={
																	dataInput.status === '2' ||
																	dataInput.status === '1' ||
																	!canEdit
																}
																name='startdate'
																value={formatDate(dataInput.startdate)}
																onChange={handleDataInputChange}
																onBlur={handleStartdateBlur}
																type='date'
																variant='outlined'
																margin='normal'
																required
																error={errorInput.startdate}
																size='small'
															/>
															{errorInput.startdate !== '' && (
																<small style={{ color: 'red' }}>
																	{errorInput.startdate}
																</small>
															)}
														</FormControl>
													)}
												</Grid>
											</Grid>
										</Grid>
										<Grid item md={6}>
											<Grid container alignItems='center'>
												<Grid item md={3} lg={3}>
													Ngày kết thúc:
													<span style={{ color: 'red' }}>&nbsp;*</span>
												</Grid>
												<Grid item md={9} lg={9}>
													{dataInput.enddate && (
														<FormControl fullWidth error={errorInput.enddate}>
															<TextField
																disabled={dataInput.status === '2' || !canEdit}
																name='enddate'
																value={formatDate(dataInput.enddate)}
																onChange={handleDataInputChange}
																onBlur={handleEnddateBlur}
																type='date'
																variant='outlined'
																margin='normal'
																required
																error={errorInput.enddate}
																size='small'
															/>
															{errorInput.enddate && (
																<small style={{ color: 'red' }}>
																	{errorInput.enddate}
																</small>
															)}
														</FormControl>
													)}
												</Grid>
											</Grid>
										</Grid>
									</Grid>

									<Grid
										container
										justify='flex-end'
										style={{ margin: '1rem 0' }}
									>
										<Dialog open={open2} onClose={handleClose2}>
											<DialogTitle>
												<Grid container>
													<Grid item md={6}>
														{'Thay đổi buổi học'}
													</Grid>
													<Grid item md={6} container justifyContent='flex-end'>
														<Button
															onClick={handleOpenDelete}
															variant='contained'
															color='secondary'
														>
															Xóa buổi học
														</Button>
														<Dialog
															open={openDelete}
															onClose={handleCloseDelete}
														>
															<Grid container>
																<Grid item xs={9} md={9} lg={9}>
																	<DialogTitle>Xóa buổi học</DialogTitle>
																</Grid>
																<Grid
																	item
																	container
																	justify='flex-end'
																	xs={3}
																	md={3}
																	lg={3}
																>
																	<IconButton onClick={handleCloseDelete}>
																		<CloseIcon />
																	</IconButton>
																</Grid>
															</Grid>
															<Divider />
															<DialogContent>
																<DialogContentText>
																	Bạn có chắc chắc muốn xóa buổi học này không ?
																</DialogContentText>
															</DialogContent>
															<DialogActions>
																<Button
																	variant='contained'
																	onClick={handleCloseDelete}
																>
																	Hủy
																</Button>
																<Button
																	variant='contained'
																	color='secondary'
																	onClick={handleDeleteDayScheduleDone}
																>
																	Xóa
																</Button>
															</DialogActions>
														</Dialog>
													</Grid>
												</Grid>
											</DialogTitle>
											<DialogContent>
												<Grid container>
													<Grid container>
														{errorEditSchedule.total2 && (
															<Typography
																variant='body2'
																style={{
																	color: 'red',
																	margin: '0.5rem',
																}}
															>
																{errorEditSchedule.total2}
															</Typography>
														)}
													</Grid>
													<Grid item md={5}>
														<Grid container>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	<Typography>Ngày: </Typography>
																</Grid>
																<Grid item md={9}>
																	<FormControl fullWidth>
																		<TextField
																			value={dataEditSchedule.oldDate}
																			disabled
																			variant='outlined'
																			margin='dense'
																			size='small'
																			type='date'
																		/>
																	</FormControl>
																</Grid>
															</Grid>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	<Typography>Slot: </Typography>
																</Grid>
																<Grid item md={9}>
																	<FormControl fullWidth>
																		<TextField
																			value={dataEditSchedule.oldSlot.display}
																			disabled
																			variant='outlined'
																			margin='dense'
																			size='small'
																		/>
																	</FormControl>
																</Grid>
															</Grid>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	<Typography>Phòng: </Typography>
																</Grid>
																<Grid item md={9}>
																	<FormControl fullWidth>
																		<TextField
																			value={dataEditSchedule.oldRoom.display}
																			disabled
																			variant='outlined'
																			margin='dense'
																			size='small'
																		/>
																	</FormControl>
																</Grid>
															</Grid>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	<Typography>GV: </Typography>
																</Grid>
																<Grid item md={9}>
																	<FormControl fullWidth>
																		<TextField
																			value={dataEditSchedule.oldTeacher}
																			disabled
																			variant='outlined'
																			margin='dense'
																			size='small'
																		/>
																	</FormControl>
																</Grid>
															</Grid>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	<Typography>TG: </Typography>
																</Grid>
																<Grid item md={9}>
																	<FormControl fullWidth>
																		<TextField
																			value={dataEditSchedule.oldTutor}
																			disabled
																			variant='outlined'
																			margin='dense'
																			size='small'
																		/>
																	</FormControl>
																</Grid>
															</Grid>
														</Grid>
													</Grid>
													<Grid
														item
														md={2}
														container
														alignItems='center'
														justify='center'
													>
														<ArrowRightAltIcon />
													</Grid>
													<Grid item md={5}>
														<Grid container>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	<Typography>Ngày: </Typography>
																</Grid>
																<Grid item md={9}>
																	<FormControl fullWidth>
																		<TextField
																			name='newDate'
																			value={dataEditSchedule.newDate}
																			variant='outlined'
																			margin='dense'
																			size='small'
																			type='date'
																			onChange={handleEditScheduleChange}
																		/>
																		{errorEditSchedule.newDate !== '' && (
																			<small style={{ color: 'red' }}>
																				{errorAddSchedule.date}
																			</small>
																		)}
																	</FormControl>
																</Grid>
															</Grid>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	<Typography>Slot: </Typography>
																</Grid>
																<Grid item md={9}>
																	<FormControl
																		size='small'
																		fullWidth
																		margin='dense'
																	>
																		<Select
																			name='newSlot'
																			required
																			value={dataEditSchedule.newSlot}
																			variant='outlined'
																			onChange={handleEditScheduleChange}
																		>
																			<MenuItem value=''>Bỏ chọn</MenuItem>
																			{slotEditSchedule &&
																				slotEditSchedule.map((slot, index) => (
																					<MenuItem
																						key={index}
																						value={slot.slot_id}
																					>
																						{slot.slot_name}
																					</MenuItem>
																				))}
																		</Select>
																	</FormControl>
																</Grid>
															</Grid>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	<Typography>Phòng: </Typography>
																</Grid>
																<Grid item md={9}>
																	<FormControl
																		size='small'
																		fullWidth
																		margin='dense'
																	>
																		<Select
																			name='newRoom'
																			required
																			value={dataEditSchedule.newRoom}
																			variant='outlined'
																			onChange={handleEditScheduleChange}
																		>
																			<MenuItem value=''>Bỏ chọn</MenuItem>
																			{roomEditSchedule &&
																				roomEditSchedule.map((room, index) => (
																					<MenuItem
																						key={index}
																						value={room.room_id}
																					>
																						{room.room_name}
																					</MenuItem>
																				))}
																		</Select>
																	</FormControl>
																</Grid>
															</Grid>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	<Typography>GV: </Typography>
																</Grid>
																<Grid item md={9}>
																	<FormControl
																		size='small'
																		fullWidth
																		margin='dense'
																	>
																		<Select
																			name='newTeacher'
																			required
																			value={dataEditSchedule.newTeacher}
																			onChange={handleEditScheduleChange}
																			variant='outlined'
																		>
																			<MenuItem value=''>Bỏ chọn</MenuItem>
																			{teachers.map((teacher, index) => (
																				<MenuItem
																					key={index}
																					value={teacher.teacher_id}
																				>
																					{teacher.teacher_name}
																				</MenuItem>
																			))}
																		</Select>
																	</FormControl>
																</Grid>
															</Grid>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	<Typography>TG: </Typography>
																</Grid>
																<Grid item md={9}>
																	<FormControl
																		size='small'
																		fullWidth
																		margin='dense'
																	>
																		<Select
																			name='newTutor'
																			required
																			value={dataEditSchedule.newTutor}
																			onChange={handleEditScheduleChange}
																			variant='outlined'
																		>
																			<MenuItem value=''>Bỏ chọn</MenuItem>
																			{tutors.map((tutor, index) => (
																				<MenuItem
																					key={index}
																					value={tutor.tutor_id}
																				>
																					{tutor.tutor_name}
																				</MenuItem>
																			))}
																		</Select>
																	</FormControl>
																</Grid>
															</Grid>
															<Grid container>
																{errorEditSchedule.total1 && (
																	<Typography
																		variant='body2'
																		style={{
																			color: 'red',
																			marginTop: '0.5rem',
																		}}
																	>
																		{errorEditSchedule.total1}
																	</Typography>
																)}
															</Grid>
														</Grid>
													</Grid>
												</Grid>
											</DialogContent>
											<DialogActions style={{ marginTop: '1rem' }}>
												<Button
													style={{ marginRight: '0.25rem' }}
													variant='contained'
													onClick={handleClose2}
												>
													Hủy
												</Button>
												<Button
													style={{ marginRight: '1rem' }}
													onClick={handleEditScheduleDone}
													variant='contained'
													color='primary'
												>
													Thay đổi
												</Button>
											</DialogActions>
										</Dialog>
										<Box>
											{dataInput.status !== '2' && canEdit && (
												<Button
													startIcon={<AddIcon />}
													variant='outlined'
													onClick={handleOpen1}
												>
													Thêm buổi học
												</Button>
											)}
											<Dialog open={open1} onClose={handleClose1}>
												<DialogTitle>{'Thêm buổi học'}</DialogTitle>
												<DialogContent>
													<Grid container>
														{errorAddSchedule.total !== '' && (
															<Typography
																variant='body2'
																style={{ color: 'red' }}
															>
																{errorAddSchedule.total}
															</Typography>
														)}
														<Grid container alignItems='center'>
															<Grid item md={3}>
																<Typography>Ngày: </Typography>
															</Grid>
															<Grid item md={9}>
																<FormControl fullWidth>
																	<TextField
																		disabled={
																			dataInput.status === '2' || !canEdit
																		}
																		name='date'
																		value={dataAddSchedule.date}
																		variant='outlined'
																		margin='dense'
																		size='small'
																		type='date'
																		onChange={handleDateAddScheduleChange}
																		onBlur={handleDateAddDialogBlur}
																	/>
																	{errorAddSchedule.date !== '' && (
																		<small style={{ color: 'red' }}>
																			{errorAddSchedule.date}
																		</small>
																	)}
																</FormControl>
															</Grid>
														</Grid>
														<Grid container alignItems='center'>
															<Grid item md={3}>
																<Typography>Slot: </Typography>
															</Grid>
															<Grid item md={9}>
																<FormControl
																	size='small'
																	fullWidth
																	margin='dense'
																>
																	<Select
																		required
																		disabled={
																			dataAddSchedule.date === '' ||
																			dataInput.status === '2' ||
																			!canEdit
																		}
																		value={dataAddSchedule.slot}
																		variant='outlined'
																		onChange={(e) =>
																			handleDataAddScheduleChange(e, 'slot')
																		}
																	>
																		<MenuItem value=''>Bỏ chọn</MenuItem>
																		{slotAddSchedule &&
																			slotAddSchedule.map((slot, index) => (
																				<MenuItem
																					key={index}
																					value={slot.slot_id}
																				>
																					{slot.slot_name}
																				</MenuItem>
																			))}
																	</Select>
																</FormControl>
															</Grid>
														</Grid>
														<Grid container alignItems='center'>
															<Grid item md={3}>
																<Typography>Phòng: </Typography>
															</Grid>
															<Grid item md={9}>
																<FormControl
																	size='small'
																	fullWidth
																	margin='dense'
																>
																	<Select
																		required
																		disabled={
																			dataAddSchedule.date === '' ||
																			dataInput.status === '2' ||
																			!canEdit
																		}
																		value={dataAddSchedule.room}
																		variant='outlined'
																		onChange={(e) =>
																			handleDataAddScheduleChange(e, 'room')
																		}
																	>
																		<MenuItem value=''>Bỏ chọn</MenuItem>
																		{roomAddSchedule &&
																			roomAddSchedule.map((room, index) => (
																				<MenuItem
																					key={index}
																					value={room.room_id}
																				>
																					{room.room_name}
																				</MenuItem>
																			))}
																	</Select>
																</FormControl>
															</Grid>
														</Grid>
														<Grid container alignItems='center'>
															<Grid item md={3}>
																<Typography>Giáo viên: </Typography>
															</Grid>
															<Grid item md={9}>
																<FormControl
																	size='small'
																	fullWidth
																	margin='dense'
																>
																	<Select
																		disabled={
																			dataInput.status === '2' || !canEdit
																		}
																		required
																		value={dataAddSchedule.teacher}
																		onChange={(e) =>
																			handleDataAddScheduleChange(e, 'teacher')
																		}
																		variant='outlined'
																	>
																		<MenuItem value=''>Bỏ chọn</MenuItem>
																		{teachers.map((teacher, index) => (
																			<MenuItem
																				key={index}
																				value={teacher.teacher_id}
																			>
																				{teacher.teacher_name}
																			</MenuItem>
																		))}
																	</Select>
																</FormControl>
															</Grid>
														</Grid>
														<Grid container alignItems='center'>
															<Grid item md={3}>
																<Typography>Trợ giảng: </Typography>
															</Grid>
															<Grid item md={9}>
																<FormControl
																	size='small'
																	fullWidth
																	margin='dense'
																>
																	<Select
																		disabled={
																			dataInput.status === '2' || !canEdit
																		}
																		required
																		value={dataAddSchedule.tutor}
																		onChange={(e) =>
																			handleDataAddScheduleChange(e, 'tutor')
																		}
																		variant='outlined'
																	>
																		<MenuItem value=''>Bỏ chọn</MenuItem>
																		{tutors.map((tutor, index) => (
																			<MenuItem
																				key={index}
																				value={tutor.tutor_id}
																			>
																				{tutor.tutor_name}
																			</MenuItem>
																		))}
																	</Select>
																</FormControl>
															</Grid>
														</Grid>
													</Grid>
												</DialogContent>
												<DialogActions style={{ marginTop: '1rem' }}>
													<Button variant='contained' onClick={handleClose1}>
														Hủy
													</Button>
													<Button
														disabled={dataInput.status === '2' || !canEdit}
														style={{ marginRight: '1rem' }}
														onClick={handleAddScheduleDone}
														variant='contained'
														color='primary'
														autoFocus
													>
														Đồng ý
													</Button>
												</DialogActions>
											</Dialog>
										</Box>
									</Grid>
									<Grid container style={{ marginBottom: '2rem' }}>
										<TableContainer component={Paper}>
											<Table>
												<TableHead>
													<TableRow>
														<TableCell
															style={{
																padding: '0.25rem',
																width: '250px',
															}}
															rowSpan={2}
														>
															<Grid container>
																<Grid
																	container
																	alignItems='center'
																	style={{ marginBottom: '0.25rem' }}
																>
																	<Typography>Năm:&nbsp;&nbsp;</Typography>
																	<FormControl size='small'>
																		<Select
																			variant='outlined'
																			value={year}
																			onChange={handleYearChange}
																		>
																			<MenuItem
																				value={new Date().getFullYear() - 2}
																			>
																				{new Date().getFullYear() - 2}
																			</MenuItem>
																			<MenuItem
																				value={new Date().getFullYear() - 1}
																			>
																				{new Date().getFullYear() - 1}
																			</MenuItem>
																			<MenuItem
																				value={new Date().getFullYear()}
																			>
																				{new Date().getFullYear()}
																			</MenuItem>
																			<MenuItem
																				value={new Date().getFullYear() + 1}
																			>
																				{new Date().getFullYear() + 1}
																			</MenuItem>
																		</Select>
																	</FormControl>
																</Grid>
																<Grid container alignItems='center'>
																	<Typography>Tuần:&nbsp;&nbsp;</Typography>
																	<FormControl size='small'>
																		<Select
																			variant='outlined'
																			value={week}
																			onChange={handleWeekChange}
																		>
																			{getWeeks().map((week, index) => (
																				<MenuItem
																					key={index}
																					value={week.value}
																				>
																					{week.display}
																				</MenuItem>
																			))}
																		</Select>
																	</FormControl>
																</Grid>
															</Grid>
														</TableCell>
														<TableCell
															style={{ padding: '0.25rem' }}
															align='center'
														>
															Thứ 2
														</TableCell>
														<TableCell
															style={{ padding: '0.25rem' }}
															align='center'
														>
															Thứ 3
														</TableCell>
														<TableCell
															style={{ padding: '0.25rem' }}
															align='center'
														>
															Thứ 4
														</TableCell>
														<TableCell
															style={{ padding: '0.25rem' }}
															align='center'
														>
															Thứ 5
														</TableCell>
														<TableCell
															style={{ padding: '0.25rem' }}
															align='center'
														>
															Thứ 6
														</TableCell>
														<TableCell
															style={{ padding: '0.25rem' }}
															align='center'
														>
															Thứ 7
														</TableCell>
														<TableCell
															style={{ padding: '0.25rem' }}
															align='center'
														>
															CN
														</TableCell>
													</TableRow>
													<TableRow>
														{displayThisWeek.map((w, index) => (
															<TableCell
																key={index}
																style={{ padding: '0.25rem' }}
																align='center'
															>
																{w}
															</TableCell>
														))}
													</TableRow>
												</TableHead>
												<TableBody>
													{defaultSlot.map((sl, index) => (
														<TableRow key={index}>
															<TableCell component='th' scope='row'>
																{sl.start_time} - {sl.end_time}
															</TableCell>
															<TableCell
																style={{
																	cursor: checkCursor(1, sl.slot_id)
																		? 'pointer'
																		: '',
																}}
																align='center'
																onClick={() =>
																	handleOpenChangeScheduleDialog(1, sl.slot_id)
																}
															>
																{showInfoScheduleDay(
																	showDetailSchedule,
																	1,
																	sl.slot_id
																)}
															</TableCell>
															<TableCell
																style={{
																	cursor: checkCursor(2, sl.slot_id)
																		? 'pointer'
																		: '',
																}}
																align='center'
																onClick={() =>
																	handleOpenChangeScheduleDialog(2, sl.slot_id)
																}
																align='center'
															>
																<Typography variant='caption'>
																	{showInfoScheduleDay(
																		showDetailSchedule,
																		2,
																		sl.slot_id
																	)}
																</Typography>
															</TableCell>
															<TableCell
																style={{
																	cursor: checkCursor(3, sl.slot_id)
																		? 'pointer'
																		: '',
																}}
																align='center'
																onClick={() =>
																	handleOpenChangeScheduleDialog(3, sl.slot_id)
																}
																align='center'
															>
																<Typography variant='caption'>
																	{showInfoScheduleDay(
																		showDetailSchedule,
																		3,
																		sl.slot_id
																	)}
																</Typography>
															</TableCell>
															<TableCell
																style={{
																	cursor: checkCursor(4, sl.slot_id)
																		? 'pointer'
																		: '',
																}}
																align='center'
																onClick={() =>
																	handleOpenChangeScheduleDialog(4, sl.slot_id)
																}
																align='center'
															>
																<Typography variant='caption'>
																	{showInfoScheduleDay(
																		showDetailSchedule,
																		4,
																		sl.slot_id
																	)}
																</Typography>
															</TableCell>
															<TableCell
																style={{
																	cursor: checkCursor(5, sl.slot_id)
																		? 'pointer'
																		: '',
																}}
																align='center'
																onClick={() =>
																	handleOpenChangeScheduleDialog(5, sl.slot_id)
																}
																align='center'
															>
																<Typography variant='caption'>
																	{showInfoScheduleDay(
																		showDetailSchedule,
																		5,
																		sl.slot_id
																	)}
																</Typography>
															</TableCell>
															<TableCell
																style={{
																	cursor: checkCursor(6, sl.slot_id)
																		? 'pointer'
																		: '',
																}}
																align='center'
																onClick={() =>
																	handleOpenChangeScheduleDialog(6, sl.slot_id)
																}
																align='center'
															>
																<Typography variant='caption'>
																	{showInfoScheduleDay(
																		showDetailSchedule,
																		6,
																		sl.slot_id
																	)}
																</Typography>
															</TableCell>
															<TableCell
																style={{
																	cursor: checkCursor(0, sl.slot_id)
																		? 'pointer'
																		: '',
																}}
																align='center'
																onClick={() =>
																	handleOpenChangeScheduleDialog(0, sl.slot_id)
																}
																align='center'
															>
																{showInfoScheduleDay(
																	showDetailSchedule,
																	0,
																	sl.slot_id
																)}
															</TableCell>
														</TableRow>
													))}
												</TableBody>
											</Table>
										</TableContainer>
									</Grid>
									<Grid container style={{ marginBottom: '1rem' }}>
										<Grid container alignItems='center'>
											{dataInput.status !== '2' && canEdit && (
												<Button
													variant='outlined'
													startIcon={<AddIcon />}
													onClick={handleAddMoreSchedule}
													style={{ marginRight: '2rem' }}
												>
													Thêm lịch học
												</Button>
											)}
											{errorInput.schedule && (
												<Typography variant='body2' style={{ color: 'red' }}>
													{errorInput.schedule}
												</Typography>
											)}
										</Grid>
									</Grid>
									{isScheduleLoad && (
										<Grid container spacing={5}>
											<Grid item md={1}></Grid>
											<Grid item md={2}>
												<Grid container alignItems='center'>
													<Grid item md={3}>
														Thứ:
													</Grid>
													<Grid item md={9}>
														<TextField
															disabled
															variant='outlined'
															size='small'
															margin='normal'
															value={getDayByDate1(dataInput.startdate)}
														/>
													</Grid>
												</Grid>
											</Grid>
											<Grid item md={2}>
												<Grid container alignItems='center'>
													<Grid item md={3}>
														Slot:
													</Grid>
													<Grid item md={9}>
														<FormControl
															style={{
																padding: '1rem 0',
															}}
															size='small'
															fullWidth
														>
															<Select
																required
																name='slot'
																disabled={
																	schedules[0].day === -1 ||
																	dataInput.status === '2' ||
																	!canEdit
																}
																value={schedules[0].slot}
																onChange={(e) => handleChangeSchedule(e, 0)}
																variant='outlined'
															>
																<MenuItem value=''>Bỏ chọn</MenuItem>
																{slots.schedule0.map((slot, index) => (
																	<MenuItem key={index} value={slot.slot_id}>
																		{slot.slot_name}
																	</MenuItem>
																))}
															</Select>
														</FormControl>
													</Grid>
												</Grid>
											</Grid>
											<Grid item md={3}>
												<Grid container alignItems='center'>
													<Grid item md={5}>
														Phòng học:
													</Grid>
													<Grid item md={7}>
														<FormControl
															style={{
																padding: '1rem 0',
															}}
															size='small'
															fullWidth
														>
															<Select
																required
																name='room'
																disabled={
																	schedules[0].day === -1 ||
																	dataInput.status === '2' ||
																	!canEdit
																}
																value={schedules[0].room}
																variant='outlined'
																onChange={(e) => handleChangeSchedule(e, 0)}
															>
																<MenuItem value=''>Bỏ chọn</MenuItem>
																{rooms.schedule0.map((room, index) => (
																	<MenuItem key={index} value={room.room_id}>
																		{room.room_name}
																	</MenuItem>
																))}
															</Select>
														</FormControl>
													</Grid>
												</Grid>
											</Grid>
										</Grid>
									)}
									{schedules.length > 1 &&
										isScheduleLoad &&
										schedules.map(
											(schedule, index) =>
												index !== 0 &&
												schedule.type !== '2' && (
													<Grid key={index} container spacing={5}>
														<Grid item md={1}></Grid>
														<Grid item md={2}>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	Thứ:
																</Grid>
																<Grid item md={9}>
																	<FormControl
																		style={{
																			padding: '1rem 0',
																		}}
																		size='small'
																		fullWidth
																	>
																		<Select
																			disabled={
																				dataInput.status === '2' || !canEdit
																			}
																			required
																			name='day'
																			value={schedule.day}
																			variant='outlined'
																			onChange={(e) =>
																				handleChangeSchedule(e, index)
																			}
																		>
																			<MenuItem value={-1}>Bỏ chọn</MenuItem>
																			{days.map((day, index) => (
																				<MenuItem key={index} value={day.value}>
																					{day.display}
																				</MenuItem>
																			))}
																		</Select>
																	</FormControl>
																</Grid>
															</Grid>
														</Grid>
														<Grid item md={2}>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	Slot:
																</Grid>
																<Grid item md={9}>
																	<FormControl
																		style={{
																			padding: '1rem 0',
																		}}
																		size='small'
																		fullWidth
																	>
																		<Select
																			required
																			name='slot'
																			disabled={
																				schedule.day === -1 ||
																				dataInput.status === '2' ||
																				!canEdit
																			}
																			value={schedule.slot}
																			variant='outlined'
																			onChange={(e) =>
																				handleChangeSchedule(e, index)
																			}
																		>
																			<MenuItem value=''>Bỏ chọn</MenuItem>
																			{slots[Object.keys(slots)[index]] &&
																				slots[Object.keys(slots)[index]].map(
																					(slot, index) => (
																						<MenuItem
																							key={index}
																							value={slot.slot_id}
																						>
																							{slot.slot_name}
																						</MenuItem>
																					)
																				)}
																		</Select>
																	</FormControl>
																</Grid>
															</Grid>
														</Grid>
														<Grid item md={3}>
															<Grid container alignItems='center'>
																<Grid item md={5}>
																	Phòng học:
																</Grid>
																<Grid item md={7}>
																	<FormControl
																		style={{
																			padding: '1rem 0',
																		}}
																		size='small'
																		fullWidth
																	>
																		<Select
																			required
																			name='room'
																			disabled={
																				schedule.day === -1 ||
																				dataInput.status === '2' ||
																				!canEdit
																			}
																			value={schedule.room}
																			variant='outlined'
																			onChange={(e) =>
																				handleChangeSchedule(e, index)
																			}
																		>
																			<MenuItem value=''>Bỏ chọn</MenuItem>
																			{rooms[Object.keys(rooms)[index]] &&
																				rooms[Object.keys(rooms)[index]].map(
																					(room, index) => (
																						<MenuItem
																							key={index}
																							value={room.room_id}
																						>
																							{room.room_name}
																						</MenuItem>
																					)
																				)}
																		</Select>
																	</FormControl>
																</Grid>
															</Grid>
														</Grid>
														<Grid item md={1} container alignItems='center'>
															{dataInput.status !== '2' && canEdit && (
																<MyDialog1
																	title={`Xóa`}
																	description='Bạn có chắc chắn muốn xóa lịch học này không ?'
																	doneText='Xóa'
																	cancelText='Hủy'
																	colorDone='secondary'
																	action={() => handleDeleteSchedule(index)}
																/>
															)}
														</Grid>
													</Grid>
												)
										)}
								</Grid>
							</AccordionDetails>
						</Accordion>
						<Accordion className={classes.root} defaultExpanded>
							<AccordionSummary expandIcon={<ExpandMoreIcon />}>
								<Grid container alignItems='center'>
									<GroupIcon />
									<Typography
										style={{ fontWeight: 'bold', fontSize: '1.25rem' }}
									>
										&nbsp;Danh sách học sinh
									</Typography>
								</Grid>
							</AccordionSummary>
							<AccordionDetails>
								<Grid container>
									<Grid item md={6}>
										<Button
											variant='outlined'
											startIcon={<AssignmentTurnedInIcon />}
											onClick={() => {
												history.push({
													pathname: `/class/${classId}/attendancehistory`,
													state: {
														classname: dataInput.classname,
													},
												});
											}}
										>
											Xem lịch sử điểm danh
										</Button>
									</Grid>
									<Grid item md={6}>
										<Grid
											container
											style={{ marginBottom: '1rem' }}
											justify='flex-end'
										>
											<Box>
												{dataInput.status !== '2' && canEdit && (
													<Button
														onClick={handleOpenAddStudent}
														startIcon={<AddIcon />}
														variant='outlined'
													>
														Thêm học sinh
													</Button>
												)}
												<Dialog
													open={openAddStudent}
													onClose={handleCloseAddStudent}
												>
													<DialogTitle>
														<Grid container>
															<Grid item md={6}>
																{'Thêm học sinh'}
															</Grid>
															<Grid item md={6} container justify='flex-end'>
																<Button
																	startIcon={<AddIcon />}
																	variant='outlined'
																	onClick={() => {
																		window.open(`/create/student`);
																	}}
																>
																	Tạo mới
																</Button>
															</Grid>
														</Grid>
													</DialogTitle>
													<DialogContent>
														<Grid container>
															<Grid container>
																{errorAddStudent !== '' && (
																	<Typography
																		variant='body2'
																		style={{ color: 'red' }}
																	>
																		{errorAddStudent}
																	</Typography>
																)}
															</Grid>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	<Typography>Mã học sinh: </Typography>
																</Grid>
																<Grid item md={9}>
																	<FormControl fullWidth>
																		<TextField
																			disabled={
																				dataInput.status === '2' || !canEdit
																			}
																			name='studentId'
																			value={addStudentData.studentId}
																			variant='outlined'
																			margin='dense'
																			size='small'
																			onChange={handleStudentIdChange}
																			onBlur={handleStudentIdBlur}
																		/>
																	</FormControl>
																</Grid>
															</Grid>
															<Grid container alignItems='center'>
																<Grid item md={3}>
																	<Typography>Họ và tên: </Typography>
																</Grid>
																<Grid item md={9}>
																	<FormControl fullWidth>
																		<TextField
																			disabled
																			value={addStudentData.studentName}
																			variant='outlined'
																			margin='dense'
																			size='small'
																		/>
																	</FormControl>
																</Grid>
															</Grid>
														</Grid>
													</DialogContent>
													<DialogActions style={{ marginTop: '1rem' }}>
														<Button
															variant='contained'
															onClick={handleCloseAddStudent}
														>
															Hủy
														</Button>
														<Button
															disabled={dataInput.status === '2' || !canEdit}
															style={{ marginRight: '1rem' }}
															variant='contained'
															color='primary'
															autoFocus
															onClick={handleAddStudentDone}
														>
															Thêm
														</Button>
													</DialogActions>
												</Dialog>
											</Box>
										</Grid>
									</Grid>

									<Grid container>
										<TableContainer component={Paper}>
											<Table>
												<TableHead>
													<TableRow>
														<TableCell align='center'>STT</TableCell>
														<TableCell align='center'>Ảnh đại diện</TableCell>
														<TableCell align='left'>Mã học sinh</TableCell>
														<TableCell align='left'>Họ và tên</TableCell>
														{dataInput.status !== '2' && (
															<TableCell align='center'></TableCell>
														)}
													</TableRow>
												</TableHead>
												<TableBody>
													{students.length > 0 ? (
														students.map((student, index) => (
															<TableRow>
																<TableCell align='center'>
																	{index + 1}
																</TableCell>
																<TableCell align='center'>
																	<Grid container justify='center'>
																		<Avatar src={student.avt_link} alt='a' />
																	</Grid>
																</TableCell>
																<TableCell align='left'>
																	{student.user_id}
																</TableCell>
																<TableCell align='left'>
																	{student.full_name}
																</TableCell>
																{dataInput.status !== '2' && canEdit && (
																	<TableCell align='center'>
																		<MyDialog
																			title={`Xóa`}
																			description='Bạn có chắc chắn xóa học sinh này khỏi lớp học ?'
																			doneText='Xóa'
																			cancelText='Hủy'
																			colorDone='secondary'
																			iconType={'0'}
																			action={() =>
																				handleDeleteStudentInClass(
																					student.user_id
																				)
																			}
																		/>
																	</TableCell>
																)}
															</TableRow>
														))
													) : (
														<TableRow>
															<TableCell align='center' colSpan={5}>
																Không tìm thấy dữ liệu
															</TableCell>
														</TableRow>
													)}
												</TableBody>
											</Table>
										</TableContainer>
									</Grid>
								</Grid>
							</AccordionDetails>
						</Accordion>
						<Grid container className={classes.postPanel} justify='flex-end'>
							{dataInput.status !== '2' && canEdit && (
								<Button
									type='submit'
									style={{
										padding: '0.5rem 1.5rem',
										marginRight: '0.5rem',
									}}
									variant='contained'
									color='primary'
								>
									Cập nhập
								</Button>
							)}
							{dataInput.status !== '2' && canDelete && (
								<Button
									style={{
										padding: '0.5rem 1.5rem',
									}}
									variant='contained'
									color='secondary'
									onClick={handleClickOpenDeleteClass}
								>
									Xóa lớp học
								</Button>
							)}
							<Dialog open={openDeleteClass} onClose={handleCloseDeleteClass}>
								<DialogTitle>{'Xóa lớp học'}</DialogTitle>
								<DialogContent>
									<DialogContentText>
										Bạn có chắc chắn muốn xóa lớp học này không?
									</DialogContentText>
								</DialogContent>
								<DialogActions>
									<Button onClick={handleCloseDeleteClass} variant='contained'>
										Hủy
									</Button>
									<Button
										onClick={handleDeleteClass}
										variant='contained'
										color='secondary'
										autoFocus
									>
										Xóa
									</Button>
								</DialogActions>
							</Dialog>
						</Grid>
					</Grid>
				</form>
			</Box>
		</MasterLayout>
	);
}

export default ClassDetail;
