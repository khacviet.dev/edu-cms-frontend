import { createTheme } from '@material-ui/core/styles';

export const theme = createTheme({
	palette: {
		primary: {
			main: '#2C323D',
		},

		type: 'light',
	},
	overrides: {
		MuiPaper: {
			root: {
				boxShadow: '0 1px 4px 0 rgb(0 0 0 / 14%) !important',
			},
		},
		MuiListItemIcon: {
			root: {
				color: '#ccc',
			},
		},
	},
});
