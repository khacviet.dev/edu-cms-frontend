import userApi from 'api/userApi';
import AdminDashboard from 'features/Admin/pages/AdminDashboard';
import MarketerDashboard from 'features/Maketing/pages/MarketerDashboard';
import ManagerDashboard from 'features/Manager/pages/ManagerDashboard';
import ManagerPostDashboard from 'features/Manager/pages/ManagerPostDashboard';
import StudentClassList from 'features/Student/pages/StudentClassList';
import TeacherDashboard from 'features/Teacher/pages/TeacherDashboard';
import TutorDashboard from 'features/Tutor/pages/TutorDashboard';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import MasterLayout from '../MasterLayout';

const showDashboardByRole = (role) => {
	switch (role) {
		case 1:
			return <AdminDashboard />;
		case 2:
			return <ManagerDashboard />;
		case 3:
			return <TeacherDashboard />;
		case 5:
			return <StudentClassList />;
		case 4:
			return <TutorDashboard />;
		case 6:
			return <MarketerDashboard />;
		case 7:
			return <ManagerPostDashboard />;
		default:
			return (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			);
	}
};

export default function DashBoard() {
	const notify = useContext(ToastifyContext);
	const [role, setRole] = useState(-1);
	let history = useHistory();

	useEffect(() => {
		const get = async () => {
			try {
				const response = await userApi.checkRoleByUser();
				setRole(parseInt(response));
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		get();
	}, []);

	return (
		<MasterLayout>
			<Helmet>
				<title>Xoài CMS</title>
			</Helmet>
			{showDashboardByRole(role)}
		</MasterLayout>
	);
}
