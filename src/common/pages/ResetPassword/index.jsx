import { CircularProgress } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import authApi from 'api/authApi';
import InputField from 'custom-fields/InputField';
import { FastField, Form, Formik } from 'formik';
import React, { useContext, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as Yup from 'yup';
import { useStyles } from './ResetPasswordStyle';

export default function ResetPassword() {
	const notify = useContext(ToastifyContext);
	const classes = useStyles();
	let history = useHistory();

	const [loading, setLoading] = useState(false);

	const backToSignIn = () => {
		history.push('/signin');
	};

	const initialValues = {
		username: '',
	};

	const validationSchema = Yup.object().shape({
		username: Yup.string()
			.required('Bạn cần nhập mục này')
			.email('Email bạn nhập không hợp lệ'),
	});

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationSchema}
			onSubmit={(values) => {
				const post = async () => {
					try {
						setLoading(true);
						const response = await authApi.resetPassword(values);
						setLoading(false);
						const userId = response.userId;
						const username = response.username;
						history.push({
							pathname: `/reset/recover/${userId}`,
							state: { username },
						});
					} catch (error) {
						setLoading(false);
						notify('error', error.response.data.message);
					}
				};
				post();
			}}
		>
			{(formikProps) => {
				return (
					<Grid container component='main' className={classes.root}>
						<Helmet>
							<title>Quên mật khẩu | Xoài CMS</title>
						</Helmet>
						<Grid
							item
							xs={false}
							sm={false}
							md={7}
							lg={7}
							className={classes.image}
						/>
						<Grid
							item
							xs={12}
							sm={12}
							md={5}
							lg={5}
							component={Paper}
							elevation={6}
							square
						>
							<Grid
								container
								justify='center'
								style={{ height: '40px', marginTop: '40px' }}
							>
								{loading && <CircularProgress />}
							</Grid>
							<Grid container className={classes.panel}>
								<Grid container justify='center'>
									<Typography
										variant='h5'
										style={{
											marginBottom: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										Tìm tài khoản của bạn
									</Typography>
								</Grid>
								<Grid container justify='center'>
									<Typography variant='body1' className={classes.description}>
										Vui lòng nhập địa chỉ email của bạn để lấy lại mật khẩu.
									</Typography>
								</Grid>
								<Grid container justify='center'>
									<Form className={classes.form}>
										<FastField
											name='username'
											component={InputField}
											label='Tài khoản'
											fullWidth={true}
											required={true}
										/>
										<Grid
											container
											className={classes.submit}
											justify='flex-end'
										>
											<Button
												onClick={backToSignIn}
												variant='contained'
												color='default'
												style={{
													marginRight: '1rem',
													textTransform: 'none',
												}}
											>
												Hủy bỏ
											</Button>
											<Button
												type='submit'
												variant='contained'
												color='primary'
												style={{
													textTransform: 'none',
												}}
											>
												Gửi
											</Button>
										</Grid>
									</Form>
								</Grid>
							</Grid>
						</Grid>
					</Grid>
				);
			}}
		</Formik>
	);
}
