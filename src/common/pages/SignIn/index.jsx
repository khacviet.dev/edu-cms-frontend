import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import authApi from 'api/authApi';
import si from 'assets/images/si.jpg';
import x from 'assets/logos/xoai.png';
import InputField from 'custom-fields/InputField';
import { FastField, Form, Formik } from 'formik';
import React, { useContext } from 'react';
import { Helmet } from 'react-helmet';
import { Link, useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as Yup from 'yup';

const useStyles = makeStyles((theme) => ({
	root: {
		height: '100vh',
	},
	image: {
		backgroundImage: `url(${si})`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'cover',
		backgroundPosition: 'center',
	},
	paper: {
		margin: theme.spacing(8, 4),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '80%',
		[theme.breakpoints.down('xs')]: {
			width: '100%',
		},
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

export default function SignIn() {
	const classes = useStyles();
	const notify = useContext(ToastifyContext);
	let history = useHistory();

	const initialValues = {
		username: '',
		password: '',
	};

	const validationSchema = Yup.object().shape({
		username: Yup.string().required('Bạn cần nhập mục này'),
		password: Yup.string().required('Required'),
	});

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationSchema}
			onSubmit={(values) => {
				const login = async () => {
					try {
						await authApi.signin(values);
						history.push('/');
					} catch (error) {
						notify('error', error.response.data.message);
					}
				};
				login();
			}}
		>
			{(formikProps) => {
				return (
					<Grid container component='main' className={classes.root}>
						<Helmet>
							<title>Đăng nhập | Xoài CMS</title>
						</Helmet>
						<Grid
							item
							xs={false}
							sm={false}
							md={7}
							lg={7}
							className={classes.image}
						/>
						<Grid
							item
							xs={12}
							sm={12}
							md={5}
							lg={5}
							component={Paper}
							elevation={6}
							square
						>
							<div className={classes.paper}>
								<img
									src={x}
									style={{
										width: '100px',
										height: 'auto',
										marginBottom: '0.5rem',
									}}
								/>
								<Typography
									component='h1'
									variant='h5'
									style={{ fontWeight: 'bold' }}
								>
									Đăng nhập
								</Typography>
								<Form className={classes.form}>
									<FastField
										name='username'
										component={InputField}
										label='Tài khoản'
										autoFocus={false}
										fullWidth={true}
										required={true}
									/>
									<FastField
										name='password'
										component={InputField}
										label='Mật khẩu'
										autoFocus={false}
										fullWidth={true}
										required={true}
										type='password'
									/>
									<Grid container style={{ marginTop: '0.5rem' }}>
										<Link to='/reset' variant='body2' style={{ color: '#555' }}>
											Quên mật khẩu ?
										</Link>
									</Grid>
									<Button
										type='submit'
										fullWidth
										variant='contained'
										color='primary'
										className={classes.submit}
									>
										Đăng nhập
									</Button>
								</Form>
							</div>
						</Grid>
					</Grid>
				);
			}}
		</Formik>
	);
}
