import { useMediaQuery, useTheme } from '@material-ui/core';
import clsx from 'clsx';
import MyDrawer from 'common/components/MyDrawer';
import MyNavBar from 'common/components/MyNavBar';
import 'csspin/csspin.css';
import React, { useEffect, useState } from 'react';
import { useStyles } from './MasterLayoutStyle';

function MasterLayout(props) {
	const { referer, header } = props;

	const theme = useTheme();
	const matches = useMediaQuery(theme.breakpoints.between('xs', 'sm'));

	const classes = useStyles();

	const [open, setOpen] = useState(() => {
		return localStorage.getItem('drawer-state') === 'true' ? true : false;
	});

	const handleDrawerOpen = () => {
		localStorage.setItem('drawer-state', 'true');
		setOpen(true);
	};

	const handleDrawerClose = () => {
		localStorage.setItem('drawer-state', 'false');
		setOpen(false);
	};

	useEffect(() => {
		if (matches === true) {
			setOpen(false);
		}
	}, [matches]);

	return (
		<div className={classes.root}>
			<MyNavBar
				referer={referer}
				header={header}
				handleDrawerOpen={handleDrawerOpen}
				open={open}
			/>
			<MyDrawer handleDrawerClose={handleDrawerClose} open={open} />
			<main
				className={clsx(classes.content, {
					[classes.contentShift]: open,
				})}
			>
				<div className={classes.drawerHeader} />
				<div>{props.children}</div>
			</main>
		</div>
	);
}

export default React.memo(MasterLayout);
