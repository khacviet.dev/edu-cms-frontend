import { Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import notfoundImage from 'assets/images/notfound4.jpg';
import React from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';

export default function NotFound() {
	let history = useHistory();

	return (
		<div
			style={{
				backgroundImage: `url(${notfoundImage})`,
				backgroundPosition: 'center',
				backgroundRepeat: 'no-repeat',
				backgroundSize: 'cover',
				width: '100vw',
				height: '100vh',
			}}
		>
			<Helmet>
				<title>404 Not Found</title>
			</Helmet>
			<Button
				color='primary'
				style={{ margin: '0.5rem 0 0 0.5rem' }}
				size='large'
				startIcon={<ArrowBackIcon />}
				onClick={() => {
					history.push('/');
				}}
			>
				Về trang chủ
			</Button>
		</div>
	);
}
