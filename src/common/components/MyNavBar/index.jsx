import {
	AppBar,
	Avatar,
	Box,
	Breadcrumbs,
	Grid,
	IconButton,
	ListItemIcon,
	ListItemText,
	Menu,
	MenuItem,
	Toolbar,
	Typography,
} from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import MenuIcon from '@material-ui/icons/Menu';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import authApi from 'api/authApi';
import clsx from 'clsx';
import { getAvatar } from 'features/User/avatarSlice';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { useStyles } from './MyNavBarStyle';

MyNavBar.propTypes = {
	handleDrawerOpen: PropTypes.func.isRequired,
	open: PropTypes.bool.isRequired,
};

function MyNavBar(props) {
	const { handleDrawerOpen, open, referer, header } = props;
	const avatars = useSelector((state) => state.avatars);
	const dispatch = useDispatch();
	const classes = useStyles();
	const history = useHistory();

	const [anchorEl, setAnchorEl] = useState(null);

	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleCloseMenu = () => {
		setAnchorEl(null);
	};

	const routeChange = (e, path) => {
		e.preventDefault();
		history.push(path);
	};

	const logout = () => {
		const handleLogout = async () => {
			await authApi.logout();
			history.push('/signin');
		};
		handleLogout();
	};

	useEffect(() => {
		if (avatars.avtLink === '') {
			dispatch(getAvatar());
		}
	}, []);

	return (
		<AppBar
			elevation={1}
			color='inherit'
			position='fixed'
			className={clsx(classes.appBar, {
				[classes.appBarShift]: open,
			})}
		>
			<Toolbar>
				<Grid container alignItems='center'>
					<Grid item xs={10} md={10} lg={10}>
						<Grid container alignItems='center'>
							{!open && (
								<IconButton
									color='inherit'
									onClick={handleDrawerOpen}
									edge='start'
									className={clsx(classes.menuButton, open && classes.hide)}
								>
									<MenuIcon />
								</IconButton>
							)}
							<Breadcrumbs style={{ paddingLeft: '1rem' }}>
								<Link style={{ fontSize: '1.1rem', color: 'inherit' }} to='/'>
									Trang chủ
								</Link>
								{referer &&
									referer.map((r, index) => (
										<Link
											key={index}
											style={{ fontSize: '1.1rem', color: 'inherit' }}
											to={{
												pathname: `/${r.value}`,
												state: {
													title: `${r.state}`,
													course_id: `${r.course_id}`,
												},
											}}
										>
											{r.display}
										</Link>
									))}

								{header && (
									<Typography
										style={{ fontSize: '1.1rem' }}
										color='textPrimary'
									>
										{header}
									</Typography>
								)}
							</Breadcrumbs>
						</Grid>
					</Grid>
					<Grid
						item
						xs={2}
						md={2}
						lg={2}
						container
						justify='flex-end'
						alignItems='center'
					>
						<Avatar
							src={avatars.avtLink}
							className={classes.avatar}
							onClick={handleClick}
						/>

						<Menu
							anchorEl={anchorEl}
							keepMounted
							open={Boolean(anchorEl)}
							onClose={handleCloseMenu}
						>
							<Box onClick={(e) => routeChange(e, '/profile')}>
								<MenuItem onClick={handleCloseMenu}>
									<ListItemIcon>
										<PersonOutlineIcon
											style={{ color: '#555' }}
											fontSize='small'
										/>
									</ListItemIcon>
									<ListItemText primary='Trang cá nhân' />
								</MenuItem>
							</Box>
							<Box onClick={(e) => routeChange(e, '/changepassword')}>
								<MenuItem onClick={handleCloseMenu}>
									<ListItemIcon>
										<VpnKeyIcon style={{ color: '#555' }} fontSize='small' />
									</ListItemIcon>
									<ListItemText primary='Đổi mật khẩu' />
								</MenuItem>
							</Box>
							<Box onClick={logout}>
								<MenuItem onClick={handleCloseMenu}>
									<ListItemIcon>
										<ExitToAppIcon style={{ color: '#555' }} fontSize='small' />
									</ListItemIcon>
									<ListItemText primary='Đăng xuất' />
								</MenuItem>
							</Box>
						</Menu>
					</Grid>
				</Grid>
			</Toolbar>
		</AppBar>
	);
}

export default MyNavBar;
