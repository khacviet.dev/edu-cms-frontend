import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	Divider,
	Grid,
	IconButton,
	Tooltip,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import RestoreIcon from '@material-ui/icons/Restore';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

MyDialog.propTypes = {
	title: PropTypes.string.isRequired,
	doneText: PropTypes.string.isRequired,
	cancelText: PropTypes.string.isRequired,
	action: PropTypes.func.isRequired,
};

function MyDialog(props) {
	const {
		title,
		description,
		doneText,
		cancelText,
		action,
		colorDone,
		iconType,
	} = props;
	const [openDelete, setOpenDelete] = useState(false);

	const handleOpenDelete = () => {
		setOpenDelete(true);
	};

	const handleCloseDelete = (e) => {
		e.stopPropagation();
		setOpenDelete(false);
	};

	const handleAction = () => {
		if (action) {
			action();
			setOpenDelete(false);
		}
	};

	return (
		<div>
			{iconType === '0' ? (
				<Tooltip title='Xóa'>
					<DeleteIcon
						icon='0'
						color='secondary'
						style={{ cursor: 'pointer' }}
						onClick={handleOpenDelete}
					/>
				</Tooltip>
			) : (
				<Tooltip title='Khôi phục'>
					<RestoreIcon
						style={{ cursor: 'pointer', color: 'green' }}
						onClick={handleOpenDelete}
					/>
				</Tooltip>
			)}
			<Dialog open={openDelete} onClose={handleCloseDelete}>
				<Grid container>
					<Grid item xs={9} md={9} lg={9}>
						<DialogTitle>{title}</DialogTitle>
					</Grid>
					<Grid item container justify='flex-end' xs={3} md={3} lg={3}>
						<IconButton onClick={handleCloseDelete}>
							<CloseIcon />
						</IconButton>
					</Grid>
				</Grid>
				<Divider />
				<DialogContent>
					<DialogContentText>{description}</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button variant='contained' onClick={handleCloseDelete}>
						{cancelText}
					</Button>
					<Button variant='contained' color={colorDone} onClick={handleAction}>
						{doneText}
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	);
}

export default MyDialog;
