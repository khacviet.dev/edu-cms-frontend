import { Button } from '@material-ui/core';
import GetAppIcon from '@material-ui/icons/GetApp';
import FileSaver from 'file-saver';
import PropTypes from 'prop-types';
import React from 'react';
import xlsx from 'xlsx';

ExportCSV.propTypes = {
	csvData: PropTypes.array.isRequired,
	fileName: PropTypes.string.isRequired,
};

export default function ExportCSV({ csvData, fileName }) {
	const fileType =
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
	const fileExtension = '.xlsx';

	const exportToCSV = (csvData, fileName) => {
		const ws = xlsx.utils.json_to_sheet(csvData);
		const wb = { Sheets: { data: ws }, SheetNames: ['data'] };
		const excelBuffer = xlsx.write(wb, { bookType: 'xlsx', type: 'array' });
		const data = new Blob([excelBuffer], { type: fileType });
		FileSaver.saveAs(data, fileName + fileExtension);
	};

	return (
		<Button
			startIcon={<GetAppIcon />}
			variant='text'
			onClick={(e) => exportToCSV(csvData, fileName)}
		>
			Xuất file
		</Button>
	);
}
