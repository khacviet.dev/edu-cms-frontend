import {
	Button,
	Checkbox,
	FormControl,
	Grid,
	Input,
	InputLabel,
	ListItemText,
	MenuItem,
	Select,
	TextField,
	Typography,
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import EditIcon from '@material-ui/icons/Edit';
import FilterListIcon from '@material-ui/icons/FilterList';
import GetAppIcon from '@material-ui/icons/GetApp';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import DeleteDialog from 'common/components/DeleteDialog';
import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import Pagination from '../Pagination';
import './style.css';

const useStyles = makeStyles({
	table: {
		minWidth: 650,
	},
	formControl: {
		minWidth: 120,
	},
	td: {
		whiteSpace: 'nowrap',
	},
});

const LightTooltip = withStyles((theme) => ({
	tooltip: {
		backgroundColor: theme.palette.common.black,
		color: 'white',
		boxShadow: theme.shadows[1],

		padding: '16px',
		fontSize: '0.875rem',
		fontFamily: 'Roboto ,Helvetica,Arial,sans-serif',
		fontWeight: '400',
		lineHeight: ' 1.43',
		borderBottom: '1px solid rgba(224, 224, 224, 1)',
		letterSpacing: ' 0.01071em',
		verticalAlign: 'inherit',
	},
}))(Tooltip);

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
		},
	},
};

export default function CommonTableT(
	filterList1,
	filterList2,
	filterList3,
	listFilterLabel,
	id
) {
	return function (props) {
		const {
			handleDelete,
			handleColumnChange,
			changeDisplayColumn,
			handleFilterChange,
			columnSelected,
			keys,
			pagination,
			data,
			exportExcel,
			handlePageChange,
			roleName,
			filter1,
			filter2,
			filter3,
			resetFilter,
			role,
			totalRow,
			tableName,
			subjectList,
			listFunctionTable,
			handleRevert,
			tooltip,
			course_id,
			lesson_title,
			listClass,
			listTeacher,
			listCourse,
			handleChangeFeedBackStatus,
		} = props;

		if (pagination != undefined) {
			pagination.totalRows = totalRow;
		}
		if (
			subjectList &&
			(tableName == 'courseList' || tableName == 'lessonList')
		) {
			filterList1 = subjectList;
		} else if (tableName == 'feedback_class') {
			filterList1 = listClass;
			filterList2 = listTeacher;
		} else if (tableName == 'feedback_course') {
			filterList1 = listCourse;
			filterList2 = listTeacher;
		} else if (tableName == 'salary') {
			const listYear = [];
			const created_year = 2019;
			const current_year = new Date().getFullYear();
			for (let i = created_year; i <= current_year; i++) {
				const year = {
					text: i,
					value: i,
				};
				listYear.push(year);
			}
			filterList2 = listYear;
		} else if (tableName == 'revenue') {
			filterList1 = listClass;
		} else if (tableName == 'revenueOn') {
			filterList1 = listCourse;
		}

		let history = useHistory();

		const handleLesonList = (course_id, title) => {
			history.push({
				pathname: `/course/lessonlist`,
				state: {
					title: title,
					course_id: course_id,
				},
			});
		};

		const handleEdit = (data, lessonTitle, userName, sourceStatus) => {
			if (tableName == 'post') {
				history.push({
					pathname: `/postdetail`,
					state: {
						post_id: data,
					},
				});
			} else if (tableName == 'salary') {
				history.push({
					pathname: '/salarydetail',
					state: {
						user_id: data,
						user_name: userName,
						month: filter1,
						year: filter2,
					},
				});
			} else if (tableName == 'courseList') {
				history.push({
					pathname: '/coursedetail',
					state: {
						course_id: data,
					},
				});
			} else if (tableName == 'lessonList') {
				let status = '';
				if (sourceStatus == '0') {
					status = 'Chưa duyệt';
				} else if (sourceStatus == '1') {
					status = 'Đã duyệt';
				} else if (sourceStatus == '2') {
					status = 'Từ chối';
				} else {
					status = 'Đã xóa';
				}
				history.push({
					pathname: `/lessonDetail`,
					state: {
						source_id: data,
						title: lesson_title,
						course_id: course_id,
						is_detail: lessonTitle,
						source_status: status,
					},
				});
			} else if (tableName == 'revenue') {
			}
		};

		const classes = useStyles();

		return (
			<Grid container>
				<Grid container alignItems='center'>
					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						{filterList1.length > 0 && (
							<FormControl className={classes.formControl}>
								<InputLabel>{listFilterLabel[0]}</InputLabel>
								<Select
									renderValue={() => {
										const a = filterList1.find(
											(filter) => filter.value === filter1
										);
										return a == undefined
											? ''
											: a.text.length < 10
											? a.text
											: a.text.substring(0, 10).concat('...');
									}}
									value={filter1}
									onChange={(e) => handleFilterChange(e, 'filter1')}
								>
									{filterList1.map((filter, index) => (
										<MenuItem key={`filter1_${index}`} value={filter.value}>
											{filter.text}
										</MenuItem>
									))}
								</Select>
							</FormControl>
						)}
					</Grid>

					<Grid
						item
						md={tableName == 'revenue' || tableName == 'revenueOn' ? 3 : 2}
						style={{ marginBottom: '0.75rem' }}
					>
						{(tableName == 'revenue' || tableName == 'revenueOn') && (
							<TextField
								id='date'
								label='From'
								type='month'
								inputProps={{
									pattern: '[0-9]{4}-[0-9]{2}',
								}}
								onChange={(e) => handleFilterChange(e, 'filter2')}
								defaultValue={new Date().toISOString().slice(0, 7)}
								className={classes.textField}
								InputLabelProps={{
									shrink: true,
								}}
							/>
						)}

						{filterList2.length > 0 && (
							<FormControl className={classes.formControl}>
								<InputLabel>{listFilterLabel[1]}</InputLabel>
								<Select
									value={filter2}
									onChange={(e) => handleFilterChange(e, 'filter2')}
								>
									{filterList2.map((filter, index) => (
										<MenuItem key={`filter2_${index}`} value={filter.value}>
											{filter.text}
										</MenuItem>
									))}
								</Select>
							</FormControl>
						)}
					</Grid>

					<Grid
						item
						md={tableName == 'revenue' || tableName == 'revenueOn' ? 3 : 2}
						style={{ marginBottom: '0.75rem' }}
					>
						{(tableName == 'revenue' || tableName == 'revenueOn') && (
							<TextField
								id='date'
								label='To'
								type='month'
								inputProps={{
									pattern: '[0-9]{4}-[0-9]{2}',
								}}
								onChange={(e) => handleFilterChange(e, 'filter3')}
								defaultValue={new Date().toISOString().slice(0, 7)}
								className={classes.textField}
								InputLabelProps={{
									shrink: true,
								}}
							/>
						)}

						{filterList3.length > 0 && (
							<FormControl className={classes.formControl}>
								<InputLabel>{listFilterLabel[2]}</InputLabel>
								<Select
									value={filter3}
									onChange={(e) => handleFilterChange(e, 'filter3')}
								>
									{filterList3.map((filter, index) => (
										<MenuItem key={`filter3_${index}`} value={filter.value}>
											{filter.text}
										</MenuItem>
									))}
								</Select>
							</FormControl>
						)}
					</Grid>

					<Grid item md={2}>
						{((filter1 !== '' && filter1 !== undefined) ||
							(filter2 !== '' && filter2 !== undefined) ||
							(filter3 !== '' && filter3 !== undefined)) &&
							tableName != 'revenue' &&
							tableName != 'revenueOn' && (
								<Button
									onClick={resetFilter}
									startIcon={<FilterListIcon />}
									variant='contained'
									color='secondary'
								>
									Xóa lọc
								</Button>
							)}

						{filter1 !== '' &&
							filter1 !== undefined &&
							(tableName == 'revenue' || tableName == 'revenueOn') && (
								<Button
									onClick={resetFilter}
									startIcon={<FilterListIcon />}
									variant='contained'
									color='secondary'
								>
									Xóa lọc
								</Button>
							)}
					</Grid>

					<Grid item md={2} container justify='flex-end'>
						{listFunctionTable['view'] == true && (
							<Button
								startIcon={<GetAppIcon />}
								variant='text'
								onClick={exportExcel}
							>
								Xuất file
							</Button>
						)}
					</Grid>
					{tableName != 'revenue' && tableName != 'revenueOn' && (
						<Grid item md={2} container justify='flex-end'>
							<Select
								multiple
								value={columnSelected}
								onChange={handleColumnChange}
								input={<Input />}
								renderValue={() => 'Chọn cột hiển thị'}
								MenuProps={MenuProps}
							>
								{keys.map((key, index) => (
									<MenuItem
										onClick={() => changeDisplayColumn(key)}
										key={index}
										value={key.header}
									>
										<Checkbox
											checked={columnSelected.indexOf(key.header) > -1}
										/>
										<ListItemText primary={key.header} />
									</MenuItem>
								))}
							</Select>
						</Grid>
					)}
				</Grid>

				<TableContainer component={Paper} style={{ marginTop: '1rem' }}>
					<Table className={classes.table} stickyHeader>
						<TableHead>
							<TableRow>
								<TableCell style={{ width: '5%' }}>ID</TableCell>
								{keys.map((key, index) => (
									<TableCell
										style={{ display: key.display ? '' : 'none' }}
										key={index}
										align={key.align}
									>
										{key.header}
									</TableCell>
								))}
								{(listFunctionTable['edit'] || listFunctionTable['review']) && (
									<TableCell style={{ width: '5%' }}></TableCell>
								)}
								{listFunctionTable['delete'] && (
									<TableCell style={{ width: '5%' }}></TableCell>
								)}
							</TableRow>
						</TableHead>
						<TableBody>
							{data && data.length > 0 ? (
								data.map((row, index) => (
									<TableRow hover key={index}>
										<TableCell>{row['id']}</TableCell>
										{keys.map((key, index) => (
											<LightTooltip
												arrow
												placement='top'
												disableHoverListener={
													key.field == 'feedback' ? false : true
												}
												title={`${row[key.field]}`}
											>
												<TableCell
													style={{
														display: key.display ? '' : 'none',
														color:
															(key.field == 'salary_status' &&
															row[key.field] == '0'
																? 'red'
																: row[key.field] == '1'
																? 'green'
																: '') ||
															((key.field == 'course_status' ||
																key.field == 'post_status' ||
																key.field == 'source_status') &&
																((row[key.field] == '0' && '') ||
																	(row[key.field] == '1' && 'green') ||
																	(row[key.field] == '2' && 'red') ||
																	(row[key.field] == '3' && 'red') ||
																	(row[key.field] == '4' && 'blue'))),
														maxWidth: '200px',
														overflow: 'hidden',
														textOverflow: 'ellipsis',
														whiteSpace: 'nowrap',
													}}
													align={key.align}
													key={index}
												>
													{(key.field == 'post_status' &&
														((row[key.field] == '0' && 'Lưu nháp') ||
															(row[key.field] == '1' && 'Đã Duyệt') ||
															(row[key.field] == '2' && 'Từ chối') ||
															(row[key.field] == '3' && 'Đã xóa') ||
															(row[key.field] == '4' && 'Đã hoàn thành'))) ||
														(key.field == 'course_status' &&
															((row[key.field] == '0' && 'Lưu nháp') ||
																(row[key.field] == '1' && 'Đã Duyệt') ||
																(row[key.field] == '2' && 'Từ chối') ||
																(row[key.field] == '3' && 'Đã xóa') ||
																(row[key.field] == '4' && 'Đã hoàn thành'))) ||
														(key.field == 'source_status' &&
															((row[key.field] == '0' && 'Chưa Duyệt') ||
																(row[key.field] == '1' && 'Đã Duyệt') ||
																(row[key.field] == '2' && 'Từ chối') ||
																(row[key.field] == '3' && 'Đã xóa'))) ||
														(key.field == 'salary_status' &&
															((row[key.field] == '0' && 'Chưa Thanh Toán') ||
																(row[key.field] == '1' && 'Đã Thanh Toán'))) ||
														(key.field == 'salary_total' &&
															row[key.field].toLocaleString('vi-VN', {
																style: 'currency',
																currency: 'VND',
															})) ||
														(key.field == 'course_cost' &&
															row[key.field].toLocaleString('vi-VN', {
																style: 'currency',
																currency: 'VND',
															})) ||
														(key.field == 'payment_date' &&
															row[key.field] !== null &&
															row[key.field]) ||
														(key.field == 'payment_date' &&
															row[key.field] === null &&
															'Chưa thu') ||
														(key.field == 'join_date' &&
															row[key.field] !== null &&
															row[key.field]) ||
														(key.field == 'join_date' &&
															row[key.field] === null &&
															'Chưa thu') ||
														(key.field == 'amount' &&
															row[key.field].toLocaleString('vi-VN', {
																style: 'currency',
																currency: 'VND',
															})) ||
														(key.field == 'price' &&
															row[key.field].toLocaleString('vi-VN', {
																style: 'currency',
																currency: 'VND',
															})) ||
														(key.field == 'number_lesson' &&
															listFunctionTable['view'] && (
																<Grid container md={12}>
																	<Grid item md={6}>
																		{row[key.field]}
																	</Grid>
																	<Grid item md={6}>
																		<LightTooltip
																			placement='top'
																			title={
																				tableName == 'courseList'
																					? 'Danh sách bài học'
																					: 'Danh sách xx'
																			}
																		>
																			<ImportContactsIcon
																				onClick={() =>
																					handleLesonList(
																						row['id'],
																						row['title']
																					)
																				}
																				color='action'
																				style={{ cursor: 'pointer' }}
																			/>
																		</LightTooltip>
																	</Grid>
																</Grid>
															)) ||
														(key.field == 'feedback_status' &&
															((row[key.field] == '0' && 'Chưa Xem') ||
																(row[key.field] == '1' && 'Đã Xem'))) ||
														row[key.field]}
												</TableCell>
											</LightTooltip>
										))}
										{listFunctionTable['edit'] &&
											(tableName == 'revenue' || tableName == 'revenueOn') && (
												<TableCell>
													<LightTooltip
														placement='top'
														title={'Chỉnh sửa trạng thái học phí'}
													>
														<Link
															to={{ pathname: `/student/${row['user_id']}` }}
														>
															<ArrowForwardIcon></ArrowForwardIcon>
														</Link>
													</LightTooltip>
												</TableCell>
											)}
										{(listFunctionTable['edit'] ||
											listFunctionTable['review']) &&
											tableName != 'revenue' &&
											tableName != 'revenueOn' && (
												<TableCell>
													<LightTooltip
														placement='top'
														title={`Chỉnh sửa ${tooltip}`}
													>
														{tableName == 'feedback_class' ||
														tableName == 'feedback_course' ? (
															row['feedback_status'] == '0' ? (
																<DeleteDialog
																	title={`Thay đổi trạng thái`}
																	description='Bạn có chắc chắn muốn đổi trạng thái thành đã xem?'
																	doneText='Có'
																	cancelText='Không'
																	tooltip_text={`Thay dổi trạng thái`}
																	icon='6'
																	action={() =>
																		handleChangeFeedBackStatus(row['id'], '1')
																	}
																/>
															) : (
																<DeleteDialog
																	title={`Thay đổi trạng thái`}
																	description='Bạn có chắc chắn muốn đổi trạng thái thành chưa xem?'
																	doneText='Có'
																	cancelText='Không'
																	tooltip_text={`Thay dổi trạng thái`}
																	icon='5'
																	action={() =>
																		handleChangeFeedBackStatus(row['id'], '0')
																	}
																/>
															)
														) : (
															<EditIcon
																onClick={() =>
																	handleEdit(
																		row['id'],
																		row['source_title'],
																		row['full_name'],
																		row['source_status']
																	)
																}
																color='action'
																style={{ cursor: 'pointer' }}
															></EditIcon>
														)}
													</LightTooltip>
												</TableCell>
											)}

										{listFunctionTable['delete'] &&
											((row['course_status'] == '3' &&
												tableName == 'courseList') ||
												(row['post_status'] == '3' && tableName == 'post') ||
												(row['source_status'] == '3' &&
													tableName == 'lessonList')) && (
												<TableCell>
													<DeleteDialog
														title={`Khôi phục ${tooltip}`}
														description='Bạn có chắc chắn muốn khôi phục không'
														doneText='Có'
														cancelText='Không'
														tooltip_text={`Khôi phục ${tooltip}`}
														icon='2'
														action={() => handleRevert(row['id'])}
													/>
												</TableCell>
											)}

										{listFunctionTable['delete'] &&
											((row['course_status'] != '3' &&
												tableName == 'courseList') ||
												(row['post_status'] != '3' && tableName == 'post') ||
												(row['source_status'] != '3' &&
													tableName == 'lessonList')) && (
												<TableCell>
													<DeleteDialog
														icon='0'
														title={`Xóa ${tooltip}`}
														description='Bạn có chắc chắn muốn xóa không'
														doneText='Xóa'
														cancelText='Hủy'
														tooltip_text={`Xóa ${tooltip}`}
														action={() => handleDelete(row['id'])}
													/>
												</TableCell>
											)}
									</TableRow>
								))
							) : (
								<TableRow>
									<TableCell align='center' colSpan={keys.length + 3}>
										Không tìm thấy dữ liệu
									</TableCell>
								</TableRow>
							)}
						</TableBody>
					</Table>
				</TableContainer>

				{data && (
					<Grid container alignItems='center' style={{ marginTop: '2rem' }}>
						<Grid item md={6}>
							<Typography style={{ fontWeight: 'bold' }}>
								Tổng số {roleName} ({pagination.totalRows})
							</Typography>
						</Grid>
						<Grid item md={6}>
							<Pagination
								pagination={pagination}
								onPageChange={handlePageChange}
							/>
						</Grid>
					</Grid>
				)}
			</Grid>
		);
	};
}
