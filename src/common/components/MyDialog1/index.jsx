import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	Divider,
	Grid,
	IconButton,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

MyDialog1.propTypes = {
	title: PropTypes.string.isRequired,
	doneText: PropTypes.string.isRequired,
	cancelText: PropTypes.string.isRequired,
	action: PropTypes.func.isRequired,
};

function MyDialog1(props) {
	const { title, description, doneText, cancelText, action, colorDone } = props;
	const [openDelete, setOpenDelete] = useState(false);

	const handleOpenDelete = () => {
		setOpenDelete(true);
	};

	const handleCloseDelete = (e) => {
		e.stopPropagation();
		setOpenDelete(false);
	};

	const handleAction = () => {
		if (action) {
			action();
			setOpenDelete(false);
		}
	};

	return (
		<div>
			<IconButton onClick={handleOpenDelete}>
				<CloseIcon />
			</IconButton>

			<Dialog open={openDelete} onClose={handleCloseDelete}>
				<Grid container>
					<Grid item xs={9} md={9} lg={9}>
						<DialogTitle>{title}</DialogTitle>
					</Grid>
					<Grid item container justify='flex-end' xs={3} md={3} lg={3}>
						<IconButton onClick={handleCloseDelete}>
							<CloseIcon />
						</IconButton>
					</Grid>
				</Grid>
				<Divider />
				<DialogContent>
					<DialogContentText>{description}</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button variant='contained' onClick={handleCloseDelete}>
						{cancelText}
					</Button>
					<Button variant='contained' color={colorDone} onClick={handleAction}>
						{doneText}
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	);
}

export default MyDialog1;
