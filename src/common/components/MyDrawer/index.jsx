import {
	Collapse,
	Divider,
	Drawer,
	Grid,
	IconButton,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	Typography,
} from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import AssessmentIcon from '@material-ui/icons/Assessment';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import BusinessIcon from '@material-ui/icons/Business';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import CreateIcon from '@material-ui/icons/Create';
import DashboardIcon from '@material-ui/icons/Dashboard';
import DateRangeIcon from '@material-ui/icons/DateRange';
import DoneIcon from '@material-ui/icons/Done';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import EuroIcon from '@material-ui/icons/Euro';
import FeedbackIcon from '@material-ui/icons/Feedback';
import ListIcon from '@material-ui/icons/List';
import ScheduleIcon from '@material-ui/icons/Schedule';
import SchoolIcon from '@material-ui/icons/School';
import SettingsIcon from '@material-ui/icons/Settings';
import permissionApi from 'api/permissionApi';
import xoaiicon from 'assets/logos/xoai.png';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useStyles } from './MyDrawerStyle';

MyDrawer.propTypes = {
	handleDrawerClose: PropTypes.func.isRequired,
	open: PropTypes.bool.isRequired,
};

const checkShow = (functions, menuCode) => {
	if (functions.findIndex((f) => f.function_id === menuCode) !== -1) {
		return true;
	}
	return false;
};

function MyDrawer({ handleDrawerClose, open }) {
	const classes = useStyles();
	let history = useHistory();

	const [functions, setFunctions] = useState(
		localStorage.getItem('menu') !== null
			? JSON.parse(localStorage.getItem('menu'))
			: []
	);

	const [isChange, setIsChange] = useState(false);

	useEffect(() => {
		const post = async () => {
			try {
				const response = await permissionApi.checkMenuChange({
					local: JSON.parse(localStorage.getItem('menu')),
				});

				if (response === '0') {
					return;
				}

				setIsChange(true);
			} catch (error) {}
		};

		post();
	}, []);

	useEffect(() => {
		if (isChange) {
			localStorage.removeItem('menu');
			const get = async () => {
				try {
					const response = await permissionApi.getMenu();
					localStorage.setItem('menu', JSON.stringify(response));

					setFunctions(response);
				} catch (error) {}
			};

			get();
		}
	}, [isChange]);

	const [menuOpen, setMenuOpen] = useState({
		settingManagerment: false,
		staffManagerment: false,
	});

	const routeChange = (path) => {
		history.push(path);
	};

	const handleMenuExpand = (element) => {
		setMenuOpen({
			...menuOpen,
			[element]: !menuOpen[element],
		});
	};
	return (
		<Drawer
			color='primary'
			className={classes.drawer}
			variant='persistent'
			anchor='left'
			open={open}
			classes={{
				paper: classes.drawerPaper,
			}}
		>
			<Grid container alignItems='center' style={{ padding: '0.5rem' }}>
				<Grid item md={10} container alignItems='center'>
					<img
						src={xoaiicon}
						alt='no image'
						style={{ width: '48px', height: '48px' }}
					/>
					<Typography
						style={{
							marginTop: '0.5rem',
							fontSize: '1.1rem',
							fontWeight: 'bold',
						}}
					>
						&nbsp;&nbsp;Xoài Education
					</Typography>
				</Grid>
				<Grid item md={2} container justifyContent='flex-end'>
					<IconButton onClick={handleDrawerClose}>
						<ChevronLeftIcon style={{ color: '#ccc' }} />
					</IconButton>
				</Grid>
			</Grid>
			<Divider />
			<List>
				<ListItem button onClick={() => routeChange('/')}>
					<ListItemIcon>
						<DashboardIcon />
					</ListItemIcon>
					<ListItemText primary={'Tổng quan'} />
				</ListItem>
				{checkShow(functions, 'function_list_revenue') && (
					<ListItem button onClick={() => routeChange('/revenue')}>
						<ListItemIcon>
							<EuroIcon />
						</ListItemIcon>
						<ListItemText primary={'Doanh số'} />
					</ListItem>
				)}
				{checkShow(functions, 'function_list_class') && (
					<ListItem button onClick={() => routeChange('/class')}>
						<ListItemIcon>
							<BusinessIcon />
						</ListItemIcon>
						<ListItemText primary={'Lớp học'} />
					</ListItem>
				)}

				{(checkShow(functions, 'function_list_teacher') ||
					checkShow(functions, 'function_list_tutor') ||
					checkShow(functions, 'function_list_student') ||
					checkShow(functions, 'function_list_manager') ||
					checkShow(functions, 'function_list_maketing')) && (
					<ListItem button onClick={() => handleMenuExpand('staffManagerment')}>
						<ListItemIcon>
							<EmojiPeopleIcon />
						</ListItemIcon>
						<ListItemText primary={'Người dùng'} />
						{menuOpen.staffManagerment ? (
							<ExpandLess style={{ color: '#ccc' }} />
						) : (
							<ExpandMore style={{ color: '#ccc' }} />
						)}
					</ListItem>
				)}
				<Collapse in={menuOpen.staffManagerment} timeout='auto' unmountOnExit>
					<List component='div' disablePadding>
						{checkShow(functions, 'function_list_manager') && (
							<ListItem
								button
								className={classes.nested}
								onClick={() => routeChange('/manager')}
							>
								<ListItemIcon>
									<ListIcon />
								</ListItemIcon>
								<ListItemText primary={'Quản lý'} />
							</ListItem>
						)}
						{checkShow(functions, 'function_list_teacher') && (
							<ListItem
								button
								className={classes.nested}
								onClick={() => routeChange('/teacher')}
							>
								<ListItemIcon>
									<ListIcon />
								</ListItemIcon>
								<ListItemText primary={'Giáo viên'} />
							</ListItem>
						)}
						{checkShow(functions, 'function_list_tutor') && (
							<ListItem
								button
								className={classes.nested}
								onClick={() => routeChange('/tutor')}
							>
								<ListItemIcon>
									<ListIcon />
								</ListItemIcon>
								<ListItemText primary={'Trợ giảng'} />
							</ListItem>
						)}
						{checkShow(functions, 'function_list_student') && (
							<ListItem
								button
								className={classes.nested}
								onClick={() => routeChange('/student')}
							>
								<ListItemIcon>
									<ListIcon />
								</ListItemIcon>
								<ListItemText primary={'Học sinh'} />
							</ListItem>
						)}
						{checkShow(functions, 'function_list_maketing') && (
							<ListItem
								button
								className={classes.nested}
								onClick={() => routeChange('/maketing')}
							>
								<ListItemIcon>
									<ListIcon />
								</ListItemIcon>
								<ListItemText primary={'Marketer'} />
							</ListItem>
						)}
					</List>
				</Collapse>

				{checkShow(functions, 'function_list_register') && (
					<ListItem button onClick={() => routeChange('/register')}>
						<ListItemIcon>
							<SchoolIcon />
						</ListItemIcon>
						<ListItemText primary={'Đăng ký'} />
					</ListItem>
				)}

				{checkShow(functions, 'function_list_attendance') && (
					<ListItem button onClick={() => routeChange('/attendance')}>
						<ListItemIcon>
							<DoneIcon />
						</ListItemIcon>
						<ListItemText primary={'Điểm danh'} />
					</ListItem>
				)}
				{checkShow(functions, 'function_list_salary') && (
					<ListItem button onClick={() => routeChange('/salary')}>
						<ListItemIcon>
							<AttachMoneyIcon />
						</ListItemIcon>
						<ListItemText primary={'Bảng lương'} />
					</ListItem>
				)}
				{checkShow(functions, 'function_course_list') && (
					<ListItem button onClick={() => routeChange('/course')}>
						<ListItemIcon>
							<AssignmentIcon />
						</ListItemIcon>
						<ListItemText primary={'Khóa học'} />
					</ListItem>
				)}
				{checkShow(functions, 'function_feedback_list') && (
					<ListItem button onClick={() => routeChange('feedBack')}>
						<ListItemIcon>
							<FeedbackIcon />
						</ListItemIcon>
						<ListItemText primary={'Phản hồi'} />
					</ListItem>
				)}
				{checkShow(functions, 'function_post_list') && (
					<ListItem button onClick={() => routeChange('/post')}>
						<ListItemIcon>
							<CreateIcon />
						</ListItemIcon>
						<ListItemText primary={`Bài viết`} />
					</ListItem>
				)}

				{checkShow(functions, 'function_attendance_report') && (
					<ListItem button onClick={() => routeChange('/attendstudent')}>
						<ListItemIcon>
							<AssessmentIcon />
						</ListItemIcon>
						<ListItemText primary={`Báo cáo điểm danh`} />
					</ListItem>
				)}

				{checkShow(functions, 'function_student_timetable') && (
					<ListItem button onClick={() => routeChange('/studentweekly')}>
						<ListItemIcon>
							<DateRangeIcon />
						</ListItemIcon>
						<ListItemText primary={`Thời khóa biểu`} />
					</ListItem>
				)}

				{checkShow(functions, 'function_schedule') && (
					<ListItem button onClick={() => routeChange('/schedulecenter')}>
						<ListItemIcon>
							<ScheduleIcon />
						</ListItemIcon>
						<ListItemText primary={`Lịch giảng dạy`} />
					</ListItem>
				)}

				{checkShow(functions, 'function_setting_list') && (
					<ListItem button onClick={() => routeChange('/setting')}>
						<ListItemIcon>
							<SettingsIcon />
						</ListItemIcon>
						<ListItemText primary={'Tùy chỉnh'} />
					</ListItem>
				)}
			</List>
		</Drawer>
	);
}

export default MyDrawer;
