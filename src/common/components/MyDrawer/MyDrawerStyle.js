const { makeStyles } = require('@material-ui/core');

const drawerWidth = 280;

export const useStyles = makeStyles((theme) => ({
	drawer: {
		width: drawerWidth,
		flexShrink: 0,
		color: '#ccc',
	},
	drawerPaper: {
		width: drawerWidth,
		background: theme.palette.primary.main,
		color: '#ccc',
	},
	drawerHeader: {
		display: 'flex',
		alignItems: 'center',
		padding: theme.spacing(0, 1),
		...theme.mixins.toolbar,
		justifyContent: 'flex-end',
	},
	nested: {
		paddingLeft: theme.spacing(4),
	},
}));
