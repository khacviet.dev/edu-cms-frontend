import {
	Button,
	Checkbox,
	FormControl,
	Grid,
	Input,
	InputLabel,
	ListItemText,
	MenuItem,
	Select,
	Typography,
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FilterListIcon from '@material-ui/icons/FilterList';
import DeleteDialog from 'common/components/DeleteDialog';
import React from 'react';
import ExportCSV from '../ExportCSV';
import Pagination from '../Pagination';

const useStyles = makeStyles({
	table: {
		minWidth: 650,
	},
	formControl: {
		minWidth: 120,
	},
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
		},
	},
};

export default function CommonTable(
	CreateComponent,
	DetailComponent,
	filterList1,
	filterList2,
	id,
	listFunction
) {
	return function (props) {
		const {
			handleCreate,
			handleDelete,
			handleColumnChange,
			changeDisplayColumn,
			handleFilterChange,
			columnSelected,
			keys,
			pagination,
			data,
			handlePageChange,
			roleName,
			filter1,
			filter2,
			resetFilter,
			role,
		} = props;
		const classes = useStyles();

		return (
			<Grid container>
				<Grid container alignItems='center'>
					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						<FormControl className={classes.formControl}>
							<InputLabel>Trạng thái</InputLabel>
							<Select
								value={filter1}
								onChange={(e) => handleFilterChange(e, 'filter1')}
							>
								{filterList1.map((filter, index) => (
									<MenuItem key={index} value={filter.value}>
										{filter.display}
									</MenuItem>
								))}
							</Select>
						</FormControl>
					</Grid>
					<Grid item md={2} style={{ marginBottom: '0.75rem' }}>
						<FormControl className={classes.formControl}>
							<InputLabel>Đóng tiền</InputLabel>
							<Select
								value={filter2}
								onChange={(e) => handleFilterChange(e, 'filter2')}
							>
								{filterList2.map((filter, index) => (
									<MenuItem key={index} value={filter.value}>
										{filter.display}
									</MenuItem>
								))}
							</Select>
						</FormControl>
					</Grid>
					<Grid item md={2}>
						{(filter1 !== '' || filter2 !== '') && (
							<Button
								onClick={resetFilter}
								startIcon={<FilterListIcon />}
								variant='contained'
								color='secondary'
							>
								Xóa lọc
							</Button>
						)}
					</Grid>
					<Grid item md={2}>
						{listFunction.create && (
							<CreateComponent
								handleCreateUser={handleCreate}
								role={role}
								roleName={roleName}
							/>
						)}
					</Grid>
					<Grid item md={2} container justify='flex-end'>
						{listFunction.export && (
							<ExportCSV csvData={data} fileName='data' />
						)}
					</Grid>
					<Grid item md={2} container justify='flex-end'>
						<Select
							multiple
							value={columnSelected}
							onChange={handleColumnChange}
							input={<Input />}
							renderValue={() => 'Chọn cột hiển thị'}
							MenuProps={MenuProps}
						>
							{keys.map((key, index) => (
								<MenuItem
									onClick={() => changeDisplayColumn(key)}
									key={index}
									value={key.header}
								>
									<Checkbox checked={columnSelected.indexOf(key.header) > -1} />
									<ListItemText primary={key.header} />
								</MenuItem>
							))}
						</Select>
					</Grid>
				</Grid>

				<TableContainer component={Paper} style={{ marginTop: '1rem' }}>
					<Table className={classes.table} stickyHeader>
						<TableHead>
							<TableRow>
								<TableCell align='center' style={{ width: '5%' }}>
									STT
								</TableCell>
								{keys.map((key, index) => (
									<TableCell
										style={{ display: key.display ? '' : 'none' }}
										key={index}
										align='center'
									>
										{key.header}
									</TableCell>
								))}
								{listFunction.edit && (
									<TableCell align='center' style={{ width: '5%' }}></TableCell>
								)}
								{listFunction.delele && (
									<TableCell align='center' style={{ width: '5%' }}></TableCell>
								)}
							</TableRow>
						</TableHead>
						<TableBody>
							{data ? (
								data.map((row, index) => (
									<TableRow hover key={index}>
										<TableCell align='center'>
											{(pagination.page - 1) * pagination.limit + index + 1}
										</TableCell>
										{keys.map((key, index) => (
											<TableCell
												style={{
													display: key.display ? '' : 'none',
												}}
												align='center'
												key={index}
											>
												{row[key.field]}
											</TableCell>
										))}
										<TableCell align='center'>
											{listFunction.edit && <DetailComponent />}
										</TableCell>
										{listFunction.delele && (
											<TableCell align='center'>
												<DeleteDialog
													icon='0'
													title={`Xóa ${roleName}`}
													description='Bạn có chắc chắn muốn xóa không'
													doneText='Xóa'
													cancelText='Hủy'
													action={() => handleDelete(row[id])}
												/>
											</TableCell>
										)}
									</TableRow>
								))
							) : (
								<TableRow>
									<TableCell align='center' colSpan={keys.length + 3}>
										Không tìm thấy dữ liệu
									</TableCell>
								</TableRow>
							)}
						</TableBody>
					</Table>
				</TableContainer>
				{data && (
					<Grid container alignItems='center' style={{ marginTop: '2rem' }}>
						<Grid item md={6}>
							<Typography style={{ fontWeight: 'bold' }}>
								Tổng số {roleName} ({pagination.totalRows})
							</Typography>
						</Grid>
						<Grid item md={6}>
							<Pagination
								pagination={pagination}
								onPageChange={handlePageChange}
							/>
						</Grid>
					</Grid>
				)}
			</Grid>
		);
	};
}
