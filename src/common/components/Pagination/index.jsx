import { Box, Grid } from '@material-ui/core';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import './Pagination.css';

Pagination.propTypes = {
	pagination: PropTypes.object.isRequired,
	onPageChange: PropTypes.func.isRequired,
};

const generateFirstPage = (totalPages) => {
	switch (totalPages) {
		case 1:
			return [{ number: 1, active: true }];
		case 2:
			return [
				{ number: 1, active: true },
				{ number: 2, active: false },
			];
		default:
			return [
				{ number: 1, active: true },
				{ number: 2, active: false },
				{ number: 3, active: false },
			];
	}
};

const generateLastPage = (totalPages) => {
	switch (totalPages) {
		case 1:
			return [{ number: totalPages, active: true }];
		case 2:
			return [
				{ number: totalPages - 1, active: false },
				{ number: totalPages, active: true },
			];
		default:
			return [
				{ number: totalPages - 2, active: false },
				{ number: totalPages - 1, active: false },
				{ number: totalPages, active: true },
			];
	}
};

function Pagination(props) {
	const { pagination, onPageChange } = props;

	const { page, totalRows, limit } = pagination;
	const totalPages = Math.ceil(totalRows / limit);

	const [isChange, setIsChange] = useState(false);

	const [showPageList, setShowPageList] = useState([]);

	useEffect(() => {
		if (page === 1) {
			setIsChange(() => {
				return true;
			});
		} else {
			setIsChange(() => {
				return false;
			});
		}
	}, [page]);

	useEffect(() => {
		if (totalPages > 0 && isChange === true) {
			setShowPageList(generateFirstPage(totalPages));
		}
	}, [isChange, totalPages]);

	const handlePageChange = (newPage, over) => {
		if (!over) {
			const currentClick = showPageList.find((item) => item.number === newPage);
			if (currentClick.active === true) return;

			if (newPage !== 1 && newPage !== totalPages) {
				const newList = [
					{ number: newPage - 1, active: false },
					{ number: newPage, active: true },
					{ number: newPage + 1, active: false },
				];
				setShowPageList(newList);
			} else {
				if (newPage === 1) {
					const newList = generateFirstPage(totalPages);
					setShowPageList(newList);
				} else {
					const newList = generateLastPage(totalPages);
					setShowPageList(newList);
				}
			}
		} else {
			if (newPage === 1) {
				const check = showPageList.find((item) => item.number === 1);
				if (check !== undefined && check.active === true) return;

				const newList = generateFirstPage(totalPages);
				setShowPageList(newList);
			} else {
				const check = showPageList.find((item) => item.number === newPage);
				if (check !== undefined && check.active === true) return;

				const newList = generateLastPage(totalPages);
				setShowPageList(newList);
			}
		}

		if (onPageChange) {
			onPageChange(newPage);
		}
	};
	return (
		totalPages > 0 && (
			<Grid container justify='flex-end'>
				<Box
					className={clsx('box', { over: page <= 1 })}
					onClick={() => handlePageChange(1, true)}
				>
					&#10072;&lt;
				</Box>
				{showPageList.map((item, index) => (
					<Box
						key={index}
						className={clsx('box', { 'box-active': item.active })}
						onClick={() => handlePageChange(item.number, false)}
					>
						{item.number}
					</Box>
				))}
				<Box
					className={clsx('box', { over: page >= totalPages })}
					onClick={() => handlePageChange(totalPages, true)}
				>
					&gt;&#10072;
				</Box>
			</Grid>
		)
	);
}

export default Pagination;
