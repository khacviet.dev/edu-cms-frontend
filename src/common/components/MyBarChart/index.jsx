import React from 'react';
import { Bar } from 'react-chartjs-2';

const options = {
	scales: {
		yAxes: [
			{
				ticks: {
					beginAtZero: true,
				},
			},
		],
	},
};

function MyBarChart({ data }) {
	return <Bar data={data} options={options} />;
}

export default React.memo(MyBarChart);
