import React from 'react';
import { Line } from 'react-chartjs-2';

MyLineChart.propTypes = {};

const options = {
	plugins: {
		legend: {
			display: true,
		},
	},
};

function MyLineChart({ data }) {
	return <Line data={data} options={options} />;
}

export default React.memo(MyLineChart);
