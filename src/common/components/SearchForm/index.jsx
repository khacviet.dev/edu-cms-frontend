import { Grid, TextField } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';
SearchForm.propTypes = {
	onSubmit: PropTypes.func.isRequired,
};

function SearchForm(props) {
	const { onSubmit, label } = props;
	const [search, setSearch] = useState('');
	const typingRef = useRef(null);

	const handleSearchChange = (e) => {
		const value = e.target.value;
		setSearch(value);

		if (!onSubmit) return;

		if (typingRef.current) {
			clearTimeout(typingRef.current);
		}

		typingRef.current = setTimeout(() => {
			const formValues = {
				value,
			};

			onSubmit(formValues);
		}, 400);
	};

	return (
		<Grid container alignItems='center'>
			<SearchIcon fontSize='large' style={{ color: '#666' }} />
			<TextField
				style={{ minWidth: '40vw', marginLeft: '1rem' }}
				variant='outlined'
				size='small'
				label={label}
				value={search}
				onChange={handleSearchChange}
			/>
		</Grid>
	);
}

export default SearchForm;
