import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	Divider,
	Grid,
	IconButton,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import CallIcon from '@material-ui/icons/Call';
import CallEndIcon from '@material-ui/icons/CallEnd';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import HistoryIcon from '@material-ui/icons/History';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

DeleteDialog.propTypes = {
	title: PropTypes.string.isRequired,
	doneText: PropTypes.string.isRequired,
	cancelText: PropTypes.string.isRequired,
	action: PropTypes.func.isRequired,
};

const LightTooltip = withStyles((theme) => ({
	tooltip: {
		backgroundColor: theme.palette.common.black,
		color: 'white',
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

function DeleteDialog(props) {
	const {
		title,
		description,
		doneText,
		cancelText,
		action,
		icon,
		tooltip_text,
	} = props;
	const [openDelete, setOpenDelete] = useState(false);

	const handleOpenDelete = () => {
		setOpenDelete(true);
	};

	const handleCloseDelete = (e) => {
		e.stopPropagation();
		setOpenDelete(false);
	};

	const handleAction = () => {
		if (action) {
			action();
			setOpenDelete(false);
		}
	};

	return (
		<div>
			<Grid item md={12} container alignItems='center'>
				{icon == '1' && (
					<LightTooltip placement='top' title={tooltip_text}>
						<IconButton
							style={{ cursor: 'pointer' }}
							onClick={handleOpenDelete}
						>
							<CloseIcon />
						</IconButton>
					</LightTooltip>
				)}

				{icon == '2' && (
					<LightTooltip placement='top' title={tooltip_text}>
						<HistoryIcon
							color='secondary'
							style={{ cursor: 'pointer' }}
							onClick={handleOpenDelete}
						/>
					</LightTooltip>
				)}

				{icon == '3' && (
					<LightTooltip placement='top' title={tooltip_text}>
						<CallIcon
							color='secondary'
							style={{ cursor: 'pointer' }}
							onClick={handleOpenDelete}
						/>
					</LightTooltip>
				)}

				{icon == '4' && (
					<LightTooltip placement='top' title={tooltip_text}>
						<CallEndIcon
							color='secondary'
							style={{ cursor: 'pointer' }}
							onClick={handleOpenDelete}
						/>
					</LightTooltip>
				)}

				{icon == '5' && (
					<LightTooltip placement='top' title={tooltip_text}>
						<VisibilityIcon
							color='secondary'
							style={{ cursor: 'pointer' }}
							onClick={handleOpenDelete}
						/>
					</LightTooltip>
				)}

				{icon == '6' && (
					<LightTooltip placement='top' title={tooltip_text}>
						<VisibilityOffIcon
							color='secondary'
							style={{ cursor: 'pointer' }}
							onClick={handleOpenDelete}
						/>
					</LightTooltip>
				)}

				{icon == '0' && (
					<LightTooltip placement='top' title={tooltip_text}>
						<DeleteIcon
							icon='0'
							color='secondary'
							style={{ cursor: 'pointer' }}
							onClick={handleOpenDelete}
						/>
					</LightTooltip>
				)}
			</Grid>
			<Dialog open={openDelete} onClose={handleCloseDelete}>
				<Grid container>
					<Grid item xs={9} md={9} lg={9}>
						<DialogTitle>{title}</DialogTitle>
					</Grid>
					<Grid item container justify='flex-end' xs={3} md={3} lg={3}>
						<IconButton onClick={handleCloseDelete}>
							<CloseIcon />
						</IconButton>
					</Grid>
				</Grid>
				<Divider />
				<DialogContent>
					<DialogContentText>{description}</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button variant='contained' onClick={handleCloseDelete}>
						{cancelText}
					</Button>
					<Button variant='contained' color='secondary' onClick={handleAction}>
						{doneText}
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	);
}

export default DeleteDialog;
