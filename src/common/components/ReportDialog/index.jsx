import { Button, Grid, Tooltip } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import ReportIcon from '@material-ui/icons/Report';
import feedbackApi from 'api/feedbackApi';
import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';

export default function ReportDialog({ classId, className }) {
	const notify = useContext(ToastifyContext);
	const [open, setOpen] = useState(false);
	const [feedback, setFeedback] = useState('');
	let history = useHistory();

	const handleFeedbackChange = (e) => {
		setFeedback(e.target.value);
	};

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleFormSubmit = (e) => {
		e.preventDefault();

		if (feedback === '') {
			return;
		}

		const post = async () => {
			try {
				const data = {
					classId,
					feedback,
				};
				const response = await feedbackApi.sendFeedback(data);
				setOpen(false);
				setFeedback('');
				notify('success', response.message);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/');
				}
			}
		};

		post();
		setOpen(false);
	};

	return (
		<div>
			<Tooltip title='Đánh giá'>
				<ReportIcon
					onClick={handleClickOpen}
					color='secondary'
					style={{ cursor: 'pointer' }}
				/>
			</Tooltip>
			<Dialog open={open} onClose={handleClose}>
				<DialogTitle>Gửi phản hồi ({className})</DialogTitle>
				<form onSubmit={handleFormSubmit}>
					<DialogContent>
						<DialogContentText>
							Những phản hồi của bạn giúp chúng tôi phát triển hơn, chúng tôi
							rất mong nhận được nhiều ý kiến hơn từ bạn.
						</DialogContentText>
						<TextField
							required
							value={feedback}
							onChange={handleFeedbackChange}
							variant='outlined'
							label='Nhập gì đó...'
							multiline
							rows={6}
							fullWidth
						/>
					</DialogContent>
					<DialogActions>
						<Grid
							container
							style={{ marginRight: '1rem', marginTop: '1rem' }}
							justify='flex-end'
						>
							<Button
								variant='contained'
								onClick={handleClose}
								style={{ marginRight: '0.5rem' }}
							>
								Hủy
							</Button>
							<Button variant='contained' type='submit' color='primary'>
								Gửi
							</Button>
						</Grid>
					</DialogActions>
				</form>
			</Dialog>
		</div>
	);
}
