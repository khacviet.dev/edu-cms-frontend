// get all cookies
import xlsx from 'xlsx';
import FileSaver from 'file-saver';

export const getCookie = (cname) => {
	var name = cname + '=';
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
};

// check login
export const isLogin = () => {
	if (getCookie('c_user') !== '' && getCookie('c_role') !== '') {
		return true;
	}

	return false;
};

// check object empty
export const isEmpty = (obj) => {
	for (let key in obj) {
		if (obj.hasOwnProperty(key)) return false;
	}
	return true;
};

export const checkStartDateGreaterThanNow = (startdate) => {
	return new Date(startdate) >= new Date();
};

export const compareStartDateWithEndDate = (startdate, enddate) => {
	return new Date(enddate) > new Date(startdate);
};

export const getDayByDate = (date) => {
	if (date === '') return '';

	switch (new Date(date).getDay()) {
		case 0:
			return 'CN';
		case 1:
			return 'thứ 2';
		case 2:
			return 'thứ 3';
		case 3:
			return 'thứ 4';
		case 4:
			return 'thứ 5';
		case 5:
			return 'thứ 6';
		case 6:
			return 'thứ 7';
	}
};

export const getDayByDate1 = (date) => {
	if (date === '') return '';

	switch (new Date(date).getDay()) {
		case 0:
			return 'CN';
		case 1:
			return 'thứ 2';
		case 2:
			return 'thứ 3';
		case 3:
			return 'thứ 4';
		case 4:
			return 'thứ 5';
		case 5:
			return 'thứ 6';
		case 6:
			return 'thứ 7';
	}
};

export const displayDayOfWeek = (day) => {
	switch (day) {
		case 0:
			return 'chủ nhật';
		case 1:
			return 'thứ 2';
		case 2:
			return 'thứ 3';
		case 3:
			return 'thứ 4';
		case 4:
			return 'thứ 5';
		case 5:
			return 'thứ 6';
		case 6:
			return 'thứ 7';
	}
};

export const days = [
	{
		value: 0,
		display: 'CN',
	},
	{
		value: 1,
		display: 'Thứ 2',
	},
	{
		value: 2,
		display: 'Thứ 3',
	},
	{
		value: 3,
		display: 'Thứ 4',
	},
	{
		value: 4,
		display: 'Thứ 5',
	},
	{
		value: 5,
		display: 'Thứ 6',
	},
	{
		value: 6,
		display: 'Thứ 7',
	},
];

export const grades = [
	{
		value: '1',
		display: 'Lớp 1',
	},
	{
		value: '2',
		display: 'Lớp 2',
	},
	{
		value: '3',
		display: 'Lớp 3',
	},
	{
		value: '4',
		display: 'Lớp 4',
	},
	{
		value: '5',
		display: 'Lớp 5',
	},
	{
		value: '6',
		display: 'Lớp 6',
	},
	{
		value: '7',
		display: 'Lớp 7',
	},
	{
		value: '8',
		display: 'Lớp 8',
	},
	{
		value: '9',
		display: 'Lớp 9',
	},
	{
		value: '10',
		display: 'Lớp 10',
	},
	{
		value: '11',
		display: 'Lớp 11',
	},
	{
		value: '12',
		display: 'Lớp 12',
	},
];

export const checkShow = (functions, functionCode) => {
	return functions.findIndex((f) => f.function_id === functionCode) !== -1
		? true
		: false;
};

export const getWeeks = () => {
	const d = new Date(`${new Date().getFullYear()}-01-01`);
	const month = 0;
	let mondays = '';

	d.setDate(1);

	while (d.getDay() !== 1) {
		d.setDate(d.getDate() + 1);
	}

	if (d.getMonth() === month) {
		mondays = new Date(d.getTime());
	}

	let temp = new Date(mondays);
	let result = [];
	let count = 0;
	while (temp < new Date(`${new Date().getFullYear()}-12-31`)) {
		let text =
			`0${temp.getDate()}`.slice(-2) +
			'/' +
			`0${temp.getMonth() + 1}`.slice(-2) +
			' ' +
			'đến' +
			' ';
		const sunday = new Date(temp.setDate(temp.getDate() + 6));

		text +=
			`0${sunday.getDate()}`.slice(-2) +
			'/' +
			`0${sunday.getMonth() + 1}`.slice(-2);

		result.push({
			value: count,
			display: text,
		});

		count++;
		temp.setDate(temp.getDate() + 1);
	}

	return result;
};

export const checkDayBelongWeek = () => {
	const d = new Date(`${new Date().getFullYear()}-01-01`);
	const month = 0;
	let mondays = '';

	d.setDate(1);

	while (d.getDay() !== 1) {
		d.setDate(d.getDate() + 1);
	}

	if (d.getMonth() === month) {
		mondays = new Date(d.getTime());
	}

	let temp = new Date(mondays);
	let count = 0;
	while (temp < new Date(`${new Date().getFullYear()}-12-31`)) {
		const monday = new Date(temp.getTime());
		const sunday = new Date(temp.setDate(temp.getDate() + 6));

		if (
			monday.getTime() <= new Date().getTime() &&
			new Date().getTime() <= sunday.getTime()
		) {
			return count;
		}
		count++;
		temp.setDate(temp.getDate() + 1);
	}

	return count;
};

export const getDisplayThisWeek = (value) => {
	const d = new Date(`${new Date().getFullYear()}-01-01`);
	const month = 0;
	let mondays = '';

	d.setDate(1);

	while (d.getDay() !== 1) {
		d.setDate(d.getDate() + 1);
	}

	if (d.getMonth() === month) {
		mondays = new Date(d.getTime());
	}

	let temp = new Date(mondays);
	let result = [];
	let count = 0;
	while (temp < new Date(`${new Date().getFullYear()}-12-31`)) {
		if (count === value) {
			for (let i = 0; i < 7; i++) {
				result.push(
					`0${temp.getDate()}`.slice(-2) +
						'/' +
						`0${temp.getMonth() + 1}`.slice(-2)
				);

				temp.setDate(temp.getDate() + 1);
			}
		} else {
			temp.setDate(temp.getDate() + 7);
		}

		count++;
	}

	return result;
};

export const getFirstMonday = () => {
	const d = new Date(`${new Date().getFullYear()}-01-01`);
	const month = 0;
	let mondays = '';

	d.setDate(1);

	while (d.getDay() !== 1) {
		d.setDate(d.getDate() + 1);
	}

	if (d.getMonth() === month) {
		mondays = new Date(d.getTime());
	}

	return mondays;
};

export const getDayInWeek = () => {
	const compare = new Date().getTime() - new Date(getFirstMonday()).getTime();

	return Math.floor(compare / 1000 / 86400 / 7);
};

export const getDayInWeek1 = (thisdate) => {
	const compare =
		new Date(thisdate).getTime() - new Date(getFirstMonday()).getTime();

	return Math.floor(compare / 1000 / 86400 / 7);
};

export const getDays = (date, day) => {
	var d = new Date(date),
		month = d.getMonth(),
		days = [];

	d.setDate(d.getDate());

	while (d.getDay() !== day) {
		d.setDate(d.getDate() + 1);
	}

	while (d.getMonth() === month) {
		days.push(new Date(d.getTime()));
		d.setDate(d.getDate() + 7);
	}

	return days;
};

export const getDays1 = (startdate, day, enddate) => {
	let d1 = new Date(startdate);
	let d2 = new Date(enddate);
	let days = [];

	while (d1.getDay() !== day) {
		d1.setDate(d1.getDate() + 1);
	}

	while (d1 <= d2) {
		days.push(new Date(d1.getTime()));
		d1.setDate(d1.getDate() + 7);
	}
	return days;
};

export const exportToCSV = (csvData, fileName) => {
	const fileType =
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
	const fileExtension = '.xlsx';

	for (let property in csvData) {
		csvData[property] = xlsx.utils.json_to_sheet(csvData[property]);
	}

	const wb = { Sheets: csvData, SheetNames: Object.keys(csvData) };
	const excelBuffer = xlsx.write(wb, { bookType: 'xlsx', type: 'array' });
	const data = new Blob([excelBuffer], { type: fileType });
	FileSaver.saveAs(data, fileName + fileExtension);
};
