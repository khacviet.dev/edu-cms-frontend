import { FormControl, FormHelperText, TextField } from '@material-ui/core';
import { ErrorMessage } from 'formik';
import PropTypes from 'prop-types';
import React from 'react';

InputField.propTypes = {
	label: PropTypes.string,
	autoFocus: PropTypes.bool,
	fullWidth: PropTypes.bool,
	required: PropTypes.bool,
	type: PropTypes.string,
};

InputField.defaultProps = {
	label: '',
	autoFocus: false,
	fullWidth: false,
	required: false,
	type: 'text',
};

export default function InputField(props) {
	const {
		field,
		form,
		type,
		label,
		required,
		fullWidth,
		autoFocus,
		multiline,
		size,
		variant,
		disabled,
		rows,
	} = props;

	const { name } = field;
	const { errors, touched } = form;
	const showError = errors[name] && touched[name];

	return (
		<FormControl fullWidth={fullWidth} error={showError}>
			<TextField
				disabled={disabled}
				id={name}
				{...field}
				label={label}
				type={type}
				variant={variant ? variant : 'outlined'}
				margin='normal'
				required={required}
				autoFocus={autoFocus}
				error={showError}
				multiline={multiline}
				rows={rows}
				size={size}
				inputProps={{ maxLength: 255 }}
			/>
			<ErrorMessage name={name} component={FormHelperText} />
		</FormControl>
	);
}
