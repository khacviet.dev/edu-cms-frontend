import { CssBaseline, ThemeProvider } from '@material-ui/core';
import ChangeNewPassword from 'common/pages/ChangeNewPassword';
import ChangePassword from 'common/pages/ChangePassword';
import DashBoard from 'common/pages/DashBoard';
import NotFound from 'common/pages/NotFound';
import RecoverCode from 'common/pages/RecoverCode';
import ResetPassword from 'common/pages/ResetPassword';
import SignIn from 'common/pages/SignIn';
import UserProfile from 'common/pages/UserProfile';
import ScheduleCenter from 'features/Admin/pages/ScheduleCenter';
import AttendanceDetail from 'features/Attendance/components/AttendanceDetail';
import AttendanceHistory from 'features/Attendance/pages/AttendanceHistory';
import AttendanceList from 'features/Attendance/pages/AttendanceList';
import ClassDetail from 'features/Classroom/pages/ClassDetail';
import ClassList from 'features/Classroom/pages/ClassList';
import CreateClass from 'features/Classroom/pages/CreateClass';
import CourseDetail from 'features/Course/pages/CourseDetail';
import CourseList from 'features/Course/pages/CourseList';
import feedBackList from 'features/Feedback/pages/feedBackList';
import LessonDetail from 'features/Lesson/pages/LesonDetail';
import LessonList from 'features/Lesson/pages/LessonList';
import CreateMaketing from 'features/Maketing/pages/CreateMaketing';
import MaketingDetail from 'features/Maketing/pages/MaketingDetail';
import MaketingList from 'features/Maketing/pages/MaketingList';
import CreateManager from 'features/Manager/pages/CreateManager';
import ManagerDetail from 'features/Manager/pages/ManagerDetail';
import ManagerList from 'features/Manager/pages/ManagerList';
import PostDetail from 'features/Post/pages/PostDetail';
import PostList from 'features/Post/pages/PostList';
import RegisterList from 'features/Register/pages/RegisterList';
import RevenueList from 'features/Revenue/pages/RevenueList';
import SalaryDetail from 'features/Salary/pages/SalaryDetail';
import SalaryList from 'features/Salary/pages/SalaryList';
import SettingPage from 'features/Setting/pages/SettingPage';
import CreateStudent from 'features/Student/pages/CreateStudent';
import StudentClassAttendance from 'features/Student/pages/StudentClassAttendance';
import StudentClassDetail from 'features/Student/pages/StudentClassDetail';
import StudentDetail from 'features/Student/pages/StudentDetail';
import StudentList from 'features/Student/pages/StudentList';
import StudentWeeklyTimetable from 'features/Student/pages/StudentWeeklyTimetable';
import CreateTeacher from 'features/Teacher/pages/CreateTeacher';
import TeacherDetail from 'features/Teacher/pages/TeacherDetail';
import TeacherList from 'features/Teacher/pages/TeacherList';
import CreateTutor from 'features/Tutor/pages/CreateTutor';
import TutorDetail from 'features/Tutor/pages/TutorDetail';
import TutorList from 'features/Tutor/pages/TutorList';
import { Suspense } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { theme } from 'themes';
import PrivateRoute from 'utils/PrivateRoute';
import PublicRoute from 'utils/PublicRoute';
import ToastifyProvider from 'utils/ToastifyConfig';

export default function App() {
	return (
		<ThemeProvider theme={theme}>
			<CssBaseline />
			<Suspense>
				<BrowserRouter>
					<ToastifyProvider>
						<Switch>
							<PrivateRoute component={DashBoard} path='/' exact />
							<PrivateRoute component={feedBackList} path='/feedback' exact />
							<PrivateRoute component={UserProfile} path='/profile' exact />
							<PrivateRoute component={StudentList} path='/student' exact />
							<PrivateRoute component={RevenueList} path='/revenue' exact />
							<PrivateRoute
								component={CreateStudent}
								path='/create/student'
								exact
							/>
							<PrivateRoute
								component={StudentDetail}
								path='/student/:studentId'
								exact
							/>

							<PrivateRoute component={ManagerList} path='/manager' exact />
							<PrivateRoute
								component={CreateManager}
								path='/create/manager'
								exact
							/>
							<PrivateRoute
								component={ManagerDetail}
								path='/manager/:managerId'
								exact
							/>
							<PrivateRoute component={TeacherList} path='/teacher' exact />
							<PrivateRoute
								component={CreateTeacher}
								path='/create/teacher'
								exact
							/>
							<PrivateRoute
								component={TeacherDetail}
								path='/teacher/:teacherId'
								exact
							/>
							<PrivateRoute component={TutorList} path='/tutor' exact />
							<PrivateRoute
								component={CreateTutor}
								path='/create/tutor'
								exact
							/>
							<PrivateRoute
								component={TutorDetail}
								path='/tutor/:tutorId'
								exact
							/>
							<PrivateRoute component={MaketingList} path='/maketing' exact />
							<PrivateRoute
								component={CreateMaketing}
								path='/create/maketing'
								exact
							/>
							<PrivateRoute
								component={MaketingDetail}
								path='/maketing/:maketingId'
								exact
							/>
							<PrivateRoute component={RegisterList} path='/register' exact />
							<PrivateRoute component={ClassList} path='/class' exact />
							<PrivateRoute
								component={ClassDetail}
								path='/class/:classId'
								exact
							/>
							<PrivateRoute
								component={CreateClass}
								path='/create/class'
								exact
							/>
							<PrivateRoute
								component={AttendanceList}
								path='/attendance'
								exact
							/>
							<PrivateRoute
								component={AttendanceDetail}
								path='/attendancedetail'
								exact
							/>
							<PrivateRoute
								component={AttendanceHistory}
								path='/class/:classId/attendancehistory'
								exact
							/>
							<PrivateRoute component={SalaryList} path='/salary' exact />
							<PrivateRoute
								component={SalaryDetail}
								path='/salarydetail'
								exact
							/>
							<PrivateRoute component={CourseList} path='/course' exact />
							<PrivateRoute
								component={CourseDetail}
								path='/coursedetail'
								exact
							/>
							<PrivateRoute
								component={LessonList}
								path='/course/lessonlist'
								exact
							/>
							<PrivateRoute
								component={LessonDetail}
								path='/createlesson'
								exact
							/>
							<PrivateRoute
								component={LessonDetail}
								path='/lessonDetail'
								exact
							/>
							<PrivateRoute
								component={StudentClassDetail}
								path='/myclass/:classId'
								exact
							/>

							<PrivateRoute
								component={StudentClassAttendance}
								path='/attendstudent'
								exact
							/>
							<PrivateRoute
								component={StudentWeeklyTimetable}
								path='/studentweekly'
								exact
							/>
							<PrivateRoute
								component={ScheduleCenter}
								path='/schedulecenter'
								exact
							/>
							<PrivateRoute component={PostList} path='/post' exact />
							<PrivateRoute component={PostDetail} path='/postdetail' exact />
							<PrivateRoute component={SettingPage} path='/setting' exact />
							<PrivateRoute
								component={ChangeNewPassword}
								path='/changepassword'
								exact
							/>
							<PublicRoute
								restricted={true}
								component={SignIn}
								path='/signin'
								exact
							/>
							<PublicRoute
								restricted={true}
								component={ResetPassword}
								path='/reset'
								exact
							/>
							<PublicRoute
								restricted={true}
								component={RecoverCode}
								path='/reset/recover/:userId'
								exact
							/>
							<PublicRoute
								restricted={true}
								component={ChangePassword}
								path='/reset/newpassword/:userId'
								exact
							/>
							<Route component={NotFound} />
						</Switch>
					</ToastifyProvider>
				</BrowserRouter>
			</Suspense>
		</ThemeProvider>
	);
}
