import { configureStore } from '@reduxjs/toolkit';
import avatarReducer from 'features/User/avatarSlice';

const rootReducer = {
	avatars: avatarReducer,
};

const store = configureStore({
	reducer: rootReducer,
});

export default store;
