import axiosClient from './axiosClient';

const tutorApi = {
	getTutors: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/tutor',
			params: params,
		});
	},

	getTutorById: (tutorId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/tutor/${tutorId}`,
		});
	},

	createTutor: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/tutor/create',
			data: params,
		});
	},

	updateTutor: (tutorId, params) => {
		return axiosClient({
			method: 'PATCH',
			url: `/api/v1/tutor/${tutorId}`,
			data: params,
		});
	},

	deleteTutor: (tutorId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/tutor/delete/${tutorId}`,
		});
	},

	restoreTutor: (tutorId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/tutor/restore/${tutorId}`,
		});
	},
};

export default tutorApi;
