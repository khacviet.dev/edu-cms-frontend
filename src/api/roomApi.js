import axiosClient from './axiosClient';

const roomApi = {
	getRooms: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/room`,
		});
	},
};

export default roomApi;
