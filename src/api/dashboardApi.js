import axiosClient from './axiosClient';

const dashboardApi = {
	getAdminDashboard: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/dashboard/admin',
		});
	},

	getManagerDashboard: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/dashboard/manager',
		});
	},

	getTeacherDashboard: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/dashboard/teacher',
		});
	},

	getManagerPostDashboard: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/dashboard/managerPost',
		});
	},
	getMarketerDashboard: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/dashboard/marketer',
		});
	},
	getRevenus: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/dashboard/getRevenus',
			params: params.filters,
		});
	},
	getCountRevenus: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/dashboard/getCountRevenus',
			params: params.filters,
		});
	},
	getProceedsAndSale: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/dashboard/getProceedsAndSale',
			params: params.filters,
		});
	},
	getRevenusOnline: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/dashboard/getRevenusOnline',
			params: params.filters,
		});
	},
	getCountRevenusOnline: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/dashboard/getCountRevenusOnline',
			params: params.filters,
		});
	},
	getProceedsAndSaleOn: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/dashboard/getProceedsAndSaleOn',
			params: params.filters,
		});
	},
	getRefundMoney: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/dashboard/getRefundMoney',
			params: params.filters,
		});
	},
	updateRefundMoney: (data) => {
		return axiosClient({
			method: 'PATCH',
			url: `/api/v1/dashboard/updateRefundMoney`,
			data: data,
		});
	},
	getCountRefundMoney: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/dashboard/getCountRefundMoney`,
			params: params.filters,
		});
	},
	getTotalAdditionalChargesAndresidualFee: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/dashboard/getTotalAdditionalChargesAndresidualFee`,
			params: params.filters,
		});
	},
};

export default dashboardApi;
