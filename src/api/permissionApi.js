import axiosClient from './axiosClient';

const permissionApi = {
	getMenu: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/permission/menu',
		});
	},

	getFunctions: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/permission/functions',
		});
	},

	checkPermission: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/permission/check',
			params: params,
		});
	},

	checkMenuChange: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/permission/checkmenu',
			data: params,
		});
	},

	updatePermission: (data) => {
		const url = `/api/v1/permission/updatePermission`;
		return axiosClient.patch(url, data);
	},
};

export default permissionApi;
