import axiosClient from './axiosClient';

const managerApi = {
	getManagers: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/manager',
			params: params,
		});
	},

	getManagerById: (managerId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/manager/${managerId}`,
		});
	},

	createManager: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/manager/create',
			data: params,
		});
	},

	updateManager: (managerId, params) => {
		return axiosClient({
			method: 'PATCH',
			url: `/api/v1/manager/${managerId}`,
			data: params,
		});
	},

	deleteManager: (managerId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/manager/delete/${managerId}`,
		});
	},

	restoreManager: (managerId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/manager/restore/${managerId}`,
		});
	},
};

export default managerApi;
