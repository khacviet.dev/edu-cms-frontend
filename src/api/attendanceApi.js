import axiosClient from './axiosClient';

const attendanceApi = {
	getAttendances: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/attendance/getattendancedaily',
			params: params,
		});
	},
	getCountAttendance: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/attendance/getCountAttendance',
			params: params,
		});
	},
	getStudentsAttendanceDaily: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/attendance/getStudentsAttendanceDaily',
			params: params,
		});
	},
	updateAttendanceDaily: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/attendance/updateAttendanceDaily`,
			data: data,
		});
	},

	getDetailAttendanceHistory: (classId, params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/attendance/history/${classId}`,
			params: params,
		});
	},

	getStudentAttendanceByClassId: (classId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/attendance/attendstudent/${classId}`,
		});
	},
};

export default attendanceApi;
