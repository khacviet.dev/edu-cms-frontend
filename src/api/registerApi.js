import axiosClient from './axiosClient';

const registerApi = {
	getRegisterYear: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/register/getRegisterYear',
		});
	},
	getRegisterList: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/register/getRegisterList',
			params: params.filters,
		});
	},
	updateRegisStatus: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/register/updateRegisStatus`,
			data: data,
		});
	},

	getTotalRegister: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/register/getTotalRegister',
			params: params.filters,
		});
	},
};

export default registerApi;
