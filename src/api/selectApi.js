import axiosClient from './axiosClient';

const selectApi = {
	getTeacherAddClassScreen: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/select/addclass/teacher',
			params: params,
		});
	},

	getTutorAddClassScreen: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/select/addclass/tutor',
			params: params,
		});
	},

	getSubjectAddClassScreen: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/select/addclass/subject',
			params: params,
		});
	},

	getRoomAddClassScreen: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/select/addclass/room',
			params: params,
		});
	},

	getSlotAddClassScreen: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/select/addclass/slot',
			params: params,
		});
	},
};

export default selectApi;
