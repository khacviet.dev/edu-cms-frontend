import axiosClient from './axiosClient';

const feedBackApi = {
	getFeedBackClass: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/feedback/getFeedBackClass',
			params: params.filters,
		});
	},
	getCountFeedBackClass: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/feedback/getCountFeedBackClass',
			params: params.filters,
		});
	},
	getClassesSelect: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/feedback/getClassesSelect',
		});
	},
	updateFeedBackStatus: (data) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/feedback/updateFeedBackStatus',
			data: data,
		});
	},
	getFeedBackCourse: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/feedback/getFeedBackCourse',
			params: params.filters,
		});
	},

	getCourseSelect: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/feedback/getCourseSelect',
		});
	},
	getCountFeedBackCourse: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/feedback/getCountFeedBackCourse',
			params: params.filters,
		});
	},

	sendFeedback: (data) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/feedback/send',
			data: data,
		});
	},
};

export default feedBackApi;
