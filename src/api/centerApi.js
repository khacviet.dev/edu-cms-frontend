import axiosClient from './axiosClient';

const centerApi = {
	getScheduleOfCenter: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/center',
			params: params,
		});
	},
};

export default centerApi;
