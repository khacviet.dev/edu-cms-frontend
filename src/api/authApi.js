import axiosClient from './axiosClient';

const authApi = {
	signin: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/auth/signin',
			data: params,
		});
	},
	resetPassword: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/auth/reset',
			data: params,
		});
	},

	checkRecoverCode: (userId, params) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/auth/reset/recover/${userId}`,
			data: params,
		});
	},
	changePassword: (userId, params) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/auth/reset/newpassword/${userId}`,
			data: params,
		});
	},
	logout: () => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/auth/logout',
		});
	},
};

export default authApi;
