import axiosClient from './axiosClient';

const maketingApi = {
	getMaketings: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/maketing',
			params: params,
		});
	},

	getMaketingById: (maketingId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/maketing/${maketingId}`,
		});
	},

	createMaketing: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/maketing/create',
			data: params,
		});
	},

	updateMaketing: (maketingId, params) => {
		return axiosClient({
			method: 'PATCH',
			url: `/api/v1/maketing/${maketingId}`,
			data: params,
		});
	},

	deleteMaketing: (maketingId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/maketing/delete/${maketingId}`,
		});
	},

	restoreMaketing: (maketingId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/maketing/restore/${maketingId}`,
		});
	},
};

export default maketingApi;
