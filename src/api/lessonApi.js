import axiosClient from './axiosClient';

const lessonApi = {
	// lay khi tao lesson
	getTopics: (courseId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/lesson/getTopics/${courseId}`,
		});
	},
	// lay filter
	getTopics2: (courseId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/lesson/getTopics2/${courseId}`,
		});
	},

	createLesson: (data) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/lesson/createLesson',
			data: data,
		});
	},

	getListLesson: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/lesson/getListLesson',
			params: params.filters,
		});
	},
	getTotalLesson: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/lesson/getTotalLesson',
			params: params.filters,
		});
	},

	updateLessonSourceStatus: (data) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/lesson/updateLessonSourceStatus',
			data: data,
		});
	},

	getSourceById: (sourceId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/lesson/getSourceById/${sourceId}`,
		});
	},
	updateSource: (data) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/lesson/updateSource',
			data: data,
		});
	},
};

export default lessonApi;
