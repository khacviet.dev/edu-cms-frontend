import axiosClient from './axiosClient';

const userApi = {
	getUserById: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/user',
		});
	},

	getFunctions: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/user/functions',
		});
	},

	updateProfile: (params) => {
		return axiosClient({
			method: 'PATCH',
			url: '/api/v1/user',
			data: params,
		});
	},

	getAvatar: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/user/avatar',
		});
	},

	changeAvatar: (params) => {
		return axiosClient({
			method: 'PATCH',
			url: '/api/v1/user/avatar',
			data: params,
		});
	},

	changePassword: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/user/changepassword',
			data: params,
		});
	},

	checkRoleByUser: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/user/check',
		});
	},
};

export default userApi;
