import axiosClient from './axiosClient';

const settingApi = {
	getListFunction: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/auth/getListFunction`,
		});
	},
	getListFunctionByRole: (data) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/auth/getListFunctionByRole`,
			params: data,
		});
	},

	updateSlides: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/setting/updateSlides`,
			data: data,
		});
	},
	getListSlides: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/setting/getListSlides`,
		});
	},
	updateSlidesFlag: (data) => {
		return axiosClient({
			method: 'PATCH',
			url: `/api/v1/setting/updateSlidesFlag`,
			data: data,
		});
	},
	getListSlot: (data) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/setting/getListSlot/${data}`,
		});
	},
	updateSlot: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/setting/updateSlot`,
			data: data,
		});
	},
	updateSlotFlag: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/setting/updateSlotFlag`,
			data: data,
		});
	},
	getListRooms: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/setting/getListRooms`,
		});
	},
	updateRoomFlag: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/setting/updateRoomFlag`,
			data: data,
		});
	},
	updateRooms: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/setting/updateRooms`,
			data: data,
		});
	},
	checkExistSlot: (data) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/setting/checkExistSlot`,
			params: data,
		});
	},
	checkExistRoom: (data) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/setting/checkExistRoom`,
			params: data,
		});
	},
	getRolesSetting: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/setting/getRolesSetting`,
		});
	},
};

export default settingApi;
