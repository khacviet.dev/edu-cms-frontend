import axiosClient from './axiosClient';

const studentApi = {
	getStudents: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/student',
			params: params,
		});
	},

	getStudentsByClassId: (classId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/student/class/${classId}`,
		});
	},

	getStudentById: (studentId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/student/${studentId}`,
		});
	},

	getStudentWeeklyTimetable: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/student/weekly`,
		});
	},

	createStudent: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/student/create',
			data: params,
		});
	},

	updateStudent: (studentId, params) => {
		return axiosClient({
			method: 'PATCH',
			url: `/api/v1/student/${studentId}`,
			data: params,
		});
	},

	deleteStudent: (studentId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/student/delete/${studentId}`,
		});
	},

	restoreStudent: (studentId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/student/restore/${studentId}`,
		});
	},
};

export default studentApi;
