import axiosClient from './axiosClient';

const excelApi = {
	getSalarysExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/salarys',
			params: params,
		});
	},
	getDetailSalaryListExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/getDetailSalaryListExcel',
			params: params,
		});
	},
	getSalaryCourseExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/getSalaryCourseExcel',
			params: params,
		});
	},
	getAttendanceDailyExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/getAttendanceDailyExcel',
			params: params,
		});
	},
	getListLessonExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/getListLessonExcel',
			params: params,
		});
	},
	getFeedBackClassExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/getFeedBackClassExcel',
			params: params,
		});
	},
	getFeedBackCourseExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/getFeedBackCourseExcel',
			params: params,
		});
	},
	getPostListExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/getPostListExcel',
			params: params,
		});
	},
	getRegisterListExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/getRegisterListExcel',
			params: params,
		});
	},

	getManagersExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/managers',
			params: params,
		});
	},
	getTeachersExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/teachers',
			params: params,
		});
	},
	getTutorsExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/tutors',
			params: params,
		});
	},
	getStudentsExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/students',
			params: params,
		});
	},
	getMarketingsExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/marketings',
			params: params,
		});
	},
	getClassesExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/classes',
			params: params,
		});
	},
	getClassesExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/classes',
			params: params,
		});
	},
	getClassesByStudentExcel: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/classesStudent',
		});
	},
	getCourseListExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/getCourseListExcel',
			params: params,
		});
	},
	getRevenueExcelOff: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/getRevenueExcelOff',
			params: params,
		});
	},
	getRevenueExcelOn: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/getRevenueExcelOn',
			params: params,
		});
	},
	getRefundMoneyExcel: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/excel/getRefundMoneyExcel',
			params: params,
		});
	},
};

export default excelApi;
