import axiosClient from './axiosClient';

const teacherApi = {
	getTeachers: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/teacher',
			params: params,
		});
	},

	getTeacherById: (teacherId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/teacher/${teacherId}`,
		});
	},

	createTeacher: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/teacher/create',
			data: params,
		});
	},

	updateTeacher: (teacherId, params) => {
		return axiosClient({
			method: 'PATCH',
			url: `/api/v1/teacher/${teacherId}`,
			data: params,
		});
	},

	deleteTeacher: (teacherId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/teacher/delete/${teacherId}`,
		});
	},

	restoreTeacher: (teacherId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/teacher/restore/${teacherId}`,
		});
	},
};

export default teacherApi;
