import axiosClient from './axiosClient';

const postApi = {
	getPostCategory: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/post/categoryList',
		});
	},

	createPost: (data) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/post/createPost',
			data: data,
		});
	},

	getPostList: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/post/getPostList',
			params: params.filters,
		});
	},

	getTotalRecord: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/post/totalPost',
			params: params.filters,
		});
	},

	getApprovedPost: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/post/approvePosts',
		});
	},

	getPostById: (postId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/post/${postId}`,
		});
	},
	updatePostById: (data) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/post/updatePost',
			data: data,
		});
	},

	reviewPost: (data) => {
		return axiosClient({
			method: 'PATCH',
			url: '/api/v1/post/reviewPost',
			data: data,
		});
	},
	updatePostFlag: (data) => {
		return axiosClient({
			method: 'PATCH',
			url: '/api/v1/post/updatePostFlag',
			data: data,
		});
	},
	updatePostCategory: (data) => {
		return axiosClient({
			method: 'PATCH',
			url: '/api/v1/post/updatePostCategory',
			data: data,
		});
	},

	updateCategoryFlagD: (data) => {
		return axiosClient({
			method: 'PATCH',
			url: '/api/v1/post/updateCategoryFlagD',
			data: data,
		});
	},

	getCountPostByMaketingId: (maketingId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/post/maketing/${maketingId}`,
		});
	},
};

export default postApi;
