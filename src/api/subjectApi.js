import axiosClient from './axiosClient';

const subjectApi = {
	getSubjects: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/subject',
		});
	},

	getSubjectsByTeacherId: (teacherId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/subject/teacher/${teacherId}`,
		});
	},

	updateSubject: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/subject/updateSubject`,
			data: data,
		});
	},

	updateSubjectFlagD: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/subject/updateSubjectFlagD`,
			data: data,
		});
	},
};

export default subjectApi;
