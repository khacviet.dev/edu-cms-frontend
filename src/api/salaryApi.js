import axiosClient from './axiosClient';

const salaryApi = {
	getSalaryList: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/salary/getSalaryList`,
			params: params.filters,
		});
	},
	getTotalEmployeeSalary: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/salary/getTotalEmployeeSalary`,
			params: params.filters,
		});
	},
	getDetailSalaryList: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/salary/getDetailSalaryList`,
			params: params.filters,
		});
	},
	getSalaryCourse: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/salary/getSalaryCourse`,
			params: params.filters,
		});
	},

	updateSalary: (data) => {
		return axiosClient({
			method: 'PUT',
			url: `/api/v1/salary/updateSalary`,
			data: data,
		});
	},

	updateSalaryCourse: (data) => {
		return axiosClient({
			method: 'PUT',
			url: `/api/v1/salary/updateSalaryCourse`,
			data: data,
		});
	},

	getTotalSalary: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/salary/getTotalSalary`,
			params: params.filters,
		});
	},

	getTotalBillByUser: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/salary/getTotalBillByUser`,
			params: params.filters,
		});
	},

	getTotalBillOnlineByUser: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/salary/getTotalBillOnlineByUser`,
			params: params.filters,
		});
	},
	caculateScheduleSalary: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/salary/caculateScheduleSalary`,
			data: data,
		});
	},
};

export default salaryApi;
