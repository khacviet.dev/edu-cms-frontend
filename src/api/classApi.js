import axiosClient from './axiosClient';

const classApi = {
	getClasses: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/class',
			params: params,
		});
	},

	getAllClass: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/class/all',
		});
	},

	createClass: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/class',
			data: params,
		});
	},

	getClassByClassId: (classId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/class/${classId}`,
		});
	},

	getClassByStudentId: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/class/student`,
		});
	},

	getClassByStudentIdManagerScreen: (studentId, params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/class/managerScreen/student/${studentId}`,
			params: params,
		});
	},

	getClassDetailByStudentId: (classId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/class/student/${classId}`,
		});
	},

	getClassByTeacherId: (teacherId, params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/class/teacher/${teacherId}`,
			params: params,
		});
	},

	getClassesByManagerShift: (managerId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/class/manager/${managerId}`,
		});
	},

	updateClass: (params, classId) => {
		return axiosClient({
			method: 'PATCH',
			url: `/api/v1/class/${classId}`,
			data: params,
		});
	},

	deleteClass: (classId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/class/delete/${classId}`,
		});
	},

	restoreClass: (classId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/class/restore/${classId}`,
		});
	},

	addStudentToClass: (classId, studentId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/class/${classId}/student/${studentId}`,
		});
	},

	deleteStudentInClass: (classId, studentId) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/class/${classId}/delete/${studentId}`,
		});
	},

	getClassesSelect: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/class/getClassesSelect`,
		});
	},
};

export default classApi;
