import axiosClient from './axiosClient';

const shiftApi = {
	getManagerShifts: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/shift/manager`,
		});
	},

	getShiftsByManagerId: (managerId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/shift/manager/${managerId}`,
		});
	},

	getSlots: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/shift/slot`,
		});
	},
};

export default shiftApi;
