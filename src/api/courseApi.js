import axiosClient from './axiosClient';

const postApi = {
	getSubjectList: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/course/getSubjectList',
		});
	},

	getCourseList: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/course/getCourseList',
			params: params.filters,
		});
	},

	getTotalCourseListByFilter: (params) => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/course/getTotalCourseListByFilter',
			params: params.filters,
		});
	},

	getTeacherList: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/course/getTeacherList',
		});
	},

	getCourseByStudentId: (studentId) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/course/student/${studentId}`,
		});
	},

	createCourse: (data) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/course/createCourse',
			data: data,
		});
	},

	getCourseById: (data) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/course/${data}`,
		});
	},

	updateCourse: (data) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/course/updateCourse',
			data: data,
		});
	},

	reviewCourse: (data) => {
		return axiosClient({
			method: 'PATCH',
			url: '/api/v1/course/reviewCourse',
			data: data,
		});
	},

	updateCourseFlag: (data) => {
		return axiosClient({
			method: 'PATCH',
			url: '/api/v1/course/updateCourseFlag',
			data: data,
		});
	},

	getCourseFilter: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/course/getCourseFilter',
		});
	},
};

export default postApi;
